#include "HTKGUSBKey.h"
#include <openssl\x509.h>
#include <openssl\evp.h>
#include <openssl\crypto.h>
#include <openssl\asn1.h>
#include <string.h>


int myX509_PUBKEY_set(X509_PUBKEY **x)
{
	int ret = 0;
	X509_PUBKEY *pk=NULL;
	ASN1_OBJECT *o=NULL;//密钥（公钥）的ASN1对象
	X509_ALGOR *a=NULL;
	char arrchPublicKey[256] = {'\0'};
	char* puchPublicKey = arrchPublicKey;


	//生成新的X509_PUBKEY对象
	pk=X509_PUBKEY_new();
	if(pk==NULL)
	{
		return 0;
	}
	//得到密钥（公钥）的ASN1对象
	a=pk->algor;
	o=OBJ_nid2obj(EVP_PKEY_EC);
	a->parameter=ASN1_TYPE_new();
	a->parameter->type=V_ASN1_OBJECT;

	a->algorithm=o;
	
	ret = key_getPublicKey(arrchPublicKey);
	if ( 0 != ret )
	{
		//获取公钥失败
		return 0;
	}
	if(!M_ASN1_BIT_STRING_set(pk->public_key,puchPublicKey,strlen((char*)puchPublicKey)))
	{
		X509_PUBKEY_free(pk);
		return 0;
	}
	pk->public_key->flags&= ~(ASN1_STRING_FLAG_BITS_LEFT|0x07);
	pk->public_key->flags|=ASN1_STRING_FLAG_BITS_LEFT;
	if(*x!=NULL)
		X509_PUBKEY_free(*x);//这句话一定要加，否则会有内存泄露
	if(*x!=NULL)
		*x=pk;
	return 1;
}


X509_NAME *myparse_name(char *subject, long chtype, int multirdn)
{
	size_t buflen = strlen(subject)+1; /* to copy the types and values into. due to escaping, the copy can only become shorter */
	char *buf = (char*)OPENSSL_malloc(buflen);
	size_t max_ne = buflen / 2 + 1; /* maximum number of name elements */
	char **ne_types = (char**)OPENSSL_malloc(max_ne * sizeof (char *));
	char **ne_values = (char**)OPENSSL_malloc(max_ne * sizeof (char *));
	int *mval = (int*)OPENSSL_malloc (max_ne * sizeof (int));

	char *sp = subject, *bp = buf;
	int i=0, ne_num = 0;

	X509_NAME *n = NULL;
	int nid=0;

	if (!buf || !ne_types || !ne_values)
	{
		//		BIO_printf(bio_err, "malloc error\n");
		goto error;
	}	

	if (*subject != '/')
	{
		//		BIO_printf(bio_err, "Subject does not start with '/'.\n");
		goto error;
	}
	sp++; /* skip leading / */

	/* no multivalued RDN by default */
	mval[ne_num] = 0;

	while (*sp)
	{
		/* collect type */
		ne_types[ne_num] = bp;
		while (*sp)
		{
			if (*sp == '\\') /* is there anything to escape in the type...? */
			{
				if (*++sp)
					*bp++ = *sp++;
				else	
				{
					//					BIO_printf(bio_err, "escape character at end of string\n");
					goto error;
				}
			}	
			else if (*sp == '=')
			{
				sp++;
				*bp++ = '\0';
				break;
			}
			else
				*bp++ = *sp++;
		}
		if (!*sp)
		{
			//			BIO_printf(bio_err, "end of string encountered while processing type of subject name element #%d\n", ne_num);
			goto error;
		}
		ne_values[ne_num] = bp;
		while (*sp)
		{
			if (*sp == '\\')
			{
				if (*++sp)
					*bp++ = *sp++;
				else
				{
					//					BIO_printf(bio_err, "escape character at end of string\n");
					goto error;
				}
			}
			else if (*sp == '/')
			{
				sp++;
				/* no multivalued RDN by default */
				mval[ne_num+1] = 0;
				break;
			}
			else if (*sp == '+' && multirdn)
			{
				/* a not escaped + signals a mutlivalued RDN */
				sp++;
				mval[ne_num+1] = -1;
				break;
			}
			else
				*bp++ = *sp++;
		}
		*bp++ = '\0';
		ne_num++;
	}	

	if (!(n = X509_NAME_new()))
		goto error;

	for (i = 0; i < ne_num; i++)
	{
		if ((nid=OBJ_txt2nid(ne_types[i])) == NID_undef)
		{
			//			BIO_printf(bio_err, "Subject Attribute %s has no known NID, skipped\n", ne_types[i]);
			continue;
		}

		if (!*ne_values[i])
		{
			//			BIO_printf(bio_err, "No value provided for Subject Attribute %s, skipped\n", ne_types[i]);
			continue;
		}

		//add by kangl for chinese 
		if(nid==NID_countryName)

		{

			ASN1_STRING_TABLE *stable=NULL;

			int rv=0;

			stable=ASN1_STRING_TABLE_get(nid);

			rv=ASN1_STRING_TABLE_add(nid,2,128, DIRSTRING_TYPE,0);

			if(!rv)

				goto error;



			if (!X509_NAME_add_entry_by_NID(n, nid, chtype, (unsigned char*)ne_values[i], -1,-1,mval[i]))

				goto error;



			rv=ASN1_STRING_TABLE_add(nid,stable->minsize,stable->maxsize, stable->mask,stable->flags);

			if(!rv)

				goto error;

		}

		else

		{

			if (!X509_NAME_add_entry_by_NID(n, nid, chtype, (unsigned char*)ne_values[i], -1,-1,mval[i]))
				goto error;
		}

	}

	OPENSSL_free(ne_values);
	OPENSSL_free(ne_types);
	OPENSSL_free(buf);
	OPENSSL_free(mval);
	return n;

error:
	X509_NAME_free(n);
	if (ne_values)
		OPENSSL_free(ne_values);
	if (ne_types)
		OPENSSL_free(ne_types);
	if (buf)
		OPENSSL_free(buf);
	if(mval)
		OPENSSL_free(mval);
	return NULL;
}


int X509_REQ_sign_sdt(X509_REQ *x)
{
	int ret = 0;
	int inl=0,outl=0,outll=0;
	unsigned char *buf_in=NULL,*buf_out=NULL;
	void *asn = x->req_info;
	const ASN1_ITEM *it = ASN1_ITEM_rptr(X509_REQ_INFO);
	unsigned char* puchSignedValue = NULL;
	long iSignedLen = 0;
	inl=ASN1_item_i2d((ASN1_VALUE *)asn,&buf_in, it);

	ret = key_sign_ex((char*)buf_in, inl, puchSignedValue, &iSignedLen);
	if ( 0 != ret )
	{
		goto end;
	}
	puchSignedValue = (unsigned char*)malloc(iSignedLen * sizeof(unsigned char));
	ret = key_sign_ex((char*)buf_in, inl, puchSignedValue, &iSignedLen);
	if ( 0 != ret )
	{
		goto end1;
	}
	ASN1_BIT_STRING_set(x->signature,puchSignedValue, iSignedLen);
end1:
	if ( NULL != puchSignedValue )
	{
		free(puchSignedValue);
		puchSignedValue = NULL;
	}
end:
	if (buf_in != NULL)
	{ OPENSSL_cleanse((char *)buf_in,(unsigned int)inl); OPENSSL_free(buf_in); }

	return ret;
}






static char* iSignCertreq(char *pchSubject)
{
	int rv=0;

	int keytype=0;

	char* status;
	X509_REQ *ret = NULL;

	X509_NAME *subject = NULL;

	char *extconf = NULL;

	EVP_PKEY *puk=NULL;

	EVP_PKEY *prktmp=NULL;

	EVP_PKEY *prk=NULL;

	/**************************************************/
	ret = X509_REQ_new();

	if (NULL==ret)

	{

		//status = CS_ERROR_MEMORY;

		//ap_log_error(APLOG_MARK, APLOG_ERR, 0, r->server,"X509_REQ_new failed");

		goto end;

	}



	if (!X509_REQ_set_version(ret,2))

	{

		//status = CS_ERROR_INTERNAL;

		//ap_log_error(APLOG_MARK, APLOG_ERR, 0, r->server,"X509_REQ_set_version failed");

		goto end;

	}

	rv=myX509_PUBKEY_set(&ret->req_info->pubkey);

	if (!rv)

	{

		//status = CS_ERROR_INTERNAL;

		//ap_log_error(APLOG_MARK, APLOG_ERR, 0, r->server,"myX509_PUBKEY_set failed");

		goto end;

	}



	if(!pchSubject)

	{

		/*使用设置的subjectDN和公钥*/


		char sname[256];

		memset(sname,'\0',sizeof(sname));
		subject = myparse_name(pchSubject, MBSTRING_UTF8, 1);


		if (NULL==subject)

		{

			/*	status = CS_ERROR_INTERNAL;

			ap_log_error(APLOG_MARK, APLOG_ERR, 0, r->server,"parse_name failed");*/

			goto end;

		}

		rv = X509_REQ_set_subject_name(ret,subject);

		X509_NAME_oneline(subject,sname,sizeof(sname));

		//ap_log_error(APLOG_MARK, APLOG_DEBUG, 0, r->server,"subject[%s]",sname);

		X509_NAME_free(subject);


		if (!rv)

		{

			/*	status = CS_ERROR_INTERNAL;

			ap_log_error(APLOG_MARK, APLOG_ERR, 0, r->server,"X509_REQ_set_subject_name failed");*/

			goto end;

		}

	}



	/*sign*/

	//签名（X509，根私钥，摘要算法）

	if (!X509_REQ_sign_sdt(ret))

	{

		//status = CS_ERROR_CERTREQ_SIGNED;

		//ap_log_error(APLOG_MARK, APLOG_ERR, 0, r->server,"X509_REQ_sign failed");

		goto end;

	}


end:



	if(puk)

		EVP_PKEY_free(puk);

	if(prk)

		EVP_PKEY_free(prk);

	/*if (!csStatusOK(status))
	{

	if(ret)

	X509_REQ_free(ret);

	}
	*/

	/*ap_log_error(APLOG_MARK, APLOG_DEBUG, 0,r->server,"Exit iSignCertreq=%d.", status);*/
	return status;
}