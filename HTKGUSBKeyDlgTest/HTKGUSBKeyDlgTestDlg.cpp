
// HTKGUSBKeyDlgTestDlg.cpp : implementation file
//

#include "stdafx.h"
#include "HTKGUSBKeyDlgTest.h"
#include "HTKGUSBKeyDlgTestDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

static int  threadCount=0;
CWinThread* thd[10];
HANDLE hThd[10];

typedef int (WINAPI *pkey_login)(const char *pin,int *pLeftCount);
pkey_login		Gpkey_login = NULL;

HMODULE G_hDll=NULL; //设备句柄

UINT multiThreadFunc(LPVOID pParam)
{
	int ret = -1, leftTryTimes = 0;
	if(Gpkey_login == NULL)
	{
		AfxMessageBox("Gpkey_login is NULL");
		return ret;
	}

	for(int i = 0;i<10;i++)
	{
		ret = Gpkey_login("123456",&leftTryTimes);
		if(ret == 0)
		{
			//		AfxMessageBox("login success!");
		}
		else
		{
			AfxMessageBox("login failed!");
		}
	}

	return ret;
}

// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CHTKGUSBKeyDlgTestDlg dialog




CHTKGUSBKeyDlgTestDlg::CHTKGUSBKeyDlgTestDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CHTKGUSBKeyDlgTestDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CHTKGUSBKeyDlgTestDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CHTKGUSBKeyDlgTestDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUT_ADDTHREAD, &CHTKGUSBKeyDlgTestDlg::OnBnClickedButAddthread)
	ON_BN_CLICKED(IDC_BUT_KILLTHREAD, &CHTKGUSBKeyDlgTestDlg::OnBnClickedButKillthread)
END_MESSAGE_MAP()


// CHTKGUSBKeyDlgTestDlg message handlers

BOOL CHTKGUSBKeyDlgTestDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here
	if(G_hDll == NULL)
	{
		G_hDll = LoadLibrary("TJJUSBKey.dll");
	}

	if(G_hDll == NULL)
	{
		AfxMessageBox("load TJJUSBKey.dll failed!");
	}

	Gpkey_login = (pkey_login)GetProcAddress(G_hDll,"key_login");

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CHTKGUSBKeyDlgTestDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CHTKGUSBKeyDlgTestDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CHTKGUSBKeyDlgTestDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CHTKGUSBKeyDlgTestDlg::OnBnClickedButAddthread()
{
	// TODO: Add your control notification handler code here
	threadCount++;
	CString str;
	str.Format("线程数为：%d",threadCount);
	this->SetWindowText(str);

	thd[threadCount]=AfxBeginThread(multiThreadFunc,NULL,THREAD_PRIORITY_NORMAL,0,CREATE_SUSPENDED);
	thd[threadCount]->ResumeThread();
	hThd[threadCount]=thd[threadCount]->m_hThread;
}


void CHTKGUSBKeyDlgTestDlg::OnBnClickedButKillthread()
{
	// TODO: Add your control notification handler code here
	//等待所有线程结束
	WaitForMultipleObjects(threadCount,hThd,true,INFINITE);

	for(int i=0;i<threadCount;i++)
	{
		SuspendThread(hThd[i]);
		TerminateThread(hThd[i],0);
	}
	this->SetWindowText("线程数为：0");
	threadCount = 0;
}
