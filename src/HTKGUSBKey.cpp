
#include "311usbkey_funlist.h"
#include "HTKGUSBKey.h"
#include <time.h>
#include "functionType.h"
#include "HTKG_USBKey_Errno.h"
#include "Win32_to_Linux.h"

#ifdef WIN32
#include <tchar.h>
#include "comutil.h"
#pragma comment(lib, "comsuppw.lib")
#pragma comment(lib, "KeyLog.lib")
#pragma comment(lib, "crypt32.lib")

int SysLog (char *lpszFormat, ...);
int SysHexLog (unsigned char* lpBuf,unsigned long dwLen);
#else
#define FALSE 0
#include <SdtLog.h>
#include <string.h>
#include "iniparser.h"
dictionary	*server_ini = NULL;//解析文件句柄
#include <arch/unix/apr_arch_thread_mutex.h>
#include <arch/unix/apr_arch_thread_cond.h>
#include <apr_thread_mutex.h>
#include <apr_pools.h>
#include <x509.h>
#include <iconv.h>
#endif


//摘要预处理参数值
short int hash_ecc_entl=128;

unsigned char hash_ecc_id[]={0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38,0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38};

unsigned char hash_ecc_a[]={0xFF,0xFF,0xFF,0xFE,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0x00,0x00,0x00,0x00,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFC};

unsigned char hash_ecc_b[]={0x28,0xE9,0xFA,0x9E,0x9D,0x9F,0x5E,0x34,0x4D,0x5A,0x9E,0x4B,0xCF,0x65,0x09,0xA7,0xF3,0x97,0x89,0xF5,0x15,0xAB,0x8F,0x92,0xDD,0xBC,0xBD,0x41,0x4D,0x94,0x0E,0x93};

unsigned char hash_ecc_xg[]={0x32,0xC4,0xAE,0x2C,0x1F,0x19,0x81,0x19,0x5F,0x99,0x04,0x46,0x6A,0x39,0xC9,0x94,0x8F,0xE3,0x0B,0xBF,0xF2,0x66,0x0B,0xE1,0x71,0x5A,0x45,0x89,0x33,0x4C,0x74,0xC7};

unsigned char hash_ecc_yg[]={0xBC,0x37,0x36,0xA2,0xF4,0xF6,0x77,0x9C,0x59,0xBD,0xCE,0xE3,0x6B,0x69,0x21,0x53,0xD0,0xA9,0x87,0x7C,0xC6,0x2A,0x47,0x40,0x02,0xDF,0x32,0xE5,0x21,0x39,0xF0,0xA0}; 


//证书属性类型
#define CERT_NAME_EMAIL_TYPE            1
#define CERT_NAME_RDN_TYPE              2
#define CERT_NAME_ATTR_TYPE             3
#define CERT_NAME_SIMPLE_DISPLAY_TYPE   4
#define CERT_NAME_FRIENDLY_DISPLAY_TYPE 5
#define CERT_NAME_DNS_TYPE              6
#define CERT_NAME_URL_TYPE              7
#define CERT_NAME_UPN_TYPE              8
#define CALG_MS2_49						32779
#define CALG_SCH2						32773
#define	LABELEN							48
//公章文件段长
#define CACHETSEGLEN					2000 //公章段长,太长有问题
//设备类型宏
enum DEV_TYPE
{
	TIANYU = 1,	
	HUASHEN,		
	HXT,
	SDTKEY,
	HXTOLD,
	SK311
};

#define TIANYU_TYPE						1
#define	HUASHEN_TYPE	 				2
#define	HXT_TYPE						3
//常量字符串
#define ECC_CONTAINER_WHCA_SIG			"LAB_USERCERT_SIG"
#define ECC_CONTAINER_WHCA_ENC			"LAB_USERCERT_ENC"
#define ECC_SIGN_CER_WHCA_SIG			"LAB_USERCERT_SIG"
#define ECC_CRY_CER_WHCA_ENC			"LAB_USERCERT_ENC"
#define ECC_ROOT_CER_WHCA				"CA_CERT"
#define KEY_PUBKEY_SIG					"LAB_PUBKEY_SIG"
#define KEY_PUBKEY_ENC					"LAB_PUBKEY_ENC"
#define OTHER_SIG_SN					"LAB_USERCERT_SIG_SN"
#define OTHER_ENC_SN					"LAB_USERCERT_ENC_SN"
#define CERT_ROOT1						"root1"
#define CERT_ROOT2						"root2"
#define CERT_E_C						"e_c"

#define ECC_CONTAINER					"e_k"
#define ECC_CONTAINER_PRO				"e_k_0"
#define ECC_CRY_CER						"e_c"
#define ECC_SIGN_CER					"s_c"
#define ECC_ROOT_CER					"root"
#define ECC_KEY_FILE					"ecc_key_file"
#define RSA1024_1						"RSA1024-1"
#define CONFIGFILENAME					"configure"
#define CONFIGFILELEN					256
#define USELIMITE						"测试"
//设备库类型宏
#ifdef WIN32
#define	GBWTHARDWAREDLL					"GBWTHardware.dll"
#define GBHDHARDWAREDLL					"GBHDHardware.dll" 
#define WHSDKDLL						"whSdk.dll"
#define SDTKEYDLL						"MFCSdtkey.dll"
#define DEVSNDLL						"HXT_FileAPI.dll"
#define CERTPARSEDLL					"certparse_openssl.dll"
#define SK311DLL						"311UKeyDll.dll"
#else
#define	GBWTHARDWAREDLL					"GBWTHardware.so"
#define GBHDHARDWAREDLL					"GBHDHardware.so" 
#define WHSDKDLL						"whSdk.so"
#define SDTKEYDLL						"MFCSdtkey.so"
#define DEVSNDLL						"HXT_FileAPI.so"
#define CERTPARSEDLL					"certparse_openssl.so"
#define SK311DLL						"311UKeyDll.so"
#endif

#pragma data_seg("Shared")
char	ECC_CONTAINER_CHOICE[LABELEN]={'\0'}; // 签名时判断当前key中存在的容器为哪个
char	ECC_ENCCONTAINER_CHOICE[LABELEN]={'\0'};  //加解密时选择的密钥容器
char	ConfigContent[CONFIGFILELEN]={'\0'};
char	ECC_SIGN_CER_LABEL[LABELEN]={'\0'};  //ECC签名标签
char	ECC_CRY_CER_LABEL[LABELEN]={'\0'};   //ECC加密标签
char	ECC_ROOT_CER_LABEL[LABELEN]={'\0'}; //ECC根证书标签
int		G_CallKeyUpdateInfoFlag = 0;		//key_updateInfo接口是否调用标志(0-未调用,1-已调用)
int		G_CallKeyFindKeyContainerFlag = 0;  //key_findKeyContainer是否被调用标志（0-未调用，1-已调用）
int     G_VerifyUseLimiteFlag = 0;			//验证使用限制标志，（0-超出使用范围，1-在使用范围内）

#ifdef WIN32
HANDLE hMutex = NULL;//互斥句柄
#define MY_ENCODING_TYPE  (PKCS_7_ASN_ENCODING | X509_ASN_ENCODING)
#define MY_STRING_TYPE (CERT_X500_NAME_STR )
#else
apr_thread_mutex_t *hMutex = NULL;
unsigned int flags = APR_THREAD_MUTEX_DEFAULT;
apr_pool_t *g_pool = NULL;
#endif

#pragma data_seg()
#pragma comment(linker,"/section:Shared,rws")

//接口声明
int	key_exportCert(int AlgType,	int Usage,BYTE *buf,int *size);
int key_getSerial(unsigned char *pbData,DWORD dwCount,char *SerialNumber);
int getUidFromSubject(unsigned char *Subject , unsigned int Subjectlen,char *Uid);
extern const char * key_get_err_msg(int nErrCode);

// 311-key function var
pSK_GetDeviceNum		G_pSK_GetDeviceNum = NULL;
pSK_ExportFile			G_pSK_ExportFile = NULL;
pSK_Login				G_pSK_Login = NULL;
pSK_SetPin				G_pSK_SetPin = NULL;
pSK_Logout				G_pSK_Logout = NULL;
pSK_ReadData			G_pSK_ReadData = NULL;
pSK_SetUserPinStat		G_pSK_SetUserPinStat = NULL;
pSK_GetUserPinStat		G_pSK_GetUserPinStat = NULL;
pSK_DestroyShareMem		G_pSK_DestroyShareMem = NULL;

/************************************************************************/
/*
功能简介:获取配置文件路径
接口名称:int   GetConfFilePath(
					char confFilePath[CONFIGFILELEN]  [OUT]
					)
参数说明:
入参说明:无
出参说明:confFilePath:配置文件路径
返回值:
成功返回0
失败返回-1
*/
/************************************************************************/
int GetConfFilePath(char confFilePath[CONFIGFILELEN])
{	
	//查找文件TJJUSBKey.conf
#ifdef WIN32
	LPTSTR lpszCurDir;
	TCHAR tchBuffer[CONFIGFILELEN]={'\0'}; 
	lpszCurDir = tchBuffer;
	GetSystemDirectory(lpszCurDir,CONFIGFILELEN);
	strcat(lpszCurDir,"\\TJJUSBKey.conf");
	strcpy(confFilePath,lpszCurDir);
#else
	strcpy(confFilePath,"/usr/lib/TJJUSBKey.conf");
#endif

	return 0;
}


//检测文件是否存在
bool IsExistFile(char fileName[])
{
	bool flag = false;
	FILE *hfp = NULL;
	hfp = fopen(fileName, "rb");
	if ( hfp == NULL){
		SysLog("can't find file\n");
	}else{
		SysLog("查找文件存在\n");
		flag = true;
		fclose(hfp);
		hfp = NULL;
	}
	return flag;
	
}


//得到地址
void getProcAddress(HMODULE hDll)
{
	pIsPVKEnabled = (isPVKEnabled)GetProcAddress(hDll, "WH_IsPVKEnabeled");
	pLogin = (login)GetProcAddress(hDll, "WHCA_Login");
	pGetSupportedAlgId = (getSupportedAlgId)GetProcAddress(hDll, "WH_GetSupportedAlgId");
	pCreateContainer = (createContainer)GetProcAddress(hDll, "WH_CreateContainer");
	pGenAsymmetricKey = (genAsymmetricKey)GetProcAddress(hDll, "WH_GenAsymmetricKey");
//	pFormat = (format)GetProcAddress(hDll, "WHCA_Format");
	pImportPublicKey = (importPublicKey)GetProcAddress(hDll, "WH_ImportPublicKey");
	pImportPrivateKey = (importPrivateKey)GetProcAddress(hDll, "WH_ImportPrivateKey");
	pEnumCidcDev = (enumCidcDev)GetProcAddress(hDll, "WH_EnumCidcDev");
	pReadLabelList = (readLabelList)GetProcAddress(hDll, "WHCA_ReadLabelList");
	pFindContainerAndKeypair = (findContainerAndKeypair)GetProcAddress(hDll, "WH_FindContainerAndKeypair");
	pDestroyContainer = (destroyContainer)GetProcAddress(hDll, "WH_DestroyContainer");
	pGenSymmetricKey  = (genSymmetricKey )GetProcAddress(hDll, "WH_GenSymmetricKey");
	pImportSymmetricKey  = (importSymmetricKey )GetProcAddress(hDll, "WH_ImportSymmetricKey");
	pExportSymmetricKey  = (exportSymmetricKey )GetProcAddress(hDll, "WH_ExportSymmetricKey");
	pExportPublicKey  = (exportPublicKey )GetProcAddress(hDll, "WH_ExportPublicKey");
	pExportPrivateKey  = (exportPrivateKey )GetProcAddress(hDll, "WH_ExportPrivateKey");
	pSymmetricEncrypt  = (symmetricEncrypt )GetProcAddress(hDll, "WH_SymmetricEncrypt");
	pSymmetricDecrypt  = (symmetricDecrypt )GetProcAddress(hDll, "WH_SymmetryDecrypt");
	pAsymmetricEncrypt  = (asymmetricEncrypt )GetProcAddress(hDll, "WH_AsymmetricEncrypt");
	pAsymmetricDecrypt  = (asymmetricDecrypt )GetProcAddress(hDll, "WH_AsymmetricDecrypt");
	pAsymmetricSignData  = (asymmetricSignData )GetProcAddress(hDll, "WH_AsymmetricSignData");
	pAsymmetricVerifySign  = (asymmetricVerifySign )GetProcAddress(hDll, "WH_AsymmetricVerifySign");
	pDestroyKey  = (destroyKey )GetProcAddress(hDll, "WH_DestroyKey");
	pGetKeyProperty  = (getKeyProperty )GetProcAddress(hDll, "WH_GetKeyProperty");
	pDigest  = (digest )GetProcAddress(hDll, "WH_Digest");
	pDigestEx  = (digestEx )GetProcAddress(hDll, "WH_DigestEx");
	pGetKeyPropertyEx  = (getKeyPropertyEx )GetProcAddress(hDll, "WH_GetKeyPropertyEx");
	pGenRandom  = (genRandom)GetProcAddress(hDll, "WH_GenRandom");
	pEnablePVK  = (enablePVK )GetProcAddress(hDll, "WH_EnablePVK");
	pDisablePVK  = (disablePVK )GetProcAddress(hDll, "WH_DisabelPVK");
	pSetPVKEnableData  = (setPVKEnableData )GetProcAddress(hDll, "WH_SetPVKEnabelData");
	pLogout  = (logout )GetProcAddress(hDll, "WHCA_Logout");
	pSetPin  = (setPin )GetProcAddress(hDll, "WHCA_SetPin");
	pImportFile  = (importFile )GetProcAddress(hDll, "WHCA_ImportFile");
	pExportFile  = (exportFile )GetProcAddress(hDll, "WHCA_ExportFile");
	pDeleteFileEx  = (deleteFileEx )GetProcAddress(hDll, "WHCA_DeleteFile");
	pGetDeviceStatus  = (getDeviceStatus )GetProcAddress(hDll, "WHCA_GetDeviceStatus");
	pUnLock  = (unLock )GetProcAddress(hDll, "WHCA_UnLock");
	pAsymmetricVerifySignEx   = (asymmetricVerifySignEx  )GetProcAddress(hDll, "WH_AsymmetricVerifySignEx");
	pAsymmetricSend   = (asymmetricSend  )GetProcAddress(hDll, "WH_AsymmetricSend");
	pGetKeyPairDer   = (getKeyPairDer  )GetProcAddress(hDll, "WHCA_GetKeyPairDer");
	pAsymmetricReceive   = (asymmetricReceive  )GetProcAddress(hDll, "WH_AsymmetricReceive");
	pImportKEK   = (importKEK  )GetProcAddress(hDll, "WHECC_ImportKEK");
	pExportKEKEncPrk   = (exportKEKEncPrk  )GetProcAddress(hDll, "WHECC_ExportKEKEncPrk");
	pLoadKEKEncPrk   = (loadKEKEncPrk  )GetProcAddress(hDll, "WHECC_LoadKEKEncPrk");
	pClearPrk    = (clearPrk   )GetProcAddress(hDll, "WHECC_ClearPrk");
	pGetDeviceList   = (getDeviceList  )GetProcAddress(hDll, "WH_GetDeviceList");
	pGetDevice   = (getDevice  )GetProcAddress(hDll, "WH_GetDevice");
	pSetDevice   = (setDevice  )GetProcAddress(hDll, "WH_SetDevice");
	pCheckDevice    = (checkDevice   )GetProcAddress(hDll, "WH_CheckDevice");
	pEnumKeyContainerName=(enumKeyContainerName)GetProcAddress(hDll,"WHCA_EnumKeyContainerName");
	pKeyIsExist=(keyIsExist)GetProcAddress(hDll,"WH_DeviceExist");
}

//得到311usbkey地址
void get311KeyProcAddress(HMODULE hDll)
{
	G_pSK_GetDeviceNum = (pSK_GetDeviceNum)GetProcAddress(hDll,"SK_GetDeviceNum");
	G_pSK_Login = (pSK_Login)GetProcAddress(hDll,"SK_Login");
	G_pSK_ExportFile = (pSK_ExportFile)GetProcAddress(hDll,"SK_ExportFile");
	G_pSK_SetPin = (pSK_SetPin)GetProcAddress(hDll,"SK_SetPin");
	G_pSK_Logout = (pSK_Logout)GetProcAddress(hDll,"SK_Logout");
	G_pSK_ReadData = (pSK_ReadData)GetProcAddress(hDll,"SK_ReadData");
	G_pSK_SetUserPinStat = (pSK_SetUserPinStat)GetProcAddress(hDll,"SK_SetUserPinStat");
	G_pSK_GetUserPinStat = (pSK_GetUserPinStat)GetProcAddress(hDll,"SK_GetUserPinStat");
	G_pSK_DestroyShareMem = (pSK_DestroyShareMem)GetProcAddress(hDll,"SK_DestroyShareMem");

	pGetDeviceList = (getDeviceList)GetProcAddress(G_hDll,"SK_GetDeviceList");
	pGetDevice = (getDevice)GetProcAddress(G_hDll,"SK_GetDevice");
	pSetDevice = (setDevice)GetProcAddress(G_hDll,"SK_SetDevice");
	pCheckDevice = (checkDevice)GetProcAddress(G_hDll,"SK_CheckDevice");
}



/* 
	base64编码方法
	输入参数1:原文
	输入参数2:原文长度
	返回值:编码字符串
*/
char *base64_encode(unsigned char *s,int srclen)
{
	unsigned char *p = s, *e;
	char *r, *_ret;
	int len = srclen;
	int mod=0;
	unsigned char unit[4], temp[3];
	char B64[64] ={
	'A','B','C','D','E','F','G','H',
	'I','J','K','L','M','N','O','P',
	'Q','R','S','T','U','V','W','X',
	'Y','Z',
	'a','b','c','d','e','f','g','h',
	'i','j','k','l','m','n','o','p',
	'q','r','s','t','u','v','w','x',
	'y','z',
	'0','1','2','3','4','5','6','7',
	'8','9','+','/'
	};

	
	e = s + len;
	len = len / 3 * 4 + 4 + 1;
	r = _ret = (char *)malloc(len*sizeof(unsigned char));
	
	if(r == NULL){
		return NULL;
	}
	while (p < e) {
		memset(temp,0,3);
		if (e-p >= 3) {
			memcpy(temp,p,3);
			p += 3;
		} else {
			memcpy(temp,p,e-p);
			mod=3-(e-p);
			p = e;
		}
		
		unit[0] = temp[0] >> 2;
		unit[1] = temp[0] << 6;
		unit[1] = (unit[1]>>2) | (temp[1]>>4);
		unit[2] = temp[1] << 4;
		unit[2] = (unit[2]>>2) | (temp[2]>>6);
		unit[3] = temp[2] << 2;
		unit[3] = unit[3] >> 2;
		*r++ = B64[unit[0]];
		*r++ = B64[unit[1]];
	    *r++ = ( (mod==2) ? '=': B64[unit[2]]);
	    *r++ = ( ( mod==2 || mod==1 ) ? '=': B64[unit[3]]);
		//*r++ = (unit[2] ? B64[unit[2]] : '=');
		//*r++ = (unit[3] ? B64[unit[3]] : '=');


	}
	*r = 0;
	
	return _ret;
}

/*
base64解码方法
输入参数1:编码数据
输入参数2:解码后的长度
返回值:解码后的字节数组
*/
unsigned char *base64_decode(char *s,int *decode_len)
{
	char *p = s, *e;
	unsigned char *r, *_ret;
	int len = strlen(s);
	int padlen=0;
	unsigned char i, unit[4];

		char B64[64] ={
	'A','B','C','D','E','F','G','H',
	'I','J','K','L','M','N','O','P',
	'Q','R','S','T','U','V','W','X',
	'Y','Z',
	'a','b','c','d','e','f','g','h',
	'i','j','k','l','m','n','o','p',
	'q','r','s','t','u','v','w','x',
	'y','z',
	'0','1','2','3','4','5','6','7',
	'8','9','+','/'
	};
	
	*decode_len = len/4 * 3;
	e = s + len;
	len = len / 4 * 3 + 1;
	r = _ret = (unsigned char *)malloc(len+3);
	
	if(r == NULL){
		return NULL;
	}

	while (p < e) {
		memcpy(unit,p,4);
		if (unit[3] == '=')
		{
			unit[3] = 0;
			padlen++;
		}
		if (unit[2] == '=')
		{
			unit[2] = 0;
			padlen++;
		}
		p += 4;
		
		for (i=0; i<64 && unit[0]!=B64[i]; i++);
		unit[0] = i==64 ? 0 : i;
		for (i=0; i<64 && unit[1]!=B64[i]; i++);
		unit[1] = i==64 ? 0 : i;
		for (i=0; i<64 && unit[2]!=B64[i]; i++);
		unit[2] = i==64 ? 0 : i;
		for (i=0; i<64 && unit[3]!=B64[i]; i++);
		unit[3] = i==64 ? 0 : i;
		*r++ = (unit[0]<<2) | (unit[1]>>4);
		*r++ = (unit[1]<<4) | (unit[2]>>2);
		*r++ = (unit[2]<<6) | unit[3];
	}
	*r = 0;

	*decode_len=*decode_len-padlen;
	
	return _ret;
}


/*释放申请的空间
*/
int key_freeBuffer(void  **buffer)
{
	if(*buffer != NULL){
		free(*buffer);
		*buffer = NULL;
	}
	return 0;
}

#ifndef WIN32

bool IsOddMonth(int month)
{
	bool  ret=false;
	int  array[]={1,3,5,7,8,10,12};
	for(int i=0;i<sizeof(array);i++){
		if(i==month){
			ret=true;
			break;
		}
	}
	return ret;
}

bool IsEvenMonth(int month)
{	
	bool  ret=false;
	int  array[]={4,6,9,11};
	for(int i=0;i<sizeof(array);i++){
		if(i==month){
			ret=true;
			break;
		}
	}
	return ret;
}

//是否为闰年 能被4但不能被100整除或者能被100并且能被400整除
bool IsLeapYear(int year)
{
	bool ret=false;
	if((year%4 == 0)&&(year%100 != 0)||(year%100 == 0)&&(year%400 == 0))
		ret=true;
	return ret;
}

int UTCTimeToTm(unsigned char *time_src,struct tm *tm_dst)
{
	int year=0,month=0, day=0,hour=0,minuter=0,second=0;
	char arrayYear[6]={'\0'};
	char arrayMonth[4]={'\0'};
	char arrayDay[4]={'\0'};
	char arrayHour[4]={'\0'};
	char arrayMinuter[4]={'\0'};
	char arraySeconde[4]={'\0'};
    strcpy(arrayYear,"20");	

	strncat(arrayYear,(const char*)time_src,2);
	memcpy(arrayMonth,time_src+2,2);
	memcpy(arrayDay,time_src+2+2,2);
	memcpy(arrayHour,time_src+2+2+2,2);
	memcpy(arrayMinuter,time_src+2+2+2+2,2);
	memcpy(arraySeconde,time_src+2+2+2+2+2,2);
	//解析起始时间
	year=atoi(arrayYear);
	month=atoi(arrayMonth);
	day=atoi(arrayDay);
	hour=atoi(arrayHour)+8;
	minuter=atoi(arrayMinuter);
	second=atoi(arraySeconde);
	
	if(hour>=24){
		hour=hour%24;
		day+=1;
	}
	if((IsOddMonth(month) == true) && (day == 32)){
		day=1;
		month+=1;
	}
	if((IsEvenMonth(month) == true) && (day == 31)){
		day=1;
		month+=1;
	}
	if((IsLeapYear(year) == true) && (day == 30)){
		day=1;
		month+=1;
	}
	else if(day==29){
		day=1;
		month+=1;
	}
	if(month>12){
		month=1;
		year+=1;
	}

	tm_dst->tm_hour=hour;
	tm_dst->tm_mday=day;
	tm_dst->tm_min=minuter;
	tm_dst->tm_mon=month;
	tm_dst->tm_sec=second;
	tm_dst->tm_year=year;
	return 0;
}

// UTF8编码转换到GBK编码
int UTF8ToGBK(unsigned char * lpUTF8Str,unsigned char * lpGBKStr,int nGBKStrLen)
{

	int retval = -1;
	size_t inlen = strlen((const char*)lpUTF8Str);
	// FIXME: 长度计算
	size_t outlen = (inlen + 1) * 2;
	// FIXME: 为了兼容已经存在的Window程序
	if (lpGBKStr == NULL) {
		return outlen;
	}else{
		memset(lpGBKStr, '\0', outlen);
	}
	iconv_t cd;
	cd = iconv_open("GBK", "UTF-8");
	if ((iconv_t)-1 == cd) {
		return -1;
	}
	retval = iconv(cd, (char**)&(lpUTF8Str), &inlen, (char**)&(lpGBKStr), &outlen);
	return retval;

}

unsigned char * convertUTF8ToGBK(unsigned char *val, size_t len)
{
	unsigned char *out_by = NULL;
	unsigned char *strGBK = NULL;
	size_t i = 0, ii = 0, i1 = 0, i2 = 0;
	char mydata[3] = {'\0'};
	int mybyte = 0;
	out_by = (unsigned char*)malloc(sizeof(unsigned char) * len);
	if(out_by == NULL){
		strGBK = NULL;
		DebugLog("memery malloc failed!\n");
		goto end;
	}
	memset(out_by, '\0', len);
	//对密服返回数据处理 去除\x字符，将其后的两个字符作为一个字节输出
	for(i = 0; i < len; i++)
	{
		if(val[i] == 0x5c){
			if(val[i+1] == 0x78){
				for(ii=0; ii<i-i2; ii++)
					out_by[i1+ii]=val[i2+ii];

				mydata[0] = val[i+2];
				mydata[1] = val[i+3];
				mybyte = (int)strtol(mydata, NULL, 16);
				if(mybyte > 127){
					mybyte -= 256;
				}
				out_by[i1+i-i2] = mybyte;
				i1=i1+i-i2+1;
				i2=i+4;
			}
		}
	}
	for(ii=0;ii<len-i2;ii++){
		out_by[i1+ii]=val[i2+ii];
	}
	len = UTF8ToGBK(out_by, NULL, NULL);
	if(len == 0){
		ErrorLog("convert code failed!\n");
		strGBK = NULL;
		goto end;
	}
	strGBK = (unsigned char*)malloc(sizeof(unsigned char)*len);
	if(strGBK == NULL){
		//strGBK = NULL;
		ErrorLog("malloc failed\n");
		goto end;
	}
	len = UTF8ToGBK(out_by, strGBK, len);

end:
	if(out_by != NULL){
		free(out_by);
		out_by = NULL;
	}
	return strGBK;
}

/* 证书主题，发布者分隔符替换 */
void replace_certinfo_separator(char *after, const char *src, const char separator)
{
	size_t i=0, j=0, len=0;
	len = strlen(src);
	for(i=1; i<len; i++){
		if(src[i] == '/'){
			after[j++] = separator;
			after[j++] = ' ';
		}else{
			after[j++] = src[i];
		}
	}
}

/*
功能:
获取证书中的主题项
参数：
[in]	pContent:证书内容
[in]	nContentLen:证书内容长度
[out]	subject:主题项数据
返回:
0：成功  非0：相应错误码
*/
int Cert_getSubject(unsigned char* pContent,int nContentLen,char subject[CP_SUBJECT_LEN])
{
	int ret = -1;	
	//解析证书
	X509			*x=NULL;
	unsigned char	*p=NULL;
	char *tmp = NULL;
	unsigned char *strGBK = NULL;
	const char separator = ',';

	if(pContent == NULL){
		return PARAERROR;
	}

	/* 为DER编码的数字证书 
	*/	
	p=pContent;
	x=X509_new();
	d2i_X509(&x,(const unsigned char **)&p,nContentLen);

	//解析证书
	if(x == NULL){
		ErrorLog("cert content text is null\n");
		return CERTCONTEXTISNULL;
	}

	memset(subject,'\0',CP_SUBJECT_LEN);
	tmp=X509_NAME_oneline(X509_get_subject_name(x),NULL,0);

	memcpy(subject, tmp, strlen(tmp));
	strGBK = convertUTF8ToGBK((unsigned char*)subject, strlen(tmp));
	if(strGBK == NULL){
		ErrorLog("convert utf8 to gbk error");
		ret = UTF8_TO_GBK_ERR;
		return ret;
	}
	memset(subject,'\0',CP_SUBJECT_LEN);
	//remove / , use separator ','
	replace_certinfo_separator(subject, (const char*)strGBK, separator);
	ret = 0;
	DebugLog("subject is:%s",subject);

	X509_free(x);
	return ret;
}



/*
功能:
获取证书中的公钥
参数:
[in]	pContent:证书内容
[in]	nContentLen:证书内容长度
[in-out]pKeyValue:公钥的值
[in-out]pnKeyLength:公钥的长度
返回:
0：成功  非0：相应错误码
注释:
此接口为二次调用接口,设置公钥值为NULL,得到公钥值的长度。再为pKeyValue
分配相应大小的内存。
*/
int  Cert_getPublicKey (unsigned char* pContent,int nContentLen,unsigned char * pKeyValue,int *pnKeyLength)
{
	int ret = -1;	
	//解析证书
	X509			*x=NULL;
	unsigned char	*p=NULL;

	if(pContent==NULL){
		return PARAERROR;
	}

	/* 为DER编码的数字证书 
	*/
	p=pContent;
	x=X509_new();
	d2i_X509(&x,(const unsigned char **)&p,nContentLen);

	if(x == NULL){
		ErrorLog("cert content text is null\n");
		ret = CERTCONTEXTISNULL;
		return ret;
	}
	if(pKeyValue==NULL){
		*pnKeyLength=x->cert_info->key->public_key->length;
		if((*pnKeyLength == 65) || (*pnKeyLength == 97)){
			*pnKeyLength -= 1;
		}
		ret = 0;
	}else{
		*pnKeyLength=x->cert_info->key->public_key->length;
		if((*pnKeyLength == 65) || (*pnKeyLength == 97)){
			*pnKeyLength -= 1;
			memset(pKeyValue,'\0',*pnKeyLength);
			memcpy(pKeyValue,x->cert_info->key->public_key->data+1,*pnKeyLength);
		}else{
			memset(pKeyValue,'\0',*pnKeyLength);
			memcpy(pKeyValue,x->cert_info->key->public_key->data,*pnKeyLength);
		}

		DebugLog("public key is:\n");
		DebugHexLog(pKeyValue,*pnKeyLength);
	}

	X509_free(x);
	x = NULL;
	ret = 0;
	return ret;
}



/*
功能:
获取证书中的发布者
参数:
[in]	pContent:证书内容
[in]	nContentLen:证书内容长度
[out]	issuer:发布者的信息
返回:
0：成功  非0：相应错误码
*/
 int  Cert_getIssuer(unsigned char* pContent,int nContentLen,char issuer[CP_ISSUE_LEN])
 {
	 int ret = -1;
	 //解析证书
	 X509			*x=NULL;
	 unsigned char	*p=NULL;
	 char *tmp = NULL;
	 unsigned char *strGBK = NULL;
	 const char separator = ',';

	 if(pContent == NULL) {
		 return PARAERROR;
	 }

	/* 为DER编码的数字证书 
	*/	
	p=pContent;
	x=X509_new();
	d2i_X509(&x,(const unsigned char **)&p,nContentLen);
	if(x == NULL){
		ErrorLog("cert content text is null\n");
		return CERTCONTEXTISNULL;
	}

	memset(issuer,'\0',CP_ISSUE_LEN);
	tmp=X509_NAME_oneline(X509_get_issuer_name(x),NULL,0);

	memcpy(issuer, tmp, strlen(tmp));
	strGBK = convertUTF8ToGBK((unsigned char*)issuer, strlen(tmp));
	if(strGBK == NULL){
		ErrorLog("convert utf8 to gbk error\n");
		ret = UTF8_TO_GBK_ERR;
		return ret;
	}
	memset(issuer, '\0', CP_ISSUE_LEN);
	//remove / , use separator ','
	replace_certinfo_separator(issuer, (const char*)strGBK, separator);

	DebugLog("issue is:%s\n",issuer);

	ret = 0;
	X509_free(x);
	 return ret;
 }


/*
功能:
获取证书中的有效期的天数
参数:
[in]	pContent:证书内容
[in]	nContentLen:证书内容长度
[out]	pnDays:有效期的天数
返回:
0：成功  非0：相应错误码			
*/
 int  Cert_getValidDays(unsigned char* pContent,int nContentLen,int *pnDays)
 {
	 int ret=-1;
	 //解析证书
	 X509			*x=NULL;
	 unsigned char	*p=NULL;

	 if(pContent==NULL){
		 return PARAERROR;
	 }

	time_t notAfterTime;
	time_t notBeforeTime;
  struct tm notAfterTm;
	struct tm notBeforeTm;
	double seconds=0;
    	
	/* 为DER编码的数字证书 
	*/	
	p=pContent;
	x=X509_new();
	d2i_X509(&x,(const unsigned char **)&p,nContentLen);
	
	if(x == NULL){
		ErrorLog("x509 x is null\n");
		return CERTCONTEXTISNULL;
	}
	DebugLog("not after is:\n");
	DebugHexLog(x->cert_info->validity->notAfter->data,x->cert_info->validity->notAfter->length);

	DebugLog("not before is:\n");
	DebugHexLog(x->cert_info->validity->notBefore->data,x->cert_info->validity->notBefore->length);

	if(x->cert_info->validity->notAfter->data == NULL ){
        return -1;
    }
    
	if(x->cert_info->validity->notBefore->data == NULL){
		return -1;
	}
	UTCTimeToTm(x->cert_info->validity->notAfter->data,&notAfterTm);
	UTCTimeToTm(x->cert_info->validity->notBefore->data,&notBeforeTm);

	notAfterTm.tm_year-=1900;
	notAfterTm.tm_mon-=1;
	notBeforeTm.tm_year-=1900;
	notBeforeTm.tm_mon-=1;

	notAfterTime=mktime(&notAfterTm);
	notBeforeTime=mktime(&notBeforeTm);
	seconds=difftime(notAfterTime,notBeforeTime);
	*pnDays=(int)seconds/(24*60*60);
	DebugLog("valid days is:%d day\n",*pnDays);
	ret = 0;
	X509_free(x);
	 return ret;
 }


/*
功能:
获取证书中的到期日
参数:
[in]	pContent:证书内容
[in]	nContentLen:证书内容长度
[out]	endDays:到期日，日期的格式yyyy-MM-dd HH:mm:ss
返回:
0：成功  非0：相应错误码
*/
 int  Cert_getEndDays(unsigned char* pContent,int nContentLen,char endDays[CP_ENDDAYS_LEN])
 {
	 int ret = -1;
	 //解析证书
	 X509			*x=NULL;
	 unsigned char	*p=NULL;
	 struct tm dst_tm;

	 if(pContent==NULL){
		 return PARAERROR;
	 }

	/* 为DER编码的数字证书 
	*/	
	p=pContent;
	x=X509_new();
	d2i_X509(&x,(const unsigned char **)&p,nContentLen);
	
	if(x == NULL){
		ErrorLog("x509 x is null\n");
		return CERTCONTEXTISNULL;
	}
	DebugLog("not after is:\n");
	DebugHexLog(x->cert_info->validity->notAfter->data,x->cert_info->validity->notAfter->length);
	memset(endDays,'\0',CP_ENDDAYS_LEN);
	
	UTCTimeToTm(x->cert_info->validity->notAfter->data,&dst_tm);
	
	sprintf (
		endDays,
		"%04d-%02d-%02d %02d:%02d:%02d",
		dst_tm.tm_year,
		dst_tm.tm_mon,
		dst_tm.tm_mday,
		dst_tm.tm_hour,
		dst_tm.tm_min,
		dst_tm.tm_sec
		);	
	if(endDays[0]!='\0'){
		DebugLog("end days is:%s\n",endDays);
	}
	ret = 0;
	X509_free(x);
	return ret;
 }

/*
功能:
获取证书中的属性
参数:
[in]	pContent:证书内容
[in]	nContentLen:证书内容长度
[in]	pOid:证书属性pOid
[in-out]pAttributeValue:属性值
[in-out]pnAttributeLen:属性值的长度
返回:
0：成功  非0：相应错误码
*/
 int  Cert_getAttribute(unsigned char* pContent,int nContentLen,char *pOid,
									   unsigned char* pAttributeValue, unsigned long *pnAttributeLen)
 {
	 int ret = -1;
	 //解析证书
	 X509			*x=NULL;
	 unsigned char	*p=NULL;

	 if(pContent==NULL) {
		 return PARAERROR;
	 }

	/* 为DER编码的数字证书 
	*/	
	p=pContent;
	x=X509_new();
	d2i_X509(&x,(const unsigned char **)&p,nContentLen);
//	x->cert_info->extensions->object->data;
	X509_free(x);
	ret = 0;
   return ret;
 }


  /*
 功能:
 获取证书中的签名项
 参数:
 [in]	pContent:证书内容
 [in]	nContentLen:证书内容长度
 [out]	signData:签名项内容
 返回:
 0：成功  非0：相应错误码
 */
 int  Cert_getSignData(unsigned char* pContent,int nContentLen,char signData[CP_SIGNDATALEN])
 {
	 	int ret = -1;	
	//解析证书
	X509			*x=NULL;
	unsigned char	*p=NULL;

	if(pContent==NULL){
		return PARAERROR;
	}

	/* 为DER编码的数字证书 
	*/
	p=pContent;
	x=X509_new();
	d2i_X509(&x,(const unsigned char **)&p,nContentLen);

	if(x == NULL){
		ErrorLog("证书上下文为空\n");
		ret = CERTCONTEXTISNULL;
		return ret;
	}

	memcpy(signData,x->signature->data,x->signature->length);

	DebugLog("signData is:\n");
	DebugHexLog(x->signature->data,x->signature->length);
	ret = 0;
	return ret;
 }

#endif

/*
函数名: key_findKeyContainer

  功能：遍历密钥容器，查找是否含有威豪的密钥容器标签
  如果有，则认定该key为威豪的下证系统所下，否则
  为数据所的下证系统下的
  
	输入参数:无
	输出参数:无
	返回值: 无
	
*/
void key_findKeyContainer()
{
	SysLog("进入到 key_findKeyContainer\n");
	int state=0;
	KEYVALUE PublicSigKey,PrivateSigKey,PublicExKey,PrivateExKey;
	PrivateSigKey.cbData= 0;	PrivateSigKey.pbData = NULL;
	PublicSigKey.cbData=0;	PublicSigKey.pbData = NULL;
	PublicExKey.cbData=0;	PublicExKey.pbData=NULL;
	PrivateExKey.cbData=0;	PrivateExKey.pbData=NULL;

	SysLog("findKeyContainer  flag is:%d\n",G_CallKeyFindKeyContainerFlag);
	//key_findKeyContainer是否被调用标志，调用过就不需要再次调用
	if(G_CallKeyFindKeyContainerFlag == 1) {
		return;
	}

	memset(ECC_CONTAINER_CHOICE,'\0',LABELEN);
	memset(ECC_SIGN_CER_LABEL,'\0',LABELEN);
	memset(ECC_CRY_CER_LABEL,'\0',LABELEN);
	memset(ECC_ROOT_CER_LABEL,'\0',LABELEN);

	if(pFindContainerAndKeypair == NULL){
		SysLog("key_pFindContainerAndKeypair指针为空\n");
		return;
	}
	if ( (state=pFindContainerAndKeypair("LAB_USERCERT_SIG",&PublicSigKey,&PrivateSigKey,&PublicExKey,&PrivateExKey))==0){
		SysLog("调用的容器为：LAB_USERCERT_SIG\n");
		SysLog("ECC_CONTAINER_CHOICE为：LAB_USERCERT_SIG\n");
		SysLog("ECC_SIGN_CER_LABEL为：LAB_USERCERT_SIG\n");
		SysLog("ECC_CRY_CER_LABEL为：LAB_USERCERT_ENC\n");
		SysLog("ECC_ROOT_CER_LABEL为：CA_CERT\n");
		SysLog("ECC_ENCCONTAINER_CHOICE为：LAB_USERCERT_ENC\n");
		
		strcpy(ECC_SIGN_CER_LABEL,"LAB_USERCERT_SIG");
		strcpy(ECC_CRY_CER_LABEL,"LAB_USERCERT_ENC");
		strcpy(ECC_ROOT_CER_LABEL,"CA_CERT");
		strcpy(ECC_CONTAINER_CHOICE,"LAB_USERCERT_SIG");
		strcpy(ECC_ENCCONTAINER_CHOICE,"LAB_USERCERT_ENC");
		
	}else if((state=pFindContainerAndKeypair("e_k",&PublicSigKey,&PrivateSigKey,&PublicExKey,&PrivateExKey))==0){
		SysLog("调用的容器为：e_k\n");
		SysLog("ECC_CONTAINER_CHOICE为：e_k\n");
		SysLog("ECC_SIGN_CER_LABEL为：s_c\n");
		SysLog("ECC_CRY_CER_LABEL为：e_c\n");
		SysLog("ECC_ROOT_CER_LABEL为：root\n");
		
		strcpy(ECC_SIGN_CER_LABEL,"s_c");
		strcpy(ECC_CRY_CER_LABEL,"e_c");
		strcpy(ECC_ROOT_CER_LABEL,"root");
		strcpy(ECC_CONTAINER_CHOICE,"e_k");
		strcpy(ECC_ENCCONTAINER_CHOICE,"e_k");
	}else{
		SysLog("未查找到容器\n");
		return ;
	}
	
	G_CallKeyFindKeyContainerFlag = 1;
	return;
	
}

int key_getCertInfo(unsigned char *buf, int size,int index)
{
	int ret = -1;
	long cbSize = 0;

#ifdef WIN32
	PCCERT_CONTEXT   pCertContext = NULL;
	SYSTEMTIME begin,end;
	struct tm begin_tm,end_tm;
	struct tm *tmp_tm=NULL;
	time_t begin_tm_time, end_tm_time,now;
	long  elapsed_time;
	FILETIME  localTime;
	SYSTEMTIME endTime;
	//解析待使用证书得到证书上下文
	pCertContext=CertCreateCertificateContext(X509_ASN_ENCODING,buf,size);
	if(pCertContext == NULL){
		SysLog("证书上下文为空");
		ret=CERTCONTEXTISNULL;
		return ret;
	}
	// 得到主题项
	//获取字符串形式的主题项
	cbSize = CertNameToStr(
		pCertContext->dwCertEncodingType,
		&(pCertContext->pCertInfo->Subject),
		MY_STRING_TYPE,
		NULL,
		0);

	if (1 == cbSize){
		ret= SUBJECTISNULL;
		SysLog("证书的主题项为空串!\n");
	}else{
		cbSize = CertNameToStr(
			pCertContext->dwCertEncodingType,
			&(pCertContext->pCertInfo->Subject),
			MY_STRING_TYPE,
			keyInfo[index].Subject,
			cbSize);

		if (1 == cbSize){
			ret = SUBJECTISNULL;
			SysLog("证书的主题项为空串!\n");
		}else{
			++G_SubjectNum;
			SysLog("证书主题项为%s\n",keyInfo[index].Subject);
		}			
	}

	//得到公钥
	++G_PubkeyNum;
	keyInfo[index].nPubKeyLen=pCertContext->pCertInfo->SubjectPublicKeyInfo.PublicKey.cbData;
	memcpy(keyInfo[index].Pubkey,pCertContext->pCertInfo->SubjectPublicKeyInfo.PublicKey.pbData,keyInfo[index].nPubKeyLen);
	SysLog("得到公钥，内容为：\n");
	SysHexLog((unsigned char*)keyInfo[index].Pubkey,keyInfo[index].nPubKeyLen);

	//得到发行者
	//获取字符串形式的主题项
	cbSize = CertNameToStr(
		pCertContext->dwCertEncodingType,
		&(pCertContext->pCertInfo->Issuer),
		MY_STRING_TYPE,
		NULL,
		0);

	if (1 == cbSize){
		ret= ISSUERISNULL;
		SysLog("证书的发布者项为空串!\n");
	}else{
		cbSize = CertNameToStr(
			pCertContext->dwCertEncodingType,
			&(pCertContext->pCertInfo->Issuer),
			MY_STRING_TYPE,
			keyInfo[index].Issuer,
			cbSize);
		if (1 == cbSize){
			ret = ISSUERISNULL;
			SysLog("证书的发布者项为空串!\n");
		}else{
			++G_IssuerNum;
			SysLog("证书发布者项为%s\n",keyInfo[index].Issuer);
		}			
	}

	//得到有效期
	now = time(NULL);
	tmp_tm = localtime( &now ); 

	FileTimeToSystemTime(&(pCertContext->pCertInfo->NotBefore), &begin);
	FileTimeToSystemTime(&(pCertContext->pCertInfo->NotAfter), &end);

	begin_tm.tm_year = begin.wYear - 1900;
	begin_tm.tm_mon  = begin.wMonth - 1;
	begin_tm.tm_mday = begin.wDay ;
	begin_tm.tm_hour = begin.wHour;
	begin_tm.tm_min  = begin.wMinute;
	begin_tm.tm_sec  = begin.wSecond;
	begin_tm.tm_wday = begin.wDayOfWeek;
	begin_tm.tm_isdst= 0;

	end_tm.tm_year=end.wYear - 1900;
	end_tm.tm_mon= end.wMonth -1;
	end_tm.tm_mday=end.wDay ;
	end_tm.tm_hour=end.wHour;
	end_tm.tm_min=end.wMinute;
	end_tm.tm_sec=end.wSecond;
	end_tm.tm_wday = end.wDayOfWeek;
	end_tm.tm_isdst = 0;

	begin_tm_time = mktime(&begin_tm);
	end_tm_time = mktime(&end_tm);

	elapsed_time =(long) difftime( end_tm_time,begin_tm_time );
	keyInfo[index].nValidDays = elapsed_time/ (24*3600) ;
	SysLog("有效期为：%d\n",keyInfo[index].nValidDays);
	++G_VaildDayNum;

	//得到失效日期
	memset(&endTime,0,sizeof(endTime));
	FileTimeToLocalFileTime(&pCertContext->pCertInfo->NotAfter,&localTime);

	FileTimeToSystemTime(&localTime,&endTime);
	sprintf(keyInfo[index].EndDays, "%d年%d月%d日 %d:%d:%d",end.wYear,end.wMonth,
		end.wDay,end.wHour,end.wMinute,end.wSecond);
	SysLog("到期日为：\n");
	SysHexLog((unsigned char*)keyInfo[index].EndDays,strlen(keyInfo[index].EndDays));	

	++G_EndDayNum;

	//得到uid
	ret= getUidFromSubject(pCertContext->pCertInfo->Subject.pbData,pCertContext->pCertInfo->Subject.cbData,keyInfo[index].UserId);

	if ( ret != 1){
		SysLog("未得到uid\n");
		ret = GETUSERIDFAILED;
	}else{
		ret=0;
		++G_UserIdNum;
		SysLog("用户uid为:%s\n",keyInfo[index].UserId);
	}		

	CertFreeCertificateContext(pCertContext);
	pCertContext = NULL;


#else
	X509			*x=NULL;
	unsigned char	*p=NULL;
	p=buf;
	x=X509_new();
	d2i_X509(&x,(const unsigned char **)&p,size);
	if(x){
		ret = Cert_getSubject(buf,size,keyInfo[index].Subject);

		if (0 != ret){
			ret = SUBJECTISNULL;
			ErrorLog("subject is null!\n");
		}else{
			G_SubjectNum++;
			DebugLog("subject is:%s\n",keyInfo[index].Subject);
		}

		//得到公钥
		G_PubkeyNum++;
		keyInfo[index].nPubKeyLen=x->cert_info->key->public_key->length;
		memcpy(keyInfo[index].Pubkey,x->cert_info->key->public_key->data,keyInfo[index].nPubKeyLen);
		DebugLog("public key is:\n");
		DebugHexLog((unsigned char*)keyInfo[index].Pubkey,keyInfo[index].nPubKeyLen);
		//得到发行者
		//获取字符串形式的主题项
		ret = Cert_getIssuer(buf,size,keyInfo[index].Issuer);
		if (0 != ret){
			ret = ISSUERISNULL;
			ErrorLog("issue is null!\n");
		}else{
			G_IssuerNum++;
			DebugLog("issue is:%s\n",keyInfo[index].Issuer);
		}	

		//得到有效期														
		//SYSTEMTIME begin,end;
		struct tm begin_tm,end_tm;
		struct tm *tmp_tm=NULL;
		time_t begin_tm_time, end_tm_time,now;
		long  elapsed_time;
		now = time(NULL);
		tmp_tm = localtime( &now ); 
		unsigned char* begin = x->cert_info->validity->notBefore->data;
		unsigned char* end = x->cert_info->validity->notAfter->data;
		begin_tm.tm_year = 100 + (begin[0]-'0')*10 + (begin[1]-'0');
		begin_tm.tm_mon  = (begin[2]-'0')*10 + (begin[3]-'1');
		begin_tm.tm_mday = (begin[4]-'0')*10 + (begin[5]-'0');
		begin_tm.tm_hour = (begin[6]-'0')*10 + (begin[7]-'0');
		begin_tm.tm_min = (begin[8]-'0')*10 + (begin[9]-'0');
		begin_tm.tm_sec = (begin[10]-'0')*10 + (begin[11]-'0');
		end_tm.tm_year = 100 + (end[0]-'0')*10 + (end[1]-'0');
		end_tm.tm_mon  = (end[2]-'0')*10 + (end[3]-'1');
		end_tm.tm_mday = (end[4]-'0')*10 + (end[5]-'0');
		end_tm.tm_hour = (end[6]-'0')*10 + (end[7]-'0');
		end_tm.tm_min = (end[8]-'0')*10 + (end[9]-'0');
		end_tm.tm_sec = (end[10]-'0')*10 + (end[11]-'0');

		begin_tm_time = mktime(&begin_tm);
		end_tm_time = mktime(&end_tm);

		elapsed_time =(long) difftime( end_tm_time,begin_tm_time );
		keyInfo[index].nValidDays = elapsed_time/ (24*3600) ;
		DebugLog("valid days is:%d\n",keyInfo[index].nValidDays);					
		G_VaildDayNum++;
		sprintf(keyInfo[index].EndDays, "%d年%d月%d日 %d:%d:%d",(end_tm.tm_year+1900),(end_tm.tm_mon+1),
			end_tm.tm_mday,end_tm.tm_hour,end_tm.tm_min,end_tm.tm_sec);
		DebugLog("end days is:%s\n",keyInfo[index].EndDays);
		G_EndDayNum++;
		//得到uid
		ret= getUidFromSubject((unsigned char*)(x->cert_info->subject->bytes->data),x->cert_info->subject->bytes->length,keyInfo[index].UserId);
		if ( ret!=1){
			ErrorLog("get uid failed!\n");
			ret =GETUSERIDFAILED;
		}else{
			ret=0;
			G_UserIdNum++;
			DebugLog("uid is:%s\n",keyInfo[index].UserId);
		}					
	}else{
		ErrorLog("cert context is null\n");
		ret=CERTCONTEXTISNULL;
	}

	X509_free(x);
	x = NULL;

#endif

	return ret;
}

/*
更新设备信息
成功：返回0
失败：返回错误码	
 */
int key_updateDevInfo()
{
	SysLog("进入到 key_updateDevInfo\n");

	unsigned char *buf=NULL;
	HANDLE *pKeyList=NULL;
	int ret = -1,i=0;
	int nCount=0,	nKeyNum = 0,result=0, size=0, signDataLen = 96;
	unsigned int certSize = 0;
	long cbSize = 0;
	char serial[SERIALEN]={'\0'};
	char username[USERNAMELEN]={'\0'};
	HANDLE handle = NULL;

	SysLog("update key info flag is:%d\n",G_CallKeyUpdateInfoFlag);
	//key_updateDevInfo是否被调用标志，调用过就不需要再次调用
	if(G_CallKeyUpdateInfoFlag == 1) {
		ret = 0;
		goto end;
	}

	memset(serial,'\0',SERIALEN);
	memset(username,'\0',USERNAMELEN);

	if(pGetDeviceList){
		nKeyNum=pGetDeviceList(NULL, nCount);
		nKeyNum=pGetDeviceList(NULL, nCount);
		G_DevNum=nKeyNum;
	}else{
		SysLog("pGetDeviceList 指针为空\n");
		ret = POINTERISNULL;
		goto end;
	}
	
	if(pSetDevice == NULL){
		SysLog("pSetDevice 为空\n");
		ret = POINTERISNULL;
		goto end;
	}

	if(G_NowDev == SK311){
		if(G_pSK_ExportFile == NULL){
			SysLog("G_pSK_ExportFile is null\n");
			ret = POINTERISNULL;
			goto end;
		}
	}

	for(i=0;i<KEYNUM;i++){
		memset(keyInfo[i].CertSerialNo,'\0',SERIALEN);
		memset(keyInfo[i].UserName,'\0',USERNAMELEN);
		memset(keyInfo[i].Subject,'\0',SUBJECTLEN);
		memset(keyInfo[i].Pubkey,'\0',PUBKEYLEN);
		memset(keyInfo[i].Issuer,'\0',ISSUERLEN);
		memset(keyInfo[i].EndDays,'\0',ENDDAYLEN);
		memset(keyInfo[i].UserId,'\0',USERIDLEN);
		memset(keyInfo[i].SignData,'\0',SIGNDATALEN);
		keyInfo[i].nValidDays=0;
		keyInfo[i].keyHandle=NULL;
		memset(keyInfo[i].Cert,'\0',CERTLEN);
		keyInfo[i].nCertLen=0;
		keyInfo[i].nPubKeyLen=0;
		keyInfo[i].nSignDataLen = 0;
	}
	
	G_CertNum=0;//证书数量
	G_UserNameNum=0;//用户名数量
	G_SubjectNum=0;
	G_PubkeyNum=0;
	G_IssuerNum=0;
	G_VaildDayNum=0;
	G_EndDayNum=0;
	G_UserIdNum=0;
	G_SignDataNum = 0;

	SysLog("设备数量为：%d\n",G_DevNum);
	//根据不同的设备数量进行不同的操作
	if(G_DevNum == 1){
		if(G_NowDev == SK311){
			ret = G_pSK_ExportFile(4,1,buf,&certSize);
			size = certSize;
		}else {
			ret=key_exportCert(G_AsyType,1,buf,&size);
		}

		if(ret != 0){
			SysLog("第一次导出证书失败,此设备不存在此证书\n");
			ret = SIGNCERTNOTEXIST;
			goto end;
		}
		buf = (unsigned char*)malloc(size*sizeof(unsigned char));
		memset(buf,'\0',size);
		if(G_NowDev == SK311){
			ret = G_pSK_ExportFile(4,1,buf,&certSize);
			size = certSize;
		}else {
			ret=key_exportCert(G_AsyType,1,buf,&size);
		}
		if(ret != 0){
			SysLog("第二次导出证书失败\n");
			ret = EXPORTCERTFAILED;
			goto end;
		}

		G_CallKeyUpdateInfoFlag = 1;  //北京市委延期用

		//保存证书
		memcpy(keyInfo[0].Cert,buf,size);
		keyInfo[0].nCertLen=size;

		ret=key_getSerial(buf,(unsigned long)size,serial);//得到证书序列号
		if(ret==0)//未得到证书序列号
		{
			SysLog("设备数量为1时,未得到证书序列号!\n");
			ret=GETSERIALFAILED;
		}else if(ret==1)//得到证书序列号
		{
			ret=0;
			G_CertNum=1;
			memcpy(keyInfo[0].CertSerialNo,serial,strlen(serial));
		}

		//得到用户名
		ret=key_getSubjectUserName(buf,size,username);
		if(ret == 0){
			memcpy(keyInfo[0].UserName,username,strlen(username));
			G_UserNameNum=1;
		}else{
			SysLog("获取用户名失败\n");
			ret=GETUSERNAMEFAILED;
		}

		//获取签名值
		G_SignDataNum = 1;
		keyInfo[0].nSignDataLen = signDataLen;
		memcpy(keyInfo[0].SignData,buf+(size-signDataLen),signDataLen);	
		SysLog("证书的签名项为：\n");
		SysHexLog((unsigned char*)(keyInfo[0].SignData),signDataLen);

		ret = key_getCertInfo(buf,size,0);

	}else if(G_DevNum==2){
		pKeyList = new HANDLE[nKeyNum];
		nKeyNum = pGetDeviceList(pKeyList, nCount);//得到设备的句柄列表

		handle=pGetDevice();//得到当前正在操作的设备		
		for(i=0;i<G_DevNum;i++)//此循环中使用G_DevNum,不使用nKeyNum，因为nKeyNum有时会出错
		{
			SysLog("设备数量为2时，操作的当前的设备句柄为：%d\n",pKeyList[i]);
			if(pSetDevice(pKeyList[i]) != TRUE)//设置当前设备
			{
				SysLog("设置当前操作的设备失败\n");
				ret=SETDEVICEFAILED;
				goto end;
			}
			SysLog("设置当前操作的设备成功\n");
			keyInfo[i].keyHandle=pKeyList[i];
			if((ret=key_exportCert(G_AsyType,1,buf,&size)) != 0)//导出证书
			{
				SysLog("设备数量为2时,第一次调用ExportCert失败，此设置不存在此类型的证书\n");
				ret=SIGNCERTNOTEXIST;
				goto end;
			}
			buf = (unsigned char*)malloc(size);
			memset(buf,'\0',size);
			if((ret=key_exportCert(G_AsyType,1,buf,&size)) != 0){
				SysLog("设备数量为2时,第二次调用ExportCert失败!\n");
				ret=EXPORTCERTFAILED;
				goto end;
			}

			G_CallKeyUpdateInfoFlag = 1;  //北京市委延期用

			//保存证书
			memcpy(keyInfo[i].Cert,buf,size);
			keyInfo[i].nCertLen=size;

			ret=key_getSerial(buf,(unsigned long)size,serial);//得到证书序列号
			if(ret==0)//未得到证书序列号
			{
				SysLog("设备数量为2时,未得到证书序列号!\n");
				ret=GETSERIALFAILED;
			}else if(ret==1)//得到证书序列号
			{
				ret=0;
				memcpy(keyInfo[i].CertSerialNo,serial,strlen(serial));//逻辑错误：keyInfo[G_CertNum].CertSerialNo
				G_CertNum++;
			}

			//得到用户名
			ret=key_getSubjectUserName(buf,size,username);
			if(ret==0){
				memcpy(keyInfo[i].UserName,username,strlen(username));
				G_UserNameNum++;
			}else{
				SysLog("获取用户名失败\n");
				ret=GETUSERNAMEFAILED;
			}

			//获取签名值
			G_SignDataNum++;
			keyInfo[i].nSignDataLen = signDataLen;
			memcpy(keyInfo[i].SignData,buf+(size-signDataLen),signDataLen);	
			SysLog("证书的签名项为：\n");
			SysHexLog((unsigned char*)(keyInfo[i].SignData),signDataLen);

			ret = key_getCertInfo(buf,size,i);

			key_freeBuffer((void **)&buf);
			size=0;
		}
		pSetDevice(handle);//设置当前操作的设备
	}else if(G_DevNum == 0){
		SysLog("设备数量为0\n");
		ret = DEVNUMISZERO;
		goto end;
	}

	if(strstr(keyInfo[0].Issuer,USELIMITE) != NULL){
		G_VerifyUseLimiteFlag = 1;
	}else{
		G_VerifyUseLimiteFlag = 1;
		ret = 0;
//		ret = OUTOFUSERRANGE;
	}

end:

	key_freeBuffer((void **)&buf);

	if(pKeyList){
		delete []pKeyList;
		pKeyList = NULL;
	}
	SysLog("ret is:%d\n",ret);
	return ret;
}

/*
加载动态库
成功：返回0
失败：错误码	
 */
int  key_loadLibrary()
{
	SysLog("进入到 key_loadLibrary\n");
	int ret = -1;
	int nDevType=0;
	bool confIsExist = false;

	if(G_hDll){
		SysLog("句柄不为空，无需重新加载\n");
		if(G_NowDev != SK311){
			key_findKeyContainer();
		}
		ret = key_updateDevInfo();
	}
	
	//读取配置文件
	char retValue[255];
	int result = 0;
	char lpszCurDir[CONFIGFILELEN]; 
	memset(lpszCurDir,'\0',CONFIGFILELEN);

	char lpszSysDir[CONFIGFILELEN] = {0};

#ifdef WIN32
	GetSystemDirectory(lpszSysDir,CONFIGFILELEN);
#else
	strcpy(lpszSysDir,"/usr/lib");
#endif

	//如果调用了key_setDevice则调用设置的设备，否则读取配置文件，调用默认的设备类型
	if(G_DeviceState == 1){
		SysLog("调用了key_setDevice接口\n");
		nDevType=G_NowDev;
		switch(nDevType) {
		case TIANYU:
			G_hDll=::LoadLibrary(GBWTHARDWAREDLL);
			break;
		case HUASHEN:
			G_hDll=::LoadLibrary(GBHDHARDWAREDLL);
			break;
		case HXT:
			G_hDll=::LoadLibrary(WHSDKDLL);
			break;
		case SDTKEY:
			G_hDll = LoadLibrary(SDTKEYDLL);
			break;
		case HXTOLD:
			break;
		case SK311:
		case 7:
		case 8:
		case 9:
		case 10:
			G_hDll=::LoadLibrary(SK311DLL);
			break;
		default:
			SysLog("未知设备类型\n");
			ret=UNKNOWNDEVTYPE;
			G_hDll = NULL;
			return ret;
		}
	}else{
		//读取配置文件，从中得到默认的设备类型
		SysLog("tjjusbkey conf file is:%s\n",lpszCurDir);
		GetConfFilePath(lpszCurDir);
		confIsExist = IsExistFile(lpszCurDir);
		SysLog("tjjusbkey conf file is:%s\n",lpszCurDir);

		if(confIsExist == true){
			memset (retValue,'\0',255);
			
#ifdef WIN32
			result =GetPrivateProfileString("DevType","DevType","    ",retValue,255,lpszCurDir);
#else
			server_ini = iniparser_load(( char *)lpszCurDir);			
			strcat(( char *)retValue,iniparser_getstring(server_ini,"DevType:DevType",NULL));			
#endif
			if(retValue){
				nDevType=atoi(retValue);
				SysLog("from conf file get device type is:%d\n",nDevType);
				G_NowDev = nDevType;

				char cDevType[8]={'\0'};
#ifdef WIN32
				itoa(nDevType,cDevType,8);
#else
				sprintf(cDevType,"%d",nDevType);
#endif
				SysLog("转化为字符串后的设备类型为：%s\n",cDevType);

#ifdef WIN32
				result =GetPrivateProfileString("AsyType","AsyType","    ",retValue,255,lpszCurDir);
#else
				server_ini = iniparser_load(( char *)lpszCurDir);
				strcat(( char *)retValue,iniparser_getstring(server_ini,"AsyType:AsyType","    "));			
#endif
				if(retValue){
					G_AsyType = atoi(retValue);
				}else{
					SysLog("未查找到此算法类型，按默认算法类型\n");
				}
			
				memset(retValue,'\0',255);
#ifdef WIN32
				result =GetPrivateProfileString("DigestType","DigestType","    ",retValue,255,lpszCurDir);
#else
				server_ini = iniparser_load(( char *)lpszCurDir);
				strcat(( char *)retValue,iniparser_getstring(server_ini,"DigestType:DigestType","    "));			

#endif
				if(retValue){
					G_DigestType=atoi(retValue);
				}else{
					SysLog("未查找到此摘要类型，按默认摘要类型\n");
				}
				memset (retValue,'\0',255);
#ifdef WIN32
				result =GetPrivateProfileString("DLL",cDevType,"    ",retValue,255,lpszCurDir);
#else				
				char ParseStr[100]; 			
				strcat(ParseStr,"DLL");
				strcat(ParseStr,":");
				strcat(ParseStr,cDevType);
				server_ini = iniparser_load(( char *)lpszCurDir);
				SysLog("ParseStr is:%s\n",ParseStr);

				strcat(( char *)retValue,iniparser_getstring(server_ini,ParseStr,"    "));	
				SysLog("retValue is:%s\n",retValue);
#endif				
				if(retValue){
#ifdef WIN32
					strcat(lpszSysDir,"\\");
#else
					strcat(lpszSysDir,"/");
#endif
					strcat(lpszSysDir,retValue);
					SysLog("设备库文件路径为：%s\n",lpszSysDir);
					if(IsExistFile(lpszSysDir) == true){
						G_hDll=::LoadLibrary(retValue);
					}else{
#ifdef WIN32
						::MessageBox(NULL,"设备库文件不存在","error",MB_OK);
#else
						ErrorLog("设备库文件不存在\n");
#endif	
						ret=DEVDLLISNOTEXIST;
					}
				}else{
					SysLog("未查找到此类型的key\n");
					ret=UNKNOWNDEVTYPE;
				}
			}else{
				SysLog("未查找到此类型的设备\n");
				ret=UNKNOWNDEVTYPE;
			}
		}else{
			SysLog("未找到配置文件\n");
			ret=CONFISNOTEXIST;
		}
	}
	
	if(!G_hDll){
		SysLog("加载库失败\n");
		ret=LOADLIBRARYFAILED;
	}else{
		SysLog("得到当前设备句柄的各个接口地址\n");
		if(G_NowDev == SK311){
			get311KeyProcAddress(G_hDll);
		}else{
			getProcAddress(G_hDll);
			key_findKeyContainer();
		}
		ret = key_updateDevInfo();
	}

	return ret;
}

void key_enter_lock_area()
{
#ifdef MUTEXFLAG
	SysLog("enter key_enter_lock_area\n");
#ifdef WIN32
	WaitForSingleObject(hMutex,INFINITE);
#else
	apr_thread_mutex_lock(mutex);
#endif
#endif
}

void key_leave_lock_area()
{
#ifdef MUTEXFLAG
	SysLog("enter key_leave_lock_area\n");
#ifdef WIN32
	ReleaseMutex(hMutex);
#else	
	apr_thread_mutex_unlock(mutex);
#endif
#endif
}

/*
功能: 
设置当前操作的USB-KEY的类型。

  参数:
  [in] nDevType
  设置USB-KEY的设备类型。
  USB-KEY设备类型1 - TIANYU_TYPE
  USB-KEY设备类型2 - HUASHEN_TYPE
  USB-KEY设备类型3 - HXT_TYPE
  USB-KEY设备类型4 - SDT_TYPE
	返回:
	0：成功  非0：相应错误码
	
	  注释:
	  该接口为当用户同时插入多个不同类型的USB-KEY时，用户需要选择设定要操作的为哪种KEY。
	  若应用场景为用户当前只插入一个KEY，则不需要调用该函数接口。	  
 */

EXPORTDLL int WINAPIV key_setDevice (int nDevType)
{
	key_enter_lock_area();
	int ret=0;
	SysLog("函数key_setDevice中，传进来的设备类型为：%d\n",nDevType);
	if(nDevType <= 0){
		SysLog("para nDevType is:%d\n",nDevType);
		return UNKNOWNDEVTYPE;
	}

	G_NowDev=nDevType;
	G_DeviceState=1;
	
	SysLog("退出key_setDevice\n");
	key_leave_lock_area();
	return ret;
}

/*
功能：	
设置接口类型

参数：
[in]Type：接口类型，固定为1
返回:
成功返回0，失败返回非0值
注释	
该接口暂时未用
*/
EXPORTDLL int WINAPIV key_setInterface(int nInterfaceType)
{
	return 0;
};

/*
功能: 
获取指定类型的USB-KEY的设备数量。

  参数: 
  [out] pDevNum
  当前插入的指定类型的USB-KEY的个数。
  返回:
  0：成功  非0：相应错误码
  
	注释:
	该接口获取当前插入的指定类型的USB-KEY的数量。调用该接口前，
	若调用SetDevice设定了插入的USB-KEY的类型，则该接口得到的是该指定类型的USB-KEY的数量，
	否则为系统默认的USB-KEY类型的USB-KEY的数量。	
 */

EXPORTDLL int WINAPIV key_getDeviceNum(int *pDevNum)
{
	key_enter_lock_area();
	SysLog("进入到 key_getDeviceNum\n");
	int ret=0,	nCount=0,	keyNum=0;

	ret = key_loadLibrary();
	if(ret != 0){
		goto end;
	}
	//得到设备数量
	if(pGetDeviceList){		
		keyNum=pGetDeviceList(NULL,nCount);
		keyNum=pGetDeviceList(NULL,nCount);
		G_DevNum=keyNum;
		*pDevNum=keyNum;
	}else{
		SysLog("pGetDeviceList 为空\n");
		ret=POINTERISNULL;
	}

end:

	SysLog("退出 key_getDeviceNum,设备数量为：%d\n",G_DevNum);
	key_leave_lock_area();
	return ret;
}

/*
 从USB-KEY中导出证书
  输入参数：AlgType-非对称密钥算法类型1 -.RSA1024  2-RSA2048  3-ECC256  4-ECC384电子政务算法 
            Usage-密钥用途-0-.用于导出加密证书 1-.用于导出签名证书  2-导出根证书 3-导出私钥文件
            buf-证书文件内容
            size-证书文件的长度
  输出参数：无
  返回:0-正确 -1：错误
*/
int key_exportCert(int AlgType,	int Usage,BYTE *buf,int *size)
{
	int state=-1;
	SysLog("进入到 key_exportCert\n");
	SysLog("AlgType is:%d\n",AlgType);
	char CertName[32];
	memset(CertName,'\0',32);

	if(pExportFile == NULL){
		SysLog("指针为空，退出\n");
		state = POINTERISNULL;
		goto end;
	}
	switch (AlgType){
	case RSA1024://RSA1024算法
		if (buf == NULL){
			switch(Usage){
			case ENC:
				state = pExportFile((BYTE *)"RSA1024-0",LBL_CERT,NULL,size);
				break;
			case SIG:
				state = pExportFile((BYTE *)"RSA1024-1",LBL_CERT,NULL,size);
				break;
			case ROOT:
				state = pExportFile((BYTE *)"RSA1024-2",LBL_CERT,NULL,size);
				break;
			case PRIFILE:
				state = pExportFile((BYTE *)"RSA1024-3",LBL_CERT,NULL,size);
				break;
			default:
				break;
			}
			goto end;
		}			
		if (Usage == ENC)
			state = pExportFile((BYTE *)"RSA1024-0",LBL_CERT,buf,size);	

		if ( Usage == SIG)
			state = pExportFile((BYTE *)"RSA1024-1",LBL_CERT,buf,size);	

		if (Usage == ROOT)
			state = pExportFile((BYTE *)"RSA1024-2",LBL_CERT,buf,size);	

		if ( Usage == PRIFILE)
			state = pExportFile((BYTE *)"RSA1024-3",LBL_CERT,buf,size);			

		break;

	case RSA2048: //RSA2048算法
		if (buf == NULL){
			if  ( Usage == ENC)
				state = pExportFile((BYTE *)"RSA2048-0",LBL_CERT,NULL,size);	

			if ( Usage == SIG)
				state = pExportFile((BYTE *)"RSA2048-1",LBL_CERT,NULL,size);			

			if ( Usage ==  ROOT)
				state = pExportFile((BYTE *)"RSA2048-2",LBL_CERT,NULL,size);

			if ( Usage ==  PRIFILE)
				state = pExportFile((BYTE *)"RSA2048-3",LBL_CERT,NULL,size);

			goto end;
		}

		if (Usage == ENC)
			state = pExportFile((BYTE *)"RSA2048-0",LBL_CERT,buf,size);	

		if ( Usage == SIG)
			state = pExportFile((BYTE *)"RSA2048-1",LBL_CERT,buf,size);	

		if (Usage == ROOT)
			state = pExportFile((BYTE *)"RSA2048-2",LBL_CERT,buf,size);	

		if ( Usage == PRIFILE)
			state = pExportFile((BYTE *)"RSA2048-3",LBL_CERT,buf,size);			

		break;
	case ECC256://ECC256算法
		if (buf == NULL){
			if ( Usage == ENC)
				state = pExportFile((BYTE *)ECC_CRY_CER_LABEL,LBL_CERT,NULL,size);	

			if ( Usage == SIG){
				state = pExportFile((BYTE *)ECC_SIGN_CER_LABEL,LBL_CERT,NULL,size);								
			}			
			if ( Usage == ROOT)
				state = pExportFile((BYTE *)ECC_ROOT_CER_LABEL,LBL_CERT,NULL,size);	

			if ( Usage == PRIFILE)
				state = pExportFile((BYTE *)ECC_KEY_FILE,LBL_CERT,NULL,size);	
			SysLog("第一次导出时，状态码为：%.2x\n",state);
			goto end;
		}

		if ( Usage == ENC){
			state = pExportFile((BYTE *)ECC_CRY_CER_LABEL,LBL_CERT,buf,size);					
		}	
		if ( Usage == SIG)		
			state = pExportFile((BYTE *)ECC_SIGN_CER_LABEL,LBL_CERT,buf,size);	

		if ( Usage == ROOT)
			state = pExportFile((BYTE *)ECC_ROOT_CER_LABEL,LBL_CERT,buf,size);

		if ( Usage == PRIFILE)
			state = pExportFile((BYTE *)ECC_KEY_FILE,LBL_CERT,buf,size);
		SysLog("第二次导出时，状态码为：%.2x\n",state);
		break;

	case ECC384://ECC384算法
		if (buf == NULL){
			if ( Usage == ENC)
				state = pExportFile((BYTE *)ECC_CRY_CER_LABEL,LBL_CERT,NULL,size);	

			if ( Usage == SIG)
			{
				state = pExportFile((BYTE *)ECC_SIGN_CER_LABEL,LBL_CERT,NULL,size);								
			}				
			if ( Usage == ROOT)
				state = pExportFile((BYTE *)ECC_ROOT_CER_LABEL,LBL_CERT,NULL,size);	

			if ( Usage == PRIFILE)
				state = pExportFile((BYTE *)ECC_KEY_FILE,LBL_CERT,NULL,size);	
			SysLog("第一次导出时，状态码为：%.2x\n",state);
			goto end;
		}

		if ( Usage == ENC)
			state = pExportFile((BYTE *)ECC_CRY_CER_LABEL,LBL_CERT,buf,size);				
		if ( Usage == SIG)		
			state = pExportFile((BYTE *)ECC_SIGN_CER_LABEL,LBL_CERT,buf,size);			
		if ( Usage == ROOT)
			state = pExportFile((BYTE *)ECC_ROOT_CER_LABEL,LBL_CERT,buf,size);		
		if ( Usage == PRIFILE)
			state = pExportFile((BYTE *)ECC_KEY_FILE,LBL_CERT,buf,size);	
		SysLog("第二次导出时，状态码为：%.2x\n",state);
		break;		
	default:
		return -1;
	}

	if ( state == 0){
		SysLog("退出ExportCert\n");
	}else{
		SysLog("导出证书失败，错误码为：%.2x\n",state);
		state=EXPORTCERTFAILED;
	}

end:

	return state;
}

#ifdef WIN32
// 去除指定字符
// 下面的代码用于字符串替换
static char* trim(char *str, char chr){
	return (*str==0)?str:(((*str!=chr)?(((trim(str+1, chr)-1)==str)?str:(*(trim(str+1,chr)-1)=*str,*str=chr,trim(str+1,chr))):trim(str+1,chr)));
}

// 删除字符串中的指定字符, 返回字符串
static char *trimString(char *str, char chr)
{
#pragma   warning( disable: 4996 )
	return lstrcpy(str, trim(str, chr));
#pragma   warning( default: 4996 )
}

// 提取X509证书序列号,返回与IE看到的证书序列一致(去掉空格)
BOOL parseX509CertSerialNumber(PCCERT_CONTEXT  hCert, char sn[])
{
	int      i, len;
	char	 c0, c1;
	BOOL	 bResult;
	CRYPT_INTEGER_BLOB  SerialNumber;
	char	*s=NULL;
	// 取得要解码的信息尺寸
	bResult = CryptFormatObject(
		hCert->dwCertEncodingType,
		0,
		0,
		NULL,
		0,
		hCert->pCertInfo->SerialNumber.pbData,
		hCert->pCertInfo->SerialNumber.cbData,
		NULL,
		&SerialNumber.cbData);
	
	if (!bResult)
		return FALSE;
	// 分配解码数据存放
	SerialNumber.pbData = (unsigned char *) malloc(SerialNumber.cbData);
	memset(SerialNumber.pbData,'\0',SerialNumber.cbData);
	// 解码数据
	bResult = CryptFormatObject(
		hCert->dwCertEncodingType,
		0,
		0,
		NULL,
		0,
		hCert->pCertInfo->SerialNumber.pbData,
		hCert->pCertInfo->SerialNumber.cbData,
		SerialNumber.pbData ,
		&SerialNumber.cbData);
	
	if (!bResult){
		free(SerialNumber.pbData);
		return FALSE;
	}
//	s = (char*) malloc(SERIALEN);  //-------------2011-06-22    原始为48
//	memset(s,'\0',SERIALEN);
	
	s = ::_com_util::ConvertBSTRToString((BSTR)SerialNumber.pbData);
	
	if(s != NULL)
		lstrcpy(sn, s);

	if(s){
		free(s);
		s=NULL;
	}

	if(SerialNumber.pbData){
		free(SerialNumber.pbData);
		SerialNumber.pbData=NULL;
	}

	// 去除空格
	trimString(sn, 32);
	// 颠倒字符串
	len = strlen(sn);
	
	for(i=0; i<len/2; i+=2){
		c0 = sn[i];
		c1 = sn[i+1];
		sn[i]=sn[len-i-2];
		sn[i+1]=sn[len-i-1];
		sn[len-i-2]=c0;
		sn[len-i-1]=c1;
	}
	return TRUE;
}

#else

static char *cert_serial( X509 *xs)
{
	char *result;
	BIO *bio;
	int n;

	if ((bio = BIO_new(BIO_s_mem())) == NULL)
		return NULL;
	i2a_ASN1_INTEGER(bio, X509_get_serialNumber(xs));
	n = BIO_pending(bio);
	result =(char *)malloc(n+1);
	n = BIO_read(bio, result, n);
	result[n] = 0;
	BIO_free(bio);
	return result;
}

//代码转换:从一种编码转为另一种编码
int code_convert(char *from_charset,char *to_charset,char *inbuf,int inlen,char *outbuf,int outlen)
{
	iconv_t cd;
	int rc;
	char **pin = &inbuf;
	char **pout = &outbuf;

	cd = iconv_open(to_charset,from_charset);
	if (cd==0) return 1;
	memset(outbuf,0,outlen);
	if (iconv(cd,pin,(size_t*)&inlen,pout,(size_t*)&outlen)==-1) return 1;
	iconv_close(cd);
	return 0;
}

/*
	从证书主题中读取CN项
	返回值: 1-正确 0-错误
*/
int getCnFromSubject(unsigned char *Subject,unsigned int Subjectlen,char *Name)
{
	int FindOK=0;
	unsigned char Search[]={0x06,0x03,0x55,0x04,0x03,0x14};
	unsigned char Search1[]={0x06,0x03,0x55,0x04,0x03,0x13};
	unsigned char Search2[]={0x06,0x03,0x55,0x04,0x03,0x0c};
	unsigned int i=0;
	for (i=0; i<Subjectlen-8; i++)
	{
		FindOK=1;
		for (unsigned int j=0; j< sizeof(Search); j++)
		{
			if (  (Subject[i+j] != Search[j]) && (Subject[i+j] != Search1[j])  && (Subject[i+j] != Search2[j]) )
			{
				FindOK=0;
				break;
			}
		}

		if ( FindOK == 1)
			break;
				 	
	}
		
	if (FindOK)
	{
		int NameLen = 0;
		NameLen = *(Subject+i+6);
		//编码转换，UTF-8-->UNICODE-->GBK
		unsigned short * UnicodeBuf = 0;
		int Unicode_Len = 0;
		unsigned char utf8[255];

		memset(utf8,'\0',255);
		memcpy(utf8,Subject+i+7,NameLen);
		//strcpy(*Name,(char*)utf8);
		int inlen =strlen((char*)utf8);
		code_convert("UTF-8","GBK",(char*)utf8,inlen,Name,255);
	}	
  return FindOK;
}

#endif

/*
获取证书序列号
输入1:pbData-证书字节流
输入2:dwCount-证书字节流长度
输出:SerialNumber
返回: 0-失败 1-成功
*/
int  key_getSerial(unsigned char *pbData,DWORD dwCount,char *SerialNumber)
{
	int ret=0;
	#ifdef WIN32
	PCCERT_CONTEXT   pCertContext;
	if   ((pCertContext=CertCreateCertificateContext(X509_ASN_ENCODING,pbData,dwCount)))  
	{
		ret=parseX509CertSerialNumber(pCertContext,SerialNumber);
	}
	if(pCertContext)
	{
		CertFreeCertificateContext(pCertContext);
	}

	#else
		//解析证书
	X509			*x=NULL;
	unsigned char	*p=NULL;
	char *tmp = NULL;
	/* 为DER编码的数字证书 
	*/	
	p=pbData;
	x=X509_new();
	d2i_X509(&x,(const unsigned char **)&p,dwCount);
	tmp = cert_serial(x);

	if(tmp != NULL){
		memcpy(SerialNumber,tmp,strlen(tmp));
		DebugLog("get SerialNumber :%s\n",SerialNumber);	
		ret = 1;
	}else{
		ErrorLog("serial is null,or get serial failed");
		return GETSERIALFAILED;
	}

	if(tmp != NULL){
		free(tmp);
		tmp = NULL;
	}
	
	X509_free(x);
	ret = 1;
	#endif	

	return ret;
}


/*
	从证书主体中读取Uid项
	返回值: 1-正确 0-错误
*/
int getUidFromSubject(unsigned char *Subject , unsigned int Subjectlen,char *Uid)
{

	int FindOK=0;
	unsigned char Search[]={0x06,0x0a,0x09,0x92,0x26,0x89,0x93,0xf2,0x2c,0x64,0x01,0x01};
	unsigned int i =0;

	for ( i=0; i<Subjectlen-14; i++){
		FindOK=1;
		for (unsigned int j=0; j< sizeof(Search); j++){

			if (  (Subject[i+j] != Search[j])  ){
				FindOK=0;
				break;
			}
		}

		if ( FindOK == 1){
			break;
		}			 	
	}

	if (FindOK){
		int UidLen = *(Subject+i+13);
		memset(Uid,'\0',255);
		memcpy(Uid,Subject+i+14,UidLen);
	}
  return FindOK;
}

/*
功能:
从证书主题中读取CN项
参数:
[in]	pContent:证书内容
[in]	nContentLen:证书内容长度
[out]	username:用户名
返回值: 0:成功  非0:相应错误码
*/
EXPORTDLL int WINAPIV key_getSubjectUserName(unsigned char* pContent,int nContentLen,char username[256])
{
	SysLog("进入到 key_getSubjectUserName\n");
	int ret=-1;
	long cbSize=0;
	if(pContent == NULL){
		SysLog("证书内容为空\n");
		ret = CERTISNULL;
		return ret;
	}
#ifdef WIN32

	PCCERT_CONTEXT   pCertContext = NULL;
	//解析待使用证书得到证书上下文
	pCertContext=CertCreateCertificateContext(X509_ASN_ENCODING,pContent,nContentLen);
	if(pCertContext){
		if(!(cbSize = CertGetNameStringA(   
            pCertContext,   
            CERT_NAME_SIMPLE_DISPLAY_TYPE,   
            0,
            NULL,   
            NULL,   
            0))){
			SysLog("获取证书用户名失败!\n");
			ret = GETUSERNAMEFAILED;	  
		}
		
		if(CertGetNameStringA(
            pCertContext,
            CERT_NAME_SIMPLE_DISPLAY_TYPE,
            0,
            NULL,
            username,
            cbSize)){
            SysLog("用户名为: %s\n", username);
			ret=0;
        }else{
			SysLog("获取证书用户名失败!\n");
			ret = GETUSERNAMEFAILED;	 
		}   
    }else{
		SysLog("证书上下文为空\n");
		ret=CERTCONTEXTISNULL;
	}
	if(pCertContext){
		SysLog("释放证书上下文\n");
		CertFreeCertificateContext(pCertContext);
		pCertContext = NULL;
	}

	#else
	//解析证书
	X509			*x=NULL;
	unsigned char	*p=NULL;

	/* 为DER编码的数字证书 
	*/	
	p=pContent;
	x=X509_new();
	d2i_X509(&x,(const unsigned char **)&p,nContentLen);
	memset(username,'\0',256);
	ret = getCnFromSubject((unsigned char *)x->cert_info->subject->bytes->data,x->cert_info->subject->bytes->length,username);
	//printf("%s\n",username);
	if(username)
	{
		DebugLog("user name is:%s\n",username);
		ret = 0;
	}else{
		ret = GETUSERNAMEFAILED;
	}
	X509_free(x);	
	#endif

	SysLog("退出 key_getSubjectUserName\n");
	return ret;
}


/*
功能: 
获取指定类型USB-KEY中用户签名证书的个数。

  参数: 
  [out] pCertNum
  当前插入的指定类型的USB-KEY中证书的个数。
  返回:
  0：成功  非0：相应错误码
  
	注释:
	该接口获取当前插入的指定类型的USB-KEY中用户签名证书的数量。调用该接口前，
	若调用SetDevice设定了插入的USB-KEY的类型，则该接口得到的是该指定类型的USB-KEY中签名证书的数量，
	否则为系统默认的USB-KEY类型的USB-KEY中签名证书的数量。	
 */
EXPORTDLL int WINAPIV key_getCertNum(int *pCertNum)
{
	key_enter_lock_area();
	SysLog("enter key_getCertNum\n");
	int ret = -1;

	/******************20121214 lch add****************************/
	ret = key_loadLibrary();
	if(ret != 0){
		goto end;
	}
	/******************20121214 lch add****************************/

	*pCertNum=G_CertNum;
	if(G_CertNum == 0){
		ret= GETCERTNUMFAILED;
	}else{
		ret=0;
	}

end:

	SysLog("exit key_getCertNum，certNum is:%d\n",G_CertNum);
	key_leave_lock_area();
	return ret;
}


/*
功能: 
获取指定索引的证书序列号。

参数:
	[in] nIndex 
	证书的索引，默认为-1，从0开始。
  
	[out] serial
	证书的序列号。
	
返回:
	0：成功  非0：相应错误码。
		
注释:
	该接口获取指定索引的用户签名证书的序列号。若不输索引，
则系统默认为当前插入的唯一的USB-KEY中的用户签名证书序列号。		  
 */
EXPORTDLL int WINAPIV key_getCertId (char serial[128] ,int nIndex)
{
	key_enter_lock_area();
	SysLog("enter key_getCertId\n");
	SysLog("nIndex is:%d\n",nIndex);
	int ret = -1;
	/******************20121214 lch add****************************/
	ret = key_loadLibrary();
	if(ret != 0){
		goto end;
	}
	/******************20121214 lch add****************************/

	if(nIndex==-1){
		if(G_DevNum==2){
#ifdef WIN32
			::MessageBox(NULL,"当输入参数为默认时，设备数量不唯一,请修改输入参数","paraError",MB_OK);
#else
			fprintf(stdout,"当输入参数为默认时，设备数量不唯一,请修改输入参数\n");
#endif
			SysLog("当参数index为-1时，设备数量不唯一\n");
			ret=PARAERROR;
		}else if(G_DevNum==1){
			memset(serial,'\0',128);
			memcpy(serial,keyInfo[0].CertSerialNo,strlen(keyInfo[0].CertSerialNo));
			if(keyInfo[0].CertSerialNo[0]!='\0'){
				SysLog("序列号为：%s\n",keyInfo[0].CertSerialNo);
				ret=0;
			}else{
				SysLog("序列号为空\n");
				ret=SERIALISNULL;
			}
		}else if(G_DevNum==0)
		{
			SysLog("设备数量为0\n");
			ret=DEVNUMISZERO;
		}
	}else{	
		if(G_CertNum==1){
			for(int i=0;i<G_DevNum;i++){
				if(keyInfo[i].CertSerialNo[0]!='\0'){
					SysLog("签名证书数量为1时，得到的证书序列号为：\n");	
					SysHexLog((unsigned char *)(keyInfo[i].CertSerialNo),strlen(keyInfo[i].CertSerialNo));
					SysLog("\n");
					memcpy(serial,keyInfo[i].CertSerialNo,strlen(keyInfo[i].CertSerialNo));
				}
			}
		}else if(G_CertNum==2){
			if(keyInfo[nIndex].CertSerialNo[0]!='\0'){
				SysLog("签名证书数量为2时，得到的证书序列号为：\n");
				SysHexLog((unsigned char *)(keyInfo[nIndex].CertSerialNo),strlen(keyInfo[nIndex].CertSerialNo));
				SysLog("\n");
				memcpy(serial,keyInfo[nIndex].CertSerialNo,strlen(keyInfo[nIndex].CertSerialNo));
			}else{
				SysLog("签名证书数量为2时，得到的证书序列号为空\n");
				memset(serial,'\0',128);
			}
		}else if(G_CertNum==0){
			SysLog("得到的签名证书数量为0\n");
			memset(serial,'\0',128);
		}
	}

end:

	SysLog("exit key_getCertId\n");
	key_leave_lock_area();
	return ret;
}


/*
功能: 
获取指定索引的证书中的用户名。

参数:
	[in] nIndex 
	证书的索引，默认为-1。
  
	[out] userName
	证书中的用户名。
	
返回:
	0：成功  非0：相应错误码。
	  
注释:
	该接口获取指定索引的用户签名证书中的用户名。若不输索引，则系统默认为当前插入的唯一的USB-KEY中的用户签名证书中的用户名。		
 */
EXPORTDLL int WINAPIV key_getUserName (char userName[256] ,int nIndex)
{
	key_enter_lock_area();
	SysLog("enter key_getUserName\n");
	SysLog("nIndex is:%d\n",nIndex);
	int ret = -1;
	/******************20121214 lch add****************************/
	ret = key_loadLibrary();
	if(ret != 0){
		goto end;
	}
	/******************20121214 lch add****************************/

	if(nIndex==-1){
		if(G_DevNum==2){
			SysLog("当参数index为-1时，设备数量不唯一\n");
			ret=PARAERROR;
		}else if(G_DevNum==1){
			memset(userName,'\0',256);
			memcpy(userName,keyInfo[0].UserName,strlen(keyInfo[0].UserName));
			if(keyInfo[0].UserName[0]!='\0'){
				SysLog("用户名为：%s\n",keyInfo[0].UserName);
			}else{
				SysLog("用户名为空\n");
				ret=USERNAMEISNULL;
			}
		}else if(G_DevNum==0){
			SysLog("设备数量为0\n");
			ret=DEVNUMISZERO;
		}
	}else{	
		if(G_UserNameNum==1){
			for(int i=0;i<G_DevNum;i++){
				if(keyInfo[i].UserName[0]!='\0'){
					SysLog("用户名数量为1时，得到的用户名为：%s\n",keyInfo[i].UserName);	
					memcpy(userName,keyInfo[i].UserName,strlen(keyInfo[i].UserName));
				}
			}
		}else if(G_UserNameNum==2){
			if(keyInfo[nIndex].UserName[0]!='\0'){
				SysLog("用户名数量为2时，得到的用户名为：%s\n",keyInfo[nIndex].UserName);
				memcpy(userName,keyInfo[nIndex].UserName,strlen(keyInfo[nIndex].UserName));
			}else{
				SysLog("用户名数量为2时，得到的用户名为空\n");
				memset(userName,'\0',USERNAMELEN);
			}
		}else if(G_UserNameNum==0){
			SysLog("得到的用户名数量为0\n");
			memset(userName,'\0',USERNAMELEN);
		}
	}

end:

	SysLog("exit key_getUserName\n");
	key_leave_lock_area();
	return ret;
}


/*
  功能: 
  获取指定索引的证书中的主题项。
  
	参数:
	[in] nIndex
	证书的索引，默认为-1。
	
	  [out] subject
	  证书中的主题项。
	  
		返回:
		0：成功  非0：相应错误码。
		
		  注释:
		  该接口获取指定索引的用户签名证书中的主题项。若不输索引，则系统默认为当前插入的唯一的USB-KEY中的用户签名证书中的主题项。		  
 */
EXPORTDLL int WINAPIV key_getSubject(char subject[256] ,int nIndex )
{
	key_enter_lock_area();
	SysLog("enter key_getSubject\n");
	SysLog("nIndex is:%d\n",nIndex);
	int ret = -1;
	/******************20121214 lch add****************************/
	ret = key_loadLibrary();
	if(ret != 0){
		goto end;
	}
	/******************20121214 lch add****************************/
	if(nIndex==-1){
		if(G_DevNum==2){
			SysLog("当参数index为-1时，设备数量不唯一\n");
			ret=PARAERROR;
		}else if(G_DevNum==1){
			memset(subject,'\0',256);
			memcpy(subject,keyInfo[0].Subject,strlen(keyInfo[0].Subject));
			if(keyInfo[0].Subject[0]!='\0'){
				SysLog("主题为：%s\n",keyInfo[0].Subject);
			}else{
				SysLog("主体为空\n");
				ret=SUBJECTISNULL;
			}
		}else if(G_DevNum==0){
			SysLog("设备数量为0\n");
			ret=DEVNUMISZERO;
		}
	}else{	
		if(G_SubjectNum==1){
			for(int i=0;i<G_DevNum;i++){
				if(keyInfo[i].Subject[0]!='\0'){
					SysLog("主题项数量为1时，得到的主题项为：%s\n",keyInfo[i].Subject);	
					memcpy(subject,keyInfo[i].Subject,strlen(keyInfo[i].Subject));
				}
			}
		}else if(G_SubjectNum==2){
			if(keyInfo[nIndex].Subject[0]!='\0'){
				SysLog("主题项数量为2时，得到的主题项为：%s\n",keyInfo[nIndex].Subject);
				memcpy(subject,keyInfo[nIndex].Subject,strlen(keyInfo[nIndex].Subject));
			}else{
				SysLog("主题项数量为2时，得到的主题项为空\n");
				memset(subject,'\0',SUBJECTLEN);
			}
		}else if(G_SubjectNum==0){
			SysLog("得到的主题项数量为0\n");
			memset(subject,'\0',SUBJECTLEN);
		}
	}

end:

	SysLog("exit key_getSubject\n");
	key_leave_lock_area();
	return ret;
}


/*
功能: 
获取指定索引的证书中的证书公钥。

参数:
[in] nIndex
证书的索引，默认为-1。

[out] publicKey
证书中的证书公钥。

返回:
  0：成功  非0：相应错误码。

注释:
 该接口获取指定索引的用户签名证书中的证书公钥。若不输索引，则系统默认为当前插入的唯一的USB-KEY中的用户签名证书中的证书公钥。
	
 */
EXPORTDLL int WINAPIV key_getPublicKey(char publicKey[256] ,int nIndex )
{
	key_enter_lock_area();
	SysLog("enter key_getPublicKey\n");
	SysLog("nIndex is:%d\n",nIndex);
	int ret = -1;
	/******************20121214 lch add****************************/
	ret = key_loadLibrary();
	if(ret != 0){
		goto end;
	}
	/******************20121214 lch add****************************/
	if(nIndex==-1){
		if(G_DevNum==2){
			SysLog("当参数index为-1时，设备数量不唯一\n");
			ret=PARAERROR;
		}else if(G_DevNum==1){
			memset(publicKey,'\0',256);
			memcpy(publicKey,keyInfo[0].Pubkey,keyInfo[0].nPubKeyLen);
			if(keyInfo[0].Pubkey[0]!='\0'){
				SysLog("公钥为：%s\n",keyInfo[0].Pubkey);
			}else{
				SysLog("公钥为空\n");
				ret=PUBKEYISNULL;
			}
		}else if(G_DevNum==0){
			SysLog("设备数量为0\n");
			ret=DEVNUMISZERO;
		}
	}else{	
		if(G_PubkeyNum==1){
			for(int i=0;i<G_DevNum;i++){
				if(keyInfo[i].Pubkey[0]!='\0'){
					SysLog("公钥数量为1时，得到的公钥为：\n");	
					SysHexLog((unsigned char *)(keyInfo[i].Pubkey),keyInfo[i].nPubKeyLen);
					SysLog("\n");
					memcpy(publicKey,keyInfo[i].Pubkey,keyInfo[i].nPubKeyLen);
				}
			}
		}else if(G_PubkeyNum==2){
			if(keyInfo[nIndex].Pubkey[0]!='\0'){
				SysLog("公钥数量为2时，得到的公钥为：\n");
				SysHexLog((unsigned char *)(keyInfo[nIndex].Pubkey),keyInfo[nIndex].nPubKeyLen);
				SysLog("\n");
				memcpy(publicKey,keyInfo[nIndex].Pubkey,keyInfo[nIndex].nPubKeyLen);
			}else{
				SysLog("公钥数量为2时，得到的公钥为空\n");
				memset(publicKey,'\0',PUBKEYLEN);
			}
		}else if(G_PubkeyNum==0){
			SysLog("得到的公钥数量为0\n");
			memset(publicKey,'\0',PUBKEYLEN);
		}
	}

end:

	SysLog("exit key_getPublicKey\n");
	key_leave_lock_area();
	return ret;
}


/*
功能: 
获取指定索引的证书中的发布者。

参数:
[in] nIndex
证书的索引，默认为-1。

[out] Issuer
证书中的发布者。

返回:
  0：成功  非0：相应错误码。

注释:
 该接口获取指定索引的用户签名证书中的发布者。若不输索引，则系统默认为当前插入的唯一的USB-KEY中的用户签名证书中的发布者。	
 */
EXPORTDLL int WINAPIV key_getIssuer(char Issuer[256] ,int nIndex)
{
	key_enter_lock_area();
	SysLog("enter key_getIssuer\n");
	SysLog("nIndex is:%d\n",nIndex);
	int ret = -1;
	/******************20121214 lch add****************************/
	ret = key_loadLibrary();
	if(ret != 0){
		goto end;
	}
	/******************20121214 lch add****************************/
	if(nIndex==-1){
		if(G_DevNum==2){
			SysLog("当参数index为-1时，设备数量不唯一\n");
			ret=PARAERROR;
		}else if(G_DevNum==1){
			memset(Issuer,'\0',256);
			memcpy(Issuer,keyInfo[0].Issuer,strlen(keyInfo[0].Issuer));
			if(keyInfo[0].Issuer[0]!='\0'){
				SysLog("发行者为：%s\n",keyInfo[0].Issuer);
			}else{
				SysLog("发行者为空\n");
				ret=ISSUERISNULL;
			}
		}else if(G_DevNum==0){
			SysLog("设备数量为0\n");
			ret=DEVNUMISZERO;
		}
	}else{	
		if(G_IssuerNum == 1){
			for(int i=0;i<G_DevNum;i++){
				if(keyInfo[i].Issuer[0]!='\0'){
					SysLog("发行者数量为1时，得到的发行者为：%s\n",keyInfo[i].Issuer);	
					memcpy(Issuer,keyInfo[i].Issuer,strlen(keyInfo[i].Issuer));
				}
			}
		}else if(G_IssuerNum == 2){
			if(keyInfo[nIndex].Issuer[0]!='\0'){
				SysLog("发行者数量为2时，得到的发行者为：%s\n",keyInfo[nIndex].Issuer);
				memcpy(Issuer,keyInfo[nIndex].Issuer,strlen(keyInfo[nIndex].Issuer));
			}else{
				SysLog("发行者数量为2时，得到的发行者为空\n");
				memset(Issuer,'\0',ISSUERLEN);
			}
		}else if(G_IssuerNum == 0){
			SysLog("得到的发行者数量为0\n");
			memset(Issuer,'\0',ISSUERLEN);
		}
	}

end:

	SysLog("exit key_getIssuer\n");
	key_leave_lock_area();
	return ret;
}


/*
功能: 
获取指定索引的证书的有效期。

参数:
[in] nIndex
证书的索引，默认为-1。

[out] pnDays
证书的有效期。

返回:
  0：成功  非0：相应错误码。

注释:
 该接口获取指定索引的用户签名证书的有效期。若不输索引，则系统默认为当前插入的唯一的USB-KEY中的用户签名证书的有效期。	
 */
EXPORTDLL int WINAPIV key_getValidDays (int *pnDays ,int nIndex)
{
	key_enter_lock_area();
	SysLog("enter key_getValidDays\n");
	SysLog("nIndex is:%d\n",nIndex);
	int ret = -1;
	/******************20121214 lch add****************************/
	ret = key_loadLibrary();
	if(ret != 0){
		goto end;
	}
	/******************20121214 lch add****************************/
	if(nIndex==-1){
		if(G_DevNum==2){
			SysLog("当参数index为-1时，设备数量不唯一\n");
			ret=PARAERROR;
		}else if(G_DevNum==1){
			*pnDays=keyInfo[0].nValidDays;
		}else if(G_DevNum==0){
			SysLog("设备数量为0\n");
			ret=DEVNUMISZERO;
		}
	}else{	
		if(G_VaildDayNum==1){
			for(int i=0;i<G_DevNum;i++){
				if(keyInfo[i].nValidDays!=0)
				*pnDays=keyInfo[i].nValidDays;
			}
		}else if(G_VaildDayNum==2){
			*pnDays=keyInfo[nIndex].nValidDays;	
		}else if(G_VaildDayNum==0){
			SysLog("得到的有效期数量为0\n");
		}
	}

end:
	SysLog("exit key_getValidDays\n");
	key_leave_lock_area();
	return ret;
}


/*
功能: 
获取指定索引的证书的到期日。

参数:
[in] nIndex
证书的索引，默认为-1。

[out] pnDays
证书的到期日。

返回:
  0：成功  非0：相应错误码。

注释:
 该接口获取指定索引的用户签名证书的有效期。若不输索引，则系统默认为当前插入的唯一的USB-KEY中的用户签名证书的到期日。	
 */
EXPORTDLL int WINAPIV key_getEndDays (char EndDays[ENDDAYLEN] ,int nIndex)
{
	key_enter_lock_area();
	SysLog("enter key_getEndDays\n");
	SysLog("nIndex is:%d\n",nIndex);
	int ret = -1;
	/******************20121214 lch add****************************/
	ret = key_loadLibrary();
	if(ret != 0){
		goto end;
	}
	/******************20121214 lch add****************************/

	if(nIndex==-1){
		if(G_DevNum==2){
			SysLog("当参数index为-1时，设备数量不唯一\n");
			ret=PARAERROR;
		}else if(G_DevNum==1){
			memset(EndDays,'\0',ENDDAYLEN);
			memcpy(EndDays,keyInfo[0].EndDays,strlen(keyInfo[0].EndDays));
			if(keyInfo[0].EndDays[0]!='\0'){
				SysLog("到期日为：%s\n",keyInfo[0].EndDays);
			}else{
				SysLog("到期日为空\n");
				ret=ENDDAYISNULL;
			}
		}else if(G_DevNum==0){
			SysLog("设备数量为0\n");
			ret=DEVNUMISZERO;
		}
	}else{	
		if(G_EndDayNum==1){
			for(int i=0;i<G_DevNum;i++){
				if(keyInfo[i].EndDays[0]!='\0'){
					SysLog("到期日数量为1时，得到的到期日为：%s\n",keyInfo[i].EndDays);	
					memcpy(EndDays,keyInfo[i].EndDays,strlen(keyInfo[i].EndDays));
				}
			}
		}else if(G_EndDayNum==2){
			if(keyInfo[nIndex].EndDays[0]!='\0'){
				SysLog("到期日数量为2时，得到的到期日为：%s\n",keyInfo[nIndex].EndDays);
				memcpy(EndDays,keyInfo[nIndex].EndDays,strlen(keyInfo[nIndex].EndDays));
			}else{
				SysLog("到期日数量为2时，得到的到期日为空\n");
				memset(EndDays,'\0',ENDDAYLEN);
			}
		}else if(G_EndDayNum==0){
			SysLog("得到的到期日数量为0\n");
			memset(EndDays,'\0',ENDDAYLEN);
		}
	}

end:
	SysLog("exit key_getEndDays\n");
	key_leave_lock_area();
	return ret;
}



/*
功能: 
获取指定索引的证书属性。

参数:
[in] nIndex
证书的索引，默认为-1。

[in] pOid
        证书属性pOid。

[in-out]pAttributeValue
        属性值。

 [in-out]pnAttributeLen
        属性值的长度。

返回:
  0：成功  非0：相应错误码。

注释:
 该接口获取指定索引的用户签名证书的指定扩展项的属性。若不输索引，则系统默认为当前插入的唯一的USB-KEY中的用户签名证书的指定扩展项的属性。此接口为二次调用接口,设置pAttributeValue为NULL,得到属性的长度。再为pnAttributeValue分配相应大小的内存。	
 */
EXPORTDLL int WINAPIV key_getAttribute(char *pOid,unsigned char* pAttributeValue, unsigned long *pnAttributeLen ,int nIndex)
{
	key_enter_lock_area();
	SysLog("enter key_getAttribute\n");
	SysLog("nIndex is:%d\n",nIndex);
	int nCount=0;
	long cbSize=0;
	int i=0;
	int ret = -1;

	/******************20121214 lch add****************************/
	ret = key_loadLibrary();
	if(ret != 0){
		goto end;
	}
	/******************20121214 lch add****************************/
	
	SysLog("设备数量为：%d\n",G_DevNum);
#ifdef WIN32

	PCCERT_CONTEXT   pCertContext = NULL;

	if(nIndex == -1){
		if(G_DevNum == 2){
			SysLog("当参数index为-1时，设备数量不唯一\n");
			ret=PARAERROR;
		}else if(G_DevNum==1){
			pCertContext=CertCreateCertificateContext(X509_ASN_ENCODING,(const unsigned char*)(keyInfo[0].Cert),keyInfo[0].nCertLen);
			if(pCertContext){
				for(unsigned int i=0;i<pCertContext->pCertInfo->cExtension;i++){
					if((ret=strcmp(pCertContext->pCertInfo->rgExtension[i].pszObjId,pOid))==0){
						if (pAttributeValue == NULL)
							*pnAttributeLen=pCertContext->pCertInfo->rgExtension[i].Value.cbData;
						else{
							ret=0;
							*pnAttributeLen=pCertContext->pCertInfo->rgExtension[i].Value.cbData;
							memset(pAttributeValue,'\0',*pnAttributeLen);
							memcpy(pAttributeValue,pCertContext->pCertInfo->rgExtension[i].Value.pbData,*pnAttributeLen);
							SysLog("获得证书属性，为：\n");
							SysHexLog(pAttributeValue,*pnAttributeLen);
						}	
						break;
					}
				}

				CertFreeCertificateContext(pCertContext);
				pCertContext = NULL;
			}
		}else if(G_DevNum==0){
			SysLog("设备数量为0\n");
			ret=DEVNUMISZERO;
		}
	}else{
		if(G_DevNum==1){
			pCertContext=CertCreateCertificateContext(X509_ASN_ENCODING,(const unsigned char*)(keyInfo[0].Cert),keyInfo[0].nCertLen);
			if(pCertContext){
				for(unsigned int i=0;i<pCertContext->pCertInfo->cExtension;i++){
					if((ret=strcmp(pCertContext->pCertInfo->rgExtension[i].pszObjId,pOid))==0){
						if (pAttributeValue == NULL)
							*pnAttributeLen=pCertContext->pCertInfo->rgExtension[i].Value.cbData;
						else{
							ret=0;
							*pnAttributeLen=pCertContext->pCertInfo->rgExtension[i].Value.cbData;
							memset(pAttributeValue,'\0',*pnAttributeLen);
							memcpy(pAttributeValue,pCertContext->pCertInfo->rgExtension[i].Value.pbData,*pnAttributeLen);
							SysLog("获得证书属性，为：\n");
							SysHexLog(pAttributeValue,*pnAttributeLen);
						}	
						break;
					}
				}
				CertFreeCertificateContext(pCertContext);
				pCertContext = NULL;
			}
		}
		//根据不同的设备数量进行不同的操作
		if(G_DevNum==2){		
			pCertContext=CertCreateCertificateContext(X509_ASN_ENCODING,(const unsigned char*)(keyInfo[nIndex].Cert),keyInfo[nIndex].nCertLen);
			if(pCertContext){
				for(unsigned int i=0;i<pCertContext->pCertInfo->cExtension;i++){
					if((ret=strcmp(pCertContext->pCertInfo->rgExtension[i].pszObjId,pOid))==0){
						if (pAttributeValue == NULL)
							*pnAttributeLen=pCertContext->pCertInfo->rgExtension[i].Value.cbData;
						else{
							ret=0;
							*pnAttributeLen=pCertContext->pCertInfo->rgExtension[i].Value.cbData;
							memset(pAttributeValue,'\0',*pnAttributeLen);
							memcpy(pAttributeValue,pCertContext->pCertInfo->rgExtension[i].Value.pbData,*pnAttributeLen);
							SysLog("获得证书属性，为：\n");
							SysHexLog(pAttributeValue,*pnAttributeLen);
						}	
						break;
					}
				}

				CertFreeCertificateContext(pCertContext);
				pCertContext = NULL;
			}
		}
	}

#else

#endif

end:

	SysLog("exit key_getAttribute\n");
	key_leave_lock_area();
	return ret;
}


/*
功能: 
生成随机数。

参数:
[in] nIndex
证书的索引，默认为-1。

[in] nLen
产生的随机数的长度。

[out] ranDom
产生的随机数。
返回:
0：成功  非0：相应错误码。

注释:
该接口用于产生指定长度的随机数。传入ranDom参数前为ranDom分配nLen的大小。
*/
EXPORTDLL int WINAPIV key_generateRandom(unsigned char *ranDom, unsigned int nLen,int nIndex)
{
	key_enter_lock_area();
	SysLog("enter key_generateRandom \n");
	KEYVALUE Random;
	int count=nLen/16;
	int left=nLen%16;
	int i=0;
	int j = 0;
	int ret = -1;

	Random.pbData = NULL;
	Random.cbData = 0;

	/******************20121214 lch add****************************/
	ret = key_loadLibrary();
	if(ret != 0){
		goto end;
	}
	/******************20121214 lch add****************************/

	SysLog("传进来的长度为：%d\n",nLen);
	//key处理流程
	if(pGenRandom == NULL){
		SysLog("pGenRandom is NULL\n");
		ret = POINTERISNULL;
		goto end;
	}
	for( i=0;i<count;i++){
		Random.cbData=16;
		Random.pbData=(unsigned char *)malloc(16*sizeof(unsigned char));		
		ret=pGenRandom(&Random);
		if(ret==0){
			for(j = 0;j<16;j++){
				if(Random.pbData[j] == 0x00)
					Random.pbData[j] = 0x0a;
			}
			memcpy(ranDom+i*16,Random.pbData,16);
		}else{
			SysLog("第%d次产生随机数失败,错误码为：%.2x\n",i+1,ret);
			ret=GENRANDOMFAILED;
		}
		key_freeBuffer((void **)&(Random.pbData));
	}
	if(left!=0){
		Random.cbData=left;
		Random.pbData=(unsigned char *)malloc(left*sizeof(unsigned char));		
		ret=pGenRandom(&Random);
		if(ret==0){
			for(j = 0;j<left;j++){
				if(Random.pbData[j] == 0x00)
					Random.pbData[j] = 0x0a;
			}
			memcpy(ranDom+i*16,Random.pbData,left);
		}else{
			SysLog("产生随机数失败,错误码为：%.2x\n",ret);
			ret=GENRANDOMFAILED;
		}

		key_freeBuffer((void **)&(Random.pbData));
	}
	if(ranDom){
		SysLog("产生的随机数为：\n");
		SysHexLog(ranDom,nLen);
	}

end:

	SysLog("exit key_generateRandom \n");
	key_leave_lock_area();
	return ret;
}


/*
功能: 
设置当前操作的USB-KEY。

参数:
[in] szSerialNo
证书序列号。
返回:
0：成功  非0：相应错误码。

注释:
该接口设置当前操作的USB-KEY。若应用场景为单KEY的情况，则无需调用此接口。	
 */
EXPORTDLL int WINAPIV key_setNowDeviceId(unsigned char* szSerialNo)
{
	key_enter_lock_area();
	int ret=-1,	i = 0, flag = 0;
	char *p=NULL;
	char *q=NULL;
	SysLog("enter key_setNowDeviceId \n");

	/******************20121214 lch add****************************/
	ret = key_loadLibrary();
	if(ret != 0){
		goto end;
	}
	/******************20121214 lch add****************************/
	
	if(*szSerialNo!=NULL){
		SysLog("传进来的证书序列号为：%s\n",szSerialNo);
	}else{
		SysLog("传进来的序列号为空\n");
		ret = PARAERROR;
		goto end;
	}	

	if(G_DevNum==2){
		//当SerialNo不为""时
		if(*szSerialNo!=NULL){		
			for( i=0;i<G_DevNum;i++){
				p=(char *)szSerialNo;
				//序列号不为空
				if(keyInfo[i].CertSerialNo[0]!='\0'){
					q=keyInfo[i].CertSerialNo;
					while((*p!=NULL)&&(*q!=NULL)){
						if(*p==*q){
							flag=1;
							p++;
							q++;
						}else{
							flag=0;
							break;
						}
					}
					if(flag==1){
						SysLog("key_setNowDeviceId 中当传进来的序列号不空时，当前操作的设备句柄为：%d\n",keyInfo[i].keyHandle);
						if(pSetDevice){
							pSetDevice(keyInfo[i].keyHandle);//调用底层的设置设备接口
							SysLog("-----------------------当前的证书序列号为：%s\n",keyInfo[i].CertSerialNo);
							SysLog("当传进来的序列号不为空时，设置当前操作的设备成功，退出 key_setNowDeviceId 函数。\n");
							ret = 0;
							goto end;
						}else{
							ret=POINTERISNULL;
							goto end;
						}		
					}
				}
			}	
			SysLog("当传进来的序列号不为空时，当前操作的设备中不存在你要设置的设备, 退出 key_setNowDeviceId\n");
			ret = SETDEVICEFAILED;
			goto end;
		}else{
			//当SerialNo为""时
			for(int i=0;i<G_DevNum;i++){
				if(keyInfo[i].CertSerialNo[0]=='\0'){
					SysLog("key_setNowDeviceId 中当传进来的序列号空时，当前操作的设备句柄为：%d\n",keyInfo[i].keyHandle);
					pSetDevice(keyInfo[i].keyHandle);//调用底层的设置设备接口
					SysLog("当传进来的序列号为空时，设置当前操作的设备成功，退出 key_setNowDeviceId 函数。\n");
					ret = 0;
					goto end;
				}else if(keyInfo[i].CertSerialNo[0]!='\0'){
					SysLog("当传进来的序列号为空时，这时保存的设备信息结构体中序列号不为空的设备序列号为:%s\n",keyInfo[i].CertSerialNo);
					SysLog("当传进来的序列号为空时，这时保存的设备信息结构体中序列号不为空的设备句柄为：%d\n",keyInfo[i].keyHandle);
				}
			}
			SysLog("当传进来的序列号为空时，当前操作的设备中不存在你要设置的设备，退出 key_setNowDeviceId\n");
			ret = SETDEVICEFAILED;
			goto end;
		}
	}else if(G_DevNum==1){
		SysLog("设备数量为1，无需设置\n");
		ret=0;
	}

end:

	SysLog("exit key_setNowDeviceId \n");
	key_leave_lock_area();
	return ret;
}


int  sk_setPin(char *szOld, char *szNew){
	int ret = -1;
	
	if(G_pSK_SetPin == NULL){
		SysLog("G_pSK_SetPin is null\n");
		return POINTERISNULL;
	}

	ret = G_pSK_SetPin(szOld,szNew);
	return ret;
}


/*
功能:
修改用户口令。

参数:
[in] szOld
旧口令。
[in] szNew 
新口令。
	
返回:
0：成功  非0：相应错误码	

注释：
口令必须>=6位
 */
EXPORTDLL int WINAPIV key_setPin(char *szOld, char *szNew  )
{
	key_enter_lock_area();
	SysLog("enter key_setPin\n");
	int ret = -1;
	/******************20121214 lch add****************************/
	ret = key_loadLibrary();
	if(ret != 0){
		goto end;
	}
	/******************20121214 lch add****************************/

	if(G_NowDev == SK311){
		ret = sk_setPin(szOld,szNew);
		goto end;
	}

	if((pLogin == NULL) || (pSetPin == NULL)){
		SysLog("pSetPin is NULL\n");
		ret = POINTERISNULL;
		goto end;
	}

	ret = pLogin(1,szOld);
	if(ret == 0){
		SysLog("登录成功\n");

		ret = pSetPin(1,szNew);
		if(ret!=0){
			SysLog("修改pin码失败，错误码为：%.2x\n",ret);
			ret=SETPINFAILED;
		}else{
			SysLog("修改pin码成功\n");
		}
	}else{
		SysLog("登录失败，错误码为：%.2x\n",ret);
		ret=LOGINFAILED;
	}

end:

	SysLog("exit key_setPin\n");
	key_leave_lock_area();
	return ret;
}

int  sk_login(const char *pin,int *pLeftCount)
{
	int ret = -1;
	unsigned int tryTimes = 0;
	if(G_pSK_Login == NULL){
		SysLog("G_pSK_Login is null\n");
		return POINTERISNULL;
	}

	ret = G_pSK_Login(pin,&tryTimes);
	*pLeftCount = tryTimes;

	return ret;
}

/*
功能:
登陆USB-KEY。

参数:
[out] pLeftCount
剩余尝试次数。
  
返回:
0：成功  非0：相应错误码
	
注释:
该接口用于登陆USB-KEY。	  
 */
EXPORTDLL int WINAPIV key_login(const char *pin,int *pLeftCount)
{
	key_enter_lock_area();
	SysLog("enter key_login\n");
	int ret = -1;
	long leftCount=0;

	/******************20121214 lch add****************************/
	ret = key_loadLibrary();
	if(ret != 0){
		goto end;
	}
	/******************20121214 lch add****************************/

	if(G_NowDev == SK311){
		ret = sk_login(pin,pLeftCount);
		goto end;
	}

	if((pLogin == NULL) || (pGetDeviceStatus == NULL)){
		SysLog("pointer is NULL\n");
		ret = POINTERISNULL;
		goto end;
	}

	ret=pLogin(1,(char *)pin);
	if(ret==0){
		SysLog("登录成功\n");
	}else{
		SysLog("登录失败，错误码为:%.2x\n",ret);
		ret=LOGINFAILED;

		//获取剩余次数
		ret=pGetDeviceStatus(3,&leftCount);
		if(ret==0){
			SysLog("得到设备状态成功，剩余登录次数为：%d\n",leftCount);
			*pLeftCount=leftCount;
			ret=LOGINFAILED;
		}else{
			SysLog("得到设备状态失败，错误码为：%.2x\n",ret);
			ret=GETDEVSTATUSFAILED;
		}
	}

end:

	SysLog("exit key_login\n");
	key_leave_lock_area();
	return ret;
}


/*
功能:
向设备中写入数据

参数:
[in] szInfoName
写入数据在设备中的存储标识。
[in] pContent
写入的数据的内容。  
[in] pContentLen
写入的数据内容的长度。
		
返回:
0：成功  非0：相应错误码
		  
注释:
为存储标识pInfoName分配32个字节。			
 */
EXPORTDLL int WINAPIV key_writeData(const char* szInfoName,unsigned char *pContent,unsigned long pContentLen)
{
	key_enter_lock_area();
	SysLog("enter key_writeData\n");
	int ret = -1,j = 0, num = 0, i = 0;
	int fileNameLen=strlen(szInfoName);
	int		fragmentCount=pContentLen/CACHETSEGLEN; //文件分段数量
	int		left=pContentLen%CACHETSEGLEN; //文件剩余部分长度
	char buffer[10]={'\0'};
	unsigned char lbl[2048]={'\0'};
	char label[LABELEN]={'\0'};

	/******************20121214 lch add****************************/
	ret = key_loadLibrary();
	if(ret != 0){
		goto end;
	}
	/******************20121214 lch add****************************/

	strcpy(label,szInfoName);
	if (pImportFile == NULL){
		SysLog("pImportFile 指针为空\n");
		ret = POINTERISNULL;
		goto end;
	}

	if(pDeleteFileEx == NULL){
		SysLog("pDeleteFileEx 指针为空\n");
		ret = POINTERISNULL;
		goto end;
	}

	if(label[0]!='\0'){
		SysLog("文件名称为：\n");
		SysHexLog((unsigned char*)label,strlen(label));
	}else{
		SysLog("文件名为空\n");
		ret=PARAERROR;
		goto end;
	}
	if(pContent){
		SysLog("传进来的内容为：\n");
		SysHexLog(pContent,pContentLen);
	}else{
		SysLog("要写入的内容为空\n");
		ret=PARAERROR;
		goto end;
	}

	//读取文件列表
	memset(lbl,0x00,2048);
	if(pReadLabelList){
		ret=pReadLabelList(LBL_CERT,NULL,&num);
		if(ret==0){
			SysLog("读取ReadLabelList，得到的证书数量为：%d\n",num);
			ret = pReadLabelList(LBL_CERT, lbl, &num);
			if( ret != 0 ){
				SysLog("第二次读取证书列表错误！\n");
				SysLog("错误编号为：%.2x\n",ret);
				ret=READLABELLISTFAILED;
				goto end;
			}
		}else{
			SysLog("第一次调用ReadLabelList失败！\n");
			SysLog("错误码为：%.2x\n",ret);
			ret=READLABELLISTFAILED;
			goto end;
		}
	}else{
		SysLog("pReadLabelList 指针为空\n");
		ret=POINTERISNULL;
		goto end;
	}
	
	//删除此文件
	for( i=0; i<2047; i++){
		if(lbl[i] == 0x00){
			SysLog("文件标签为：\n");
			SysHexLog(lbl+j,strlen((const char*)lbl+j));
			if(strncmp((const char*)lbl+j,szInfoName,fileNameLen) == 0){						
				ret =  pDeleteFileEx (lbl+j, LBL_CERT);
				if( ret != 0 ){
					SysLog("DeleteFileEx失败\n");
					SysLog("错误码为：%.2x\n",ret);
					ret=DELETEFILEFAILED;
					goto end;
				}else{
					SysLog("删除成功\n");
				}
			}

			j = i+1;
			if(lbl[j] == 0x00){
				break;
			}
		}
	}
	
	//文件很长时，分段写入
	for( i=0;i<fragmentCount;i++){
		if(i!=0){
#ifdef WIN32
			itoa(i,buffer,10);
#else
			sprintf(buffer,"%d",i);
#endif
			strcat(label,buffer);
		}
		if(label[0]!='\0'){
			SysLog("要写入的文件标签为：\n");
			SysHexLog((unsigned char*)label,strlen(label));
		}

		ret = pImportFile((BYTE*)label,LBL_CERT,pContent+i*CACHETSEGLEN,CACHETSEGLEN);
		if (ret ==0 ){
			SysLog("第%d次导入成功\n",i);
		}else{
			SysLog("导入失败,错误码为：%.2x\n",ret);
			ret=WRITEDATAFAILED;
			goto end;
		}
		memset(buffer,'\0',10);
		memset(label,'\0',LABELEN);
		strcpy(label,szInfoName);
	}
	if(i!=0){
#ifdef WIN32
		itoa(i,buffer,10);
#else
		sprintf(buffer,"%d",i);
#endif
		strcat(label,buffer);
	}
	if(label[0]!='\0'){
		SysLog("要写入的文件标签为：\n");
		SysHexLog((unsigned char*)label,strlen(label));
	}

	//将最后一段数据写入
	ret = pImportFile((BYTE*)label,LBL_CERT,pContent+fragmentCount*CACHETSEGLEN,left);
	if (ret ==0 ){
		SysLog("导入成功\n");
	}else{
		SysLog("导入失败,错误码为：%.2x\n",ret);
		ret=WRITEDATAFAILED;
	}
	
end:

	SysLog("exit key_writeData\n");
	key_leave_lock_area();
	return ret;
}

 int  key_sk_readData(const char *szInfoName,unsigned char *pContent,unsigned long *pContentLen)
{
	int ret = -1;

	if(G_pSK_ReadData == NULL){
		SysLog("pointer G_pSK_ReadData is null\n");
		return POINTERISNULL;
	}

	if(szInfoName == NULL){
		SysLog("para szInfoName is null\n");
		return PARAERROR;
	}

	if(pContent == NULL){
		ret = G_pSK_ReadData(szInfoName,NULL,pContentLen);

		SysLog("SK_ReadData return value is:%.2x\n",ret);
		return ret;
	}else{
		memset(pContent,'\0',*pContentLen);
		ret = G_pSK_ReadData(szInfoName,pContent,pContentLen);
		SysLog("second call SK_ReadData return value is:%.2x\n",ret);
	}
	
	return ret;
}

/*
功能:
从设备中读取数据。

参数:
[in] szInfoName
要获取的数据在设备中的存储标识。
[in-out] pContent
数据的内容。
[in-out] pContentLen
数据的长度。
	  
返回:
0：成功  非0：相应错误码
		
注释:
本接口为二次调用接口,设置pContent为NULL,pContentLen为返回的数据的长度,再根据返回的长度为pContent分配内存。		  
 */
EXPORTDLL int WINAPIV key_readData(const char *szInfoName,unsigned char *pContent,unsigned long *pContentLen)
{
	key_enter_lock_area();
	SysLog("enter key_readData\n");
	int ret = -1;	
	int  j = 0,	i = 0,	 num = 0;
	char label[LABELEN]={'\0'};
	unsigned char *segContent=NULL; //分段内容
	unsigned long contentLen=0;  //文件总长度
	int tmpLen=0;  //分段长度
	int fileNameLen = 0;
	int		fragmentCount=0; //文件分段数量
	int		left=0; //文件剩余部分长度
	char buffer[10]={'\0'};
	unsigned char lbl[2048]={'\0'};

	/******************20121214 lch add****************************/
	ret = key_loadLibrary();
	if(ret != 0){
		goto end;
	}
	/******************20121214 lch add****************************/

	if(G_NowDev == SK311){
		return key_sk_readData(szInfoName,pContent,pContentLen);
	}

	if (pExportFile == NULL){
		SysLog("pExportFile 指针为空\n");
		ret = POINTERISNULL;
		goto end;
	}

	if(pReadLabelList == NULL){
		SysLog("pReadLabelList 指针为空\n");
		ret = POINTERISNULL;
		goto end;
	}

	if(szInfoName == NULL){
		SysLog("file name is NULL\n");
		ret = PARAERROR;
		goto end;
	}
	fileNameLen = strlen(szInfoName);
	strcpy(label,szInfoName);

	memset(lbl,0x00,2048);
	SysLog("filename is:%s, filename length is:%d\n",szInfoName,fileNameLen);

	//先读一下，看看是否为单个文件还是被分段的文件
	ret = pExportFile((unsigned char*)szInfoName,LBL_CERT,NULL,&tmpLen);
	if (ret != 0 ){
		SysLog("导出文件失败,错误码为：%.2x,%d\n",ret,ret);
		ret=READDATAFAILED;
		goto end;
	}
	//单个文件
	if(tmpLen < CACHETSEGLEN){
		if(pContent == NULL){
			contentLen += tmpLen;
			SysLog("总长度为：%d\n",contentLen);
			*pContentLen = tmpLen;
			goto end;
		}else{
			segContent=(unsigned char*)malloc(tmpLen*sizeof(unsigned char));
			memset(segContent,'\0',tmpLen);
			ret = pExportFile((unsigned char*)szInfoName,LBL_CERT,segContent,&tmpLen);
			if(ret == 0){
				SysLog("导出成功,内容为：\n",i);
				SysHexLog(segContent,tmpLen);
				memcpy(pContent,segContent,tmpLen);
				*pContentLen = tmpLen;
			}else{
				SysLog("导出文件失败,错误码为：%.2x\n",ret);
				ret=READDATAFAILED;
			}
			goto end;
		}
	}
	
	tmpLen = 0;
	ret=pReadLabelList(LBL_CERT,NULL,&num);
	if(ret==0){
		SysLog("读取ReadLabelList，得到的证书数量为：%d\n",num);
		ret = pReadLabelList(LBL_CERT, lbl, &num);
		if( ret != 0 ){
			SysLog("第二次读取证书列表错误！\n");
			SysLog("错误编号为：%x\n",ret);
			ret=READLABELLISTFAILED;
			goto end;
		}
	}else{
		SysLog("第一次调用ReadLabelList失败！\n");
		SysLog("错误码为：%x\n",ret);
		ret=READLABELLISTFAILED;
		goto end;
	}		
	
	for( i=0; i<2048; i++){
		if(lbl[i] == 0x00){
			if(strncmp((const char*)lbl+j,szInfoName,fileNameLen) == 0){						
				fragmentCount++;
			}					
			j = i+1;
			if(j == 2048)
				break;
			if(lbl[j] == 0x00){
				break;
			}
		}
	}
	SysLog("分段数量为：%d\n",fragmentCount);
	if(pContent == NULL){
		for( i=0;i<fragmentCount;i++){
			if(i!=0){
#ifdef WIN32
				itoa(i,buffer,10);
#else
				sprintf(buffer,"%d",i);
#endif
				strcat(label,buffer);
			}
			if(label[0]!='\0'){
				SysLog("文件标签为:\n");
				SysHexLog((unsigned char*)label,strlen(label));
			}
			ret = pExportFile((unsigned char*)label,LBL_CERT,NULL,&tmpLen);
			if (ret ==0 ){
				SysLog("第%d次导出成功，长度为：%d\n",i,tmpLen);
				contentLen+=tmpLen;
			}else{
				SysLog("导出文件失败,错误码为：%.2x\n",ret);
				ret=READDATAFAILED;
				goto end;
			}
			memset(buffer,'\0',10);
			memset(label,'\0',LABELEN);
			strcpy(label,szInfoName);
		}
		SysLog("总长度为：%d\n",contentLen);
		*pContentLen = contentLen;
		goto end;
	}else {
		memset(pContent,'\0',*pContentLen);
		for( i=0;i<fragmentCount;i++){
			if(i!=0){
#ifdef WIN32
				itoa(i,buffer,10);
#else
				sprintf(buffer,"%d",i);
#endif
				strcat(label,buffer);
			}
			SysLog("标签为：\n");
			SysHexLog((unsigned char*)label,strlen(label));

			ret = pExportFile((unsigned char*)label,LBL_CERT,NULL,&tmpLen);
			if (ret !=0 ){
				SysLog("第一次调用导出文件失败,错误码为：%.2x\n",ret);
				ret=READDATAFAILED;
				goto end;
			}
			SysLog("第%d次导出成功，长度为：%d\n",i,tmpLen);
			contentLen+=tmpLen;
			segContent=(unsigned char*)malloc(tmpLen*sizeof(unsigned char));
			memset(segContent,'\0',tmpLen);
			ret = pExportFile((unsigned char*)label,LBL_CERT,segContent,&tmpLen);
			if(ret==0){
				SysLog("第%d次导出成功,内容为：\n",i);
				SysHexLog(segContent,tmpLen);
				memcpy(pContent+i*CACHETSEGLEN,segContent,tmpLen);
			}else{
				SysLog("第二次调用导出文件失败,错误码为：%.2x\n",ret);
				ret=READDATAFAILED;
			}

			key_freeBuffer((void **)&segContent);
			tmpLen=0;
			memset(label,'\0',LABELEN);
			strcpy(label,szInfoName);
		}
		*pContentLen = contentLen;
	}

end:

	key_freeBuffer((void **)&segContent);
	SysLog("exit key_readData\n");
	key_leave_lock_area();
	return ret;
}


/*
   摘要数据处理
   参数1:from--输入数据原文
   参数2:fromlen--输入数据原文长度
   参数3:to--输出数据
   参数4:tolen--输出数据长度
   参数5:type--输入类型 1:SCH2算法 2:ms2-49算法 
*/
int KeyDigest(unsigned char *from, int fromlen,unsigned char *to,unsigned long *tolen,int type)
{
	SysLog("enter KeyDigest\n");
	int state=0;
	if(pDigest){
		switch (type){	
		case 1://SCH2算法
			if (to == NULL){
				*tolen=32;
				return state;
			}
			state = pDigest(to,tolen,from,fromlen,CALG_SCH2);
			break;
			
		case 2://ms2-49算法
			if (to == NULL){
				*tolen=64;
				return state;
			}
			state = pDigest(to,tolen,from,fromlen,CALG_MS2_49);
			break;
		case 3://sm3算法
			if (to == NULL){
				*tolen=32;
				return state;
			}
			state = pDigest(to,tolen,from,fromlen,CALG_SM3);
			break;
		default:
			return -1;
		}
		if (state !=0 )
			SysLog("对数据进行摘要时出错,错误码为：%.2x\n",state);
	}else{
		SysLog("pDigest 指针为空，退出\n");
		state=-1;
	}
	SysLog("exit KeyDigest\n");
	return state;
}

/*
	对数据进行签名处理
	参数1：AlgType-算法类型 1-RSA1024 4-ECC384
	参数2: PartIn-明文输入
	参数3：PartInLen-明文长度
	参数4：Signature-返回的签名内容
	参数5：SignatureLen-返回的签名内容长度
	返回：0-正常 非0:失败
*/
int KeySign(int AlgType,unsigned char *PartIn,DWORD PartInLen,unsigned char *Signature,DWORD *SignatureLen)
{
	SysLog("enter KeySign\n");
	int state=0;
	long leftNum = 0;

	KEYVALUE PublicSigKey,PrivateSigKey,PublicExKey,PrivateExKey;

	PrivateSigKey.cbData=KEY_INDEX_LEN;	
	PrivateSigKey.pbData = (unsigned char*)malloc(KEY_INDEX_LEN);
	PublicSigKey.cbData=KEY_INDEX_LEN;	
	PublicSigKey.pbData = (unsigned char*)malloc(KEY_INDEX_LEN);

	if(PrivateSigKey.pbData == NULL){
		state = MALLOC_MEMORY_ERR;
		goto end;
	}

	if(PublicSigKey.pbData == NULL){
		state = MALLOC_MEMORY_ERR;
		goto end;
	}
	memset(PrivateSigKey.pbData,'\0',KEY_INDEX_LEN);
	memset(PublicSigKey.pbData,'\0',KEY_INDEX_LEN);

	PublicExKey.cbData=0;	PublicExKey.pbData=NULL;
	PrivateExKey.cbData=0;	PrivateExKey.pbData=NULL;

	if((pIsPVKEnabled == NULL) || (pLogin == NULL) || (pFindContainerAndKeypair == NULL) || (pAsymmetricSignData == NULL) 
		|| (pGetDeviceStatus == NULL)){
		SysLog("some function pointer is NULL!\n");
		state = POINTERISNULL;
		goto end;
	}

	//判断登录
	SysLog("判断是否登录\n");
	if ( !pIsPVKEnabled(NULL)){
		SysLog("未登录，调用Login\n");
		state=pLogin(1,NULL);
		if (state){
			SysLog("登录失败，错误码为：%.2x\n",state);
			state=LOGINFAILED;
			goto end;
		}
	}

	//判断是否被锁
	state = pGetDeviceStatus(PINTRYNUM_STATUS,&leftNum);
	if(state == 0){
		SysLog("GetDeviceStatus success,left try num is:%d\n",leftNum);
		if((leftNum == 0) ){
			SysLog("usbkey is locked!\n");
			state = USBKEYISLOCKED;
			goto end;
		}
	}else{
		SysLog("GetDeviceStatus failed, errorcode is:%.2x\n",state);
		goto end;
	}

	switch (AlgType){			
	case RSA1024://RSA1024算法
		if (Signature == NULL){
			*SignatureLen=128;
			goto end;
		}

		if ( (state=pFindContainerAndKeypair(RSA1024_1,&PublicSigKey,&PrivateSigKey,&PublicExKey,&PrivateExKey))){
			if(state!=0){
				state = pCreateContainer(RSA1024_1);
				if( state != 0 ){
					SysLog("无法创建RSA1024-1容器!\n");
					return -1;
				}
			}else{
				if ( (state=pAsymmetricSignData(&PrivateSigKey,PartIn,PartInLen,Signature,SignatureLen,CALG_RSA_SIGN))!=0)
					goto end;
			}
		}else{
			SysLog("没有找到RSA1024-1容器中的密钥\n!");
			goto end;
		}
		break;


	case ECC256://ECC256算法	
		if (  (state=pFindContainerAndKeypair(ECC_CONTAINER_CHOICE,&PublicSigKey,&PrivateSigKey,&PublicExKey,&PrivateExKey))==0 ){
			if ( (state=pAsymmetricSignData(&PrivateSigKey,PartIn,PartInLen,Signature,SignatureLen,CALG_ECC256_SIGN))!=0){	  
				SysLog("ECC256签名失败,错误码为：%.2x\n",state);
				goto end;
			}else{
				if (Signature == NULL){
					//	  *SignatureLen=96;
					goto end;
				}
				if ( (state=pAsymmetricSignData(&PrivateSigKey,PartIn,PartInLen,Signature,SignatureLen,CALG_ECC256_SIGN))!=0){	  
					SysLog("ECC256签名失败,错误码为：%.2x\n",state);
					goto end;
				}
			}
		}else{
			SysLog("没有找到ECC256密钥容器中的密钥,错误码为：%.2x\n",state);
			goto end;
		}
		break;

	case ECC384://ECC384算法
		if (Signature == NULL){
			*SignatureLen=96;
			goto end;
		}

		if (  (state=pFindContainerAndKeypair(ECC_CONTAINER_CHOICE,&PublicSigKey,&PrivateSigKey,&PublicExKey,&PrivateExKey))==0 ){
			if ( (state=pAsymmetricSignData(&PrivateSigKey,PartIn,PartInLen,Signature,SignatureLen,CALG_ECC_SIGN))!=0){
				SysLog("签名失败，错误码为：%.2x\n",state);
				goto end;
			}
		}else{
			SysLog("没有找到ECC384密钥容器中的密钥!\n");
			goto end;
		}
		break;

	default:
		break;
	}


end:

	key_freeBuffer((void **)&(PrivateSigKey.pbData));
	key_freeBuffer((void **)&(PublicSigKey.pbData));

	SysLog("exit KeySign\n");
	return state;
}

/*
	 RSA-PKCS1填充处理
*/
int RSA_padding_add_PKCS1_type1(unsigned char *to, int tlen,
	     const unsigned char *from, int flen){
	int j;
	unsigned char *p;

	if (flen > (tlen-11)){
			return(0);
	}
	
	p=(unsigned char *)to;

	*(p++)=0;
	*(p++)=1; /* Private Key BT (Block Type) */

	/* pad out with 0xff data */
	j=tlen-3-flen;
	memset(p,0xff,j);
	p+=j;
	*(p++)='\0';
	memcpy(p,from,(unsigned int)flen);
	return(1);
}


/*
	用RSA1024算法对数据进行签名,填充算法为pkcs1-01
	参数1:输入数据
	参数2:输入数据长度
	参数3:输出签名数据
	参数4:输出签名数据长度
*/
int SignRSA1024(unsigned char *data,unsigned int datalen,unsigned char *SignData,unsigned long *SignDataLen)
{
	int state=-1;
	unsigned long hashlen=0;
	int hashPadlen=128;
	unsigned char *hash=NULL;
	unsigned char *hashPad=NULL;

    if (  (state=KeyDigest(data, datalen,hash,&hashlen,1))==0 ){
		    hash = (unsigned char*)malloc(hashlen+16);
			
		    if (  (state=KeyDigest(data, datalen,hash,&hashlen,1))==0 )//获取摘要
			{
				//对摘要进行填充
				hashPad = (unsigned char*)malloc(hashPadlen);
				int i=RSA_padding_add_PKCS1_type1(hashPad,hashPadlen,hash,hashlen);
				if (i){
					//进行签名处理
					if ( (state=KeySign(1,hashPad,hashPadlen,NULL,SignDataLen))==0 ){
						if ( (state=KeySign(1,hashPad,hashPadlen,SignData,SignDataLen))!=0 )
							goto end;
					}else
						goto end;
				}else
					goto end;
			}else
				goto end;
	}else
		goto end;


end:

	key_freeBuffer((void **)&hash);
	key_freeBuffer((void **)&hashPad);

	return state;
}


/************************************************************************/
/*
商密预处理
*/
/************************************************************************/

int hashECC_Z(int algType,char result[210],int *resultLen, unsigned char pubkye[64], unsigned long pubkeyLen)
{
	unsigned char* CertContent=NULL; //证书内容
	int CertLen;//证书长度
	int rv = -1;
	memset(result,'\0',128);
	unsigned char buffer[256];
	memset(buffer,'\0',256);
	int pos = 0;
	int buf_len=0;
	char *pubkey = NULL;

#ifdef WIN32
	PCCERT_CONTEXT   pCertContext=NULL;
#endif

	unsigned long hashlen=0;
	unsigned char *hash=NULL;
	unsigned char *ensulInData=NULL;
	unsigned long  ensulInDataLength=0;	  

	//从配置文件中获取hash_ecc_id
	int i = 0;
	char retValue[255];
	char confFile[256]={'\0'}; 
	memset (retValue,'\0',255);
	memset(confFile,'\0',256);
	GetConfFilePath(confFile);


	//从USB-KEY中读取证书
	if ( (rv = key_exportCert(algType,1, CertContent,&CertLen))==0 ){
		CertContent = (unsigned char*)malloc(CertLen*sizeof(unsigned char));
		memset(CertContent,'\0',CertLen);
		if ( (rv=key_exportCert(algType,1, CertContent,&CertLen))==0 ){
			SysLog("export cert success，content is:\n");
			SysHexLog(CertContent,CertLen);
		}
	}else{
		SysLog("first export cert failed,errorcode is:%x\n",rv);
	}

	if(hash_ecc_entl){
		buffer[0]=0x00;
		buffer[1]=0x80;
		pos+=2;
	}


#ifdef WIN32
	rv =GetPrivateProfileString("hash_ecc_id","hash_ecc_id","    ",retValue,255,confFile);
#else
	server_ini = iniparser_load(( char *)confFile);			
	strcat(( char *)retValue,iniparser_getstring(server_ini,"hash_ecc_id:hash_ecc_id",NULL));		
#endif
	if((rv > 0) || (retValue[0] != '\0')){	
		SysLog("read hash_ecc_id from conf file :%s\n",confFile);
		SysLog("sizeof(retValue) is:%d\n",strlen(retValue));
		memcpy(buffer+pos,retValue,strlen(retValue));
		pos+=strlen(retValue);
	}else{
		memcpy(buffer+pos,hash_ecc_id,sizeof(hash_ecc_id));
		pos+=sizeof(hash_ecc_id);
	}


	memcpy(buffer+pos,hash_ecc_a,sizeof(hash_ecc_a));
	pos+=sizeof(hash_ecc_a);

	memcpy(buffer+pos,hash_ecc_b,sizeof(hash_ecc_b));
	pos+=sizeof(hash_ecc_b);

	memcpy(buffer+pos,hash_ecc_xg,sizeof(hash_ecc_xg));
	pos+=sizeof(hash_ecc_xg);

	memcpy(buffer+pos,hash_ecc_yg,sizeof(hash_ecc_yg));
	pos+=sizeof(hash_ecc_yg);


	if(pubkye == NULL)
	{
#ifdef WIN32
		if((pCertContext=CertCreateCertificateContext(X509_ASN_ENCODING,CertContent,CertLen))) {
			if(pCertContext->pCertInfo->SubjectPublicKeyInfo.PublicKey.pbData){
				SysLog("pubkey is：\n");
				SysHexLog(pCertContext->pCertInfo->SubjectPublicKeyInfo.PublicKey.pbData,pCertContext->pCertInfo->SubjectPublicKeyInfo.PublicKey.cbData);

				if(pCertContext->pCertInfo->SubjectPublicKeyInfo.PublicKey.cbData == 65){
					SysLog("pubkey len is:65 BYTE\n");
					memcpy(buffer+pos,pCertContext->pCertInfo->SubjectPublicKeyInfo.PublicKey.pbData+1,pCertContext->pCertInfo->SubjectPublicKeyInfo.PublicKey.cbData-1);
					buf_len=pos+pCertContext->pCertInfo->SubjectPublicKeyInfo.PublicKey.cbData-1;

				}else if(pCertContext->pCertInfo->SubjectPublicKeyInfo.PublicKey.cbData == 64){
					SysLog("pubkey len is:64 BYTE\n");
					memcpy(buffer+pos,pCertContext->pCertInfo->SubjectPublicKeyInfo.PublicKey.pbData,pCertContext->pCertInfo->SubjectPublicKeyInfo.PublicKey.cbData);
					buf_len=pos+pCertContext->pCertInfo->SubjectPublicKeyInfo.PublicKey.cbData;
				}
			}

			if(pCertContext != NULL){
				CertFreeCertificateContext(pCertContext);
				pCertContext = NULL;
			}
		}else{
			SysLog("解析证书上下文失败\n");
			rv = PARSECERTFAILED;
			goto end;
		}
#else
		rv = Cert_getPublicKey(CertContent,CertLen,NULL,&pubkeyLen);
		if(rv != 0){
			rv = PUBKEYISNULL;
			goto end;
		}
		pubkey = (char *)malloc(pubkeyLen*sizeof(unsigned char));
		memset(pubkey,'\0',pubkeyLen);
		if(NULL == pubkey){
			rv = MALLOC_MEMORY_ERR;
			goto end;
		}
		rv = Cert_getPublicKey(CertContent,CertLen,pubkey,&pubkeyLen);
		if(rv != 0){
			rv = PUBKEYISNULL;
			goto end;
		}
		memcpy(buffer+pos,pubkey,pubkeyLen);
		buf_len=pos+pubkeyLen;

#endif
	}else{
		SysLog("使用外送公钥，%d字节\n", pubkeyLen);
		memcpy(buffer+pos, pubkye, pubkeyLen);
		buf_len=pos+pubkeyLen;
	}

	SysLog("buffer len is:%d,内容为：\n",buf_len);
	SysHexLog(buffer,buf_len);
	//对buffer数据进行hash处理
	if (  (rv=key_digest(buffer,buf_len,hash,&hashlen))==0 )
	{
		hash = (unsigned char*)malloc(hashlen*sizeof(unsigned char));
		memset(hash,'\0',hashlen);
		if (  (rv=key_digest(buffer, buf_len,hash,&hashlen))==0 )//获取摘要
		{
			SysLog("对buffer数据进行摘要，摘要数据为：\n");
			SysHexLog(hash,hashlen);
		}else{
			rv = HASHFAILED;
			goto end;
		}
	}else{
		rv = HASHFAILED;
		goto end;
	}

	*resultLen = hashlen;
	memcpy(result,hash,hashlen);


end:
	key_freeBuffer((void **)&CertContent);

	return rv;
}

/*
商密对原文数据进行封装处理
*/
int ensule_indata(int algType,char *inData, int inDataLen,unsigned char *ensuleData, int *ensuleDataLen){
	int ret = -1;
	char proDealData[210];
	int proDealDataLen = 0;

	memset(proDealData,'\0',210);
	ret = hashECC_Z(algType,proDealData,&proDealDataLen,NULL,0);
	if(ret != 0){
		SysLog("hashECC_Z failed,return value is:%d\n",ret);
		return ret;
	}
	//对原文数据进行封装：ensuleData=proDealData+inData
	if(NULL == ensuleData){
		*ensuleDataLen=inDataLen+proDealDataLen;
		return 0;
	}
	memset(ensuleData,'\0',*ensuleDataLen);
	memcpy(ensuleData,proDealData,proDealDataLen);
	memcpy(ensuleData+proDealDataLen,inData,inDataLen);
	SysLog("封装之后的原文数据为：\n");
	SysHexLog(ensuleData,*ensuleDataLen);
	return ret;
}


/*
对数据进行签名
参数1:输入数据
参数2:输入数据长度
参数3:输出签名数据
参数4:输出签名数据长度 
*/
int SignECC(int asyAlgType,int hashFlag,unsigned char *data,unsigned int datalen,unsigned char *SignData,unsigned long *SignDataLen)
{
	int state=-1,	digestAlgType = 0;
	unsigned long hashlen=0;
	int hashPadlen=0;
	unsigned char *hash=NULL;
	unsigned char *hashPad=NULL;
	unsigned char *ensuleData = NULL;
	int ensuleDataLen = 0;

	switch(asyAlgType){
	case RSA1024:
	case RSA2048:
		break;
	case ECC256:
		hashPadlen = 32;
		digestAlgType = 3;

		state = ensule_indata(asyAlgType,(char *)data,datalen,NULL,&ensuleDataLen);
		if(state != 0){
			goto end;
		}
		ensuleData = (unsigned char *)malloc(ensuleDataLen*sizeof(unsigned char));
		memset(ensuleData,'\0',ensuleDataLen);
		state = ensule_indata(asyAlgType,(char *)data,datalen,ensuleData,&ensuleDataLen);
		if(state != 0){
			goto end;
		}
		break;
	case ECC384:
		hashPadlen = 48;
		digestAlgType = 2;
		break;
	default:
		break;
	}

	hashPad = (unsigned char*)malloc(hashPadlen);
	if(hashPad == NULL){
		state = MALLOC_MEMORY_ERR;
		goto end;
	}
	memset(hashPad,'\0',hashPadlen);

	if(hashFlag == NEED_HASH){
		if(asyAlgType == ECC256){
			state=KeyDigest(ensuleData, ensuleDataLen,hash,&hashlen,digestAlgType);
		}else{
			state=KeyDigest(data, datalen,hash,&hashlen,digestAlgType);
		}
		if (state !=0 ){
			SysLog("first hash deal error!\n");
			goto end;
		}
		hash = (unsigned char*)malloc(hashlen);
		memset(hash,'\0',hashlen);

		if(asyAlgType == ECC256){
			state=KeyDigest(ensuleData, ensuleDataLen,hash,&hashlen,digestAlgType);
		}else{
			state=KeyDigest(data, datalen,hash,&hashlen,digestAlgType);
		}
		if ( state != 0 ){
			SysLog("second hash deal error!\n");
			goto end;
		}
		//对摘要进行截除处理(特殊的填充)
		memcpy(hashPad,hash,hashPadlen);
	}else{
		memcpy(hashPad,data,hashPadlen);
	}
	SysLog("摘要数据为：\n");
	SysHexLog(hashPad,hashPadlen);
	//进行签名处理
	if ( (state=KeySign(asyAlgType,hashPad,hashPadlen,NULL,SignDataLen))==0 ){
		if ( (state=KeySign(asyAlgType,hashPad,hashPadlen,SignData,SignDataLen))!=0 ){
			SysLog("签名失败\n");
			goto end;
		}
	}
end:
	key_freeBuffer((void **)&ensuleData);
	key_freeBuffer((void **)&hash);
	key_freeBuffer((void **)&hashPad);
	return state;
}



/*
功能:
对数据进行签名。
参数:
[in] pData
要签名的数据。

[in] nDataLen
要签名的数据的长度。
  
[in-out] pSignedData
签名数据。
	
[in-out] pdwSignedLen
签名数据的长度。
	  
返回:
0：成功  非0：相应错误码
		
注释:
本接口为二次调用接口,设置pSignedData为NULL, pdwSignedLen为返回的数据的长度,
再根据返回的长度为pSignedData分配内存。
签名形式为：base64签名证书|base64签名值		  
 */
EXPORTDLL int WINAPIV key_sign(char *pData,	int nDataLen,	unsigned char *pSignedData,long  *pdwSignedLen)
{
	key_enter_lock_area();
	SysLog("enter key_sign\n");
	int ret = -1;	
	int rv=-1;
	unsigned char* CertContent=NULL; //证书内容
	int CertLen;//证书长度
	char *SignStr=NULL;//返回的数字签名字符串
	char *CerPart=NULL;//封装的数字签名的证书部分	
	char *SignPart=NULL;//封装的数字签名的签名部分
	unsigned char * buf=NULL;
	unsigned long buflen;
	int AlgType=ECC384;
	unsigned char *ensuleData = NULL;
	int ensuleDataLen = 0;

	/******************20121214 lch add****************************/
	ret = key_loadLibrary();
	if(ret != 0){
		goto end;
	}
	/******************20121214 lch add****************************/

	//从USB-KEY中读取证书
	if ( (rv=key_exportCert(AlgType, 1,CertContent,&CertLen)) != 0 ){
		SysLog("第一次获取签名证书失败\n");
		ret = EXPORTCERTFAILED;
		goto end;
	}
	SysLog("key_sign 中第一次导出证书正确\n");
	CertContent = (unsigned char*)malloc(CertLen);
	memset(CertContent,'\0',CertLen);
	if ( (rv=key_exportCert(AlgType,1, CertContent,&CertLen)) != 0 ){
		SysLog("第二次获取签名证书失败\n");
		key_freeBuffer((void **)&CertContent);
		ret = EXPORTCERTFAILED;
		goto end;
	}
	SysLog("key_sign 中第二次导出证书正确\n");
	//对证书进行base64编码
	CerPart=base64_encode(CertContent,CertLen);
	if(CerPart == NULL){
		ret = MALLOC_MEMORY_ERR;
		goto end;
	}
	SysLog("base64编码证书长度为：%d,内容为：\n",strlen(CerPart));
	SysHexLog((unsigned char*)CerPart,strlen(CerPart));
	//如果SignData是NULL，则返回SignDataLen的长度，以便二次调用
	if ( pSignedData == NULL){
		if (AlgType == RSA1024)
			*pdwSignedLen= strlen(CerPart) + 1 + 172;

		if (AlgType==RSA2048)
			*pdwSignedLen=strlen(CerPart)  + 1 + 344;

		if (AlgType==ECC256)
			*pdwSignedLen=strlen(CerPart)  +1  + 88+1;

		if (AlgType==ECC384)
			*pdwSignedLen=strlen(CerPart)  + 1 + 128+1;

		key_freeBuffer((void **)&CertContent);
		key_freeBuffer((void **)&CerPart);
		ret = 0;
		goto end;
	}

	//进行数字签名
	switch (AlgType){
	case RSA1024:
		buflen=128;
		buf = (unsigned char*)malloc(buflen);
		if ( (rv=SignRSA1024((unsigned char*)pData,nDataLen,buf,&buflen))==0 ){
			SignPart = base64_encode(buf,buflen);
			rv=0;
		}else{
			SysLog("SignRSA1024调用失败\n");
		}
		key_freeBuffer((void **)&buf);
		break;

	case ECC256:  //ECC256
		buflen=64;	//??
		SysLog("key_sign 中调用SignECC256\n");
		buf = (unsigned char*)malloc(buflen);
		memset(buf,'\0',buflen);
		
		if ( (rv=SignECC(ECC256,NEED_HASH,(unsigned char *)pData,nDataLen,buf,&buflen))==0 ){
			SysLog("-------------------------SignECC256后的数据：\n");
			SysHexLog((unsigned char*)buf,buflen);
			SignPart = base64_encode(buf,buflen);
			SysLog("-------------------------Base64编码后的数据长度为：%d\n",strlen(SignPart));
			SysLog("数据为：\n");
			SysHexLog((unsigned char*)SignPart,strlen(SignPart));
			rv=0;
			SysLog("-------------------------SignECC256调用成功\n");
		}else{
			SysLog("SignECC256调用失败\n");
		}
		
		key_freeBuffer((void **)&buf);
		break;

	case ECC384:
		buflen=96;	
		SysLog("key_sign 中调用SignECC384\n");
		buf = (unsigned char*)malloc(buflen);
		if(buf == NULL){
			ret = MALLOC_MEMORY_ERR;
			goto end;
		}
		memset(buf,'\0',buflen);
		if ( (rv=SignECC(ECC384,NEED_HASH,(unsigned char*)pData,nDataLen,buf,&buflen))==0 ){
			SysLog("-------------------------SignECC384后的数据：\n");
			SysHexLog((unsigned char*)buf,96);
			SysLog("-------------------------Base64编码后的数据：\n");
			SignPart = base64_encode(buf,buflen);
			if(SignPart == NULL){
				ret = MALLOC_MEMORY_ERR;
				goto end;
			}
			SysHexLog((unsigned char*)SignPart,strlen(SignPart));
			rv=0;
			SysLog("-------------------------SignECC384调用成功\n");
		}else{
			SysLog("SignECC384调用失败\n");
		}
		key_freeBuffer((void **)&buf);
		break;			
	}		
		
	//封装输出数据
	if (!rv){
		memset(pSignedData,'\0',*pdwSignedLen);
		SysLog("签名之后的长度为:%d\n",*pdwSignedLen-1);
		SysLog("\n");
		memcpy(pSignedData,CerPart,strlen(CerPart));
		memcpy(pSignedData+strlen(CerPart),"|",1);
		memcpy(pSignedData+strlen(CerPart)+1,SignPart,strlen(SignPart));
		SysLog("封装的数据签名为:\n");
		SysHexLog((unsigned char*)pSignedData,strlen(CerPart)+strlen(SignPart)+1);
	}

end:

	key_freeBuffer((void **)&CertContent);
	key_freeBuffer((void **)&CerPart);
	key_freeBuffer((void **)&SignPart);
	SysLog("exit key_sign\n");
	key_leave_lock_area();
	return rv;
}


/*
	导入公钥
	入参：CertContent-证书内容
	      CertLength 传进来的证书的长度
		  pData-明文 
	出参：keyidx
	返回: 0-为正常 其他:不正常，错误

*/
int  key_importPublicKey(int AlgID,unsigned char*CertContent,int CertLen,KEYVALUE *keyidx)
{
	SysLog("进入到 key_importPublicKey\n");
	
	int rv=0;
	char publicKey[1024];
	KEYVALUE InPubKey;
	BLOBHEADER pkheader;
	char temp[512];
	InPubKey.pbData = NULL;
	InPubKey.cbData = 0;
#ifdef WIN32
	PCCERT_CONTEXT   pCertContext = NULL;  //证书上下文
	if  ((pCertContext=CertCreateCertificateContext(X509_ASN_ENCODING,CertContent,CertLen)))  {
		memset(publicKey,'\0',1024);
		int size =  pCertContext->pCertInfo->SubjectPublicKeyInfo.PublicKey.cbData ;
		memcpy(publicKey,pCertContext->pCertInfo->SubjectPublicKeyInfo.PublicKey.pbData,size );
#else	
	X509			*x=NULL;	
	unsigned char	*p=NULL;	
	p = CertContent;
	d2i_X509(&x,(const unsigned char **)&p,CertLen);
	if(x){
		memset(publicKey,'\0',1024);
		int size = x->cert_info->key->public_key->length ;//pCertContext->pCertInfo->SubjectPublicKeyInfo.PublicKey.cbData ;
		memcpy(publicKey,x->cert_info->key->public_key->data,size );
#endif
		SysLog("证书的公钥为：\n");
		SysHexLog((unsigned char *)publicKey,size);
		SysLog("size is:%d\n",size);
		
		//导入公钥
		pkheader.bType = PUBLICKEYBLOB;
		pkheader.bVersion = CUR_BLOB_VERSION;
		pkheader.reserved = 0;
		pkheader.aiKeyAlg = AlgID;
		
		memset(temp,'\0',512);
		memcpy(temp,&pkheader,sizeof(BLOBHEADER));
		if(size==97)
			memcpy(temp+sizeof(BLOBHEADER),publicKey+1,--size);
		else
			memcpy(temp+sizeof(BLOBHEADER),publicKey,size);
			
		InPubKey.cbData=size+sizeof(BLOBHEADER);
		InPubKey.pbData=(unsigned char*)malloc(InPubKey.cbData*sizeof(unsigned char));
		memset(InPubKey.pbData,'\0',InPubKey.cbData);
		memcpy((char *)InPubKey.pbData,temp,InPubKey.cbData);
			
		rv = pImportPublicKey(&InPubKey,CALG_ECC_SIGN,keyidx);
		if ( rv == 0 ){
			SysLog("import success!\n");
		}else{
			SysLog("ImportPublicKey failed ,the state is: %x\n",rv);
			rv=IMPORTPUBKEYFAILED;
		}
#ifdef WIN32
		CertFreeCertificateContext(pCertContext);
		pCertContext = NULL;
#else

#endif
	}else{
		SysLog("解析证书失败\n");
		rv=PARSECERTFAILED;
	}

	key_freeBuffer((void **)&(InPubKey.pbData));
	return rv;
}

/*
功能:验证证书是否被撤销
返回值：
0:被撤销   非0:未被撤销	
 */
int	key_crlVerify(unsigned char* pCertContent,long dwCertLen,unsigned char* pCrlContent,long dwCrlLen)
{
	SysLog("进入到 key_crlVerify\n");
	int ret=-1;
	unsigned int i=0;
#ifdef WIN32
	PCCERT_CONTEXT  pCertContext=NULL;
	PCCRL_CONTEXT	pCRLContext=NULL;
	PCRL_ENTRY		pCrlEntry=NULL;
	BOOL			bCrlFlag=FALSE;
	DWORD			nSNNum=0;

	//得到证书上下文
	pCertContext=CertCreateCertificateContext(MY_ENCODING_TYPE,pCertContent,dwCertLen);
	if(pCertContext){
		SysLog("验证证书的序列号为：\n");
		SysHexLog(pCertContext->pCertInfo->SerialNumber.pbData,pCertContext->pCertInfo->SerialNumber.cbData);
	}else{
		SysLog("获取证书上下文失败\n");
		ret = PARSECERTFAILED;
		goto end;
	}

	//得到撤销列表上下文
	pCRLContext = CertCreateCRLContext(MY_ENCODING_TYPE,pCrlContent,dwCrlLen);
	if(pCRLContext){
		//遍历撤销列表
		nSNNum=pCRLContext->pCrlInfo->cCRLEntry;
		SysLog("撤销列表中撤销证书的数量为：%d\n",nSNNum);
		for( i=0;i<nSNNum;i++){
			//撤销证书的序列号
			SysLog("撤销列表中的序号为：%d的序列号为：\n",i+1);
			SysHexLog(pCRLContext->pCrlInfo->rgCRLEntry[i].SerialNumber.pbData,pCRLContext->pCrlInfo->rgCRLEntry[i].SerialNumber.cbData);
			//检验验证证书是否存在于撤销列表中
			if(pCRLContext->pCrlInfo->rgCRLEntry[i].SerialNumber.cbData == pCertContext->pCertInfo->SerialNumber.cbData){
				ret=memcmp(pCertContext->pCertInfo->SerialNumber.pbData,pCRLContext->pCrlInfo->rgCRLEntry[i].SerialNumber.pbData,pCertContext->pCertInfo->SerialNumber.cbData);
				if(ret==0){
					SysLog("此证书处于撤销列表中\n");
					break;
				}
			}
		}
		if(ret!=0){
			SysLog("此证书未处于此撤销列表中\n");
		}else{
			ret=CERTINCRL;
		}

		CertFreeCRLContext(pCRLContext);
		pCRLContext = NULL;
	}else{
		SysLog("获取撤销列表上下文失败\n");
		ret=PARSECERTFAILED;
	}

end:
	if(pCertContext != NULL){
		CertFreeCertificateContext(pCertContext);
		pCertContext = NULL;
	}
#else
#endif
	return ret;
}

/*
功能:
对数据进行证书验证。
参数:
[in] pCertContent
要验证的证书内容。

[in] dwCertLen
要验证的证书长度。

[in] pRootCert
根证书内容。

[in] dwRootCertLen
根证书长度。

返回:
0：成功  非0：相应错误码
 */
EXPORTDLL int WINAPIV key_verifyCert(unsigned char* pCertContent,long dwCertLen,unsigned char* pRootCertContent,long dwRootCertLen)
{	
	key_enter_lock_area();
	SysLog("enter key_verifyCert\n");
	int ret=-1;
	KEYVALUE keyidx;
	keyidx.cbData=KEY_INDEX_LEN;
	keyidx.pbData= NULL;

	unsigned char	*paintData=NULL,	*signData=NULL,	*hash=NULL,	*hashPad=NULL;
	int			  paintDataLen=0,	signDataLen=0,  hashPadlen=48;
	unsigned long hashlen=0;

#ifdef WIN32

	PCCERT_CONTEXT  pCertContext=NULL;
	PCCERT_CONTEXT	pRootCertContext=NULL;
	pRootCertContext=CertCreateCertificateContext(MY_ENCODING_TYPE,pRootCertContent,dwRootCertLen);
#else
	X509	*pCertContext=NULL;
	X509	*pRootCertContext=NULL;
	pCertContext = X509_new();
	pRootCertContext = X509_new();
	unsigned char * p = NULL;
	unsigned char * pRoot = NULL;
	p=pCertContent;
	pRoot=pRootCertContent;
	d2i_X509(&pRootCertContext,(const unsigned char **)&pRoot,dwRootCertLen);
#endif

	if(pAsymmetricVerifySignEx == NULL){
		SysLog("pAsymmetricVerifySignEx 指针为空\n");
		ret = POINTERISNULL;
		goto end;
	}

	if(pRootCertContext == NULL){
		SysLog("获取根证书上下文失败\n");
		ret = PARSECERTFAILED;
		goto end;
	}
	keyidx.pbData = (unsigned char*)malloc((keyidx.cbData)*sizeof(unsigned char));
	if(keyidx.pbData == NULL){
		ret = MALLOC_MEMORY_ERR;
		goto end;
	}
	memset(keyidx.pbData,'\0',keyidx.cbData);
	ret=key_importPublicKey(CALG_ECC_SIGN,pRootCertContent,dwRootCertLen,&keyidx);
	if(ret != 0){
		SysLog("导入公钥失败\n");
		key_freeBuffer((void **)&(keyidx.pbData));
		ret = IMPORTPUBKEYFAILED;
		goto end;
	}
	SysLog("导入公钥成功\n");

	//解析待使用证书得到证书上下文
#ifdef WIN32
	pCertContext=CertCreateCertificateContext(MY_ENCODING_TYPE,pCertContent,dwCertLen);
#else
	d2i_X509(&pCertContext,(const unsigned char **)&p,dwCertLen);
#endif
	if(pCertContext == NULL){
		SysLog("获取证书上下文失败\n");
		ret=PARSECERTFAILED;
		goto end;
	}

	//得到证书的签名项
	signDataLen=96;
	signData=(unsigned char*)malloc(signDataLen*sizeof(unsigned char));
	if(signData == NULL){
		ret = MALLOC_MEMORY_ERR;
		goto end;
	}
	memset(signData,'\0',signDataLen);
	memcpy(signData,pCertContent+(dwCertLen-96),signDataLen);
	SysLog("证书的签名项为：\n");
	SysHexLog(signData,signDataLen);

	//得到证书的签名域
	SysLog("pCertContent[6] is:%d\n",pCertContent[6]);
	SysLog("pCertContent[7] is:%d\n",pCertContent[7]);
	//得到签名域
	paintDataLen=pCertContent[6]*256+pCertContent[7]+4;
	paintData = (unsigned char *)malloc(paintDataLen*sizeof(unsigned char));
	
	if(paintData == NULL){
		ret = MALLOC_MEMORY_ERR;
		goto end;
	}

	memset(paintData,'\0',paintDataLen);
	memcpy(paintData,pCertContent+4,paintDataLen);
	SysLog("证书的签名域内容为：\n");
	SysHexLog(paintData,paintDataLen);
	//对签名域内容进行hash处理				
	if (  (ret=KeyDigest(paintData, paintDataLen,hash,&hashlen,2))==0 ){
		hash = (unsigned char*)malloc(hashlen*sizeof(unsigned char));
		
		if(hash == NULL){
			ret = MALLOC_MEMORY_ERR;
			goto end;
		}

		memset(hash,'\0',hashlen);
		if (  (ret=KeyDigest(paintData, paintDataLen,hash,&hashlen,2))==0 ){
			//对摘要进行截除处理(特殊的填充)
			hashPad = (unsigned char*)malloc(hashPadlen);
			if(hashPad == NULL){
				goto end;
			}
			memset(hashPad,'\0',hashPadlen);
			memcpy(hashPad,hash,hashPadlen);
			if(hashPad){
				SysLog("摘要数据为:\n");
				SysHexLog(hashPad,hashPadlen);
			}

			SysLog("进行验签\n");
			//验证签名
			if ( (ret=pAsymmetricVerifySignEx(&keyidx,hashPad,hashPadlen,signData,signDataLen))!=0){
				SysLog("pAsymmetricVerifySignEx失败，验证签名失败!，错误码为：%.2x\n",ret);
				ret = VERIFYFAILED;
			}else{
				SysLog("验证签名成功\n");	
			}
		}else{
			SysLog("第二次hash失败\n");
			ret = HASHFAILED;
		}
	}else{
		SysLog("第一次hash失败\n");
		ret = HASHFAILED;
	}


end:

	key_freeBuffer((void **)&hash);
	key_freeBuffer((void **)&hashPad);
	key_freeBuffer((void **)&(keyidx.pbData));
	key_freeBuffer((void **)&signData);
	key_freeBuffer((void **)&paintData);

	if(pRootCertContext){
#ifdef WIN32
		CertFreeCertificateContext(pRootCertContext);
#else
		X509_free(pRootCertContext);
#endif
		pRootCertContext = NULL;
	}
	if(pCertContext){
#ifdef WIN32
		CertFreeCertificateContext(pCertContext);
#else
		X509_free(pCertContext);
#endif
		pCertContext = NULL;
	}
	key_leave_lock_area();
	return ret;
}

/*
功能:
对签名数据进行验证。

参数:
  
[in] pSignedData
签名数据。
	
[int] dwSignedLen
签名数据的长度。
	  
[int] pData
原文数据。
		
[in] dwDataLen
原文数据的长度。
		  
[in] szCACertFile
根证书文件路径。
			
[in] szCrlFile
CRL文件路径。
			  
返回:
0：成功  非0：相应错误码
				
注释:
szCrlFile默认为NULL，此时不做CRL撤销列表校验。
				  
 */

EXPORTDLL int WINAPIV key_verify(unsigned char *pSignedData, long dwSignedLen,unsigned char * pData,long dwDataLen,
	const char *szCACertFile,const char *szCrlFile)
{
	key_enter_lock_area();
	SysLog("enter key_verify\n");
	int ret = -1;
	unsigned char *pCertContent=NULL,	*decodeSignData=NULL,	*pCrlContent=NULL;
	int	nCertContentLen=0,	decodeSignDataLen=0,	nCodeCertLen=0,	nCodeSignDataLen=0;
	char *tmp=NULL;
	unsigned long	dwCrlContentLen=0,	dwPropId=0,	hashlen=0;
	unsigned int	i=0,	j=0;
	int hashPadlen=48;
	unsigned char *hash=NULL,	*hashPad=NULL,	*p=NULL;

	KEYVALUE keyidx;
	FILE *CACertFile = NULL;
	unsigned char *CACertContent=NULL;
	int CACertContentLen = 0;
	FILE *hfp = NULL;

	keyidx.pbData = NULL;
	keyidx.cbData = 0;
	/******************20121214 lch add****************************/
	ret = key_loadLibrary();
	if(ret != 0){
		goto  end;
	}

	if(szCACertFile == NULL){
		SysLog("para szCACertFile is NULL\n");
		ret = PARAERROR;
		goto end;
	}
	SysLog("CACertFile path is:%s\n",szCACertFile);

	/******************20121214 lch add****************************/
	keyidx.cbData=KEY_INDEX_LEN;
	keyidx.pbData = (unsigned char*)malloc(keyidx.cbData);
	if(keyidx.pbData == NULL){
		ret = MALLOC_MEMORY_ERR;
		goto end;
	}
	memset(keyidx.pbData,'\0',keyidx.cbData);
	if(dwSignedLen!=0){
		SysLog("签名数据长度为：%d\n",dwSignedLen);
	}else{
		SysLog("参数签名数据长度为0\n");
		ret=PARAERROR;
		goto end;
	}

	if(dwDataLen!=0){
		SysLog("明文数据长度为：%d\n",dwDataLen);
	}else{
		SysLog("参数明文数据长度为0\n");
		ret=PARAERROR;
		goto end;
	}

	if(pAsymmetricVerifySignEx == NULL){
		SysLog("pAsymmetricVerifySignEx is NULL\n");
		ret = POINTERISNULL;
		goto end;
	}

	//拆分签名数据
	p=(unsigned char*)strstr((const char*)pSignedData,"|");
	if(p == NULL){
		SysLog("未找到分隔符\n");
		ret = ECC384SIGNFAILED;
		goto end;
	}

	nCodeCertLen=p-pSignedData;
	tmp=( char*)malloc(nCodeCertLen+1);
	if(tmp == NULL){
		ret = MALLOC_MEMORY_ERR;
		goto end;
	}
	memset(tmp,'\0',nCodeCertLen+1);
	memcpy(tmp,pSignedData,nCodeCertLen);
	SysLog("base64编码证书长度为：%d,内容为：\n",nCodeCertLen);
	SysHexLog((unsigned char*)tmp,nCodeCertLen);
	pCertContent=base64_decode(tmp,&nCertContentLen);
	if(pCertContent == NULL){
		ret = MALLOC_MEMORY_ERR;
		goto end;
	}
	if(pCertContent){
		SysLog("证书内容为：\n");
		SysHexLog(pCertContent,nCertContentLen);
	}

	key_freeBuffer((void **)&tmp);

	nCodeSignDataLen=dwSignedLen-nCodeCertLen-1;
	SysLog("base64编码签名数据长度为：%d,内容为：\n",nCodeSignDataLen);
	SysHexLog((unsigned char*)p+1,nCodeSignDataLen);

	decodeSignData=base64_decode((char *)p+1,&decodeSignDataLen);
	if(decodeSignData){
		SysLog("签名数据为：\n");
		SysHexLog(decodeSignData,decodeSignDataLen);
	}

	//得到CA证书内容				
	CACertFile=fopen(szCACertFile,"rb");

	if ( CACertFile != NULL){
		fseek(CACertFile, 0L, SEEK_END);
		CACertContentLen = ftell(CACertFile);
		rewind(CACertFile);
		CACertContent =(unsigned char *) malloc(CACertContentLen*sizeof(unsigned char));
		if(CACertContent == NULL){
			ret = MALLOC_MEMORY_ERR;
			goto end;
		}
		fread(CACertContent,sizeof(unsigned char),CACertContentLen, CACertFile);
		fclose(CACertFile);
		CACertFile = NULL;
	}else{
		SysLog("szCACertFile 不存在\n");
		ret=CERTFILENOTEXIST;
		goto end;
	}
	//不进行crl撤销列表校验
	if(szCrlFile == NULL) {
		SysLog("不进行crl撤销列表校验\n");
	}else //进行撤销列表校验
	{
		SysLog("验证证书是否被撤销\n");
		//获取crl内容
		hfp = fopen(szCrlFile, "rb");
		if ( hfp == NULL){
			SysLog("can't find Crlfile\n");
			ret=CRLFILENOTEXIST;
			goto end;
		}
		fseek(hfp, 0, SEEK_END);
		dwCrlContentLen = ftell(hfp);
		pCrlContent =(unsigned char*) malloc(dwCrlContentLen*sizeof(unsigned char));
		if(pCrlContent == NULL){
			ret = MALLOC_MEMORY_ERR;
			goto end;
		}
		memset(pCrlContent,'\0',dwCrlContentLen);
		fseek(hfp, 0, SEEK_SET);
		fread(pCrlContent, 1, dwCrlContentLen, hfp);
		fclose(hfp);
		hfp = NULL;

		ret=key_crlVerify(pCertContent,nCertContentLen,pCrlContent,dwCrlContentLen);
		if(ret==0){
			SysLog("此证书处于撤销列表中\n");
			goto end;
		}else{
			SysLog("此证书未被撤销\n");
		}
	}

	//验证证书的有效性
	SysLog("验证证书的有效性\n");
	ret=key_verifyCert(pCertContent,nCertContentLen,CACertContent,CACertContentLen);
	if(ret==0){
		SysLog("证书验证成功\n");
	}else{
		SysLog("证书验证失败\n");
		ret=CERTVERIFYFAILED;
		goto end;
	}

	//验证签名
	SysLog("验证签名\n");
	//导入公钥
	ret=key_importPublicKey(CALG_ECC_SIGN,pCertContent,nCertContentLen,&keyidx);
	if(ret != 0){
		SysLog("导入公钥失败\n");
		ret=IMPORTPUBKEYFAILED;
		goto end;
	}
	//对原文进行hash处理				
	if (  (ret=KeyDigest(pData, dwDataLen,hash,&hashlen,2)) != 0 ){
		SysLog("第一次hash失败\n");
		ret=HASHFAILED;
		goto end;
	}
	hash = (unsigned char*)malloc(hashlen);
	if(hash == NULL){
		ret = MALLOC_MEMORY_ERR;
		goto end;
	}
	memset(hash,'\0',hashlen);
	//获取摘要
	if (  (ret=KeyDigest(pData, dwDataLen,hash,&hashlen,2)) != 0 ){
		SysLog("第二次hash失败\n");
		ret=HASHFAILED;
		goto end;
	}
	//对摘要进行截除处理(特殊的填充)
	hashPad = (unsigned char*)malloc(hashPadlen);
	if(hashPad == NULL){
		ret = MALLOC_MEMORY_ERR;
		goto end;
	}
	memset(hashPad,'\0',hashPadlen);
	memcpy(hashPad,hash,hashPadlen);
	if(hashPad){
		SysLog("摘要数据为:\n");
		SysHexLog(hashPad,hashPadlen);
	}

	SysLog("进行验签\n");
	//验证签名
	
	if ( (ret=pAsymmetricVerifySignEx(&keyidx,hashPad,hashPadlen,decodeSignData,decodeSignDataLen))!=0){
		SysLog("pAsymmetricVerifySignEx失败，验证签名失败!，错误码为：%.2x\n",ret);
		ret=VERIFYFAILED;
	}else{
		SysLog("验证签名成功\n");	
	}
	

end:

	if(CACertFile){
		fclose(CACertFile);
		CACertFile = NULL;
	}
	if(hfp){
		fclose(hfp);
		hfp = NULL;
	}
	key_freeBuffer((void **)&hash);
	key_freeBuffer((void **)&hashPad);
	key_freeBuffer((void **)&(keyidx.pbData));
	key_freeBuffer((void **)&pCertContent);
	key_freeBuffer((void **)&decodeSignData);
	key_freeBuffer((void **)&CACertContent);
	key_freeBuffer((void **)&pCrlContent);
	SysLog("exit key_verify\n");
	key_leave_lock_area();
	return ret;
}


//20121211 lch add
/*
功能:
对数据进行签名。
参数:
[in] pData
要签名的数据。

[in] nDataLen
要签名的数据的长度。
  
[in-out] pSignedData
签名数据。
	
[in-out] pdwSignedLen
签名数据的长度。
	  
返回:
0：成功  非0：相应错误码
		
注释:
本接口为二次调用接口,设置pSignedData为NULL, pdwSignedLen为返回的数据的长度,再根据返回的长度为pSignedData分配内存。
签名值不包括证书，只是签名值。		  
 */
EXPORTDLL int WINAPIV key_sign_ex(char *pData,	int nDataLen,	unsigned char *pSignedData,long  *pdwSignedLen)
{
	key_enter_lock_area();
	SysLog("enter key_sign_ex\n");
	int ret = -1;
	char *SignStr=NULL;//返回的数字签名字符串	
	char *SignPart=NULL;//封装的数字签名的签名部分
	unsigned char *buf=NULL;
	unsigned long buflen;
	int AlgType = 0;
	if (G_NowDev==2)
	{
		AlgType=ECC256;
	}
	else
	{
		AlgType=ECC384;
	}
	/******************20121214 lch add****************************/
	ret = key_loadLibrary();
	if(ret != 0){
		goto end;
	}
	/******************20121214 lch add****************************/

	//如果SignData是NULL，则返回SignDataLen的长度，以便二次调用
	if ( pSignedData == NULL){
		if (AlgType==RSA1024)
			*pdwSignedLen=172;

		if (AlgType==2)
			*pdwSignedLen=344;

		if (AlgType==3)
			*pdwSignedLen=88+1;

		if (AlgType==4)
			*pdwSignedLen=128+1;

		ret = 0;
		goto end;
	}

	//进行数字签名
	switch (AlgType){
	case RSA1024:
		buflen=128;
		buf = (unsigned char*)malloc(buflen);
		if ( (ret =SignRSA1024((unsigned char*)pData,nDataLen,buf,&buflen))==0 ){
			SignPart = base64_encode(buf,buflen);
			ret =0;
		}else{
			SysLog("SignRSA1024调用失败\n");
		}
		key_freeBuffer((void **)&buf);

		break;

	case ECC256:  //ECC256
		buflen=64;	//??
		SysLog("key_sign 中调用SignECC256\n");

		buf = (unsigned char*)malloc(buflen);
		memset(buf,'\0',buflen);

		if ( (ret =SignECC(ECC256,NEED_HASH,(unsigned char *)pData,nDataLen,buf,&buflen))==0 ){
			SysLog("-------------------------SignECC256后的数据：\n");
			SysHexLog((unsigned char*)buf,buflen);
			SignPart = base64_encode(buf,buflen);
			SysLog("-------------------------Base64编码后的数据长度为：%d\n",strlen(SignPart));
			SysLog("数据为：\n");
			SysHexLog((unsigned char*)SignPart,strlen(SignPart));
			ret =0;
			SysLog("-------------------------SignECC256调用成功\n");
		}else{
			SysLog("SignECC256调用失败\n");
		}

		key_freeBuffer((void **)&buf);
		break;

	case ECC384:
		buflen=96;	
		SysLog("key_sign_ex 中调用SignECC384\n");
		buf = (unsigned char*)malloc(buflen);
		if(buf == NULL){
			ret = MALLOC_MEMORY_ERR;
			goto end;
		}
		memset(buf,'\0',buflen);
		if ( (ret =SignECC(ECC384,NEED_HASH,(unsigned char*)pData,nDataLen,buf,&buflen))==0 ){
			SysLog("-------------------------SignECC384后的数据：\n");
			SysHexLog((unsigned char*)buf,96);
			SysLog("-------------------------Base64编码后的数据：\n");
			SignPart = base64_encode(buf,buflen);
			if(SignPart == NULL){
				ret = MALLOC_MEMORY_ERR;
				goto end;
			}
			SysHexLog((unsigned char*)SignPart,strlen(SignPart));
			ret =0;
			SysLog("-------------------------SignECC384调用成功\n");
		}else{
			SysLog("SignECC384调用失败\n");
		}
		key_freeBuffer((void **)&buf);
		break;
	}

	//封装输出数据
	if (!ret){
		memset(pSignedData,'\0',*pdwSignedLen);
		SysLog("签名之后的长度为:%d\n",*pdwSignedLen-1);
		SysLog("\n");

		memcpy(pSignedData,SignPart,strlen(SignPart));
		SysLog("数据签名为:\n");
		SysHexLog((unsigned char*)pSignedData,strlen(SignPart));
	}

end:

	key_freeBuffer((void **)&SignPart);
	SysLog("exit key_sign_ex\n");
	key_leave_lock_area();
	return ret;
}



//20140313 lch add
/*
功能:
对数据进行签名。
参数:
[in] pData
要签名的数据。

[in] nDataLen
要签名的数据的长度。
  
[in-out] pSignedData
签名数据。
	
[in-out] pdwSignedLen
签名数据的长度。
	  
返回:
0：成功  非0：相应错误码
		
注释:
本接口为二次调用接口,设置pSignedData为NULL, pdwSignedLen为返回的数据的长度,再根据返回的长度为pSignedData分配内存。
签名值不包括证书，只是签名值,原文数据为hash数据，不小于48字节。		  
 */
EXPORTDLL int WINAPIV key_sign_ex_nohash(char *pData,	int nDataLen,	unsigned char *pSignedData,long  *pdwSignedLen)
{
	key_enter_lock_area();
	SysLog("enter key_sign_ex_nohash\n");
	int ret = -1;
	char *SignStr=NULL;//返回的数字签名字符串	
	char *SignPart=NULL;//封装的数字签名的签名部分
	unsigned char * buf=NULL;
	unsigned long buflen;
	int AlgType=4;

	/******************20121214 lch add****************************/
	ret = key_loadLibrary();
	if(ret != 0){
		goto end;
	}
	/******************20121214 lch add****************************/

	if(pData != NULL){
		SysLog("原文数据为：\n");
		SysHexLog((unsigned char*)pData,nDataLen);
	}else{
		ret = PARAERROR;
		goto end;
	}

	if((nDataLen < 48) ){
		SysLog("原文数据长度有问题，不应小于48字节\n");
		ret = PARAERROR;
		goto end;
	}

	//如果SignData是NULL，则返回SignDataLen的长度，以便二次调用
	if ( pSignedData == NULL){
		if (AlgType == RSA1024)
			*pdwSignedLen=172;

		if (AlgType == RSA2048)
			*pdwSignedLen=344;

		if (AlgType== ECC256)
			*pdwSignedLen=88+1;

		if (AlgType == ECC384)
			*pdwSignedLen=96;

		ret = 0;
		goto end;
	}

	//进行数字签名
	switch (AlgType){
	case RSA1024:
		buflen=128;
		buf = (unsigned char*)malloc(buflen);
		if ( (ret =SignRSA1024((unsigned char*)pData,nDataLen,buf,&buflen))==0 ){
			SignPart = base64_encode(buf,buflen);
			ret =0;
		}else{
			SysLog("SignRSA1024调用失败\n");
		}

		break;

	case ECC256:  //ECC256
		buflen=64;	//??
		SysLog("key_sign 中调用SignECC256\n");

		buf = (unsigned char*)malloc(buflen);
		memset(buf,'\0',buflen);

		if ( (ret =SignECC(ECC256,0,(unsigned char*)pData,nDataLen,buf,&buflen))==0 ){
			SysLog("-------------------------SignECC256后的数据：\n");
			SysHexLog((unsigned char*)buf,buflen);
			ret =0;
			SysLog("-------------------------SignECC256调用成功\n");
		}else{
			SysLog("SignECC256调用失败\n");
		}
		break;

	case ECC384:
		buflen=96;	
		SysLog("key_sign_ex 中调用SignECC384\n");
		buf = (unsigned char*)malloc(buflen);
		if(buf == NULL){
			ret = MALLOC_MEMORY_ERR;
			goto end;
		}
		memset(buf,'\0',buflen);
		if ( (ret =SignECC(ECC384,0,(unsigned char*)pData,48,buf,&buflen))==0 ){
			SysLog("-------------------------SignECC384后的数据：\n");
			SysHexLog((unsigned char*)buf,buflen);
			ret =0;
			SysLog("-------------------------SignECC384调用成功\n");
		}else{
			SysLog("SignECC384调用失败\n");
		}
		break;
	}

	//封装输出数据
	if (!ret ){
		memset(pSignedData,'\0',*pdwSignedLen);
		SysLog("签名之后的长度为:%d\n",*pdwSignedLen);

		memcpy(pSignedData,buf,buflen);
		SysLog("数据签名为:\n");
		SysHexLog((unsigned char*)pSignedData,buflen);
	}

end:
	key_freeBuffer((void **)&buf);
	SysLog("exit key_sign_ex_nohash\n");
	key_leave_lock_area();
	return ret;
}


/*
功能:
对签名数据进行验证。

参数:
  
[in] pSignedData
签名数据。
	
[int] dwSignedLen
签名数据的长度。
	  
[int] pData
原文数据。
		
[in] dwDataLen
原文数据的长度。

[int] pCertContent
验证证书数据。

[in] dwCertLen
验证证书数据的长度。

		  
[in] szCACertFile
根证书文件路径。
			
[in] szCrlFile
CRL文件路径。
			  
返回:
0：成功  非0：相应错误码
				
注释:
szCrlFile默认为NULL，此时不做CRL撤销列表校验。				  
 */
EXPORTDLL int WINAPIV key_verify_ex(unsigned char *pSignedData, long dwSignedLen,unsigned char * pData,long dwDataLen,
	unsigned char* pCertContent,long dwCertLen,
	const char *szCACertFile,const char *szCrlFile)
{
	key_enter_lock_area();
	SysLog("enter key_verify_ex\n");
	int ret=-1;
	unsigned char	*pCrlContent=NULL,	*CACertContent=NULL,*decodeSignData=NULL;
	unsigned long	dwCrlContentLen=0,	dwPropId=0;
	unsigned int	i=0, j = 0, CACertContentLen = 0;

	unsigned long hashlen=0;
	int hashPadlen=48,decodeSignDataLen=0;
	unsigned char *hash=NULL, *hashPad=NULL;
	FILE *CACertFile = NULL, *hfp = NULL;
	KEYVALUE keyidx;
	keyidx.cbData=KEY_INDEX_LEN;
	keyidx.pbData = NULL;

	/******************20121214 lch add****************************/
	ret = key_loadLibrary();
	if(ret != 0){
		goto end;
	}
	/******************20121214 lch add****************************/

	if(pAsymmetricVerifySignEx == NULL){
		SysLog("pAsymmetricVerifySignEx 指针为空\n");
		ret = POINTERISNULL;
		goto end;
	}
	keyidx.pbData = (unsigned char*)malloc(keyidx.cbData);
	if(keyidx.pbData == NULL){
		ret = MALLOC_MEMORY_ERR;
		goto end;
	}
	memset(keyidx.pbData,'\0',keyidx.cbData);

	if(dwSignedLen!=0){
		SysLog("签名数据长度为：%d\n",dwSignedLen);
	}else{
		SysLog("参数签名数据长度为0\n");
		ret=PARAERROR;
		goto end;
	}

	if(dwDataLen!=0){
		SysLog("明文数据长度为：%d\n",dwDataLen);
	}else{
		SysLog("参数明文数据长度为0\n");
		ret=PARAERROR;
		goto end;
	}

	if(dwCertLen){
		SysLog("验证证书长度为：%d\n",dwCertLen);
	}else{
		SysLog("验证证书数据长度为0\n");
		ret=PARAERROR;
		goto end;
	}

	if(pCertContent){
		SysLog("验证证书内容为：\n");
		SysHexLog(pCertContent,dwCertLen);
	}else{
		SysLog("参数验证证书内容为空\n");
		ret=PARAERROR;
		goto end;
	}

	if(pSignedData){
		SysLog("base64签名数据为：\n");
		SysHexLog(pSignedData,dwSignedLen);
	}else{
		SysLog("参数签名内容为空\n");
		ret=PARAERROR;
		goto end;
	}

	decodeSignData=base64_decode((char *)pSignedData,&decodeSignDataLen);
	if(decodeSignData){
		SysLog("解码后签名数据为：\n");
		SysHexLog(decodeSignData,decodeSignDataLen);
	}

	//得到CA证书内容				
	CACertFile=fopen(szCACertFile,"rb");

	if ( CACertFile != NULL){
		fseek(CACertFile, 0L, SEEK_END);
		CACertContentLen = ftell(CACertFile);
		rewind(CACertFile);
		CACertContent =(unsigned char *) malloc(CACertContentLen*sizeof(unsigned char));
		if(CACertContent == NULL){
			ret = MALLOC_MEMORY_ERR;
			goto end;
		}
		fread(CACertContent,sizeof(unsigned char),CACertContentLen, CACertFile);
		fclose(CACertFile);
	}else{
		SysLog("szCACertFile 不存在\n");
		ret=CERTFILENOTEXIST;
		goto end;
	}
	//不进行crl撤销列表校验
	if(szCrlFile==NULL) {
		SysLog("不进行crl撤销列表校验\n");
	}else //进行撤销列表校验
	{
		SysLog("验证证书是否被撤销\n");
		//获取crl内容
		hfp = fopen(szCrlFile, "rb");

		if ( hfp == NULL){
			SysLog("can't find Crlfile\n");
			ret=CRLFILENOTEXIST;
			goto end;
		}
		fseek(hfp, 0, SEEK_END);
		dwCrlContentLen = ftell(hfp);
		pCrlContent =(unsigned char*) malloc(dwCrlContentLen*sizeof(unsigned char));
		if(pCrlContent == NULL){
			ret = MALLOC_MEMORY_ERR;
			goto end;
		}
		memset(pCrlContent,'\0',dwCrlContentLen);
		fseek(hfp, 0, SEEK_SET);
		fread(pCrlContent, 1, dwCrlContentLen, hfp);
		fclose(hfp);

		ret=key_crlVerify(pCertContent,dwCertLen,pCrlContent,dwCrlContentLen);
		if(ret==0){
			SysLog("此证书处于撤销列表中\n");
			goto end;
		}else{
			SysLog("此证书未被撤销\n");
		}
	}

	//验证证书的有效性
	SysLog("验证证书的有效性\n");
	ret=key_verifyCert(pCertContent,dwCertLen,CACertContent,CACertContentLen);
	if(ret==0){
		SysLog("证书验证成功\n");
	}else{
		SysLog("证书验证失败\n");
		ret=CERTVERIFYFAILED;
		goto end;
	}

	//验证签名
	SysLog("验证签名\n");
	//导入公钥
	ret=key_importPublicKey(CALG_ECC_SIGN,pCertContent,dwCertLen,&keyidx);
	if(ret != 0){
		SysLog("导入公钥失败\n");
		ret=IMPORTPUBKEYFAILED;
		goto end;
	}
	//对原文进行hash处理				
	if (  (ret=KeyDigest(pData, dwDataLen,hash,&hashlen,2)) != 0 ){
		SysLog("第一次hash失败\n");
		ret=HASHFAILED;
		goto end;
	}
	hash = (unsigned char*)malloc(hashlen);
	if(hash == NULL){
		goto end;
	}
	memset(hash,'\0',hashlen);
	if (  (ret=KeyDigest(pData, dwDataLen,hash,&hashlen,2)) !=0 ){
		SysLog("第二次hash失败\n");
		ret=HASHFAILED;
		goto end;
	}
	//对摘要进行截除处理(特殊的填充)
	hashPad = (unsigned char*)malloc(hashPadlen);
	if(hashPad == NULL){
		ret = MALLOC_MEMORY_ERR;
		goto end;
	}
	memset(hashPad,'\0',hashPadlen);
	memcpy(hashPad,hash,hashPadlen);
	if(hashPad){
		SysLog("摘要数据为:\n");
		SysHexLog(hashPad,hashPadlen);
	}
	//验证签名

	if ( (ret=pAsymmetricVerifySignEx(&keyidx,hashPad,hashPadlen,decodeSignData,decodeSignDataLen))!=0){
		SysLog("pAsymmetricVerifySignEx失败，验证签名失败!，错误码为：%.2x\n",ret);
		ret=VERIFYFAILED;
	}else{
		SysLog("验证签名成功\n");	
	}

end:
	if(CACertFile){
		fclose(CACertFile);
		CACertFile = NULL;
	}

	if(hfp){
		fclose(hfp);
		hfp = NULL;
	}
	key_freeBuffer((void **)&hash);
	key_freeBuffer((void **)&hashPad);
	key_freeBuffer((void **)&(keyidx.pbData));
	key_freeBuffer((void **)&decodeSignData);
	key_freeBuffer((void **)&CACertContent);
	key_freeBuffer((void **)&pCrlContent);

	SysLog("exit key_verify_ex\n");
	key_leave_lock_area();
	return ret;
}


/*
功能:
对数据进行摘要。
参数:
[in] pData
要摘要的数据。
  
[in] nDataLen
要摘要的数据的长度。
	
[in-out] pDigestData
摘要值。
	  
[in-out] pDigestLen
摘要值的长度。
		
返回:
0：成功  非0：相应错误码
		  
注释:
本接口为二次调用接口,设置pDigestData为NULL, pDigestLen为返回的数据的长度,再根据返回的长度为pDigestData分配内存。			
 */
EXPORTDLL int WINAPIV key_digest(unsigned char *pData,unsigned long nDataLen,unsigned char *pDigestData, unsigned long *pDigestLen)
{
	key_enter_lock_area();
	SysLog("enter key_digest\n");
	int ret = -1;
	int type=3;
	/******************20121214 lch add****************************/
	ret = key_loadLibrary();
	if(ret != 0){
		goto end;
	}
	/******************20121214 lch add****************************/

	SysLog("摘要类型为：%d\n",G_DigestType);

	if(pDigest == NULL){
		SysLog("pDigest 指针为空，退出\n");
		ret = POINTERISNULL;
		goto end;
	}
	switch (type){
	case 1://SCH2算法
		if (pDigestData == NULL){
			*pDigestLen=32;
			ret = 0;
			goto end;
		}
		ret = pDigest(pDigestData,pDigestLen,pData,nDataLen,CALG_SCH2);
		break;

	case 2://ms2-49算法
		if (pDigestData == NULL){
			*pDigestLen=64;
			ret = 0;
			goto end;
		}
		ret = pDigest(pDigestData,pDigestLen,pData,nDataLen,CALG_MS2_49);
		break;
	case 3://sm3算法
		if (pDigestData == NULL){
			*pDigestLen=32;
			ret = 0;
			goto end;
		}
		ret = pDigest(pDigestData,pDigestLen,pData,nDataLen,CALG_SM3);
		break;
	default:
		return -1;
	}
	if (ret !=0 ){
		SysLog("对数据进行摘要时出错,错误码为：%.2x, %d\n",ret,ret);
		ret = HASHFAILED;
	}else{
		SysLog("摘要成功，数据为：\n");
		SysHexLog(pDigestData,*pDigestLen);
	}
	
end:

	SysLog("exit key_digest\n");
	key_leave_lock_area();
	return ret;
}



/***************************2012-12-12 LCH ADD*******************************************************/
/*
功能:
对数据进行非对称加密。
参数:
[in] pData
要加密的数据。
  
[in] nDataLen
要加密的数据的长度。
	
[in] pCertContent
加密公钥。
	  
[in] nCertContentLen
加密公钥的长度。
		
[in-out] pOut
加密输出数据。
		  
[in-out] pOutLen
加密输出数据的长度。
			
返回:
0：成功  非0：相应错误码
			  
注释:
本接口为二次调用接口,设置pOut为NULL, pOutLen为返回的数据的长度,再根据返回的长度为pOut分配内存				
 */
EXPORTDLL int WINAPIV key_asyEnc(unsigned char *pData, unsigned long nDataLen,
								unsigned char *pCertContent,unsigned int nCertContentLen,
								unsigned char *pOut, unsigned long *pOutLen)
{
	key_enter_lock_area();
	SysLog("enter key_AsyEnc\n");
	int ret=-1;
	unsigned char paint[64]={'\0'};
	int paintLen=64;
	unsigned char *pRandom=NULL;
	int	randomLen=0;
	int AlgID = 0;
	KEYVALUE keyidx;
	keyidx.pbData = NULL;
	keyidx.cbData = 0;

	/******************20121214 lch add****************************/
	ret = key_loadLibrary();
	if(ret != 0){
		goto end;
	}
	/******************20121214 lch add****************************/

	if(pData==NULL){
		SysLog("原文数据为空\n");
		ret = PARAERROR;
		goto end;
	}else{
		SysLog("原文数据为：\n");
		SysHexLog(pData,nDataLen);
	}

	if(pCertContent ){
		SysLog("证书为：\n");
		SysHexLog(pCertContent,nCertContentLen);
	}else{
		SysLog("证书为空\n");
		ret = PARAERROR;
		goto end;
	}
	
	if(nCertContentLen == 0){
		SysLog("证书为空\n");
		ret = PARAERROR;
		goto end;
	}

	switch(G_AsyType) {
	case RSA1024:
		AlgID=CALG_RSA_KEYX;
		break;
	case RSA2048:
		AlgID=CALG_RSA_KEYX;
		break;
	case ECC256:
		AlgID=CALG_ECC256_KEYX;
		break;
	case ECC384:
		AlgID=CALG_ECC_KEYX;
		if(nDataLen>61){
			SysLog("原文数据长度有误\n");
			ret = PARAERROR;
			goto end;
		}
		break;
	default:
		break;
	}

	keyidx.cbData=KEY_INDEX_LEN;
	keyidx.pbData=(unsigned char*)malloc(keyidx.cbData);
	if(keyidx.pbData == NULL){
		ret = MALLOC_MEMORY_ERR;
		goto end;
	}
	memset(keyidx.pbData,'\0',keyidx.cbData);
	
	//导入公钥
	ret=key_importPublicKey(AlgID,pCertContent,nCertContentLen,&keyidx);
	if(ret!=0){
		SysLog("导入公钥失败\n");
		ret=IMPORTPUBKEYFAILED;
		goto end;
	}

	if(pAsymmetricEncrypt == NULL){
		SysLog("pAsymmetricEncrypt is NULL\n");
		ret=POINTERISNULL;
		goto end;
	}

	randomLen=61-nDataLen;
	SysLog("随机数长度为：%d\n",randomLen);
	if(randomLen == 0){
	}else{
		pRandom=(unsigned char*)malloc(randomLen*sizeof(unsigned char));
		memset(pRandom,'\0',randomLen);
		ret=key_generateRandom(pRandom,randomLen);
		if(ret!=0){
			SysLog("产生随机数失败!\n");
			goto end;
		}
	}

	paint[0]=0x00;
	paint[1]=0x02;
	if(randomLen != 0){
		memcpy(paint+2,pRandom,randomLen);
	}
	paint[2+randomLen]=0x00;
	memcpy(paint+3+randomLen,pData,nDataLen);
	
	SysLog("组合之后的原文数据为：\n");
	SysHexLog(paint,paintLen);

	if(pOut==NULL){
		ret=pAsymmetricEncrypt(&keyidx, NULL, WH_LAST, NULL, pOutLen, paint, paintLen, AlgID);
		if(ret==0){
			SysLog("第一次加密成功，返回长度为：%d\n",*pOutLen);
			goto end;
		}else{
			SysLog("第一次加密失败，错误码为：%.x\n",ret);
			ret=ASYENCFAILED;
			goto end;
		}
	}else{
		memset(pOut,'\0',*pOutLen);
		ret=pAsymmetricEncrypt(&keyidx, NULL, WH_LAST, pOut, pOutLen, paint, paintLen, AlgID);
		if(ret==0){
			SysLog("第二次加密成功，数据为：\n");
			SysHexLog(pOut,*pOutLen);
		}else{
			SysLog("第二次加密失败，错误码为：%.x\n",ret);
			ret=ASYENCFAILED;
		}
	}

end:

	key_freeBuffer((void **)&(keyidx.pbData));
	key_freeBuffer((void **)&pRandom);

	SysLog("exit key_AsyEnc\n");
	key_leave_lock_area();
	return ret;
}



/***************************2014-02-10 LCH ADD*******************************************************/
/*
功能:
对数据进行非对称加密。
参数:
[in] pData
要加密的数据。
  
[in] nDataLen
要加密的数据的长度。
	
[in] pPublicKey
加密公钥。
	  
[in] nPublicKeyLen
加密公钥的长度。
		
[in-out] pOut
加密输出数据。
		  
[in-out] pOutLen
加密输出数据的长度。
			
返回:
0：成功  非0：相应错误码
			  
注释:
本接口为二次调用接口,设置pOut为NULL, pOutLen为返回的数据的长度,再根据返回的长度为pOut分配内存				
 */
EXPORTDLL int WINAPIV key_asyEnc_ex(unsigned char *pData, unsigned long nDataLen,
								unsigned char *pPublicKey,unsigned int nPublicKeyLen,
								unsigned char *pOut, unsigned long *pOutLen)
{
	key_enter_lock_area();
	SysLog("enter key_asyEnc_ex\n");
	int ret=-1,	AlgID = 0,	paintLen=64,	randomLen=0;
	unsigned char paint[64]={'\0'};
	unsigned char *pRandom=NULL;
	KEYVALUE InPubKey,	keyidx;
	char temp[512];
	unsigned int size = nPublicKeyLen;
	BLOBHEADER pkheader;

	InPubKey.pbData = NULL;
	InPubKey.cbData = NULL;
	keyidx.pbData = NULL;
	keyidx.cbData = 0;

	/******************20121214 lch add****************************/
	ret = key_loadLibrary();
	if(ret != 0){
		goto end;
	}
	/******************20121214 lch add****************************/

	if(pAsymmetricEncrypt == NULL){
		SysLog("pAsymmetricEncrypt is NULL\n");
		ret = POINTERISNULL;
		goto end;
	}

	if(pData==NULL){
		SysLog("原文数据为空\n");
		ret = PARAERROR;
		goto end;
	}else{
		SysLog("原文数据为：\n");
		SysHexLog(pData,nDataLen);
	}

	switch(G_AsyType) {
	case RSA1024:
		AlgID=CALG_RSA_KEYX;
		break;
	case RSA2048:
		AlgID=CALG_RSA_KEYX;
		break;
	case ECC256:
		AlgID=CALG_ECC256_KEYX;
		break;
	case ECC384:
		AlgID=CALG_ECC_KEYX;
		if(nDataLen>60){
			SysLog("原文数据长度有误\n");
			ret = PARAERROR;
			goto end;
		}
		break;
	default:
		break;
	}

	//导入公钥
	memset(temp,'\0',512);
	pkheader.bType = PUBLICKEYBLOB;
	pkheader.bVersion = CUR_BLOB_VERSION;
	pkheader.reserved = 0;
	pkheader.aiKeyAlg = AlgID;

	memcpy(temp,&pkheader,sizeof(BLOBHEADER));
	if(size == 97)
		memcpy(temp+sizeof(BLOBHEADER),pPublicKey+1,--size);
	else
		memcpy(temp+sizeof(BLOBHEADER),pPublicKey,size);

	InPubKey.cbData=size+sizeof(BLOBHEADER);
	InPubKey.pbData=(unsigned char*)malloc(InPubKey.cbData*sizeof(unsigned char));
	if(InPubKey.pbData == NULL){
		ret = MALLOC_MEMORY_ERR;
		goto end;
	}
	memset(InPubKey.pbData,'\0',InPubKey.cbData);
	memcpy((char *)InPubKey.pbData,temp,InPubKey.cbData);

	keyidx.cbData=KEY_INDEX_LEN;
	keyidx.pbData=(unsigned char*)malloc(keyidx.cbData);
	if(keyidx.pbData == NULL){
		ret = MALLOC_MEMORY_ERR;
		goto end;
	}
	memset(keyidx.pbData,'\0',keyidx.cbData);

	ret = pImportPublicKey(&InPubKey,AlgID,&keyidx);
	if ( ret == 0 ){
		SysLog("import success!\n");
	}else{
		SysLog("ImportPublicKey failed ,the state is: %x\n",ret);
		ret = IMPORTPUBKEYFAILED;
		goto end;
	}

	randomLen=61-nDataLen;
	SysLog("随机数长度为：%d\n",randomLen);
	if(randomLen == 0)
	{

	}else{
		pRandom=(unsigned char*)malloc(randomLen*sizeof(unsigned char));
		memset(pRandom,'\0',randomLen);
		ret=key_generateRandom(pRandom,randomLen);
		if(ret != 0){
			SysLog("产生随机数失败!\n");
			ret = GENRANDOMFAILED;
			goto end;
		}
	}

	paint[0]=0x00;
	paint[1]=0x02;
	if(randomLen != 0){
		memcpy(paint+2,pRandom,randomLen);
	}
	paint[2+randomLen]=0x00;
	memcpy(paint+3+randomLen,pData,nDataLen);

	SysLog("组合之后的原文数据为：\n");
	SysHexLog(paint,paintLen);
	
	if(pOut==NULL){
		ret=pAsymmetricEncrypt(&keyidx, NULL, WH_LAST, NULL, pOutLen, paint, paintLen, AlgID);
		if(ret==0){
			SysLog("第一次加密成功，返回长度为：%d\n",*pOutLen);
			goto end;
		}else{
			SysLog("第一次加密失败，错误码为：%.x\n",ret);
			ret = ASYENCFAILED;
			goto end;
		}
	}else{
		memset(pOut,'\0',*pOutLen);
		ret=pAsymmetricEncrypt(&keyidx, NULL, WH_LAST, pOut, pOutLen, paint, paintLen, AlgID);
		if(ret == 0){
			SysLog("第二次加密成功，数据为：\n");
			SysHexLog(pOut,*pOutLen);
		}else{
			SysLog("第二次加密失败，错误码为：%.x\n",ret);
			ret=ASYENCFAILED;
		}
	}

end:
	key_freeBuffer((void **)&(InPubKey.pbData));
	key_freeBuffer((void **)&(keyidx.pbData));
	key_freeBuffer((void **)&pRandom);

	SysLog("exit key_AsyEnc_ex\n");
	key_leave_lock_area();
	return ret;
}



/*
功能:
对数据进行非对称解密。
参数:
[in] pData
要解密的数据。
  
[in] nDataLen
要解密的数据的长度。
	
[in-out] pOut
解密输出数据。
	  
[in-out] pOutLen
解密输出数据的长度。
		
返回:
0：成功  非0：相应错误码
		  
注释:
本接口为二次调用接口,设置pOut为NULL, pOutLen为返回的数据的长度,再根据返回的长度为pOut分配内存。
 */
EXPORTDLL int WINAPIV key_asyDec(unsigned char *pData, unsigned long nDataLen, 
	unsigned char *pOut, unsigned long *pOutLen)
{
	key_enter_lock_area();
	SysLog("enter key_AsyDec\n");
	int ret=-1,	randomLen=0, AlgID = 0;
	unsigned char *decData=NULL;
	unsigned long decDataLen=0;
	unsigned int i=0;	
	KEYVALUE  PublicSigKey,PrivateSigKey,PublicExKey,PrivateExKey;
	PublicSigKey.cbData=0;	PublicSigKey.pbData=NULL;
	PrivateSigKey.cbData=0;	PrivateSigKey.pbData=NULL;

	PublicExKey.cbData=0;	PublicExKey.pbData=NULL;
	PrivateExKey.cbData=0;	PrivateExKey.pbData=NULL;

	/******************20121214 lch add****************************/
	ret = key_loadLibrary();
	if(ret != 0){
		goto end;
	}
	/******************20121214 lch add****************************/

	if(pFindContainerAndKeypair == NULL ){
		SysLog("pFindContainerAndKeypair is NULL\n");
		ret = POINTERISNULL;
		goto end;
	}

	if(pAsymmetricDecrypt == NULL){
		SysLog("pAsymmetricDecrypt is NULL\n");
		ret = POINTERISNULL;
		goto end;
	}

	if((pData == NULL) || (pData[0] == '\0')){
		SysLog("para pData is NULL\n");
		ret = PARAERROR;
		goto end;
	}

	SysLog("encrypt data is:\n");
	SysHexLog(pData,nDataLen);

	switch(G_AsyType) {
	case RSA1024:
		AlgID=CALG_RSA_KEYX;
		break;
	case RSA2048:
		AlgID=CALG_RSA_KEYX;
		break;
	case ECC256:
		AlgID=CALG_ECC256_KEYX;
		break;
	case ECC384:
		AlgID=CALG_ECC_KEYX;
		//if(nDataLen!=224){
		//	SysLog("密文数据长度有误\n");
		//	ret = PARAERROR;
		//	goto end;
		//}
		break;
	default:
		break;
	}

	//取保护密钥
	ret = pFindContainerAndKeypair(ECC_CONTAINER_CHOICE,&PublicSigKey,&PrivateSigKey,&PublicExKey,&PrivateExKey);
	if( ret != 0 ){
		SysLog("The findContainerAndKeypair ret is:%x\n",ret);
		ret = FINDCONTAINERFAILED;
		goto end;
	}
	PublicSigKey.pbData = (unsigned char*)malloc(sizeof(unsigned char)*PublicSigKey.cbData);
	PrivateSigKey.pbData = (unsigned char*)malloc(sizeof(unsigned char)*PrivateSigKey.cbData);
	PublicExKey.pbData = (unsigned char*)malloc(sizeof(unsigned char)*PublicExKey.cbData);
	PrivateExKey.pbData = (unsigned char*)malloc(sizeof(unsigned char)*PrivateExKey.cbData);

	memset(PublicSigKey.pbData,'\0',PublicSigKey.cbData);
	memset(PrivateSigKey.pbData,'\0',PrivateSigKey.cbData);
	memset(PublicExKey.pbData,'\0',PublicExKey.cbData);
	memset(PrivateExKey.pbData,'\0',PrivateExKey.cbData);
	ret = pFindContainerAndKeypair(ECC_CONTAINER_CHOICE,&PublicSigKey,&PrivateSigKey,&PublicExKey,&PrivateExKey);
	if( ret != 0 ){
		SysLog("The findContainerAndKeypair ret is:%x\n",ret);
		ret = FINDCONTAINERFAILED;
		goto end;
	}

	if(pOut==NULL){
		ret = pAsymmetricDecrypt(&PrivateExKey, NULL, WH_LAST, decData, &decDataLen, pData, nDataLen, AlgID);
		if( ret != 0 ){
			SysLog("The first AsymmetricDecrypt failed,ret is:%.2x,%d\n",ret,ret);
			ret=ASYDECFAILED;
			goto end;
		}

		SysLog("The first AsymmetricDecrypt  success,decrpt len is:%d\n",decDataLen);
		decData=(unsigned char*)malloc(decDataLen*sizeof(unsigned char));
		if(decData == NULL){
			ret = MALLOC_MEMORY_ERR;
			goto end;
		}
		memset(decData,'\0',decDataLen);
		ret = pAsymmetricDecrypt(&PrivateExKey, NULL, WH_LAST, decData, &decDataLen, pData, nDataLen, AlgID);
		if(ret!=0){
			SysLog("解密错误\n");
			ret=ASYDECFAILED;
			goto end;
		}
		SysLog("The second AsymmetricDecrypt  success，paint is:\n");
		SysHexLog(decData,decDataLen);
		//对数据进行拆解
		if(decData[0]==0x00 && decData[1]==0x02){
			for(i=2;i<decDataLen;i++){
				if(decData[i]==0x00)
					break;
				randomLen++;
			}
			*pOutLen=decDataLen-3-randomLen;
			SysLog("原文长度为：%d\n",*pOutLen);
			goto end;

		}else{
			SysLog("对数据解析失败，解密错误\n");
			ret=ASYDECFAILED;
			goto end;
		}

	}else{
		memset(pOut,'\0',*pOutLen);
		ret = pAsymmetricDecrypt(&PrivateExKey, NULL, WH_LAST, decData, &decDataLen, pData, nDataLen, AlgID);
		if( ret != 0 ){
			SysLog("The first AsymmetricDecrypt failed,ret is:%.2x,%d\n",ret,ret);
			ret=ASYDECFAILED;
			goto end;
		}

		SysLog("The first AsymmetricDecrypt  success,decrpt len is:%d\n",decDataLen);
		decData=(unsigned char*)malloc(decDataLen*sizeof(unsigned char));
		if(decData == NULL){
			ret = MALLOC_MEMORY_ERR;
			goto end;
		}
		memset(decData,'\0',decDataLen);
		ret = pAsymmetricDecrypt(&PrivateExKey, NULL, WH_LAST, decData, &decDataLen, pData, nDataLen, AlgID);
		if(ret!=0){
			SysLog("解密错误\n");
			ret=ASYDECFAILED;
			goto end;
		}
		SysLog("The second AsymmetricDecrypt  success\n");
		//对数据进行拆解
		if(decData[0]==0x00 && decData[1]==0x02){
			for(i=2;i<decDataLen;i++){
				if(decData[i]==0x00)
					break;
				randomLen++;
			}
			memcpy(pOut,decData+(randomLen+3),*pOutLen);
			if(pOut){
				SysLog("解密成功，原文为：\n");
				SysHexLog(pOut,*pOutLen);
			}
		}else{
			SysLog("对数据解析失败，解密错误\n");
			ret=ASYDECFAILED;
			goto end;
		}

	}	
end:

	key_freeBuffer((void **)&(PublicSigKey.pbData));
	key_freeBuffer((void **)&(PrivateSigKey.pbData));
	key_freeBuffer((void **)&(PublicExKey.pbData));
	key_freeBuffer((void **)&(PrivateExKey.pbData));
	key_freeBuffer((void **)&decData);

	SysLog("exit key_AsyDec\n");
	key_leave_lock_area();
	return ret;
}



/*****************2013-01-17 lch add********************
/*解锁被锁的USB-KEY
*/
EXPORTDLL int  WINAPIV key_unLock(char *  password)
{
	key_enter_lock_area();
	SysLog("enter key_unLock\n");
	int ret=-1;

	/******************20121214 lch add****************************/
	ret = key_loadLibrary();
	if(ret != 0){
		goto end;
	}
	/******************20121214 lch add****************************/

	if(password){
		SysLog("解锁时，输入的口令为：\n");
		SysHexLog((unsigned char*)password,strlen(password));
	}
	if(pUnLock)
		ret = pUnLock((BYTE*)password,8);
	else{
		SysLog("pUnLock地址为空\n");
		ret=POINTERISNULL;
	}
	if(ret==0){
		SysLog("解锁成功\n");
	}else{
		SysLog("解锁失败，错误码为：%.2x\n",ret);
		ret=UNLOCKFAILED;
	}

end:

	SysLog("exit key_unLock\n");
	key_leave_lock_area();
	return ret;
}


/*****************2013-03-14 lch add********************/
/*
功能: 
获取指定索引的证书中的uid。

参数:
[in] nIndex
证书的索引，默认为-1。

[out] Uid
证书中的uid。

返回:
  0：成功  非0：相应错误码。

注释:
 该接口获取指定索引的用户签名证书中的uid。若不输索引，则系统默认为当前插入的唯一的USB-KEY中的用户签名证书中的uid。	
 */
EXPORTDLL int WINAPIV key_getUid(unsigned char Uid[256],int nIndex)
{
	key_enter_lock_area();
	SysLog("enter key_getUid\n");
	SysLog("nIndex is:%d\n",nIndex);

	int ret = -1;
	/******************20121214 lch add****************************/
	ret = key_loadLibrary();
	if(ret != 0){
		goto end;
	}
	/******************20121214 lch add****************************/

	if(nIndex==-1){
		if(G_DevNum==2){
			SysLog("当参数index为-1时，设备数量不唯一\n");
			ret=PARAERROR;
		}else if(G_DevNum==1){
			memset(Uid,'\0',256);
			memcpy(Uid,keyInfo[0].UserId,strlen(keyInfo[0].UserId));
			if(keyInfo[0].UserId[0]!='\0'){
				SysLog("uid为：%s\n",keyInfo[0].UserId);
				ret=0;
			}else{
				SysLog("uid为空\n");
				ret=GETUSERIDFAILED;
			}
		}else if(G_DevNum==0){
			SysLog("设备数量为0\n");
			ret=DEVNUMISZERO;
		}
	}else{	
		if(G_UserIdNum==1){
			for(int i=0;i<G_DevNum;i++){
				if(keyInfo[i].UserId[0]!='\0'){
					SysLog("uid数量为1时，得到的uid为：\n");	
					SysHexLog((unsigned char *)(keyInfo[i].UserId),strlen(keyInfo[i].UserId));
					SysLog("\n");
					memcpy(Uid,keyInfo[i].UserId,strlen(keyInfo[i].UserId));
				}
			}
		}else if(G_UserIdNum==2){
			if(keyInfo[nIndex].UserId[0]!='\0'){
				SysLog("uid数量为2时，得到的uid为：\n");
				SysHexLog((unsigned char *)(keyInfo[nIndex].UserId),strlen(keyInfo[nIndex].UserId));
				SysLog("\n");
				memcpy(Uid,keyInfo[nIndex].UserId,strlen(keyInfo[nIndex].UserId));
			}else{
				SysLog("uid数量为2时，得到的uid为空\n");
				memset(Uid,'\0',256);
			}
		}else if(G_UserIdNum==0){
			SysLog("得到的uid数量为0\n");
			memset(Uid,'\0',256);
		}
	}

end:

	SysLog("exit key_getUid\n");
	key_leave_lock_area();
	return ret;
}

/*****************2013-10-23 Feng add********************/
/*
功能: 
获取usb-key的SN。

参数:
[out] DeviceSN
证书中的SN。

返回:
  0：成功  非0：相应错误码。

注释:
 该接口获取usb-key设备的SN。	
 */
EXPORTDLL int WINAPIV key_getDeviceSN(char DeviceSN[256])
{
	key_enter_lock_area();
	SysLog("Enter key_getDeviceSN\n");
	// Get dll function pointer
	// Dll name: HXT_FileAPI.dll
	// Function form: int WINAPIV HXT_GetDeviceSN(char *szDevSN, int *nSnLen);
	int ret = -1;
	int isFailed = 0;
	int nSnLen = 0;
	char *tmpDevSN = NULL;

	if(G_hSnDll == NULL){
		G_hSnDll = LoadLibrary(DEVSNDLL);
	}

	if(G_hSnDll != NULL){
		pSnFunction = (pSnFunctionType)GetProcAddress(G_hSnDll, "HXT_GetDeviceSN");
	}else{
		SysLog("Load HXT_FileAPI.dll failed!\n");
		ret = LOADGETDEVSNDLLFAILED;
		goto end;
	}

	if(pSnFunction == NULL){
		SysLog("pSnFunction is null\n");
		ret = POINTERISNULL;
		goto end;
	}

	// Get SN through DLL interface
	isFailed = pSnFunction(tmpDevSN, &nSnLen);
	if(isFailed == 0){
		tmpDevSN = (char *)malloc(nSnLen*sizeof(unsigned char));
		memset(tmpDevSN,'\0',nSnLen);
		isFailed = pSnFunction(tmpDevSN, &nSnLen);
	}

	if(isFailed){
		SysLog("Call HXT_GetDeviceSN function failed,errno is:%x\n",isFailed);
		ret = GETDEVSNFAILED;
		goto end;
	}

	SysLog("deviceSN len is:%d\n",nSnLen);
	SysHexLog((unsigned char*)tmpDevSN,nSnLen);

	memcpy(DeviceSN,tmpDevSN,nSnLen);
	ret = 0;

end:

	key_freeBuffer((void **)&tmpDevSN);

	SysLog("exit key_getDeviceSN\n");
	key_leave_lock_area();
	return ret;
}


/*
功能: 
获取指定索引的证书。

参数: 
[in-out] certContent
证书的内容。
[in-out] certContentLen
证书内容长度。
[in] nIndex
证书的索引，默认为-1，从0开始。

返回:
0：成功  非0：相应错误码。

注释:
本接口为二次调用接口,设置certContent为NULL, certContentLen为返回的数据的长度,再根据返回的长度为certContent分配内存。
该接口获取指定索引的用户证书内容。若不输索引，则系统默认为当前插入的唯一的USB-KEY中的用户证书。
 */
EXPORTDLL int WINAPIV key_getCert (char *certContent, int *certContentLen,int nIndex)
{
	key_enter_lock_area();
	SysLog("enter key_getCert\n");
	SysLog("nIndex is:%d\n",nIndex);
	int ret= -1;
	/******************20121214 lch add****************************/
	ret = key_loadLibrary();
	if(ret != 0){
		goto end;
	}
	/******************20121214 lch add****************************/

	if(nIndex==-1){
		if(G_DevNum==2){
			SysLog("当参数index为-1时，设备数量不唯一\n");
			ret=PARAERROR;
		}else if(G_DevNum==1){
			if(certContent == NULL){
				*certContentLen = keyInfo[0].nCertLen;
				ret = 0;
				goto end;
			}else{
				memcpy(certContent,keyInfo[0].Cert,keyInfo[0].nCertLen);
				if(keyInfo[0].Cert[0]!='\0'){
					SysLog("cert is：%s\n",keyInfo[0].Cert);
					ret = 0;
				}else{
					SysLog("证书为空\n");
					ret = CERTISNULL;
				}
			}
		}else if(G_DevNum==0){
			SysLog("设备数量为0\n");
			ret = DEVNUMISZERO;
		}
	}else{	
		if(G_CertNum == 1){
			for(int i=0;i<G_DevNum;i++){
				if(keyInfo[i].Cert[0]!='\0'){
					if(certContent == NULL){
						*certContentLen = keyInfo[0].nCertLen;
						ret = 0;
						goto end;
					}else{
						SysLog("签名证书数量为1时，得到的证书为：\n");	
						SysHexLog((unsigned char *)(keyInfo[i].Cert),keyInfo[i].nCertLen);
						SysLog("\n");
						memcpy(certContent,keyInfo[i].Cert,keyInfo[i].nCertLen);
					}
				}
			}
		}else if(G_CertNum==2){
			if(keyInfo[nIndex].Cert[0]!='\0'){
				if(certContent == NULL){
					*certContentLen = keyInfo[0].nCertLen;
					ret = 0;
					goto end;
				}else{
					SysLog("签名证书数量为2时，得到的证书为：\n");
					SysHexLog((unsigned char *)(keyInfo[nIndex].Cert),keyInfo[nIndex].nCertLen);
					SysLog("\n");
					memcpy(certContent,keyInfo[nIndex].Cert,keyInfo[nIndex].nCertLen);
				}
			}else{
				SysLog("签名证书数量为2时，得到索引为：%d的证书序为空\n",nIndex);
			}
		}else if(G_CertNum==0){
			SysLog("得到的签名证书数量为0\n");
		}
	}

end:

	SysLog("exit key_getCert\n");
	key_leave_lock_area();
	return ret;
}

/*
功能:
对签名数据进行验证。
参数:
[in] pSignedData
签名数据。
[int] dwSignedLen
签名数据的长度。
[int] pData
原文数据。
[in] dwDataLen
原文数据的长度。
[int] pPubKey
公钥数据。
[in] nPubKeyLen
公钥数据的长度。
返回:
0：成功  非0：相应错误码
注释:
目前系统采用的ECC算法公钥长度为96字节。
*/
EXPORTDLL int WINAPIV key_verify_ex1(unsigned char *pSignedData, long dwSignedLen,unsigned char * pData,long dwDataLen, 
	unsigned char* pPubKey,long nPubKeyLen)
/*{
	key_enter_lock_area();
	SysLog("enter key_verify_ex1\n");
	int ret = -1;
	unsigned char	*decodeSignData=NULL, *hash=NULL, *hashPad=NULL;
	int decodeSignDataLen=0,	hashPadlen=48;
	unsigned int	i=0, j  = 0;
	long size = nPubKeyLen;
	unsigned long hashlen=0;
	KEYVALUE keyidx;
	KEYVALUE InPubKey;
	BLOBHEADER pkheader;
	char temp[512];

	keyidx.pbData = NULL;
	keyidx.cbData = 0;
	InPubKey.pbData = NULL;
	InPubKey.cbData = 0;

	
	ret = key_loadLibrary();
	if(ret != 0){
		goto end;
	}

	if(pAsymmetricVerifySignEx == NULL){
		SysLog("pAsymmetricVerifySignEx 指针为空\n");
		ret = POINTERISNULL;
		goto end;
	}

	keyidx.cbData=KEY_INDEX_LEN;
	keyidx.pbData = (unsigned char*)malloc(KEY_INDEX_LEN);
	if(keyidx.pbData == NULL){
		ret = MALLOC_MEMORY_ERR;
		goto end;
	}
	memset(keyidx.pbData,'\0',KEY_INDEX_LEN);

	pkheader.bType = PUBLICKEYBLOB;
	pkheader.bVersion = CUR_BLOB_VERSION;
	pkheader.reserved = 0;
	pkheader.aiKeyAlg = CALG_ECC_SIGN;

	memset(temp,'\0',512);
	if(dwSignedLen!=0){
		SysLog("签名数据长度为：%d\n",dwSignedLen);
	}else{
		SysLog("参数签名数据长度为0\n");
		ret=PARAERROR;
		goto end;
	}

	if(dwDataLen!=0){
		SysLog("明文数据长度为：%d\n",dwDataLen);
	}else{
		SysLog("参数明文数据长度为0\n");
		ret=PARAERROR;
		goto end;
	}

	if(nPubKeyLen){
		SysLog("公钥长度为：%d\n",nPubKeyLen);
	}else{
		SysLog("公钥长度为0\n");
		ret=PARAERROR;
		goto end;
	}

	if(pPubKey){
		SysLog("公钥内容为：\n");
		SysHexLog((unsigned char*)pPubKey,nPubKeyLen);
	}else{
		SysLog("公钥内容为空\n");
		ret=PARAERROR;
		goto end;
	}

	if(pSignedData){
		SysLog("base64签名数据为：\n");
		SysHexLog(pSignedData,dwSignedLen);
	}else{
		SysLog("参数签名内容为空\n");
		ret=PARAERROR;
		goto end;
	}

	decodeSignData=base64_decode((char *)pSignedData,&decodeSignDataLen);
	if(decodeSignData){
		SysLog("解码后签名数据为：\n");
		SysHexLog(decodeSignData,decodeSignDataLen);
	}

	//验证签名
	SysLog("验证签名\n");

	//导入公钥
	memcpy(temp,&pkheader,sizeof(BLOBHEADER));
	if(size == 97)
		memcpy(temp+sizeof(BLOBHEADER),pPubKey+1,--size);
	else
		memcpy(temp+sizeof(BLOBHEADER),pPubKey,size);


	InPubKey.cbData=size+sizeof(BLOBHEADER);
	InPubKey.pbData=(unsigned char*)malloc(InPubKey.cbData*sizeof(unsigned char));
	memset(InPubKey.pbData,'\0',InPubKey.cbData);
	memcpy((char *)InPubKey.pbData,temp,InPubKey.cbData);

	ret = pImportPublicKey(&InPubKey,CALG_ECC_SIGN,&keyidx);
	if ( ret == 0 ){
		SysLog("import success!\n");
	}else{
		SysLog("ImportPublicKey failed ,the state is: %.2x,%d\n",ret,ret);
		ret=IMPORTPUBKEYFAILED;
		goto end;
	}

	//对原文进行hash处理				
	if (  (ret=KeyDigest(pData, dwDataLen,hash,&hashlen,2))==0 ){
		hash = (unsigned char*)malloc(hashlen);
		if(hash == NULL){
			ret = MALLOC_MEMORY_ERR;
			goto end;
		}
		memset(hash,'\0',hashlen);
		if (  (ret=KeyDigest(pData, dwDataLen,hash,&hashlen,2))==0 ){
			//对摘要进行截除处理(特殊的填充)
			hashPad = (unsigned char*)malloc(hashPadlen);
			if(hashPad == NULL){
				goto end;
			}
			memset(hashPad,'\0',hashPadlen);
			memcpy(hashPad,hash,hashPadlen);
			if(hashPad){
				SysLog("摘要数据为:\n");
				SysHexLog(hashPad,hashPadlen);
			}
			//验证签名

			if ( (ret=pAsymmetricVerifySignEx(&keyidx,hashPad,hashPadlen,decodeSignData,decodeSignDataLen))!=0){
				SysLog("pAsymmetricVerifySignEx失败，验证签名失败!，错误码为：%.2x\n",ret);
				ret=VERIFYFAILED;
			}else{
				SysLog("验证签名成功\n");	
			}

		}else{
			SysLog("第二次hash失败\n");
			ret=HASHFAILED;
		}
	}else{
		SysLog("第一次hash失败\n");
		ret=HASHFAILED;
	}


end:

	key_freeBuffer((void **)&hash);
	key_freeBuffer((void **)&hashPad);
	key_freeBuffer((void **)&(keyidx.pbData));
	key_freeBuffer((void **)&decodeSignData);

	SysLog("exit key_verify_ex1\n");
	key_leave_lock_area();
	return ret;
}*/

{
	SysLog("enter key_verify_ex1\n");
#ifdef MUTEXFLAG
	WaitForSingleObject(hMutex,INFINITE);
#endif
	int ret = -1;
	unsigned char	*decodeSignData=NULL, *hash=NULL, *hashPad=NULL;
	int decodeSignDataLen=0,	hashPadlen=48;
	unsigned int	i=0, j  = 0;
	long size = nPubKeyLen;
	unsigned long hashlen=0;
	KEYVALUE keyidx;
	KEYVALUE InPubKey;
	BLOBHEADER pkheader;
	char temp[512];
	unsigned char *ensuleData = NULL;
	int ensuleDataLen = 0;

	ret = key_loadLibrary();
	if(ret != 0)
	{
		goto end;
	}


	if(pAsymmetricVerifySignEx == NULL)
	{
		SysLog("pAsymmetricVerifySignEx 指针为空\n");
		ret = POINTERISNULL;
		goto end;
	}

	keyidx.cbData=4;
	keyidx.pbData = (unsigned char*)malloc(4);
	memset(keyidx.pbData,'\0',4);

	pkheader.bType = PUBLICKEYBLOB;
	pkheader.bVersion = CUR_BLOB_VERSION;
	pkheader.reserved = 0;
	pkheader.aiKeyAlg = CALG_ECC_SIGN;

	memset(temp,'\0',512);
	if(dwSignedLen!=0){
		SysLog("签名数据长度为：%d\n",dwSignedLen);
	}
	else
	{
		SysLog("参数签名数据长度为0\n");
		ret=PARAERROR;
		goto end;
	}

	if(dwDataLen!=0){
		SysLog("明文数据长度为：%d\n",dwDataLen);
	}
	else
	{
		SysLog("参数明文数据长度为0\n");
		ret=PARAERROR;
		goto end;
	}

	if(nPubKeyLen)
	{
		SysLog("公钥长度为：%d\n",nPubKeyLen);
	}
	else
	{
		SysLog("公钥长度为0\n");
		ret=PARAERROR;
		goto end;
	}

	if(pPubKey)
	{
		SysLog("公钥内容为：\n");
		SysHexLog((unsigned char*)pPubKey,nPubKeyLen);
	}
	else
	{
		SysLog("公钥内容为空\n");
		ret=PARAERROR;
		goto end;
	}

	if(pSignedData)
	{
		SysLog("base64签名数据为：\n");
		SysHexLog(pSignedData,dwSignedLen);
	}
	else
	{
		SysLog("参数签名内容为空\n");
		ret=PARAERROR;
		goto end;
	}

	decodeSignData=base64_decode((char *)pSignedData,&decodeSignDataLen);
	if(decodeSignData)
	{
		SysLog("解码后签名数据为：\n");
		SysHexLog(decodeSignData,decodeSignDataLen);
	}

	//验证签名
	SysLog("验证签名\n");

	//导入公钥
	memcpy(temp,&pkheader,sizeof(BLOBHEADER));
	if(size == 97)
		memcpy(temp+sizeof(BLOBHEADER),pPubKey+1,--size);
	else
		memcpy(temp+sizeof(BLOBHEADER),pPubKey,size);


	InPubKey.cbData=size+sizeof(BLOBHEADER);
	InPubKey.pbData=(unsigned char*)malloc(InPubKey.cbData*sizeof(unsigned char));
	memset(InPubKey.pbData,'\0',InPubKey.cbData);
	memcpy((char *)InPubKey.pbData,temp,InPubKey.cbData);
	int digest = 0;
	if (G_NowDev==2)
	{
		ret = pImportPublicKey(&InPubKey,CALG_ECC256_SIGN,&keyidx);
		digest = 3;
		hashPadlen = 32;

		int asyAlgType = 3;
		int state = 0;
		state = ensule_indata(asyAlgType,(char *)pData,dwDataLen,NULL,&ensuleDataLen);
		if(state != 0){
			goto end;
		}
		ensuleData = (unsigned char *)malloc(ensuleDataLen*sizeof(unsigned char));
		memset(ensuleData,'\0',ensuleDataLen);
		state = ensule_indata(asyAlgType,(char *)pData,dwDataLen,ensuleData,&ensuleDataLen);
		if(state != 0){
			goto end;
		}
		pData = ensuleData;
		dwDataLen = ensuleDataLen;
	}
	else
	{
		ret = pImportPublicKey(&InPubKey,CALG_ECC_SIGN,&keyidx);
		digest = 2;
		hashPadlen = 48;
	}
	if ( ret == 0 ){
		SysLog("import success!\n");
	}
	else
	{
		SysLog("ImportPublicKey failed ,the state is: %x\n",ret);
		ret=IMPORTPUBKEYFAILED;
		goto end;
	}

	//对原文进行hash处理				

	if (  (ret=KeyDigest(pData, dwDataLen,hash,&hashlen,digest))==0 )
	{
		hash = (unsigned char*)malloc(hashlen);
		memset(hash,'\0',hashlen);
		if (  (ret=KeyDigest(pData, dwDataLen,hash,&hashlen,digest))==0 )//获取摘要
		{
			//对摘要进行截除处理(特殊的填充)
			hashPad = (unsigned char*)malloc(hashPadlen);
			if(hashPad == NULL)
			{
				goto end;
			}
			memset(hashPad,'\0',hashPadlen);
			memcpy(hashPad,hash,hashPadlen);
			if(hashPad)
			{
				SysLog("摘要数据为:\n");
				SysHexLog(hashPad,hashPadlen);
			}
			//验证签名

			if ( (ret=pAsymmetricVerifySignEx(&keyidx,hashPad,hashPadlen,decodeSignData,decodeSignDataLen))!=0)
			{
				SysLog("pAsymmetricVerifySignEx失败，验证签名失败!，错误码为：%.2x\n",ret);
				ret=VERIFYFAILED;
			}
			else
			{
				SysLog("验证签名成功\n");	
			}

		}
		else
		{
			SysLog("第二次hash失败\n");
			ret=HASHFAILED;
		}
	}
	else
	{
		SysLog("第一次hash失败\n");
		ret=HASHFAILED;
	}


end:

	key_freeBuffer((void **)&hash);
	key_freeBuffer((void **)&hashPad);
	key_freeBuffer((void **)&(keyidx.pbData));
	key_freeBuffer((void **)&decodeSignData);

#ifdef MUTEXFLAG
	ReleaseMutex(hMutex);
#endif
	SysLog("exit key_verify_ex1\n");
	return ret;
}


/*
功能:
对签名数据进行验证(原文无需hash)。

参数:
[in] pSignedData
签名数据。

[int] dwSignedLen
签名数据的长度。

[int] pData
原文数据。

[in] dwDataLen
原文数据的长度。

[int] pPubKey
公钥数据。

[in] nPubKeyLen
公钥数据的长度。


返回:
0：成功  非0：相应错误码

注释:
目前系统采用的ECC算法公钥长度为96字节。
*/
EXPORTDLL int  WINAPIV key_verify_ex1_nohash(unsigned char *pSignedData, long dwSignedLen,unsigned char * pData,long dwDataLen, unsigned char* pPubKey,long nPubKeyLen)
{
	key_enter_lock_area();
	SysLog("enter key_verify_ex1_nohash\n");
	int ret = -1;
	unsigned int	i=0,j = 0;
	long size = nPubKeyLen;
	KEYVALUE keyidx;
	KEYVALUE InPubKey;
	BLOBHEADER pkheader;
	char temp[512];

	keyidx.pbData = NULL;
	keyidx.cbData = 0;
	InPubKey.pbData = NULL;
	InPubKey.cbData = 0;

	/******************20121214 lch add****************************/
	ret = key_loadLibrary();
	if(ret != 0){
		goto end;
	}

	if(pAsymmetricVerifySignEx == NULL){
		SysLog("pAsymmetricVerifySignEx 指针为空\n");
		ret = POINTERISNULL;
		goto end;
	}

	if(dwDataLen < 48){
		SysLog("原文数据长度有问题，不应小于48字节\n");
		ret = PARAERROR;
		goto end;
	}
	
	keyidx.cbData=KEY_INDEX_LEN;
	keyidx.pbData = (unsigned char*)malloc(KEY_INDEX_LEN);
	if(keyidx.pbData == NULL){
		ret = MALLOC_MEMORY_ERR;
		goto end;
	}
	memset(keyidx.pbData,'\0',KEY_INDEX_LEN);

	pkheader.bType = PUBLICKEYBLOB;
	pkheader.bVersion = CUR_BLOB_VERSION;
	pkheader.reserved = 0;
	pkheader.aiKeyAlg = CALG_ECC_SIGN;

	memset(temp,'\0',512);

	if(dwSignedLen!=0){
		SysLog("签名数据长度为：%d\n",dwSignedLen);
	}else{
		SysLog("参数签名数据长度为0\n");
		ret=PARAERROR;
		goto end;
	}

	if(dwDataLen!=0){
		SysLog("明文数据长度为：%d\n",dwDataLen);
	}else{
		SysLog("参数明文数据长度为0\n");
		ret=PARAERROR;
		goto end;
	}

	if(nPubKeyLen){
		SysLog("公钥长度为：%d\n",nPubKeyLen);
	}else{
		SysLog("公钥长度为0\n");
		ret=PARAERROR;
		goto end;
	}

	if(pSignedData){
		SysLog("签名值为：\n");
		SysHexLog((unsigned char*)pSignedData,dwSignedLen);
	}else{
		SysLog("签名值为空\n");
		ret = PARAERROR;
		goto end;
	}

	if(pData){
		SysLog("原文为：\n");
		SysHexLog((unsigned char*)pData,dwDataLen);
	}else{
		SysLog("原文为空\n");
		ret=PARAERROR;
		goto end;
	}

	if(pPubKey){
		SysLog("公钥内容为：\n");
		SysHexLog((unsigned char*)pPubKey,nPubKeyLen);
	}else{
		SysLog("公钥内容为空\n");
		ret=PARAERROR;
		goto end;
	}
	//验证签名
	SysLog("验证签名\n");
	//导入公钥
	memcpy(temp,&pkheader,sizeof(BLOBHEADER));
	if(size == 97)
		memcpy(temp+sizeof(BLOBHEADER),pPubKey+1,--size);
	else
		memcpy(temp+sizeof(BLOBHEADER),pPubKey,size);


	InPubKey.cbData=size+sizeof(BLOBHEADER);
	InPubKey.pbData=(unsigned char*)malloc(InPubKey.cbData*sizeof(unsigned char));
	memset(InPubKey.pbData,'\0',InPubKey.cbData);
	memcpy((char *)InPubKey.pbData,temp,InPubKey.cbData);

	ret = pImportPublicKey(&InPubKey,CALG_ECC_SIGN,&keyidx);
	if ( ret == 0 ){
		SysLog("import success!\n");
	}else{
		SysLog("ImportPublicKey failed ,the state is: %x\n",ret);
		ret=IMPORTPUBKEYFAILED;
		goto end;
	}

	//验证签名
	if ( (ret=pAsymmetricVerifySignEx(&keyidx,pData,48,pSignedData,dwSignedLen))!=0){
		SysLog("pAsymmetricVerifySignEx失败，验证签名失败!，错误码为：%.2x\n",ret);
		ret=VERIFYFAILED;
	}else{
		SysLog("验证签名成功\n");	
	}
	
end:
	key_freeBuffer((void **)&(keyidx.pbData));
	key_freeBuffer((void **)&(InPubKey.pbData));

	SysLog("exit key_verify_ex1_nohash\n");
	key_leave_lock_area();
	return ret;
}


/************************************************************************/
/* 接口名称	int key_getKeyNum（char *id，int idType）
功能简介	获取USBKEY的数量
输入参数1	id:设定要查询key的用户标识。
输入参数2	idType：标识类型，取值范围为：0-uid 1-oid 2-certid
返回值	 成功返回KEY的数量，失败返回负值
注释	若id为“NULL”，表示当前所有插入的KEY的数量，若id
不为“NULL”,表示查找指定id用户标示的KEY的数量。                                                                     */
/************************************************************************/
EXPORTDLL int WINAPIV key_getKeyNum(char *id,int idType)
{
	int ret = -1;	
	int totalKeyNum = 0;
	int keyNum = 0;
	//读取配置文件
	char retValue[255];
	int result = 0;
	char lpszCurDir[CONFIGFILELEN]; 

	memset (retValue,'\0',255);
	GetConfFilePath(lpszCurDir);
	SysLog("读取配置文件:%s\n",lpszCurDir);

#ifdef WIN32
	result =GetPrivateProfileString("TestId","01","    ",retValue,255,lpszCurDir);
#endif
	if(result > 0){
		totalKeyNum++;
		if(id != NULL){
			if(strcmp(id,retValue) == 0){
				keyNum++;
			}
		}
	}

	memset(retValue,'\0',255);
#ifdef WIN32
	result =GetPrivateProfileString("TestId","02","    ",retValue,255,lpszCurDir);
#endif
	if(result > 0){
		totalKeyNum++;
		if(id != NULL){
			if(strcmp(id,retValue) == 0){
				keyNum++;
			}
		}
	}

	if(id == NULL){
		return totalKeyNum;
	}else{
		return keyNum;
	}
}

/************************************************************************/
/* 接口名称	char * key_getKeyUserId（char *id，int idType）
功能简介	当用户插入双KEY时，得到非管理员key的用户标识。
输入参数1	id:设定管理员key的用户标识。
输入参数2	idType：标识类型，固定为0
返回值	成功返回非管理员KEY的用户标识，失败返回空值。
注释	若用户插入一个key，则用户直接获取该key的用户标识；若用户插入两个key，则返回非管理员的KEY；若用户未插入KEY或插入两个以上的KEY，则返回失败。                                                                     */
/************************************************************************/
EXPORTDLL char * WINAPIV key_getKeyUserId(char *id,int idType)
{
	int totalKeyNum = 0;
	int keyNum = 0;

	//读取配置文件
	static char retValue[255];
	memset (retValue,'\0',255);
	int result = 0;
	char lpszCurDir[CONFIGFILELEN]; 
	GetConfFilePath(lpszCurDir);
	SysLog("读取配置文件:%s\n",lpszCurDir);
#ifdef WIN32
	result =GetPrivateProfileString("TestId","01","    ",retValue,255,lpszCurDir);
#endif
	if(result >0){
		totalKeyNum++;
		if(id != NULL){
			if(strcmp(id,retValue) != 0){
				goto end;
			}
		}
	}

	memset(retValue,'\0',255);
#ifdef WIN32
	result =GetPrivateProfileString("TestId","02","    ",retValue,255,lpszCurDir);
#endif
	if(result >0){
		totalKeyNum++;
		if(id != NULL){
			if(strcmp(id,retValue) != 0){
				goto end;
			}
		}
	}

	if(totalKeyNum <1 || totalKeyNum >2){
		return NULL;
	}
end:
	return retValue;
}


/************************************************************************/
/* 
功能:
获取设备状态信息(pin码尝试次数、设备剩余空间、设备是否使用等)
参数:
[in] lstatusProperty
状态属性类型。

[out] lstatusValue
状态值

返回:
0：成功  非0：相应错误码
备注：
状态属性类型定义如下：
enum LABEL_TYPE{
LBL_CONFIG,  
LBL_CERT,  
LBL_KEY,  
LBL_OTHER
};
*/
/************************************************************************/
EXPORTDLL int WINAPIV key_getDeviceStatus(long lstatusProperty, long *lstatusValue)
{
	key_enter_lock_area();
	SysLog("enter key_getDeviceStatus\n");
	int ret = -1;
	ret = key_loadLibrary();
	if(ret != 0){
		goto end;
	}

	if(pGetDeviceStatus == NULL){
		SysLog("pGetDeviceStatus is NULL\n");
		ret = POINTERISNULL;
		goto end;
	}

	ret = pGetDeviceStatus(lstatusProperty,lstatusValue);

end:

	SysLog("exit key_getDeviceStatus\n");
	key_leave_lock_area();
	return ret;
}


/************************************************************************/
/* 接口名称	int key_releaseResources（）
功能简介	释放资源。
参数说明	  无
返回值	  成功返回0，失败返回非0值
说明	当getKeyUserId调完后，需调用该函数清理资源。                                                                     */
/************************************************************************/
EXPORTDLL int WINAPIV key_releaseResources()
{
	int ret = 0;
	return ret;
}


/************************************************************************/
/*
功能:
枚举文件列表
参数:
[in] fileType
文件类型：0-配置文件 1-证书文件  2-密钥文件 3-其他文件 
[out] pFileList
文件列表
[out] pFileCount
文件数量
返回:
0：成功  非0：相应错误码
*/
/************************************************************************/
EXPORTDLL int WINAPIV key_enumFileList(int fileType,unsigned char pFileList[1024],int *pFileCount)
{
	key_enter_lock_area();
	SysLog("enter key_enumFileList\n");
	int ret = -1;
	int i = 0,	certnum = 0;
	unsigned char label[1024];
	LABEL_TYPE labelType = LBL_CERT;

	ret = key_loadLibrary();
	if(ret != 0){
		goto end;
	}

	//枚举所有的标签
	if(fileType == LBL_KEY)
		labelType=LBL_KEY;
	else if(fileType == LBL_OTHER)
		labelType=LBL_OTHER;
	else if(fileType == LBL_CERT)
		labelType=LBL_CERT;
	else if(fileType == LBL_CONFIG)
		labelType=LBL_CONFIG;
	SysLog("file type is：%d\n",fileType);

	if(pReadLabelList == NULL){
		SysLog("pReadLabelList is NULL\n");
		ret = POINTERISNULL;
		goto end;
	}

	memset(label,0x00,1024);	
	ret =pReadLabelList(labelType, NULL,&certnum);
	if ( ret == 0 ){	
		*pFileCount=certnum;
		ret =pReadLabelList (labelType,label, &certnum);
		SysLog("file count is: %d\n",certnum);
		if ( ret == 0){
			memcpy(pFileList,label,1024);
			int j=0;
			for ( i=0; i<1023; i++){
				if ( label[i] == 0x00 ){  
					SysLog("file label is：\n");
					SysHexLog(label+j,strlen((const char*)(label+j)));
					j=i+1;
					if ( label[j] == '\0') break;
				}
			}
		}
	}else {
		SysLog("ReadLabelList err,errno is:%.2x\n",ret);
	}

end:

	SysLog("exit key_enumFileList\n");
	key_leave_lock_area();
	return ret;
}


/************************************************************************/
/*
功能:
枚举容器列表
参数:
[out] pContainerList
容器列表
[out] pContainerCount
容器数量
返回:
0：成功  非0：相应错误码
*/
/************************************************************************/
EXPORTDLL int WINAPIV key_enumContainerList(unsigned char pContainerList[1024],int *pContainerCount)
{
	key_enter_lock_area();
	SysLog("enter key_enumContainerList\n");
	int ret = -1, containerNum = 0;	
	DWORD datalen=0;
	unsigned char data[1024]; 
	ret = key_loadLibrary();
	if(ret != 0){
		goto end;
	}

	//枚举设备中所有的密钥容器
	memset(data,0x00,1024);

	if(pEnumKeyContainerName == NULL){
		SysLog("pEnumKeyContainerName is NULL\n");
		ret = POINTERISNULL;
		goto end;
	}

	ret = pEnumKeyContainerName(NULL, &datalen);
	if ( ret != 0 ){
		SysLog("调用pEnumKeyContainerName失败,返回的状态值为:%x\n",ret);
		ret = ENUMCONTAINERFAILED;
		goto end;
	}
		
	ret = pEnumKeyContainerName (data, &datalen);
	SysLog("容器长度为：%d\n",datalen);
	if ( ret == 0){
		memcpy(pContainerList,data,datalen);
		int j=0;
		for (unsigned int i=0; i<datalen; i++){
			if ( data[i] == 0x00 ){  
				containerNum++;
				SysLog("容器标签为：\n");
				SysHexLog(data+j,strlen((const char*)(data+j)));
				j=i+1;
				if ( data[j] == '\0') break;
			}
		}
	}else{	
		SysLog("调用pEnumKeyContainerName失败！\n");
		SysLog("返回的状态值为:%x\n",ret);
	}

	*pContainerCount = containerNum;
	SysLog("容器数量为：%d\n",datalen);

end:

	SysLog("exit key_enumContainerList\n");
	key_leave_lock_area();
	return ret;
}


/*
登出成功，返回0
登出失败，返回错误码
*/
EXPORTDLL int WINAPIV key_logout()
{
	key_enter_lock_area();
	int ret=-1;
	SysLog("调用key_logout\n");

	if(G_NowDev == SK311){
		if(G_pSK_Logout){
			ret = G_pSK_Logout();
		}else{
			SysLog("G_pSK_Logout is null\n");
			ret=POINTERISNULL;
		}
	}else{
		//登出其他厂商的usbkey
		if(pLogout){
			ret=pLogout();
		}else{
			SysLog("pLogout地址为空\n");
			ret=POINTERISNULL;
		}
	}

	if(ret == 0){
		SysLog("key_logout成功\n");
	}else{
		SysLog("key_logout失败，错误码为：%.2x\n",ret);
		ret=LOGOUTFAILED;
	}

	key_leave_lock_area();
	return ret;
}

/************************************************************************/
/*
功能: 
获取指定索引的证书中的签名值。
参数:
[in] nIndex
	证书的索引，默认为-1。
[out] signData
	证书中的签名值。
返回:
0：成功  非0：相应错误码。
注释:
该接口获取指定索引的用户签名证书中的签名值。若不输索引，则系统默认为当前插入的唯一的USB-KEY中的用户签名证书中的签名值。
*/
/************************************************************************/

EXPORTDLL int WINAPIV key_getSignData(char signData[SIGNDATALEN] ,int nIndex)
{
	key_enter_lock_area();
	SysLog("enter key_getSignData\n");
	SysLog("nIndex is:%d\n",nIndex);
	int ret = -1;
	/******************20121214 lch add****************************/
	ret = key_loadLibrary();
	if(ret != 0){
		goto end;
	}
	/******************20121214 lch add****************************/
	if(nIndex==-1){
		if(G_DevNum==2){
			SysLog("当参数index为-1时，设备数量不唯一\n");
			ret=PARAERROR;
		}else if(G_DevNum==1){
			memset(signData,'\0',256);
			memcpy(signData,keyInfo[0].SignData,keyInfo[0].nSignDataLen);
			if(keyInfo[0].SignData[0]!='\0'){
				SysLog("签名值为:\n");
				SysHexLog((unsigned char*)(keyInfo[0].SignData),keyInfo[0].nSignDataLen);
			}else{
				SysLog("签名值为空\n");
				ret=SIGNDATAISNULL;
			}
		}else if(G_DevNum==0){
			SysLog("设备数量为0\n");
			ret=DEVNUMISZERO;
		}
	}else{	
		if(G_SignDataNum==1){
			for(int i=0;i<G_DevNum;i++){
				if(keyInfo[i].SignData[0]!='\0'){
					memcpy(signData,keyInfo[i].SignData,keyInfo[i].nSignDataLen);
				}
			}
		}else if(G_SignDataNum==2){
			if(keyInfo[nIndex].SignData[0]!='\0'){
				memcpy(signData,keyInfo[nIndex].SignData,keyInfo[nIndex].nSignDataLen);
			}else{
				SysLog("签名项数量为2时，得到的签名项为空\n");
				memset(signData,'\0',SIGNDATALEN);
			}
		}else if(G_SignDataNum==0){
			SysLog("得到的签名项数量为0\n");
			memset(signData,'\0',SIGNDATALEN);
		}
	}

end:

	SysLog("exit key_getSignData\n");
	key_leave_lock_area();
	return ret;
}


/************************************************************************/
/*
功能:
获取错误信息
参数:
[in] nErrCode
错误码
[out] pErrInfo
错误信息
返回:
无
*/
/************************************************************************/
EXPORTDLL void WINAPIV key_getErrorInfo(int nErrCode, char pErrInfo[512])
{
	key_enter_lock_area();
	SysLog("enter key_getErrorInfo\n");
	memset(pErrInfo,'\0',512);

	(key_get_err_msg(nErrCode))?(strcpy(pErrInfo,key_get_err_msg(nErrCode))):(strcpy(pErrInfo,"未知错误码"));

	SysLog("exit key_getErrorInfo\n");
	key_leave_lock_area();
}


/************************************************************************/
/*
功能: 
无编码签名。
参数:
[in] nEnsuleFlag
	封装标识，  1：对签名数据进行封装（签名值包括证书）  0：无需对签名数据进行封装（纯签名值）
[in] nHashFlag
	是否哈希标识，  1：需进行哈希处理   0：无需进行哈希处理
[in] pData
	原文数据内容
[in] nDataLen
	原文数据内容长度
[in-out] pSignedData
	签名值
[in-out] pdwSignedLen
	签名值长度
返回:
	0：成功  非0：相应错误码
注释:
	本接口为二次调用接口,设置pSignedData为NULL, pdwSignedLen为返回的数据的长度,再根据返回的长度为pSignedData分配内存。
	封装的签名数据形式为：签名证书长度+签名证书+签名值长度+签名值
	未封装的签名数据位：签名值
 */
/************************************************************************/

EXPORTDLL int WINAPIV key_sign_nocode(int nEnsuleFlag, int nHashFlag, char *pData,	int nDataLen,	unsigned char *pSignedData,long  *pdwSignedLen)
{
	key_enter_lock_area();
	SysLog("enter key_sign_nocode\n");
	int ret = -1;	
	unsigned char* CertContent=NULL; //证书内容
	int CertLen = 0;//证书长度
	unsigned char * buf=NULL;
	unsigned long buflen = 0;
	int AlgType=ECC384;

	/******************20121214 lch add****************************/
	ret = key_loadLibrary();
	if(ret != 0){
		goto end;
	}
	/******************20121214 lch add****************************/

	if(nHashFlag == NO_NEED_HASH){
		if(nDataLen < 48){
			SysLog("原文数据长度有问题，不应小于48字节\n");
			ret = PARAERROR;
			goto end;
		}
	}

	if(nEnsuleFlag == SIGN_DATA_NEED_ENSULE){
		//从USB-KEY中读取证书
		if ( (ret = key_exportCert(AlgType, 1,CertContent,&CertLen)) != 0 ){
			SysLog("第一次获取签名证书失败\n");
			ret = EXPORTCERTFAILED;
			goto end;
		}
		SysLog("key_sign 中第一次导出证书正确\n");
		CertContent = (unsigned char*)malloc(CertLen);
		memset(CertContent,'\0',CertLen);
		if ( (ret = key_exportCert(AlgType,1, CertContent,&CertLen)) != 0 ){
			SysLog("第二次获取签名证书失败\n");
			key_freeBuffer((void **)&CertContent);
			ret = EXPORTCERTFAILED;
			goto end;
		}
		SysLog("key_sign 中第二次导出证书正确\n");
	}

	//如果SignData是NULL，则返回SignDataLen的长度，以便二次调用
	if ( pSignedData == NULL){
		switch(AlgType){
		case RSA1024:
		case RSA2048:
			break;
		case ECC256:
			*pdwSignedLen=CertLen + 64+ 4*nEnsuleFlag;
			break;
		case ECC384:
			*pdwSignedLen=CertLen+ 96+4*nEnsuleFlag;
			break;
		default:
			break;
		}

		key_freeBuffer((void **)&CertContent);
		ret = 0;
		goto end;
	}

	//进行数字签名
	switch (AlgType){
	case RSA1024:
		break;

	case ECC256:  //ECC256
		buflen=64;	//??
		SysLog("key_sign 中调用SignECC256\n");
		buf = (unsigned char*)malloc(buflen);
		memset(buf,'\0',buflen);
		
		if ( (ret = SignECC(ECC256,nHashFlag,(unsigned char*)pData,nDataLen,buf,&buflen))==0 ){
			SysLog("-------------------------SignECC256后的数据：\n");
			SysHexLog((unsigned char*)buf,buflen);
			ret = 0;
			SysLog("-------------------------SignECC256调用成功\n");
		}else{
			SysLog("SignECC256调用失败\n");
		}
		break;

	case ECC384:
		buflen=96;	
		SysLog("key_sign 中调用SignECC384\n");
		buf = (unsigned char*)malloc(buflen);
		if(buf == NULL){
			ret = MALLOC_MEMORY_ERR;
			goto end;
		}
		memset(buf,'\0',buflen);
		if ( (ret = SignECC(ECC384,nHashFlag,(unsigned char*)pData,nDataLen,buf,&buflen))==0 ){
			SysLog("-------------------------SignECC384后的数据：\n");
			SysHexLog((unsigned char*)buf,96);
			ret = 0;
			SysLog("-------------------------SignECC384调用成功\n");
		}else{
			SysLog("SignECC384调用失败\n");
		}
		break;	
	default:
		ret = PARAERROR;
		break;
	}		
		
	//封装输出数据
	if (!ret){
		memset(pSignedData,'\0',*pdwSignedLen);
		SysLog("签名之后的长度为:%d\n",*pdwSignedLen);
		if(nEnsuleFlag == SIGN_DATA_NEED_ENSULE){
			pSignedData[0] = CertLen/MODULEN;
			pSignedData[1] = CertLen%MODULEN;
			memcpy(pSignedData+2,CertContent,CertLen);
			pSignedData[CertLen+2] = buflen/MODULEN;
			pSignedData[CertLen+3] = buflen%MODULEN;
			memcpy(pSignedData+CertLen+4,buf,buflen);
		}else{
			memcpy(pSignedData,buf,buflen);
		}
		SysLog("封装的数据签名为:\n");
		SysHexLog((unsigned char*)pSignedData,*pdwSignedLen);
	}

end:

	key_freeBuffer((void **)&buf);
	key_freeBuffer((void **)&CertContent);

	SysLog("exit key_sign_nocode\n");
	key_leave_lock_area();
	return ret;
}


/************************************************************************/
/*
功能: 
无编码验签。
参数:
[in] nEnsuleFlag
	封装标识，  1：签名数据为封装数据（签名值包括证书）  0：签名数据未进行封装（纯签名值）
[in] nHashFlag
	是否哈希标识，  1：需进行哈希处理   0：无需进行哈希处理
[in] pSignedData
	签名值
[in] pdwSignedLen
	签名值长度
[in] pData
	原文数据内容
[in] dwDataLen
	原文数据内容长度
[int] pCertCont
	验证证书数据。
[in] dwCertLen
	验证证书数据的长度。
[in] szCACertFile
	根证书文件路径。
[in] szCrlFile
	CRL文件路径。
返回:
	0：成功  非0：相应错误码
注释:
	szCrlFile默认为NULL，此时不做CRL撤销列表校验。如果nEnuleFlag封装标识为1，对参数：pCertCont及dwCertLen不做判断，
	如果nEnuleFlag封装标识为0，利用证书：pCertCont及dwCertLen进行验签。
*/
/************************************************************************/

EXPORTDLL int WINAPIV key_verify_nocode(int nEnuleFlag, int nHashFlag, unsigned char *pSignedData, long dwSignedLen,unsigned char *pData,long dwDataLen,
	unsigned char *pCertCont,long dwCertLen,const char *szCACertFile,const char *szCrlFile)
{
	key_enter_lock_area();
	SysLog("enter key_verify_nocode\n");
	int ret = -1;
	unsigned char *pCertContent=NULL,	*decodeSignData=NULL,	*pCrlContent=NULL;
	int	nCertContentLen=0,	decodeSignDataLen=0;
	unsigned long	dwCrlContentLen=0,	dwPropId=0,	hashlen=0;
	unsigned int	i=0,	j=0;
	int hashPadlen=48;
	unsigned char *hash=NULL,	*hashPad=NULL,	*p=NULL;

	KEYVALUE keyidx;
	FILE *CACertFile = NULL;
	unsigned char *CACertContent=NULL;
	int CACertContentLen = 0;
	FILE *hfp = NULL;

	keyidx.pbData = NULL;
	keyidx.cbData = 0;
	/******************20121214 lch add****************************/
	ret = key_loadLibrary();
	if(ret != 0){
		goto  end;
	}

	if(szCACertFile == NULL){
		SysLog("para szCACertFile is NULL\n");
		ret = PARAERROR;
		goto end;
	}
	SysLog("CACertFile path is:%s\n",szCACertFile);

	if(nHashFlag == NO_NEED_HASH){
		if((dwDataLen < 48))
		{
			SysLog("原文数据长度有问题，不应小于48字节\n");
			ret = PARAERROR;
			goto end;
		}
	}

	/******************20121214 lch add****************************/
	keyidx.cbData=KEY_INDEX_LEN;
	keyidx.pbData = (unsigned char*)malloc(keyidx.cbData);
	if(keyidx.pbData == NULL){
		ret = MALLOC_MEMORY_ERR;
		goto end;
	}
	memset(keyidx.pbData,'\0',keyidx.cbData);
	if(dwSignedLen!=0){
		SysLog("签名数据长度为：%d\n",dwSignedLen);
	}else{
		SysLog("参数签名数据长度为0\n");
		ret=PARAERROR;
		goto end;
	}

	if(dwDataLen!=0){
		SysLog("明文数据长度为：%d\n",dwDataLen);
	}else{
		SysLog("参数明文数据长度为0\n");
		ret=PARAERROR;
		goto end;
	}

	if(pAsymmetricVerifySignEx == NULL){
		SysLog("pAsymmetricVerifySignEx is NULL\n");
		ret = POINTERISNULL;
		goto end;
	}

	if(nEnuleFlag == SIGN_DATA_NEED_ENSULE){
		//拆分签名数据
		//证书
		nCertContentLen = pSignedData[0]*MODULEN+pSignedData[1];
		pCertContent = (unsigned char *)malloc(nCertContentLen);
		if(pCertContent == NULL){
			ret = MALLOC_MEMORY_ERR;
			goto end;
		}
		memset(pCertContent,'\0',nCertContentLen);
		memcpy(pCertContent,pSignedData+2,nCertContentLen);
		if(pCertContent){
			SysLog("证书内容为：\n");
			SysHexLog(pCertContent,nCertContentLen);
		}
		//签名值
		decodeSignDataLen = pSignedData[nCertContentLen+2]*MODULEN+pSignedData[nCertContentLen+3];
		decodeSignData = (unsigned char *)malloc(decodeSignDataLen*sizeof(unsigned char));
		memset(decodeSignData,'\0',decodeSignDataLen);
		memcpy(decodeSignData,pSignedData+nCertContentLen+2+2,decodeSignDataLen);
		if(decodeSignData){
			SysLog("签名数据为：\n");
			SysHexLog(decodeSignData,decodeSignDataLen);
		}
	}else{
		decodeSignDataLen =dwSignedLen;
		decodeSignData = (unsigned char *)malloc(decodeSignDataLen*sizeof(unsigned char));
		memset(decodeSignData,'\0',decodeSignDataLen);
		memcpy(decodeSignData,pSignedData,decodeSignDataLen);
		nCertContentLen = dwCertLen;
		pCertContent = (unsigned char *)malloc(nCertContentLen);
		memset(pCertContent,'\0',nCertContentLen);
		memcpy(pCertContent,pCertCont,nCertContentLen);
	}

	//得到CA证书内容				
	CACertFile=fopen(szCACertFile,"rb");

	if ( CACertFile != NULL){
		fseek(CACertFile, 0L, SEEK_END);
		CACertContentLen = ftell(CACertFile);
		rewind(CACertFile);
		CACertContent =(unsigned char *) malloc(CACertContentLen*sizeof(unsigned char));
		if(CACertContent == NULL){
			ret = MALLOC_MEMORY_ERR;
			goto end;
		}
		fread(CACertContent,sizeof(unsigned char),CACertContentLen, CACertFile);
		fclose(CACertFile);
		CACertFile = NULL;
	}else{
		SysLog("szCACertFile 不存在\n");
		ret=CERTFILENOTEXIST;
		goto end;
	}
	//不进行crl撤销列表校验
	if(szCrlFile == NULL) {
		SysLog("不进行crl撤销列表校验\n");
	}else //进行撤销列表校验
	{
		SysLog("验证证书是否被撤销\n");
		//获取crl内容
		hfp = fopen(szCrlFile, "rb");
		if ( hfp == NULL){
			SysLog("can't find Crlfile\n");
			ret=CRLFILENOTEXIST;
			goto end;
		}
		fseek(hfp, 0, SEEK_END);
		dwCrlContentLen = ftell(hfp);
		pCrlContent =(unsigned char*) malloc(dwCrlContentLen*sizeof(unsigned char));
		if(pCrlContent == NULL){
			ret = MALLOC_MEMORY_ERR;
			goto end;
		}
		memset(pCrlContent,'\0',dwCrlContentLen);
		fseek(hfp, 0, SEEK_SET);
		fread(pCrlContent, 1, dwCrlContentLen, hfp);
		fclose(hfp);
		hfp = NULL;

		ret=key_crlVerify(pCertContent,nCertContentLen,pCrlContent,dwCrlContentLen);
		if(ret==0){
			SysLog("此证书处于撤销列表中\n");
			goto end;
		}else{
			SysLog("此证书未被撤销\n");
		}
	}

	//验证证书的有效性
	SysLog("验证证书的有效性\n");
	ret=key_verifyCert(pCertContent,nCertContentLen,CACertContent,CACertContentLen);
	if(ret==0){
		SysLog("证书验证成功\n");
	}else{
		SysLog("证书验证失败\n");
		ret=CERTVERIFYFAILED;
		goto end;
	}

	//验证签名
	SysLog("验证签名\n");
	//导入公钥
	ret=key_importPublicKey(CALG_ECC_SIGN,pCertContent,nCertContentLen,&keyidx);
	if(ret != 0){
		SysLog("导入公钥失败\n");
		ret=IMPORTPUBKEYFAILED;
		goto end;
	}

	hashPad = (unsigned char*)malloc(hashPadlen);
	if(hashPad == NULL){
		ret = MALLOC_MEMORY_ERR;
		goto end;
	}
	memset(hashPad,'\0',hashPadlen);
	if(nHashFlag == NEED_HASH){
		//对原文进行hash处理				
		if (  (ret=KeyDigest(pData, dwDataLen,hash,&hashlen,2)) != 0 ){
			SysLog("第一次hash失败\n");
			ret=HASHFAILED;
			goto end;
		}
		hash = (unsigned char*)malloc(hashlen);
		if(hash == NULL){
			ret = MALLOC_MEMORY_ERR;
			goto end;
		}
		memset(hash,'\0',hashlen);
		//获取摘要
		if (  (ret=KeyDigest(pData, dwDataLen,hash,&hashlen,2)) != 0 ){
			SysLog("第二次hash失败\n");
			ret=HASHFAILED;
			goto end;
		}
		//对摘要进行截除处理(特殊的填充)
		memcpy(hashPad,hash,hashPadlen);
	}else{
		memcpy(hashPad,pData,hashPadlen);
	}

	if(hashPad){
		SysLog("摘要数据为:\n");
		SysHexLog(hashPad,hashPadlen);
	}
	SysLog("进行验签\n");
	//验证签名
	if ( (ret=pAsymmetricVerifySignEx(&keyidx,hashPad,hashPadlen,decodeSignData,decodeSignDataLen))!=0){
		SysLog("pAsymmetricVerifySignEx失败，验证签名失败!，错误码为：%.2x\n",ret);
		ret=VERIFYFAILED;
	}else{
		SysLog("验证签名成功\n");	
	}
	
end:

	if(CACertFile){
		fclose(CACertFile);
		CACertFile = NULL;
	}

	if(hfp){
		fclose(hfp);
		hfp = NULL;
	}

	key_freeBuffer((void **)&hash);
	key_freeBuffer((void **)&hashPad);
	key_freeBuffer((void **)&(keyidx.pbData));
	key_freeBuffer((void **)&pCertContent);
	key_freeBuffer((void **)&decodeSignData);
	key_freeBuffer((void **)&CACertContent);
	key_freeBuffer((void **)&pCrlContent);

	SysLog("exit key_verify\n");
	key_leave_lock_area();
	return ret;
}

/************************************************************************/
/*
功能: 
无编码公钥验签。
参数:
[in] nHashFlag
	是否哈希标识，  1：需进行哈希处理   0：无需进行哈希处理
[in] pSignedData
	签名值
[in] pdwSignedLen
	签名值长度
[in] pData
	原文数据内容
[in] dwDataLen
	原文数据内容长度
[int] pCertCont
	验证证书数据
[in] dwCertLen
	验证证书数据的长度
[int] pPubKey
	公钥数据
[in] nPubKeyLen
	公钥数据的长度
返回:
	0：成功  非0：相应错误码
注释:
	此时pSignedData为未封装的签名数据。
*/
/************************************************************************/
EXPORTDLL int WINAPIV key_verify_nocode_pubkey(int nHashFlag, unsigned char *pSignedData, long dwSignedLen,unsigned char * pData,long dwDataLen, 
	unsigned char* pPubKey,long nPubKeyLen)
{
	key_enter_lock_area();
	SysLog("enter key_verify_nocode_pubkey\n");
	int ret = -1;
	unsigned char	*hash=NULL, *hashPad=NULL;
	int hashPadlen=48;
	unsigned int	i=0, j  = 0;
	long size = nPubKeyLen;
	unsigned long hashlen=0;
	KEYVALUE keyidx;
	KEYVALUE InPubKey;
	BLOBHEADER pkheader;
	char temp[512];

	keyidx.pbData = NULL;
	keyidx.cbData = 0;
	InPubKey.pbData = NULL;
	InPubKey.cbData = 0;

	/*参数检测*/
	if(nHashFlag == NO_NEED_HASH){
		if((dwDataLen < 48))
		{
			SysLog("原文数据长度有问题，不应小于48字节\n");
			ret = PARAERROR;
			goto end;
		}
	}

	if(dwSignedLen != 0){
		SysLog("签名数据长度为：%d\n",dwSignedLen);
	}else{
		SysLog("参数签名数据长度为0\n");
		ret=PARAERROR;
		goto end;
	}

	if(dwDataLen!=0){
		SysLog("明文数据长度为：%d\n",dwDataLen);
	}else{
		SysLog("参数明文数据长度为0\n");
		ret=PARAERROR;
		goto end;
	}

	if(nPubKeyLen){
		SysLog("公钥长度为：%d\n",nPubKeyLen);
	}else{
		SysLog("公钥长度为0\n");
		ret=PARAERROR;
		goto end;
	}

	if(pPubKey){
		SysLog("公钥内容为：\n");
		SysHexLog((unsigned char*)pPubKey,nPubKeyLen);
	}else{
		SysLog("公钥内容为空\n");
		ret=PARAERROR;
		goto end;
	}

	if(pSignedData){
		SysLog("签名数据为：\n");
		SysHexLog(pSignedData,dwSignedLen);
	}else{
		SysLog("参数签名内容为空\n");
		ret=PARAERROR;
		goto end;
	}

	/******************20121214 lch add****************************/
	ret = key_loadLibrary();
	if(ret != 0){
		goto end;
	}
	/******************20121214 lch add****************************/

	if(pAsymmetricVerifySignEx == NULL){
		SysLog("pAsymmetricVerifySignEx 指针为空\n");
		ret = POINTERISNULL;
		goto end;
	}

	keyidx.cbData=KEY_INDEX_LEN;
	keyidx.pbData = (unsigned char*)malloc(keyidx.cbData);
	if(keyidx.pbData == NULL){
		ret = MALLOC_MEMORY_ERR;
		goto end;
	}
	memset(keyidx.pbData,'\0',keyidx.cbData);

	pkheader.bType = PUBLICKEYBLOB;
	pkheader.bVersion = CUR_BLOB_VERSION;
	pkheader.reserved = 0;
	pkheader.aiKeyAlg = CALG_ECC_SIGN;

	memset(temp,'\0',512);

	//验证签名
	SysLog("验证签名\n");
	//导入公钥
	memcpy(temp,&pkheader,sizeof(BLOBHEADER));
	if(size == 97)
		memcpy(temp+sizeof(BLOBHEADER),pPubKey+1,--size);
	else
		memcpy(temp+sizeof(BLOBHEADER),pPubKey,size);


	InPubKey.cbData=size+sizeof(BLOBHEADER);
	InPubKey.pbData=(unsigned char*)malloc(InPubKey.cbData*sizeof(unsigned char));
	memset(InPubKey.pbData,'\0',InPubKey.cbData);
	memcpy((char *)InPubKey.pbData,temp,InPubKey.cbData);

	ret = pImportPublicKey(&InPubKey,CALG_ECC_SIGN,&keyidx);
	if ( ret == 0 ){
		SysLog("import success!\n");
	}else{
		SysLog("ImportPublicKey failed ,the state is: %x\n",ret);
		ret=IMPORTPUBKEYFAILED;
		goto end;
	}

	hashPad = (unsigned char*)malloc(hashPadlen*sizeof(unsigned char));
	if(hashPad == NULL){
		ret = MALLOC_MEMORY_ERR;
		goto end;
	}
	memset(hashPad,'\0',hashPadlen);

	if(nHashFlag == NEED_HASH){
		//对原文进行hash处理		
		ret=KeyDigest(pData, dwDataLen,hash,&hashlen,2);
		if (  ret != 0 ){
			ret = HASHFAILED;
			goto end;
		}
		hash = (unsigned char*)malloc(hashlen*sizeof(unsigned char));
		if(hash == NULL){
			ret = MALLOC_MEMORY_ERR;
			goto end;
		}
		memset(hash,'\0',hashlen);
		ret=KeyDigest(pData, dwDataLen,hash,&hashlen,2);
		if(ret != 0){
			ret = HASHFAILED;
			goto end;
		}

		//对摘要进行截除处理(特殊的填充)
		memcpy(hashPad,hash,hashPadlen);
		if(hashPad){
			SysLog("摘要数据为:\n");
			SysHexLog(hashPad,hashPadlen);
		}
	}else{
		memcpy(hashPad,pData,hashPadlen);
	}

	//验证签名
	if ( (ret=pAsymmetricVerifySignEx(&keyidx,hashPad,hashPadlen,pSignedData,dwSignedLen))!=0){
		SysLog("pAsymmetricVerifySignEx失败，验证签名失败!，错误码为：%.2x\n",ret);
		ret=VERIFYFAILED;
	}else{
		SysLog("验证签名成功\n");	
	}

end:

	key_freeBuffer((void **)&hash);
	key_freeBuffer((void **)&hashPad);
	key_freeBuffer((void **)&(keyidx.pbData));

	SysLog("exit key_verify_ex1\n");
	key_leave_lock_area();
	return ret;
}


/*
功能:
删除数据

参数:
	[in] szInfoName
	写入数据在设备中的存储标识。
		
返回:
	0：成功  非0：相应错误码
		  
注释:
	为存储标识pInfoName分配32个字节。			
 */
EXPORTDLL int WINAPIV key_delData(const char* szInfoName){

	key_enter_lock_area();
	SysLog("进入到key_delData\n");
    int ret = 0, j = 0,num = 0;
    int fileNameLen=strlen(szInfoName);
    //判断文件是否存在
    unsigned char lbl[2048]={'\0'};
    char label[64]={'\0'};
    strcpy(label,szInfoName);
    if(label[0]!='\0'){
	    SysLog("文件名称为：%s\n",label);
	}else{
	    SysLog("文件名为空\n");
	    ret=PARAERROR;
		goto end;
	}

	ret = key_loadLibrary();
	if(ret != 0){
		goto end;
	}

	if((pReadLabelList == NULL) || (pDeleteFileEx == NULL)){
		ret = POINTERISNULL;
		goto end;
	}
 
	memset(lbl,0x00,2048);

	ret=pReadLabelList(LBL_CERT,NULL,&num);
	if(ret==0){
		SysLog("ReadLabelList success：cert area file num is:%d\n",num);
		ret = pReadLabelList(LBL_CERT, lbl, &num);
		if( ret != 0 ){
			SysLog("ReadLabelList err,errno is:%.2x\n",ret);
			ret=READLABELLISTFAILED;
			goto end;
		}
	}else{
		SysLog("ReadLabelList err,errno is:%.2x\n",ret);
		ret=READLABELLISTFAILED;
		goto end;
		goto end;
	}

	for(int i=0; i<2047; i++){
		if(lbl[i] == 0x00){
			SysLog("file label is：\n");
			SysHexLog(lbl+j,strlen((const char*)lbl+j));
			if(strncmp((const char*)lbl+j,szInfoName,fileNameLen) == 0){
				ret =  pDeleteFileEx (lbl+j, LBL_CERT);
				if( ret != 0 ){
					SysLog("DeleteFileEx file err,errno is:%.2x\n",ret);
					ret=DELETEFILEFAILED;
					goto end;
				}else{
					SysLog("del file sucess!\n");
				}
			}

			j = i+1;
			if(lbl[j] == 0x00){
				break;
			}
		}
	}

end:

	SysLog("exit key_delData\n");
	key_leave_lock_area();
    return ret;

}


/*
  删除文件
  参数：AlgType：1-cert  2-key  3-other
*/
int DeleteFiles(int AlgType)
{
	unsigned char lbl[2048]={'\0'};
	memset(lbl,0x00,2048);
	int ret =0, j = 0, num = 0;

	int flag_enckey=1;
	int flag_sigkey=1;
	int flag_encsn=1;
	int flag_sigsn=1;
	//int flag_sc=1;
	//int flag_root=1;
	int flag_root1=1;
	int flag_root2=1;
	int flag_ec=1;
	int flag_enc=1;


	char enckey[32]={'\0'};
	char sigkey[32]={'\0'};
	char encsn[32]={'\0'};
	char sigsn[32]={'\0'};
	//char sc[32]={'\0'};
	//char root[32]={'\0'};
	char root1[32]={'\0'};
	char root2[32]={'\0'};
	char ec[32]={'\0'};
	char lab_enc_cert[32]={'\0'};

	strcpy(enckey,KEY_PUBKEY_ENC);
	strcpy(sigkey,KEY_PUBKEY_SIG);
	strcpy(encsn,OTHER_ENC_SN);
	strcpy(sigsn,OTHER_SIG_SN);
	//strcpy(sc,CERT_S_C);
	//strcpy(root,CERT_ROOT);
	strcpy(root1,CERT_ROOT1);
	strcpy(root2,CERT_ROOT2);
	strcpy(ec,CERT_E_C);
	strcpy(lab_enc_cert,ECC_CRY_CER_WHCA_ENC);

	LABEL_TYPE labelType = LBL_CERT;
	if(AlgType==2)
		labelType=LBL_KEY;
	else if(AlgType==3)
		labelType=LBL_OTHER;
	else if(AlgType==1)
		labelType=LBL_CERT;


	if((pReadLabelList == NULL) || (pDeleteFileEx == NULL)){
		ret = POINTERISNULL;
		goto end;
	}
	ret=pReadLabelList(labelType,NULL,&num);
	if(ret==0){
		SysLog("ReadLabelList success，file num is:%d",num);
		ret = pReadLabelList(labelType, lbl, &num);
		if( ret != 0 ){
			SysLog("ReadLabelList err,errno is:%.2x",ret);
			ret=READLABELLISTFAILED;
			goto end;
		}
	}else{
		SysLog("ReadLabelList err,errno is:%.2x",ret);
		ret=READLABELLISTFAILED;
		goto end;
	}

	for(int i=0; i<2047; i++){
		if(lbl[i] == 0x00){
			SysLog("file label is：");
			SysHexLog(lbl+j,strlen((const char*)lbl+j));
			flag_enckey=strncmp((const char*)(lbl+j),enckey,strlen(enckey));
			flag_sigkey=strncmp((const char*)(lbl+j),sigkey,strlen(sigkey));
			flag_encsn=strncmp((const char*)(lbl+j),encsn,strlen(encsn));
			flag_sigsn=strncmp((const char*)(lbl+j),sigsn,strlen(sigsn));

			//证书区
			//flag_sc=strncmp((const char*)(lbl+j),sc,strlen(sc));
			//flag_root=strncmp((const char*)(lbl+j),root,strlen(root));
			flag_root1=strncmp((const char*)(lbl+j),root1,strlen(root1));
			flag_root2=strncmp((const char*)(lbl+j),root2,strlen(root2));
			flag_ec=strncmp((const char*)(lbl+j),ec,strlen(ec));
			flag_enc=strncmp((const char*)(lbl+j),lab_enc_cert,strlen(lab_enc_cert));

			if((flag_enckey==0)
				||(flag_sigkey==0)
				||(flag_sigsn==0)
				||(flag_encsn==0)
				||(flag_root1==0)
				||(flag_root2==0)
				||(flag_ec==0)
				||(flag_enc==0)
				){
					ret =  pDeleteFileEx (lbl+j, labelType);
					if( ret != 0 ){
						SysLog("DeleteFileEx err,errno is:%.2x\n",ret);
						ret=DELETEFILEFAILED;
						goto end;
					}else{
						SysLog("deletefile success!\n");
					}
			}
			j = i+1;
			if(lbl[j] == 0x00){
				break;
			}
		}
	}
end:
    return ret;

}

/*
功能:
删除无用数据

参数:无
		
返回:
	0：成功  非0：相应错误码
		
 */
EXPORTDLL int WINAPIV key_delUselessData(void){
	key_enter_lock_area();
	SysLog("enter key_delUselessData\n");
	int ret = -1;
	ret = key_loadLibrary();
	if(ret != 0){
		goto end;
	}

	ret = DeleteFiles(1); //删除cert区文件
	ret = DeleteFiles(2); //删除key区文件
	ret = DeleteFiles(3); //删除other区文件

end:

	SysLog("exit key_delUselessData\n");
	key_leave_lock_area();
	return ret;
}



#ifdef _WINDOWS
BOOL APIENTRY DllMain( HANDLE hModule,DWORD  ul_reason_for_call,LPVOID lpReserved) 
{
	int ret = 0;
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
		SysLog("enter DllMain:DLL_PROCESS_ATTACH,%s,%d\n",__FILE__,__LINE__);
#ifdef MUTEXFLAG
		if(NULL == hMutex){
			SysLog("mutex handle is null\n");
			hMutex = CreateMutex(NULL, TRUE, "ukeymutex");//互斥句柄
		}
		if(NULL == hMutex){
			SysLog("CreateMutex failed,last err no is:%d\n",GetLastError());

#endif
		break;

	case DLL_THREAD_ATTACH:
		SysLog("enter DllMain:DLL_THREAD_ATTACH,%s,%d\n",__FILE__,__LINE__);	
		break;

	case DLL_THREAD_DETACH:
		SysLog("enter DllMain:DLL_THREAD_DETACH,%s,%d\n",__FILE__,__LINE__);
		break;

	case DLL_PROCESS_DETACH:
		SysLog("enter DllMain:DLL_PROCESS_DETACH,%s,%d\n",__FILE__,__LINE__);
#ifdef MUTEXFLAG
		if(NULL != hMutex){
			SysLog("hMutex is not null,close hmutex\n",__FILE__,__LINE__);
			CloseHandle(hMutex);
			hMutex = NULL;
		}
#endif

		break;
	}

	return TRUE;
}

#else

__attribute__((constructor)) void before_as_util_cert_dllmain() {

	int rv = -1;
	apr_initialize();
	rv = apr_pool_create(&g_pool, NULL);
	if(rv != APR_SUCCESS)
	{
		return FALSE;
	}
#ifdef MUTEXFLAG
	if(NULL == hMutex){
		SysLog("mutex handle is null\n");
		apr_thread_mutex_create(&hMutex, APR_THREAD_MUTEX_DEFAULT, g_pool);
	}
	if(NULL == hMutex){
		SysLog("CreateMutex failed,last err no is:%d\n",GetLastError());

#endif
}

__attribute__((destructor)) void end_as_util_cert_dllmain(){

#ifdef MUTEXFLAG
	if(NULL != hMutex){
		SysLog("hMutex is not null,close hmutex\n",__FILE__,__LINE__);
		apr_thread_mutex_trylock(hMutex);
		hMutex = NULL;
	}
#endif
}

#endif



/*
* 功能：设置用户pin码状态
 * @param flag  [IN]- 1:设置长度  2:设置重试次数
 * @param para [IN] - flag =1 :para设置长度; flag = 2:para设置重试次数
 * @return 返回值
 * 成功：0
 * 失败：错误码
*/
EXPORTDLL int WINAPIV key_setUserPinStat(int flag, int para)
{
	key_enter_lock_area();
	SysLog("enter key_setUserPinStat\n");
	int ret = -1;

	ret = key_loadLibrary();
	if(ret != 0){
		goto end;
	}

	if(G_NowDev == SK311){
		if(G_pSK_SetUserPinStat == NULL){
			SysLog("G_pSK_SetUserPinStat is NULL\n");
			ret = POINTERISNULL;
			goto end;
		}
		ret = G_pSK_SetUserPinStat(flag,para);
		if(ret != 0){
			SysLog("set user pin stat failed,errorcode is:%.2x\n",ret);
			ret = SET_PIN_STAT_ERR;
		}
	}else{
		SysLog("not support\n");
		ret = DEV_NOT_SUPPORT;
	}

end:

	SysLog("exit key_setUserPinStat,return value is:%d\n",ret);
	key_leave_lock_area();
	return ret;
}

/*
* 功能：获取用户pin码长度
 * @param flag  [IN]- 1:获取长度  2:获取重试次数
 * @param para  [OUT]- flag =1  获取pin码长度; flag = 2  获取pin码重试次数
 * @return 返回值
 * 成功：0
 * 失败：错误码
*/
EXPORTDLL int WINAPIV key_getUserPinStat(int flag,int *para)
{
	key_enter_lock_area();
	SysLog("enter key_getUserPinStat\n");
	int ret = -1;

	ret = key_loadLibrary();
	if(ret != 0){
		goto end;
	}

	if(G_NowDev == SK311){
		if(G_pSK_GetUserPinStat == NULL){
			SysLog("G_pSK_GetUserPinStat is NULL\n");
			ret = POINTERISNULL;
			goto end;
		}
		ret = G_pSK_GetUserPinStat(flag,para);
		if(ret != 0){
			SysLog("get user pin stat failed,errcode is:%.2x\n",ret);
			ret = GET_PIN_STAT_ERR;
		}
	}else{
		SysLog("not support\n");
		ret = DEV_NOT_SUPPORT;
	}

end:

	SysLog("exit key_getUserPinStat,return value is:%d\n",ret);
	key_leave_lock_area();
	return ret;
}


/*
* 功能：删除共享内存
 * @return 返回值
 * 成功：0
 * 失败：错误码
*/
EXPORTDLL int WINAPIV key_destroyShareMem()
{
	key_enter_lock_area();
	SysLog("enter key_destroyShareMem\n");
	int ret = -1;

	ret = key_loadLibrary();
	if(ret != 0){
		goto end;
	}

	if(G_NowDev == SK311){
		if(G_pSK_DestroyShareMem == NULL){
			SysLog("G_pSK_DestroyShareMem is NULL\n");
			ret = POINTERISNULL;
			goto end;
		}
		ret = G_pSK_DestroyShareMem();
		if(ret != 0){
			SysLog("key_destroyShareMem failed,errcode is:%.2x\n",ret);
			ret = DESTROY_SHARE_MEM_ERR;
		}
	}else{
		SysLog("not support\n");
		ret = DEV_NOT_SUPPORT;
	}

end:

	SysLog("exit key_destroyShareMem,return value is:%d\n",ret);
	key_leave_lock_area();
	return ret;
}




/*
功能:
从设备中读取印章列表。

参数:
[in-out] pContent
数据的内容。
[in-out] pContentLen
数据的长度。
[in-out] pNum
印章数量	  
返回:
0：成功  非0：相应错误码
		
注释:
本接口为二次调用接口,设置pContent为NULL,pContentLen为返回的数据的长度,再根据返回的长度为pContent分配内存,pNum为印章的数量。		  
 */

EXPORTDLL int  WINAPIV key_readStampList(unsigned char *pContent,unsigned long *pContentLen, unsigned long *pNum)

{
	
	SysLog("enter key_readStampList\n");
  int ret = 0;
  unsigned char* readStampList = NULL;
	unsigned long listLen = 0;
	int cn = 0;
	int length = 0;
	int start = 0;
	int num = 0;
	int ending = 0;
	int count = 0;
	char esl[] = ".esl";
	ret = key_readData("STAMPDESCRIPTION.INI",readStampList,&listLen);	
	SysLog("第一次调用读key中签章配置数据的结果: %d",ret);
	SysLog("获取数据的长度: %d",ret);
	if(ret == 0)
	{
		readStampList = (unsigned char*)malloc(listLen);
		if(readStampList!=NULL)
			ret = key_readData("STAMPDESCRIPTION.INI",readStampList,&listLen);
		else
			goto end;
		if(ret == 0)
		{
			SysLog("配置文件的数据为%s\n",(char*)readStampList);
		}
	}
	else
		goto end;
	length	= strlen((char*)readStampList)+1;
	for (cn = 0;cn < length; cn++)
	{
		if (readStampList[cn]=='=')
		{
			start = cn+1;
			break;
		}
	}
	for (cn = start;cn < length; cn++)
	{
		if (readStampList[cn]=='\n')
		{
			ending = cn;
			break;
		}
	}
	for (cn = start; cn<ending;cn ++)
	{
		num = num * 10 + (readStampList[cn]-'0');
	}
	SysLog("签章的数量为%d",num);
	*pNum = num;
	if (pContent == NULL)
	{
		*pContentLen = 37 * num + 1;
		goto end;
	}
	if (pContent != NULL)
	{
		memset(pContent,0x00,*pContentLen);
		for (cn = ending + 1; cn < length;cn ++)
		{
			if (readStampList[cn]=='=')
			{				
				memcpy(pContent+(count * 37),readStampList+cn-32,32);
				memcpy(pContent+(count * 37)+32,esl,4);
				SysLog("签章的名称为：%s",pContent+count * 33);
				count++;
				break;
			}
		}
	}	
	
end:
	if(readStampList)
	{
		  free(readStampList);
		  readStampList = NULL;
	}
	return 0;
}

/*
功能:
从设备中读取印章数据。

参数:
[in] pName
签章的名字
[in-out] pContent
数据的内容的指针。
[in-out] pContentLen
数据的长度。
返回:
0：成功  非0：相应错误码
	  
 */

EXPORTDLL int  WINAPIV key_readStampData(const char *pName,unsigned char** pContent,unsigned long *pContentLen)

{
	SysLog("Enter the key_readStampData.");
	unsigned char *pData = NULL;
	unsigned long pLen = 0;	
	int cn = 0;
	int cn2 = 0;
	int deLen = 0;
	unsigned char* pDedata = NULL;
	int ret = key_readData(pName,pData,&pLen);
	if(ret == 0)
	{
		pData = (unsigned char *)malloc(pLen+1);
		if(!pData)
		{
			SysLog("空间分配错误");
			goto end;
		}
		memset(pData,0x00,pLen+1);
		ret = key_readData(pName,pData,&pLen);	
	}
	else
		goto end;
	
	SysLog("The pData2 lenth is %d\n",strlen((char*)pData));
	pDedata = base64_decode((char*)pData,&deLen);
	*pContentLen = deLen;	
	SysLog("The pContentLen is %d\n",deLen);
	if(!pContent)
	{
		goto end;
	}
	if(!(*pContent))
	{		
		*pContent=(unsigned char*)malloc(deLen);
		//goto end;
	}
	memcpy(*pContent,pDedata,deLen);
	
	
end:
	if(pData)
	{
		free(pData);
		pData = NULL;
	}
	
	if(pDedata)
	{
		free(pDedata);
		pDedata = NULL;
	}
	return 0;
}


/*
功能:
读取印章的数据列表

参数：
[in/out]puchSealListData
签章数据列表
[in/out]
签章数据列表的长度

注释：
本接口为二次调用接口,设置puchSealListData为NULL,piSealListDataLen为返回的数据的长度,再根据返回的长度为piSealListDataLen分配内存。		  
*/

EXPORTDLL int   WINAPIV key_readStampIDList(unsigned char* puchSealListData,int* piSealListDataLen)

{
	
	SysLog("enter key_readStampIDList\n");
  int ret = 0;
  unsigned char* readStampList = NULL;
	unsigned long listLen = 0;
	int cn = 0;
	int length = 0;
	int start = 0;
	int num = 0;
	int ending = 0;
	int ending2 = 0;
	int count = 0;
	char esl[] = ".esl";
	ret = key_readData("STAMPDESCRIPTION.INI",readStampList,&listLen);	
	SysLog("第一次调用读key中签章配置数据的结果: %d",ret);
	SysLog("获取数据的长度: %d",ret);
	if(ret == 0)
	{
		readStampList = (unsigned char*)malloc(listLen);
		if(readStampList!=NULL)
			ret = key_readData("STAMPDESCRIPTION.INI",readStampList,&listLen);
		else
			goto end;
		if(ret == 0)
		{
			SysLog("配置文件的数据为%s\n",(char*)readStampList);
		}
	}
	else
		goto end;
	length	= strlen((char*)readStampList)+1;
	for (cn = 0;cn < length; cn++)
	{
		if (readStampList[cn]=='=')
		{
			start = cn+1;
			break;
		}
	}
	for (cn = start;cn < length; cn++)
	{
		if (readStampList[cn]=='\n')
		{
			ending = cn;
			ending2 = cn;
			break;
		}
		
		if (readStampList[cn]=='\r'&&readStampList[cn+1]=='\n')
		{
			ending = cn;
			ending2 = cn + 1;
			break;
		}
	}
	
	for (cn = start; cn<ending;cn ++)
	{
		num = num * 10 + (readStampList[cn]-'0');
	}
	SysLog("签章的数量为%d",num);
	
	if (puchSealListData == NULL)
	{
		if(ending==ending2)
			*piSealListDataLen =listLen - ending;
		else
			*piSealListDataLen =listLen - ending2 - num;
		goto end;
	}
	else
	{
		memset(puchSealListData,0x00,*piSealListDataLen);
		for (cn = ending2 + 1; cn < listLen;)
		{
			if(readStampList[cn]!='='&&readStampList[cn]!='\r'&&readStampList[cn]!='\n'&&readStampList[cn]!='\0')
			{
				puchSealListData[count] = readStampList[cn];
				count++;
				cn++;
			}
			else if(readStampList[cn]=='=')
			{
				puchSealListData[count] = '\0';
				count++;
				cn++;
			}
			else if(readStampList[cn]=='\r')
			{
				cn++;
			}
			else if(readStampList[cn]=='\n')
			{
				puchSealListData[count] = '\0';
				count++;
				cn++;
			}
		}
	}	
	
end:
	if(readStampList)
	{
		  free(readStampList);
		  readStampList = NULL;
	}
	return 0;
}