#include <stdlib.h>
#include "HTKG_USBKey_Errno.h"

typedef struct err_string_data_st
{
	int error;
	const char *string;

} ERR_STRING_DATA;


static ERR_STRING_DATA err_str_data[] =
{
	{UNKNOWNDEVTYPE,"未识别的设备类型"},
	{DEVDLLISNOTEXIST,"设备dll库不存在"},
	{CONFISNOTEXIST,"未查找到配置文件"},
	{LOADLIBRARYFAILED,"加载库失败"},
	{POINTERISNULL,"指针为空"},
	{EXPORTCERTFAILED,"导出证书失败"},
	{GETSERIALFAILED,"得到证书序列号失败"},
	{SETDEVICEFAILED,"设置设备失败"},
	{PARAERROR,"参数错误"},
	{GETUSERNAMEFAILED,"获取用户名失败"},
	{CERTCONTEXTISNULL,"证书上下文为空"},
	{CERTISNULL,"证书内容为空"},
	{LOGINFAILED,"登录失败"},
	{SETPINFAILED,"修改pin码失败"},
	{WRITEDATAFAILED,"写入数据失败"},
	{READDATAFAILED,"读取数据失败"},
	{ECC384SIGNFAILED,"ECC384签名失败"},
	{HASHFAILED,"摘要失败"},
	{CERTFILENOTEXIST,"证书文件不存在"},
	{CRLFILENOTEXIST,"crl文件不存在"},
	{IMPORTPUBKEYFAILED,"导入公钥失败"},
	{VERIFYFAILED,"验签失败"},
	{PARSECERTFAILED,"解析证书失败"},
	{CERTINCRL,"证书已被撤销"},
	{GETDEVSTATUSFAILED,"得到设备状态失败"},
	{READLABELLISTFAILED,"读取文件列表失败"},
	{DELETEFILEFAILED,"删除文件失败"},
	{SIGNCERTNOTEXIST,"签名证书不存在"},
	{SERIALISNULL,"序列号为空"},
	{USERNAMEISNULL,"用户名为空"},
	{CERTVERIFYFAILED,"证书验证失败"},
	{SUBJECTISNULL,"主题项为空"},
	{ISSUERISNULL,"发行者为空"},
	{PUBKEYISNULL,"公钥为空"},
	{ENDDAYISNULL,"到期日为空"},
	{GENRANDOMFAILED,"产生随机数失败"},
	{DEVNUMISZERO,"设备数量为0"},
	{ASYENCFAILED,"非对称加密失败"},
	{ASYDECFAILED,"非对称解密失败"},
	{FINDCONTAINERFAILED,"查找密钥容器失败"},
	{UNLOCKFAILED,"解锁失败"},
	{GETUSERIDFAILED,"得到用户id失败"},
	{GETCERTNUMFAILED,"获取签名证书数量失败"},
	{USBKEYISLOCKED,"USB-Key被锁"},
	{LOADGETDEVSNDLLFAILED,"加载得到设备序列号库失败"},
	{GETDEVSNFAILED,"得到设备序列号失败"},
	{OUTOFUSERRANGE,"超出使用范围"},
	{ECC256SIGNFAILED,"ECC256签名失败"},
	{LOGOUTFAILED,"登出失败"},
	{SIGNDATAISNULL,"证书签名项为空"},
	{ENUMCONTAINERFAILED,"枚举密钥容器失败"},
	{LOAD_CP_DLL_ERR,"加载证书解析库失败"},
	{MALLOC_MEMORY_ERR,"分配内存失败"},
	{UTF8_TO_GBK_ERR,"UTF8_TO_GBK_ERR"},
	{DEV_NOT_SUPPORT,"该设备不支持此接口"},
	{SET_PIN_STAT_ERR,"设置pin码状态失败"},
	{GET_PIN_STAT_ERR,"获取pin码状态失败"},
	{DESTROY_SHARE_MEM_ERR,"删除共享内存失败"}

};

//根据错误码返回错误信息，不存在则返回NULL
const char * key_get_err_msg(int nErrCode){
	int size = -1, i;
	size = sizeof(err_str_data)/sizeof(err_str_data[0]);  
	for(i = 0; i < size; i++){
		if (err_str_data[i].error == nErrCode)
			return err_str_data[i].string;
	}

	return NULL;
}