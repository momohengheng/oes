#include "Win32_to_Linux.h"

#ifndef _WINDOWS
#include <stdlib.h>
#include <dlfcn.h>

HMODULE LoadLibrary( LPCSTR lpLibFileName ){
    HMODULE retval = NULL;
    retval = (HMODULE)dlopen((const char*)lpLibFileName, RTLD_LAZY);
    return retval;
}

int* GetProcAddress( HMODULE hModule, LPCSTR lpProcName) {
    int* retval = NULL;
    retval = (int*)dlsym((void*)hModule, (const char*)lpProcName);
    return retval;
}

BOOL FreeLibrary( HMODULE hModule) {
    BOOL retval = 0;
    retval = (BOOL)dlclose((void*)hModule);
    return retval;
}

#endif
