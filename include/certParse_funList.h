//CERTPARSE_FUNLIST.h
#ifndef _CERTPARSE_FUNLIST_H
#define _CERTPARSE_FUNLIST_H

#include <stdio.h>

#define CP_PATH_MAX			255
#define CP_MODULE_NAME		"certparse"	
#define CP_SERIAL_LEN		128
#define CP_SUBJECT_LEN		256
#define CP_ISSUE_LEN		256
#define CP_ENDDAYS_LEN		32
#define CP_SIGNDATALEN		256

typedef int  (*pCert_getSerial)(unsigned char* pContent,int nContentLen,char serial[CP_SERIAL_LEN]);
typedef int  (*pCert_getSubject)(unsigned char* pContent,int nContentLen,char subject[CP_SUBJECT_LEN]);
typedef int  (*pCert_getPublicKey)(unsigned char* pContent,int nContentLen,unsigned char *pKeyValue,int *pnKeyLength);
typedef int  (*pCert_getIssuer)(unsigned char* pContent,int nContentLen,char issuer[CP_ISSUE_LEN]);
typedef int  (*pCert_getValidDays)(unsigned char* pContent,int nContentLen,int *pnDays);
typedef int  (*pCert_getEndDays)(unsigned char* pContent,int nContentLen,char endDays[CP_ENDDAYS_LEN]);
typedef int  (*pCert_getSignData)(unsigned char* pContent,int nContentLen,char signData[CP_SIGNDATALEN]);


pCert_getSerial				G_pCert_getSerial = NULL;
pCert_getSubject			G_pCert_getSubject = NULL;
pCert_getPublicKey			G_pCert_getPublicKey = NULL;
pCert_getIssuer				G_pCert_getIssuer = NULL;
pCert_getValidDays			G_pCert_getValidDays = NULL;
pCert_getEndDays			G_pCert_getEndDays = NULL;
pCert_getSignData			G_pCert_getSignData = NULL;

#endif