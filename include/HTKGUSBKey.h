//HTKGUSBKey.h
#ifndef __H__HTKGUSBKEY__H__
#define __H__HTKGUSBKEY__H__

#ifdef WIN32
#define _WIN32_WINNT  0x0400
#include "windows.h"
#else
#define WINAPIV  
#ifdef __cplusplus
#define NULL    0
#else
#define NULL    ((void *)0)
#endif

typedef const char *LPCSTR;
typedef LPCSTR LPCTSTR,LPTSTR;
typedef char TCHAR, *PTCHAR;
#define ALG_CLASS_SIGNATURE             (1 << 13)
#define ALG_TYPE_RSA                    (2 << 9)
#define ALG_SID_RSA_ANY                 0
#define CALG_RSA_SIGN           (ALG_CLASS_SIGNATURE | ALG_TYPE_RSA | ALG_SID_RSA_ANY)
#define PUBLICKEYBLOB           0x6
#define CUR_BLOB_VERSION        2
#define ALG_CLASS_KEY_EXCHANGE          (5 << 13)
#define ALG_TYPE_RSA                    (2 << 9)
#define ALG_SID_RSA_ANY                 0
#define CALG_RSA_KEYX           (ALG_CLASS_KEY_EXCHANGE|ALG_TYPE_RSA|ALG_SID_RSA_ANY)

#endif

#ifdef __cplusplus
//#define  EXPORTDLL extern "C" __declspec(dllexport)
#define  EXPORTDLL extern "C"
#else
#define  EXPORTDLL __declspec(dllexport)
#endif

/*
功能: 
设置当前操作的USB-KEY的类型。

  参数:
  [in] nDevType
  设置USB-KEY的设备类型。
  USB-KEY设备类型1 - TIANYU_TYPE
  USB-KEY设备类型2 - HUASHEN_TYPE
  USB-KEY设备类型3 - HXT_TYPE
  USB-KEY设备类型4 - SDT_TYPE
  USB-KEY设备类型5 - HXT_OLD_TYPE
  USB-KEY设备类型6 - 311_TYPE
	返回:
	0：成功  非0：相应错误码
	
	  注释:
	  该接口为当用户同时插入多个不同类型的USB-KEY时，用户需要选择设定要操作的为哪种KEY。若应用场景为用户当前只插入一个KEY，则不需要调用该函数接口。	  
 */

EXPORTDLL int WINAPIV key_setDevice (int nDevType );


/*
功能：	
设置接口类型

参数：
[in]Type：接口类型，固定为1
返回:
成功返回0，失败返回非0值
注释	
该接口暂时未用
*/
EXPORTDLL int WINAPIV key_setInterface(int nInterfaceType);

/*
功能: 
获取指定类型的USB-KEY的设备数量。

  参数: 
  [out] pDevNum
  当前插入的指定类型的USB-KEY的个数。
  返回:
  0：成功  非0：相应错误码
  
	注释:
	该接口获取当前插入的指定类型的USB-KEY的数量。调用该接口前，若调用SetDevice设定了插入的USB-KEY的类型，则该接口得到的是该指定类型的USB-KEY的数量，否则为系统默认的USB-KEY类型的USB-KEY的数量。	
 */

EXPORTDLL int WINAPIV key_getDeviceNum(int *pDevNum);


/*
功能: 
获取指定类型USB-KEY中用户签名证书的个数。

  参数: 
  [out] pCertNum
  当前插入的指定类型的USB-KEY中证书的个数。
  返回:
  0：成功  非0：相应错误码
  
	注释:
	该接口获取当前插入的指定类型的USB-KEY中用户签名证书的数量。调用该接口前，若调用SetDevice设定了插入的USB-KEY的类型，则该接口得到的是该指定类型的USB-KEY中签名证书的数量，否则为系统默认的USB-KEY类型的USB-KEY中签名证书的数量。	
 */
EXPORTDLL int WINAPIV key_getCertNum(int *pCertNum);


/*
功能: 
获取指定索引的证书序列号。

  参数:
  [in] nIndex 
  证书的索引，默认为-1，从0开始。
  
	[out] serial
	证书的序列号。
	
	  
		返回:
		0：成功  非0：相应错误码。
		
		  注释:
		  该接口获取指定索引的用户签名证书的序列号。若不输索引，则系统默认为当前插入的唯一的USB-KEY中的用户签名证书序列号。		  
 */
EXPORTDLL int WINAPIV key_getCertId (char serial[128] ,int nIndex  = -1);


/*
功能: 
获取指定索引的证书中的用户名。

  参数:
  [in] nIndex 
  证书的索引，默认为-1。
  
	[out] userName
	证书中的用户名。
	
	  返回:
	  0：成功  非0：相应错误码。
	  
		注释:
		该接口获取指定索引的用户签名证书中的用户名。若不输索引，则系统默认为当前插入的唯一的USB-KEY中的用户签名证书中的用户名。		
 */
EXPORTDLL int WINAPIV key_getUserName (char userName[256] ,int nIndex  = -1);


/*
  功能: 
  获取指定索引的证书中的主题项。
  
	参数:
	[in] nIndex
	证书的索引，默认为-1。
	
	  [out] subject
	  证书中的主题项。
	  
		返回:
		0：成功  非0：相应错误码。
		
		  注释:
		  该接口获取指定索引的用户签名证书中的主题项。若不输索引，则系统默认为当前插入的唯一的USB-KEY中的用户签名证书中的主题项。		  
 */
EXPORTDLL int WINAPIV key_getSubject(char subject[256] ,int nIndex = -1);

/*
功能: 
获取指定索引的证书中的证书公钥。

参数:
[in] nIndex
证书的索引，默认为-1。

[out] publicKey
证书中的证书公钥。

返回:
  0：成功  非0：相应错误码。

注释:
 该接口获取指定索引的用户签名证书中的证书公钥。若不输索引，则系统默认为当前插入的唯一的USB-KEY中的用户签名证书中的证书公钥。
	
 */
EXPORTDLL int WINAPIV key_getPublicKey(char publicKey[256] ,int nIndex = -1);


/*
功能: 
获取指定索引的证书中的发布者。

参数:
[in] nIndex
证书的索引，默认为-1。

[out] Issuer
证书中的发布者。

返回:
  0：成功  非0：相应错误码。

注释:
 该接口获取指定索引的用户签名证书中的发布者。若不输索引，则系统默认为当前插入的唯一的USB-KEY中的用户签名证书中的发布者。	
 */
EXPORTDLL int WINAPIV key_getIssuer(char Issuer[256] ,int nIndex = -1);



/*
功能: 
获取指定索引的证书的有效期。

参数:
[in] nIndex
证书的索引，默认为-1。

[out] pnDays
证书的有效期。

返回:
  0：成功  非0：相应错误码。

注释:
 该接口获取指定索引的用户签名证书的有效期。若不输索引，则系统默认为当前插入的唯一的USB-KEY中的用户签名证书的有效期。	
 */
EXPORTDLL int WINAPIV key_getValidDays (int *pnDays ,int nIndex = -1);


/*
功能: 
获取指定索引的证书的到期日。

参数:
[in] nIndex
证书的索引，默认为-1。

[out] EndDays
证书的到期日。

返回:
  0：成功  非0：相应错误码。

注释:
 该接口获取指定索引的用户签名证书的有效期。若不输索引，则系统默认为当前插入的唯一的USB-KEY中的用户签名证书的到期日。	
 */
EXPORTDLL int WINAPIV key_getEndDays (char EndDays[256] ,int nIndex = -1);



/*
功能: 
获取指定索引的证书属性。

参数:
[in] nIndex
证书的索引，默认为-1。

[in] pOid
        证书属性pOid。

[in-out]pAttributeValue
        属性值。

 [in-out]pnAttributeLen
        属性值的长度。

返回:
  0：成功  非0：相应错误码。

注释:
 该接口获取指定索引的用户签名证书的指定扩展项的属性。若不输索引，则系统默认为当前插入的唯一的USB-KEY中的用户签名证书的指定扩展项的属性。此接口为二次调用接口,设置pAttributeValue为NULL,得到属性的长度。再为pnAttributeValue分配相应大小的内存。	
 */
EXPORTDLL int WINAPIV key_getAttribute(char *pOid,unsigned char* pAttributeValue, unsigned long *pnAttributeLen ,int nIndex = -1);


/*
功能: 
生成随机数。

参数:
[in] nIndex
证书的索引，默认为-1。

[in] nLen
产生的随机数的长度。

[out] ranDom
产生的随机数。
返回:
0：成功  非0：相应错误码。

注释:
该接口用于产生指定长度的随机数。传入ranDom参数前为ranDom分配nLen的大小。
*/
EXPORTDLL int WINAPIV key_generateRandom(unsigned char *ranDom, unsigned int nLen,int nIndex = -1);


/*
功能: 
设置当前操作的USB-KEY。

  参数:
  [in] szSerialNo
  证书序列号。
  返回:
  0：成功  非0：相应错误码。
  
	注释:
	该接口设置当前操作的USB-KEY。若应用场景为单KEY的情况，则无需调用此接口。	
 */
EXPORTDLL int WINAPIV key_setNowDeviceId(unsigned char* szSerialNo);



/*
功能:
修改用户口令。

  参数:
  [in] szOld
  旧口令。
  
	[in] szNew 
	新口令。
	
	  返回:
	  0：成功  非0：相应错误码	  

注释：
口令必须>=6位
 */
EXPORTDLL int WINAPIV key_setPin(char *szOld, char *szNew  );


/*
功能:
登陆USB-KEY。

  参数:
  [in]	pin
  pin码
  [out] pLeftCount
  剩余尝试次数。
  
	返回:
	0：成功  非0：相应错误码
	
	  注释:
	  该接口用于登陆USB-KEY。	  
 */
EXPORTDLL int WINAPIV key_login(const char *pin,int *pLeftCount);


/*
功能:
向设备中写入数据

参数:
	[in] szInfoName
	写入数据在设备中的存储标识。
	
	[in] pContent
	写入的数据的内容。
	  
	[in] pContentLen
    写入的数据内容的长度。
		
返回:
	0：成功  非0：相应错误码
		  
注释:
	为存储标识pInfoName分配32个字节。			
 */
EXPORTDLL int WINAPIV key_writeData(const char* szInfoName,unsigned char *pContent,unsigned long pContentLen);


/*
功能:
从设备中读取数据。

  参数:
  [in] szInfoName
  要获取的数据在设备中的存储标识。
  
	[in-out] pContent
	数据的内容。
	
	  [in-out] pContentLen
	  数据的长度。
	  
		返回:
		0：成功  非0：相应错误码
		
		  注释:
		  本接口为二次调用接口,设置pContent为NULL,pContentLen为返回的数据的长度,再根据返回的长度为pContent分配内存。		  
 */
EXPORTDLL int WINAPIV key_readData(const char *szInfoName,unsigned char *pContent,unsigned long *pContentLen);


/*
功能:
对数据进行签名。
参数:
[in] pData
要签名的数据。

[in] nDataLen
要签名的数据的长度。
  
[in-out] pSignedData
签名数据。
	
[in-out] pdwSignedLen
签名数据的长度。
	  
返回:
0：成功  非0：相应错误码
		
注释:
本接口为二次调用接口,设置pSignedData为NULL, pdwSignedLen为返回的数据的长度,再根据返回的长度为pSignedData分配内存。
签名形式为：证书|签名		  
 */
EXPORTDLL int WINAPIV key_sign(char *pData,	int nDataLen,	unsigned char *pSignedData,long  *pdwSignedLen);


/*
功能:
对签名数据进行验证。

参数:
  
[in] pSignedData
签名数据。
	
[int] dwSignedLen
签名数据的长度。
	  
[int] pData
原文数据。
		
[in] dwDataLen
原文数据的长度。
		  
[in] szCACertFile
根证书文件路径。
			
[in] szCrlFile
CRL文件路径。
			  
返回:
0：成功  非0：相应错误码
				
注释:
szCrlFile默认为NULL，此时不做CRL撤销列表校验。				  
 */

EXPORTDLL int WINAPIV key_verify(unsigned char *pSignedData, long dwSignedLen,unsigned char * pData,long dwDataLen,const char *szCACertFile,const char *szCrlFile=NULL);



//20121211 lch add
/*
功能:
对数据进行签名。
参数:
[in] pData
要签名的数据。

[in] nDataLen
要签名的数据的长度。
  
[in-out] pSignedData
签名数据。
	
[in-out] pdwSignedLen
签名数据的长度。
	  
返回:
0：成功  非0：相应错误码
		
注释:
本接口为二次调用接口,设置pSignedData为NULL, pdwSignedLen为返回的数据的长度,再根据返回的长度为pSignedData分配内存。
签名值不包括证书，只是签名值。		  
 */
EXPORTDLL int WINAPIV key_sign_ex(char *pData,	int nDataLen,	unsigned char *pSignedData,long  *pdwSignedLen);


/*
功能:
对签名数据进行验证。

参数:
  
[in] pSignedData
签名数据。
	
[int] dwSignedLen
签名数据的长度。
	  
[int] pData
原文数据。
		
[in] dwDataLen
原文数据的长度。

[int] pCertContent
验证证书数据。

[in] dwCertLen
验证证书数据的长度。

		  
[in] szCACertFile
根证书文件路径。
			
[in] szCrlFile
CRL文件路径。
			  
返回:
0：成功  非0：相应错误码
				
注释:
szCrlFile默认为NULL，此时不做CRL撤销列表校验。				  
 */

EXPORTDLL int WINAPIV key_verify_ex(unsigned char *pSignedData, long dwSignedLen,unsigned char * pData,long dwDataLen,
								unsigned char* pCertContent,long dwCertLen,
								const char *szCACertFile,const char *szCrlFile=NULL);



/*
功能:
对数据进行摘要。
参数:
[in] pData
要摘要的数据。
  
[in] nDataLen
要摘要的数据的长度。
	
[in-out] pDigestData
摘要值。
	  
[in-out] pDigestLen
摘要值的长度。
		
返回:
0：成功  非0：相应错误码
		  
注释:
本接口为二次调用接口,设置pDigestData为NULL, pDigestLen为返回的数据的长度,再根据返回的长度为pDigestData分配内存。			
 */
EXPORTDLL int WINAPIV key_digest(unsigned char *pData,unsigned long nDataLen,unsigned char *pDigestData, unsigned long *pDigestLen);



/***************************2012-12-12 LCH ADD*******************************************************/
/*
  功能:
  对数据进行非对称加密。
  参数:
  [in] pData
  要加密的数据。
  
	[in] nDataLen
	要加密的数据的长度。
	
	  [in] pPublicKey
	  加密公钥。
	  
		[in] nPublicKeyLen
        加密公钥的长度。
		
		  [in-out] pOut
		  加密输出数据。
		  
			[in-out] pOutLen
			加密输出数据的长度。
			
			  返回:
			  0：成功  非0：相应错误码
			  
				注释:
				本接口为二次调用接口,设置pOut为NULL, pOutLen为返回的数据的长度,再根据返回的长度为pOut分配内存				
 */
EXPORTDLL int WINAPIV key_asyEnc(unsigned char *pData, unsigned long nDataLen,
								unsigned char *pCertContent,unsigned int nCertContentLen,
								unsigned char *pOut, unsigned long *pOutLen); 




/*
功能:
对数据进行非对称解密。
参数:
[in] pData
要解密的数据。
  
[in] nDataLen
要解密的数据的长度。
	
[in-out] pOut
解密输出数据。
	  
[in-out] pOutLen
解密输出数据的长度。
		
返回:
0：成功  非0：相应错误码
		  
注释:
本接口为二次调用接口,设置pOut为NULL, pOutLen为返回的数据的长度,再根据返回的长度为pOut分配内存。
 */
EXPORTDLL int WINAPIV key_asyDec(unsigned char *pData, unsigned long nDataLen, unsigned char *pOut, unsigned long *pOutLen); 


/*
功能: 
获取指定索引的证书中的uid。

参数:
[in] nIndex
证书的索引，默认为-1。

[out] Uid
证书中的uid。

返回:
  0：成功  非0：相应错误码。

注释:
 该接口获取指定索引的用户签名证书中的uid。若不输索引，则系统默认为当前插入的唯一的USB-KEY中的用户签名证书中的uid。	
 */
EXPORTDLL int WINAPIV key_getUid(unsigned char Uid[256],int nIndex = -1);


/*****************2013-10-23 Feng add********************/
/*
功能: 
获取usb-key的SN。

参数:
[out] DeviceSN[256]
证书中的SN。

返回:
  0：成功  非0：相应错误码。

注释:
 该接口获取usb-key设备的SN。	
 */
EXPORTDLL int WINAPIV key_getDeviceSN(char DeviceSN[256]);


/****************2014-02-07 lch add*********************/
/*
功能: 
获取指定索引的证书。

参数: 
[in-out] certContent
证书的内容。
[in-out] certContentLen
证书内容长度。
[in] nIndex
证书的索引，默认为-1，从0开始。

返回:
0：成功  非0：相应错误码。

注释:
本接口为二次调用接口,设置certContent为NULL, certContentLen为返回的数据的长度,再根据返回的长度为certContent分配内存。
该接口获取指定索引的用户证书内容。若不输索引，则系统默认为当前插入的唯一的USB-KEY中的用户证书。
 */
EXPORTDLL int WINAPIV key_getCert(char *certContent, int *certContentLen,int nIndex);

/*
功能:
对签名数据进行验证。

参数:
[in] pSignedData
签名数据。

[int] dwSignedLen
签名数据的长度。

[int] pData
原文数据。

[in] dwDataLen
原文数据的长度。

[int] pPubKey
公钥数据。

[in] nPubKeyLen
公钥数据的长度。


返回:
0：成功  非0：相应错误码

注释:
目前系统采用的ECC算法公钥长度为96字节。
*/
EXPORTDLL int  WINAPIV key_verify_ex1(unsigned char *pSignedData, long dwSignedLen,unsigned char * pData,long dwDataLen,unsigned char* pPubKey,long nPubKeyLen);

/*
功能:
对数据进行证书验证。
参数:
[in] pCertContent
要验证的证书内容。

[in] dwCertLen
要验证的证书长度。

[in] pRootCert
根证书内容。

[in] dwRootCertLen
根证书长度。

返回:
0：成功  非0：相应错误码
 */
EXPORTDLL int WINAPIV key_verifyCert(unsigned char* pCertContent,long dwCertLen,unsigned char* pRootCertContent,long dwRootCertLen);

/***************************2014-02-10 LCH ADD*******************************************************/
/*
功能:
对数据进行非对称加密。
参数:
[in] pData
要加密的数据。
  
[in] nDataLen
要加密的数据的长度。
	
[in] pPublicKey
加密公钥。
	  
[in] nPublicKeyLen
加密公钥的长度。
		
[in-out] pOut
加密输出数据。
		  
[in-out] pOutLen
加密输出数据的长度。
			
返回:
0：成功  非0：相应错误码
			  
注释:
本接口为二次调用接口,设置pOut为NULL, pOutLen为返回的数据的长度,再根据返回的长度为pOut分配内存				
 */
EXPORTDLL int WINAPIV key_asyEnc_ex(unsigned char *pData, unsigned long nDataLen,
								unsigned char *pPublicKey,unsigned int nPublicKeyLen,
								unsigned char *pOut, unsigned long *pOutLen);


/*
功能:
对数据进行签名。
参数:
[in] pData
要签名的数据。

[in] nDataLen
要签名的数据的长度。
  
[in-out] pSignedData
签名数据。
	
[in-out] pdwSignedLen
签名数据的长度。
	  
返回:
0：成功  非0：相应错误码
		
注释:
本接口为二次调用接口,设置pSignedData为NULL, pdwSignedLen为返回的数据的长度,再根据返回的长度为pSignedData分配内存。
签名值不包括证书，只是签名值,原文数据为hash数据，不超过64字节。		  
 */
EXPORTDLL int WINAPIV key_sign_ex_nohash(char *pData,	int nDataLen,	unsigned char *pSignedData,long  *pdwSignedLen);



/*
功能:
对签名数据进行验证(原文无需hash)。

参数:
[in] pSignedData
签名数据。

[int] dwSignedLen
签名数据的长度。

[int] pData
原文数据。

[in] dwDataLen
原文数据的长度。

[int] pPubKey
公钥数据。

[in] nPubKeyLen
公钥数据的长度。


返回:
0：成功  非0：相应错误码

注释:
目前系统采用的ECC算法公钥长度为96字节。
*/
EXPORTDLL int  WINAPIV key_verify_ex1_nohash(unsigned char *pSignedData, long dwSignedLen,unsigned char * pData,long dwDataLen, unsigned char* pPubKey,long nPubKeyLen);


/************************************************************************/
/* 
功能:
获取设备状态信息(pin码尝试次数、设备剩余空间、设备是否使用等)
参数:
[in] lstatusProperty
状态属性类型。
[out] lstatusValue
状态值
返回:
0：成功  非0：相应错误码
备注：
状态属性类型定义如下：
enum LABEL_TYPE{
LBL_CONFIG,  
LBL_CERT,  
LBL_KEY,  
LBL_OTHER
};
*/
/************************************************************************/
EXPORTDLL int WINAPIV key_getDeviceStatus(long lstatusProperty, long *lstatusValue);

/************************************************************************/
/*
功能:
枚举文件列表
参数:
[in] fileType
文件类型：0-配置文件 1-证书文件  2-密钥文件 3-其他文件 
[out] pFileList
文件列表
[out] pFileCount
文件数量
返回:
0：成功  非0：相应错误码
*/
/************************************************************************/
EXPORTDLL int WINAPIV key_enumFileList(int fileType,unsigned char pFileList[1024],int *pFileCount);


/************************************************************************/
/*
功能:
枚举容器列表
参数:
[out] pContainerList
容器列表
[out] pContainerCount
容器数量
返回:
0：成功  非0：相应错误码
*/
/************************************************************************/
EXPORTDLL int WINAPIV key_enumContainerList(unsigned char pContainerList[1024],int *pContainerCount);


/************************************************************************/
/*
功能:
登出
参数:无
返回:
登出成功，返回0
登出失败，返回错误码
*/
/************************************************************************/
EXPORTDLL int WINAPIV key_logout();


/************************************************************************/
/*
功能: 
获取指定索引的证书中的签名值。
参数:
[in] nIndex
	证书的索引，默认为-1。
[out] signData
	证书中的签名值。
返回:
0：成功  非0：相应错误码。
注释:
该接口获取指定索引的用户签名证书中的签名值。若不输索引，则系统默认为当前插入的唯一的USB-KEY中的用户签名证书中的签名值。
*/
/************************************************************************/

EXPORTDLL int WINAPIV key_getSignData(char signData[256] ,int nIndex = -1);

/************************************************************************/
/*
功能:
获取错误信息
参数:
[in] nErrCode
错误码
[out] pErrInfo
错误信息
返回:
无
*/
/************************************************************************/
EXPORTDLL void WINAPIV key_getErrorInfo(int nErrCode, char pErrInfo[512]);


/*
功能: 
无编码签名。
参数:
[in] nEnsuleFlag
	封装标识，  1：对签名数据进行封装（签名值包括证书）  0：无需对签名数据进行封装（纯签名值）
[in] nHashFlag
	是否哈希标识，  1：需进行哈希处理   0：无需进行哈希处理
[in] pData
	原文数据内容
[in] nDataLen
	原文数据内容长度
[in-out] pSignedData
	签名值
[in-out] pdwSignedLen
	签名值长度
返回:
	0：成功  非0：相应错误码
注释:
	本接口为二次调用接口,设置pSignedData为NULL, pdwSignedLen为返回的数据的长度,再根据返回的长度为pSignedData分配内存。
	封装的签名数据形式为：签名证书长度+签名证书+签名值长度+签名值
	未封装的签名数据位：签名值
	如果nHashFlag哈希标识为0，原文长度不小于48字节。
*/
EXPORTDLL int WINAPIV key_sign_nocode(int nEnsuleFlag, int nHashFlag, char *pData,	int nDataLen,	unsigned char *pSignedData,long  *pdwSignedLen);


/************************************************************************/
/*
功能: 
无编码验签。
参数:
[in] nEnsuleFlag
	封装标识，  1：签名数据为封装数据（签名值包括证书）  0：签名数据未进行封装（纯签名值）
[in] nHashFlag
	是否哈希标识，  1：需进行哈希处理   0：无需进行哈希处理
[in] pSignedData
	签名值
[in] pdwSignedLen
	签名值长度
[in] pData
	原文数据内容
[in] dwDataLen
	原文数据内容长度
[int] pCertCont
	验证证书数据。
[in] dwCertLen
	验证证书数据的长度。
[in] szCACertFile
	根证书文件路径。
[in] szCrlFile
	CRL文件路径。
返回:
	0：成功  非0：相应错误码
注释:
	szCrlFile默认为NULL，此时不做CRL撤销列表校验。如果nEnuleFlag封装标识为1，对参数：pCertCont及dwCertLen不做判断，
	如果nEnuleFlag封装标识为0，利用证书：pCertCont及dwCertLen进行验签。
	如果nHashFlag哈希标识为0，原文长度不小于48字节。
*/
/************************************************************************/
EXPORTDLL int WINAPIV key_verify_nocode(int nEnuleFlag, int nHashFlag, unsigned char *pSignedData, long dwSignedLen,unsigned char *pData,long dwDataLen,
	unsigned char *pCertCont,long dwCertLen,const char *szCACertFile,const char *szCrlFile);

/************************************************************************/
/*
功能: 
无编码公钥验签。
参数:
[in] nHashFlag
	是否哈希标识，  1：需进行哈希处理   0：无需进行哈希处理
[in] pSignedData
	签名值
[in] pdwSignedLen
	签名值长度
[in] pData
	原文数据内容
[in] dwDataLen
	原文数据内容长度
[int] pCertCont
	验证证书数据
[in] dwCertLen
	验证证书数据的长度
[int] pPubKey
	公钥数据
[in] nPubKeyLen
	公钥数据的长度
返回:
	0：成功  非0：相应错误码
注释:
	此时pSignedData为未封装的签名数据。
*/
/************************************************************************/
EXPORTDLL int WINAPIV key_verify_nocode_pubkey(int nHashFlag, unsigned char *pSignedData, long dwSignedLen,unsigned char * pData,long dwDataLen, 
	unsigned char* pPubKey,long nPubKeyLen);


/*
功能:
删除数据

参数:
	[in] szInfoName
	写入数据在设备中的存储标识。
		
返回:
	0：成功  非0：相应错误码
		  
注释:
	为存储标识pInfoName分配32个字节。			
 */
EXPORTDLL int WINAPIV key_delData(const char* szInfoName);

/*
功能:
删除无用数据

参数:无
		
返回:
	0：成功  非0：相应错误码
		
 */
EXPORTDLL int WINAPIV key_delUselessData(void);


/*
* 功能：设置用户pin码状态
 * @param flag  [IN]- 1:设置长度  2:设置重试次数
 * @param para [IN] - flag =1 :para设置长度; flag = 2:para设置重试次数
 * @return 返回值
 * 成功：0
 * 失败：错误码
*/
EXPORTDLL int WINAPIV key_setUserPinStat(int flag, int para);

/*
* 功能：获取用户pin码长度
 * @param flag  [IN]- 1:获取长度  2:获取重试次数
 * @param para  [OUT]- flag =1  获取pin码长度; flag = 2  获取pin码重试次数
 * @return 返回值
 * 成功：0
 * 失败：错误码
*/
EXPORTDLL int WINAPIV key_getUserPinStat(int flag,int *para);

/*
* 功能：删除共享内存
 * @return 返回值
 * 成功：0
 * 失败：错误码
*/
EXPORTDLL int WINAPIV key_destroyShareMem();

/*
功能:
从设备中读取印章列表。

参数:
[in-out] pContent
数据的内容。
[in-out] pContentLen
数据的长度。
[in-out] pNum
印章数量	  
返回:
0：成功  非0：相应错误码
		
注释:
本接口为二次调用接口,设置pContent为NULL,pContentLen为返回的数据的长度,再根据返回的长度为pContent分配内存,pNum为印章的数量。		  
 */
#ifdef WIN32
EXPORTDLL int WINAPIV key_readStampList(unsigned char *pContent,unsigned long *pContentLen, unsigned long *pNum);
#else
int key_readStampList(unsigned char *pContent,unsigned long *pContentLen, unsigned long *pNum);
#endif

/*
功能:
从设备中读取印章数据。

参数:
[in] pName
签章的名字
[in-out] pContent
数据的内容的指针。
[in-out] pContentLen
数据的长度。
返回:
0：成功  非0：相应错误码

 */
#ifdef WIN32
EXPORTDLL int WINAPIV  key_readStampData(const char *pName,unsigned char** pContent,unsigned long *pContentLen);
#else
int key_readStampData(const char *pName,unsigned char** pContent,unsigned long *pContentLen);
#endif

/*
功能:
读取印章的数据列表

参数：
[in/out]puchSealListData
签章数据列表
[in/out]
签章数据列表的长度

注释：
本接口为二次调用接口,设置puchSealListData为NULL,piSealListDataLen为返回的数据的长度,再根据返回的长度为piSealListDataLen分配内存。		  
*/
#ifdef WIN32
EXPORTDLL int  WINAPIV key_readStampIDList(unsigned char* puchSealListData,int* piSealListDataLen);
#else
int key_readStampIDList(unsigned char* puchSealListData,int* piSealListDataLen);
#endif


/*
功能:
从证书主题中读取CN项
参数:
[in]	pContent:证书内容
[in]	nContentLen:证书内容长度
[out]	username:用户名
返回值: 0:成功  非0:相应错误码
*/
#ifdef WIN32
EXPORTDLL int WINAPIV  key_getSubjectUserName(unsigned char* pContent,int nContentLen,char username[256]);
#else
int key_getSubjectUserName(unsigned char* pContent,int nContentLen,char username[256]);
#endif


#endif