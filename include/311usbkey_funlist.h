
#ifdef WIN32
#define UKAPI			__stdcall
#else
#define UKAPI			
#endif

typedef  int  (UKAPI *pSK_GetDeviceNum)();

typedef int  (UKAPI *pSK_ExportFile)(int AlgType,int Usage,unsigned char *pData,unsigned int *FileLength);

typedef int  (UKAPI *pSK_Login)(const char *pin, unsigned int *leftTryTimes);

typedef int  (UKAPI *pSK_SetPin)(const char *oldPin, const char *newPin);

typedef int  (UKAPI *pSK_Logout)();

typedef  int (UKAPI *pSK_ReadData)(const char *pFileName,unsigned char *pData,unsigned long *FileLength);

typedef int (UKAPI *pSK_SetUserPinStat)(int flag, int para);

typedef int (UKAPI *pSK_GetUserPinStat)(int flag,int *para);

typedef int (UKAPI *pSK_DestroyShareMem)();

