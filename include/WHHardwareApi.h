#ifndef __WELLHOPE_HARDWARE_API_H__
#define __WELLHOPE_HARDWARE_API_H__

#ifdef WIN32
#include "wincrypt.h"
#else
typedef unsigned short      WORD;
typedef unsigned long       DWORD;
typedef unsigned char       BYTE;
typedef BYTE far            *LPBYTE;
#define WINAPI
typedef unsigned int		ALG_ID;

#ifndef IN
#define IN
#endif

#ifndef OUT
#define OUT
#endif

typedef void *HANDLE;

typedef struct _PUBLICKEYSTRUC {
	BYTE    bType;
	BYTE    bVersion;
	WORD    reserved;
	ALG_ID  aiKeyAlg;
} BLOBHEADER, PUBLICKEYSTRUC;
#endif
/////////////////////////////////////////////////////////////////////
//
//	MACRO & CONST & STRUCT DEFINITION

const DWORD WH_CLIENT = 0x01;
const DWORD WH_SERVER = 0x02;

//SSF33 ALGID
#define WH_CALG_SSF33						9
#define WH_CALG_SCB5                        15

//WH ENUM FLAG
#define WH_NEXT								1
#define WH_LAST								2

//CERT ENCODE TYPE
#define WH_X509_ASN_ENCODING				1
#define WH_PKCS_7_ASN_ENCODING				2

//CERT USAGE
#define WH_CERT_ENCRYPT						1
#define WH_CERT_SIGN						2

//CERT PROPERTY
#define WH_KEY_CONTAINER_NAME				0
#define WH_KEY_CONTAINER_TYPE				1
#define WH_CERT_USAGE						5

//CERTSTORE TYPE
#define WH_CERTSTORE_UNUSED					0
#define WH_CERTSTORE_MY						1
#define WH_CERTSTORE_OTHERS					2
#define WH_CERTSTORE_CA						3
#define WH_CERTSOTRE_ROOT					4

#define WH_CERT_ENUM_FIRST					1
#define WH_CERT_ENUM_NEXT					0

#define WH_HASH_ONLY_ONEBLOCK				1
#define WH_HASH_FIRST_BLOCK					2
#define WH_HASH_NEXT_BLOCK					3
#define WH_HASH_LAST_BLOCK					4  

//WH DATA BUFFER
typedef struct _wh_data_buffer{
	DWORD cbData;	
	LPBYTE pbData;
}CERT_CONTENT, *PCERT_CONTENT, 
CERT_INDEX, *PCERT_INDEX, 
KEYVALUE, *PKEYVALUE,
ANYVALUE, *PANYVALUE;

//LABEL
enum LABEL_TYPE{
	LBL_CONFIG,  
	LBL_CERT,  
	LBL_KEY,  
	LBL_OTHER
};

//WH PKI DATA BUFFER
typedef struct _wh_pki_data_buffer{
	int cbData;	
	LPBYTE pbData;
}PKI_DATA, *PPKI_DATA;

#define ICS_MAX_PKC_RSA_SIZE 256
typedef struct{
       BYTE  isPubKey;
       BYTE   n[ ICS_MAX_PKC_RSA_SIZE ]; 
       DWORD  nLen;
       BYTE   e[ ICS_MAX_PKC_RSA_SIZE ]; 
       DWORD  eLen;
       BYTE   d[ ICS_MAX_PKC_RSA_SIZE ]; 
       DWORD  dLen;             
       BYTE   p[ ICS_MAX_PKC_RSA_SIZE]; 
       DWORD  pLen;            
       BYTE   q[ ICS_MAX_PKC_RSA_SIZE ]; 
       DWORD  qLen;             
       BYTE   u[ ICS_MAX_PKC_RSA_SIZE ];
       DWORD  uLen;                  
       BYTE   e1[ ICS_MAX_PKC_RSA_SIZE ]; 
       DWORD  e1Len;            
       BYTE   e2[ ICS_MAX_PKC_RSA_SIZE ]; 
       DWORD  e2Len;           
}IC_PKCINFO_RSA;

#ifdef __cplusplus
extern "C" {
#endif

/////////////////////////////////////////////////////////////////////
//
//	WH_* INTERFACE DEFINITION

DWORD WINAPI
WH_GetSupportedAlgId(
	IN OUT ALG_ID* pAlgid, 
	IN OUT LPDWORD pdwCount
	);

DWORD WINAPI
WH_CreateContainer(
	IN char* szContainerName
	);

DWORD WINAPI
WH_FindContainerAndKeypair(
	IN char* szContainerName,
	IN OUT PKEYVALUE pSignPublicKey,
	IN OUT PKEYVALUE pSignPrivateKey,
	IN OUT PKEYVALUE pEncryptPublicKey,
	IN OUT PKEYVALUE pEncryptPrivateKey
    ); 

DWORD WINAPI
WH_DestroyContainer(
	IN char* szContainerName
	);

DWORD WINAPI
WH_GenSymmetricKey(
	IN ALG_ID algId,
	IN OUT PKEYVALUE pKeyIndex,
	IN DWORD  dwFlag
	);

DWORD WINAPI
WH_ImportSymmetricKey(
	IN PKEYVALUE pKeyValue,
	IN ALG_ID algId,
	IN OUT PKEYVALUE pKeyIndex
	);

DWORD WINAPI
WH_ExportSymmetricKey(
	IN PKEYVALUE pSymmKeyIndex,
	IN PKEYVALUE pEncryptKeyIndex,
	IN OUT PKEYVALUE pExportKeyValue,
	IN ALG_ID algId
	);

DWORD WINAPI
WH_SymmetricEncrypt(
	IN PKEYVALUE pKeyIndex,
	IN OUT PKEYVALUE pKeyContext,
	IN DWORD dwFlag,
	IN LPBYTE pbData,
	IN DWORD dwDataLen,
	OUT LPBYTE pbEncryptedData,
	IN OUT LPDWORD pdwEncryptedDataLen,
	IN ALG_ID algId,
	IN DWORD dwMode
	); 

DWORD WINAPI
WH_SymmetryDecrypt(
	IN PKEYVALUE pKeyIndex,
	IN OUT PKEYVALUE pKeyContext,
	IN DWORD dwFlag,
	IN LPBYTE pbEncryptedData,
	IN DWORD dwEncryptedDataLen,
	OUT LPBYTE pbData,
	IN OUT LPDWORD pdwDataLen,
	IN ALG_ID algId,
	IN DWORD dwMode
	); 
 
DWORD WINAPI
WH_GenAsymmetricKey(
	IN char* szContainerName,
	IN OUT PKEYVALUE pPublicKey,
	IN OUT PKEYVALUE pPrivateKey,
	IN DWORD dwKeySize,
	IN DWORD dwFlag,
	IN ALG_ID algId
	);

DWORD WINAPI
WH_ImportPublicKey(
	IN PKEYVALUE pKeyValue,
	IN ALG_ID algId,
	IN OUT PKEYVALUE pKeyIndex
	);

DWORD WINAPI
WH_ImportPrivateKey(
	IN char* szContainerName,
	IN PKEYVALUE pKeyValue,
	IN ALG_ID algId,
	IN OUT PKEYVALUE pPublicKeyIndex,
	IN OUT PKEYVALUE pPrivateKeyIndex
	);

DWORD WINAPI
WH_ExportPublicKey(
	IN PKEYVALUE pKeyIndex,
	IN OUT PKEYVALUE pExportKeyValue
	);

DWORD WINAPI
WH_ExportPrivateKey(
	IN PKEYVALUE pKeyIndex,
	IN PKEYVALUE pEncryptKeyIndex,
	IN OUT PKEYVALUE pExportKeyValue,
	IN ALG_ID	algId
	);

DWORD WINAPI
WH_AsymmetricEncrypt(
	IN PKEYVALUE pKeyIndex,
	IN OUT PKEYVALUE pKeyContext,
	IN DWORD dwFlag,
	OUT LPBYTE pbEncryptedData,
	IN OUT LPDWORD pdwEncryptedDataLen,
	IN LPBYTE pbData,
	IN DWORD dwDataLen,
	IN ALG_ID algId
	);

DWORD WINAPI
WH_AsymmetricDecrypt(
	IN PKEYVALUE pKeyIndex,
	IN OUT PKEYVALUE pKeyContext,
	IN DWORD dwFlag,
	OUT LPBYTE pbData,
	IN OUT LPDWORD pdwDataLen,
	IN LPBYTE pbEncryptedData,
	IN DWORD dwEncryptedDataLen,
	IN ALG_ID algId
	);

DWORD WINAPI
WH_AsymmetricSignData(
	IN PKEYVALUE pKeyIndex,
	IN LPBYTE pbData,
	IN DWORD dwDataLen,
	OUT LPBYTE pbSignature,
	IN OUT LPDWORD pdwSignatureLen,
	IN ALG_ID algId
	);

DWORD WINAPI
WH_AsymmetricVerifySign(
	IN PKEYVALUE pKeyIndex,
	OUT LPBYTE pbData,
	IN OUT LPDWORD pdwDataLen,
	IN LPBYTE pbSignature,
	IN DWORD dwSignatureLen,
	IN ALG_ID algId
	);

DWORD WINAPI
WH_DestroyKey(
	IN PKEYVALUE pKeyIndex
	);

DWORD WINAPI
WH_GetKeyProperty(
	IN PKEYVALUE pKeyIndex,
	IN DWORD dwParam,
	OUT LPBYTE pbData,
	IN OUT LPDWORD pdwDataLen
	);

DWORD WINAPI
WH_GenRandom(
	IN PKEYVALUE pRandom
	);

DWORD WINAPI
WH_Digest(
	OUT LPBYTE pbDigest,
	IN OUT LPDWORD pdwDigestLen,
	IN LPBYTE pbData,
	IN DWORD dwDataLen,
	IN DWORD algId
	);

DWORD WINAPI
WH_EnablePVK(
	IN const char* szContainerName,
	IN PANYVALUE pValue,
	IN DWORD dwFlags
	);

DWORD WINAPI
WH_DisabelPVK(
	IN const char* szContainerName
	);

DWORD WINAPI
WH_SetPVKEnabelData(
	IN const char* szContainerName,
	IN PANYVALUE pOldValue,
	IN PANYVALUE pNewValue,
	IN DWORD dwFlags
	);

BOOL WINAPI
WH_IsPVKEnabeled(
	IN const char* szContainerName
	);

/////////////////////////////////////////////////////////////////////
//
//	WHCERT_* INTERFACE DEFINITION

DWORD WINAPI 
WHCERT_CreateCertStore(
	IN char* szCertStoreName
	);

DWORD WINAPI
WHCERT_FindCertStore(
	IN char* szCertStoreName
	);

DWORD WINAPI
WHCERT_DestroyCertStore(
	IN char* szCertStoreName
	);

DWORD WINAPI
WHCERT_ImportCert(
	IN char* szCertStoreName,
	IN PCERT_CONTENT pCertContent,
	IN DWORD dwCertEncode,
	IN DWORD dwStoreType,
	IN OUT PCERT_INDEX pCertIndex,
	IN DWORD dwCertUsage
	);

DWORD WINAPI
WHCERT_ExportCert(
	IN OUT PCERT_CONTENT pCertContent,
	IN DWORD dwCertEncode,
	IN PCERT_INDEX pCertIndex
	);

DWORD WINAPI
WHCERT_DestroyCert(
	IN PCERT_INDEX pCertIndex
	);

DWORD WINAPI
WHCERT_GetCertProperty(
	IN PCERT_INDEX pCertIndex,
	IN DWORD dwParam,
	OUT LPBYTE pbData,
	IN OUT LPDWORD pdwDataLen
	);

DWORD WINAPI
WHCERT_SetCertProperty(
	IN PCERT_INDEX pCertIndex,
	IN DWORD dwParam,
	IN LPBYTE pbData,
	IN DWORD dwDataLen
	);

DWORD WINAPI
WHCERT_EnumCertInStore(
	IN char* szCertStoreName,
	IN DWORD dwStoreType,
	IN OUT PCERT_INDEX pCertIndex,
	IN DWORD dwFlags
	);

DWORD WINAPI
WHCERT_GetUserCert(
	IN char* szCertStoreName,
	IN DWORD dwCertUsage,
	IN OUT PCERT_CONTENT pCertContent
	);

/////////////////////////////////////////////////////////////////////
//
//	WHCA_* INTERFACE DEFINITION

DWORD WINAPI
WHCA_ImportPrivateKey(
	IN PPKI_DATA pKeyValue, 
	IN LPBYTE pbPassword,
	IN DWORD cbPassword,
	IN OUT PPKI_DATA pKeyIndex
	);

DWORD WINAPI 
WHCA_ExportPrivateKey(
	IN PPKI_DATA pKeyIndex,
	IN LPBYTE pbPassword,
	IN DWORD cbPassword,
	IN OUT PPKI_DATA pKeyValue 
	);

DWORD WINAPI
WHCA_UnLock(
	IN LPBYTE pbSOPin,
	IN DWORD cbSOPin
	);

//DWORD WINAPI
//WHCA_Format(
//	IN LPBYTE pbSOPin,
//	IN DWORD cbSOPin,
//	IN int nUserMaxRetry
//	);
DWORD WINAPI
WHCA_Format(
	IN LPBYTE pbSOPin,
	IN DWORD cbSOPin
	);

DWORD WINAPI
WHCA_Login(
	IN int nType, 
	IN char* szPin
	);

DWORD WINAPI
WHCA_Logout();

DWORD WINAPI
WHCA_SetPin(
	IN int nType, 
	IN char* szPin
	);

DWORD WINAPI
WHCA_ImportFile(
	IN LPBYTE pbLabel, 
	IN LABEL_TYPE type, 
	IN LPBYTE pbData, 
	IN int nSize
	);

DWORD WINAPI
WHCA_ExportFile(
	IN LPBYTE pbLabel, 
	IN LABEL_TYPE type, 
	OUT LPBYTE pbData, 
	IN OUT int* pSize
	);

DWORD WINAPI
WHCA_DeleteFile(
	IN LPBYTE pbLabel, 
	IN LABEL_TYPE type
	);

DWORD WINAPI
WHCA_ReadLabelList(
	IN LABEL_TYPE type,
	OUT LPBYTE pbLabels,
	OUT int* pCount
	);

DWORD WINAPI
WHCA_GenKeyByPassword(
	IN LPBYTE pbData, 
	IN DWORD dwDatalen, 
	IN OUT PPKI_DATA pKeyIndex
	);

DWORD WINAPI
WHCA_LockDevice();
//DWORD WINAPI
//WHCA_LockDevice();

DWORD WINAPI
WHCA_EnumCerstoreName(
	OUT LPBYTE pbData, 
	IN OUT LPDWORD pdwDataLen
	);

DWORD WINAPI
WHCA_EnumKeyContainerName(
	OUT LPBYTE pbData, 
	IN OUT LPDWORD pdwDataLen
	);

// DWORD WINAPI
// WH_ImportPrivateKeyEx(
// 	IN char* pContainerName,
// 	IN KEYVALUE* pKeyValue,
// 	IN ALG_ID algId,
// 	IN OUT PKEYVALUE pPublicKeyIndex,
// 	IN OUT PKEYVALUE pPrivateKeyIndex,
// 	IC_PKCINFO_RSA rsaKey
// 	);
DWORD WINAPI
WH_ImportPrivateKeyEx(
	IN char* pContainerName,
	IN KEYVALUE* pKeyValue,
	IN ALG_ID algId,
	IN OUT PKEYVALUE pPublicKeyIndex,
	IN OUT PKEYVALUE pPrivateKeyIndex,
	IC_PKCINFO_RSA rsaKey
	);

DWORD WINAPI
WH_GetKeyPropertyEx(
	IN ALG_ID algId,
	IN DWORD dwParam,
	IN OUT BYTE* pbData,
	IN OUT DWORD* pdwDataLen
	);


//DWORD WINAPI
//WH_GetKeyPropertyEx(
//	IN ALG_ID algId,
//	IN DWORD dwParam,
//	IN OUT BYTE* pbData,
//	IN OUT DWORD* pdwDataLen
//	);
//
DWORD WINAPI
WHCA_GetDeviceStatus(
	IN long lstatusProperty,
	IN long* lstatusValue
	);

//DWORD WINAPI
//WHCA_GetDeviceStatus(
//	IN long lstatusProperty,
//	IN long* lstatusValue
//	);

DWORD WINAPI WH_DigestEx(
	IN OUT BYTE* pbDigest,
	IN OUT DWORD* pdwDigestLen,
	IN BYTE* pbPartIn,
	IN DWORD dwPartInLen,
	IN DWORD dwDigestAlgorithm,
	IN DWORD pbTotalBit[2],
	IN DWORD dwFlag
	);

DWORD WINAPI WHCA_GetKeyPairDer(
	IN int nKeySize,
	IN PKI_DATA* pPublic,
	IN PKI_DATA* pPrivate
	);

DWORD WINAPI WH_AsymmetricVerifySignEx(
    IN KEYVALUE* KeyIndex,
	IN BYTE *Hash,
	IN DWORD dwHashLen,
	IN BYTE *Signature,
	IN DWORD SignatureLen
	);

DWORD WH_AsymmetricReceive(
	IN KEYVALUE * pKeyIndex, 
	IN BYTE *Random, 
	IN DWORD RandomLen,
	IN KEYVALUE * PubKey,
	IN BYTE *Point,
	IN DWORD PointLen,
	IN BYTE * id_a,
	IN DWORD id_aLen,
	IN BYTE *id_b,
	IN DWORD id_bLen,
	OUT BYTE *WorkKey,
	OUT	DWORD *WorkKeyLen
	);


DWORD WH_AsymmetricSend(
	IN BYTE *Random, 
    IN DWORD RandomLen,
	OUT BYTE *Point,
	OUT DWORD *PointLen
	);

//////////////////////////////////////////////////////////////////////////
//DeviceControl
int WINAPI
WH_GetDeviceList(
	HANDLE *phDevList, 
	int& nCount
	);

HANDLE WINAPI WH_GetDevice();

BOOL WINAPI WH_SetDevice(
			HANDLE hDevice
			);

BOOL WINAPI WH_CheckDevice(
			HANDLE hDevice
			);
#ifdef __cplusplus
}			// Balance extern "C" above
#endif

#endif