/**
  * @file       Win32_to_Linux.h
  * @author     朱宇
  * @date       2012-11-15 19:43
  * @version    0.1
  * @copyright  SDT
  * @brief      实现Linux环境下的Windows调用。
  * 
  * Win32_to_Linux模块为了将整个ACS工程从WINDOWS环境移植到Linux，
  * 且保证最大限度地不改变目前工程代码的。使用时，只需要在相应的头文件中添加下面三行代码。
  * #ifndef WIN32
  * #include "Win32_to_Linux.h"
  * #endif
  * 
 */
#ifndef WIN32_TO_LINUX_H
#define WIN32_TO_LINUX_H

#ifdef __cplusplus
extern "C" {
#endif

#ifndef WIN32
#define WINAPI 

// For WinNT.h
typedef void* PVOID;
typedef const char *LPCSTR, *PCSTR; // FIXME: this has been modified
typedef LPCSTR LPCTSTR;

#ifdef STRICT
typedef void *HANDLE;
#define DECLARE_HANDLE(name) struct name##__{int unused;}; typedef struct name##__ *name;
#else
typedef PVOID HANDLE;
#define DECLARE_HANDLE(name) typedef HANDLE name
#endif
typedef HANDLE *PHANDLE;

// For WinDef.h
typedef int            BOOL;
typedef unsigned char  BYTE, *LPBYTE;
typedef unsigned short WORD;
typedef unsigned long  DWORD;

DECLARE_HANDLE(HINSTANCE);
typedef HINSTANCE HMODULE;
DECLARE_HANDLE(HKEY);
typedef HKEY *PHKEY;

// For WinCrypt.h
typedef unsigned int ALG_ID;
typedef struct _PUBLICKEYSTRUC {
    BYTE    bType;
    BYTE    bVersion;
    WORD    reserved;
    ALG_ID  aiKeyAlg;
} BLOBHEADER, PUBLICKEYSTRUC;

typedef struct _RSAPUBKEY {
    DWORD   magic;
    DWORD   bitlen;
    DWORD   pubexp;
} RSAPUBKEY;

/*typedef struct _CERT_CONTEXT {
    DWORD        dwCertEncodingType;
    BYTE         *pbCertEncoded;
    DWORD        cbCertEncoded;
    PCERT_INFO   pCertInfo;
    HCERTSTORE   hCertStore;
} CERT_CONTEXT, *PCERT_CONTEXT;
typedef const CERT_CONTEXT *PCCERT_CONTEXT;
*/

// For Win32_to_Linux.h
/** @defgroup 操作库函数
 *  Windows下库操作函数的Linux实现
 *  @{
 */

/**
  * @brief  加载库
  * @param  LPCSTR 库名
  * @return 加载成功返回非0地址
 */
HMODULE LoadLibrary(LPCSTR lpLibFileName);

/**
  * @brief  得到库中函数模块
  * @param  hModule 指向成功加载的库的句柄（地址）
  * @param  lpProcName 库中函数名称
  * @return 返回函数地址，失败返回0
 */
int* GetProcAddress(HMODULE hModule, LPCSTR lpProcName);

/**
  * @brief  释放库资源
  * @param  hModule 指向成功加载的库的句柄（地址）
  * @return 成功返回TRUE,失败放回FALSE
 */
BOOL FreeLibrary(HMODULE hModule);

/** @} */

#endif
#ifdef __cplusplus
}
#endif
#endif

