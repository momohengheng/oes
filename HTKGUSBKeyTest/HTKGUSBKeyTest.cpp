// HTKGUSBKeyTest.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"


//检测内存泄露
#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>
//检测内存泄露


#define _WIN32_WINNT  0x0400
#include "windows.h"
#include "comutil.h"
#include <wincrypt.h>

#include "functionType.h"
#include "HTKG_USBKey_Errno.h"
#include "HTKGUSBKey.h"

#pragma comment(lib,"KeyLog.lib")
#pragma comment(lib,"TJJUSBKey.lib")
#pragma comment(lib, "crypt32.lib")

#define CACERT		"root.cer"
#define SIGCERT		"s_c.cer"
#define ENCCERT		"e_c.cer"
#define PAINFILE	"file.dat"
#define SIGNFILE	"sign.dat"
#define PUBKEYFILE	"pubkey.dat"
#define HASHFILE	"digest.dat"
#define CACERT_LABEL	"root"
#define SIGCERT_LABEL	"s_c"
#define ENCCERT_LABEL	"e_c"
#define ENCCERTNAME			"EncCert"			//加密证书
#define SIGNCERTNAME		"SignCert"			//签名证书
#define ROOTCERTNAME		"RootCACert"		//根证书名称

int sign_nocode(int nEnuseFlag, int nHashFlag,char *pData, int nDataLen, unsigned char **pSignData, long *pdwSignedLen);
int verify_nocode(int nEnuleFlag, int nHashFlag, unsigned char *pSignedData, long dwSignedLen,unsigned char *pData,long dwDataLen,
	unsigned char *pCertCont,long dwCertLen,const char *szCACertFile,const char *szCrlFile );
int  verify_nocode_pubkey(int nHashFlag, unsigned char *pSignedData, long dwSignedLen,unsigned char * pData,long dwDataLen, 
	unsigned char* pPubKey,long nPubKeyLen);

/* 
	base64编码方法
	输入参数1:原文
	输入参数2:原文长度
	返回值:编码字符串
*/
char *base64_encode(unsigned char *s,int srclen)

{
	unsigned char *p = s, *e;
	char *r, *_ret;
	int len = srclen;
	int mod=0;
	unsigned char unit[4], temp[3];
	char B64[64] ={
	'A','B','C','D','E','F','G','H',
	'I','J','K','L','M','N','O','P',
	'Q','R','S','T','U','V','W','X',
	'Y','Z',
	'a','b','c','d','e','f','g','h',
	'i','j','k','l','m','n','o','p',
	'q','r','s','t','u','v','w','x',
	'y','z',
	'0','1','2','3','4','5','6','7',
	'8','9','+','/'
	};

	
	e = s + len;
	len = len / 3 * 4 + 4 + 1;
	r = _ret = (char *)malloc(len);
	
	while (p < e) {
		memset(temp,0,3);
		if (e-p >= 3) {
			memcpy(temp,p,3);
			p += 3;
		} else {
			memcpy(temp,p,e-p);
			mod=3-(e-p);
			p = e;
		}
		
		unit[0] = temp[0] >> 2;
		unit[1] = temp[0] << 6;
		unit[1] = (unit[1]>>2) | (temp[1]>>4);
		unit[2] = temp[1] << 4;
		unit[2] = (unit[2]>>2) | (temp[2]>>6);
		unit[3] = temp[2] << 2;
		unit[3] = unit[3] >> 2;
		*r++ = B64[unit[0]];
		*r++ = B64[unit[1]];
	    *r++ = ( (mod==2) ? '=': B64[unit[2]]);
	    *r++ = ( ( mod==2 || mod==1 ) ? '=': B64[unit[3]]);
		//*r++ = (unit[2] ? B64[unit[2]] : '=');
		//*r++ = (unit[3] ? B64[unit[3]] : '=');


	}
	*r = 0;
	
	return _ret;
}

/*
base64解码方法
输入参数1:编码数据
输入参数2:解码后的长度
返回值:解码后的字节数组
*/
unsigned char *base64_decode(char *s,int *decode_len)
{
	char *p = s, *e;
	unsigned char *r, *_ret;
	int len = strlen(s);
	int padlen=0;
	unsigned char i, unit[4];

		char B64[64] ={
	'A','B','C','D','E','F','G','H',
	'I','J','K','L','M','N','O','P',
	'Q','R','S','T','U','V','W','X',
	'Y','Z',
	'a','b','c','d','e','f','g','h',
	'i','j','k','l','m','n','o','p',
	'q','r','s','t','u','v','w','x',
	'y','z',
	'0','1','2','3','4','5','6','7',
	'8','9','+','/'
	};
	
	*decode_len = len/4 * 3;
	e = s + len;
	len = len / 4 * 3 + 1;
	r = _ret = (unsigned char *)malloc(len+3);
	
	while (p < e) {
		memcpy(unit,p,4);
		if (unit[3] == '=')
		{
			unit[3] = 0;
			padlen++;
		}
		if (unit[2] == '=')
		{
			unit[2] = 0;
			padlen++;
		}
		p += 4;
		
		for (i=0; unit[0]!=B64[i] && i<64; i++);
		unit[0] = i==64 ? 0 : i;
		for (i=0; unit[1]!=B64[i] && i<64; i++);
		unit[1] = i==64 ? 0 : i;
		for (i=0; unit[2]!=B64[i] && i<64; i++);
		unit[2] = i==64 ? 0 : i;
		for (i=0; unit[3]!=B64[i] && i<64; i++);
		unit[3] = i==64 ? 0 : i;
		*r++ = (unit[0]<<2) | (unit[1]>>4);
		*r++ = (unit[1]<<4) | (unit[2]>>2);
		*r++ = (unit[2]<<6) | unit[3];
	}
	*r = 0;

	*decode_len=*decode_len-padlen;
	
	return _ret;
}

int WriteToFile(char * fileName, unsigned char * fileContent, int fileContentLen)
{
	int pos = 0, j = 0;
	LPTSTR lpszCurDir; 
	TCHAR tchBuffer[256]={'\0'}; 

	lpszCurDir = tchBuffer; 
	GetModuleFileName(NULL,lpszCurDir,256);

	for ( pos=strlen(lpszCurDir)-1; pos>0; pos--)
	{
		if  ( lpszCurDir[pos] == '\\' )
			break;
	}

	for (  j=pos+1; j<strlen(lpszCurDir) ; j++)
	{
		memset(lpszCurDir+j,'\0',1);
	}

	strcat(lpszCurDir,fileName);

	FILE *hfp = NULL;
	hfp = fopen(lpszCurDir, "wb");
	if ( hfp == NULL)
	{
		printf("create file failed\n");
		return -1;
	}
	fseek(hfp, 0, SEEK_END);
	fwrite(fileContent,1,fileContentLen,hfp);
	fclose(hfp);
	return 0;
}

int ReadFromFile(char * fileName,unsigned char ** fileContent,unsigned int * fileContentLen)
{
	int pos = 0, ret = 0;
	unsigned int j = 0;
	LPTSTR lpszCurDir; 
	FILE *hfp = NULL;
	TCHAR tchBuffer[256]={'\0'}; 
	lpszCurDir = tchBuffer; 

	GetModuleFileName(NULL,lpszCurDir,256);

	for ( pos=strlen(lpszCurDir)-1; pos>0; pos--)
	{
		if  ( lpszCurDir[pos] == '\\' )
			break;
	}

	for ( j=pos+1; j<strlen(lpszCurDir) ; j++)
	{
		memset(lpszCurDir+j,'\0',1);
	}

	strcat(lpszCurDir,fileName);

	hfp = fopen(lpszCurDir, "rb");

	if ( hfp == NULL)
	{
		printf("can't find file: %s\n",lpszCurDir);
		return -1;
	}

	fseek(hfp, 0, SEEK_END);
	*fileContentLen = ftell(hfp);
	*fileContent =(unsigned char*) malloc((*fileContentLen)*sizeof(unsigned char));
	memset(*fileContent,'\0',*fileContentLen);
	fseek(hfp, 0, SEEK_SET);
	fread(*fileContent, 1, *fileContentLen, hfp);
	fclose(hfp);

	return 0;
}

int getDeviceSN()
{
	int ret=-1;

	//key_getDeviceSN
	char devSN[256] = {'\0'};
	ret = key_getDeviceSN(devSN);
	if(ret == 0)
	{
		printf("getDevSN success,is:%s\n",devSN);
	}
	else
	{
		printf("getDevSN failed,err no is:%d,err string  is:%s\n",ret,key_get_err_msg(ret));
	}
	return ret;
}

int SetDevice()
{
	int ret = -1;
	ret=key_setDevice(6);
	if(ret==0)
	{
		printf("key_setDevice success\n");
	}
	else
	{
		printf("key_setDevice failed,err no is:%d,err string  is:%s\n",ret,key_get_err_msg(ret));
	}
	return ret;
}

int SetInterface()
{
	int ret = -1;
	ret=key_setInterface(3);
	if(ret==0)
	{
		printf("key_setInterface success\n");
	}
	else
	{
		printf("key_setInterface failed,err no is:%d,err string  is:%s\n",ret,key_get_err_msg(ret));
	}
	return ret;
}

int GetKeyNum()
{
	int keyNum = -1;
//	keyNum = key_getKeyNum(NULL,0);
	printf("keyNum is:%d\n",keyNum);
	return keyNum;
}

int GetId()
{
	char paraId[64];
	memset(paraId,'\0',64);
	strcpy(paraId,"029110010000822001");

	char *id =NULL;
//	id = key_getKeyUserId((char *)paraId,0);
	printf("id is:%s\n",id);
	return 0;
}

int getDeviceNum()
{
	int devNum=0;
    int ret = -1;
	ret=key_getDeviceNum(&devNum);
	if(ret==0)
	{
		printf("key_getDeviceNum success,dev num is:%d\n",devNum);
	}
	else
	{
		printf("key_getDeviceNum failed,err no is:%d,err string  is:%s\n",ret,key_get_err_msg(ret));
	}
	return ret;
}

int getCertNum()
{
	int ret = -1;
	int certNum=0;
	ret=key_getCertNum(&certNum);
	if(ret==0)
	{
		printf("key_getCertNum success,cert num is:%d\n",certNum);
	}
	else
	{
		printf("key_getCertNum failed,err no is:%d,err string  is:%s\n",ret,key_get_err_msg(ret));

	}
	return ret;
}

int getCertId(int index,char serial[128])
{
	int ret = -1;
	int k = 0;
	for( k=0;k<index;k++){
		ret=key_getCertId(serial,k);
		if(ret==0){
			printf("key_getCertId success,index %d ,serial is:%s\n",k+1,serial);
		}else{
			printf("key_getCertId failed,err no is:%d,err string  is:%s\n",ret,key_get_err_msg(ret));
		}
	}
	return ret;
}

int getAttribute(int index)
{
	int ret = -1;
	int k = 0;
	unsigned char *pAttributeValue=NULL;
	unsigned long pnAttributeLen=0;
	for( k=0;k<index;k++)
	{
		ret=key_getAttribute("2.5.29.15",pAttributeValue,&pnAttributeLen,k);
		if(ret==0)
		{
			pAttributeValue=(unsigned char*)malloc(pnAttributeLen*sizeof(unsigned char));
			memset(pAttributeValue,'\0',pnAttributeLen);
			ret=key_getAttribute("2.5.29.15",pAttributeValue,&pnAttributeLen,k);
			if(ret==0)
			{
				printf("key_getAttribute success,index is:%d ,attribute is:%s\n",k+1,pAttributeValue);
			}
			else
			{
				printf("key_getAttribute second call failed,err no is:%d,err string  is:%s\n",ret,key_get_err_msg(ret));
			}
		}
		else
		{
			printf("key_getAttribute first call failed,err no is:%d,err string  is:%s\n",ret,key_get_err_msg(ret));
		}
		if(pAttributeValue)
		{
			free(pAttributeValue);
			pAttributeValue=NULL;
		}
	}
	return ret;
}

int getUid(int index)
{
	int ret = -1;
	int k = 0;
	unsigned char uid[256]={'\0'};	
	for( k=0;k<index;k++)
	{
		ret=ret=key_getUid(uid,k);
		if(ret==0)
		{
			printf("key_getUid success,index %d ,uid is:%s\n",k+1,uid);
		}
		else
		{
			printf("key_getUid failed,err no is:%d,err string  is:%s\n",ret,key_get_err_msg(ret));
		}
	}
	return ret;
}

int getUserName(int index)
{
	int ret = -1;
	int k = 0;
	char userName[256]={'\0'};
	for( k=0;k<index;k++)
	{
		ret=key_getUserName(userName,k);
		if(ret==0)
		{
			printf("key_getUserName success,index is:%d,userName is:%s\n",k+1,userName);
		}
		else
		{
			printf("key_getUserName failed,err no is:%d,err string  is:%s\n",ret,key_get_err_msg(ret));
		}
	}
	return ret;
}

int getSubject(int index)
{
	int ret = -1;
	int k = 0;
	char subject[256]={'\0'};
	memset(subject,'\0',256);

	for( k=0;k<index;k++)
	{
		ret=key_getSubject(subject,k);
		if(ret==0)
		{
			printf("key_getSubject success,index is:%d,subject is:%s\n",k+1,subject);
		}
		else
		{
			printf("key_getSubject failed,err no is:%d,err string  is:%s\n",ret,key_get_err_msg(ret));
		}
	}
	return ret;
}

int getPublicKey(int index,char pubkey[256])
{
	int ret  = -1;
	int k =	 0;
	ret=key_getPublicKey(pubkey,index);
	if(ret==0)
	{
		printf("key_getPublicKey success,pubkey is:%s\n",pubkey);
	}
	else
	{
		printf("key_getPublicKey failed,err no is:%d,err string  is:%s\n",ret,key_get_err_msg(ret));
	}

	WriteToFile("pubkey.dat",(unsigned char*)pubkey,96);

	return ret;
}

int getSignData(int index,char signdata[256])
{
	int ret  = -1;
	int k =	 0;
	ret=key_getSignData(signdata,index);
	if(ret==0)
	{
		printf("key_getSignData success,signdata is:%s\n",signdata);
	}
	else
	{
		printf("key_getSignData failed,err no is:%d,err string  is:%s\n",ret,key_get_err_msg(ret));
	}

	WriteToFile("signdata.dat",(unsigned char*)signdata,96);

	return ret;
}

int getIssuer(int index)
{
	int ret = -1;
	int k = 0;
	char issuer[256]={'\0'};
	ret=key_getIssuer(issuer,index);
	if(ret==0)
	{
		printf("key_getIssuer success,issuer is:%s\n",issuer);
	}
	else
	{
		printf("key_getIssuer failed,err no is:%d,err string  is:%s\n",ret,key_get_err_msg(ret));
	}
	return ret;
}

int getValidDays(int index)
{
	int ret = -1;
	int k = 0;
	int nValidDays=0;
	ret=key_getValidDays(&nValidDays,index);
	if(ret==0)
	{
		printf("key_getValidDays success,validDays is:%d\n",nValidDays);
	}
	else
	{
		printf("key_getValidDays failed,err no is:%d,err string  is:%s\n",ret,key_get_err_msg(ret));
	}
	return ret;
}

int getEndDays(int index)
{
	int ret = -1;
	int k = 0;
	char endDays[256]={'\0'};
	ret=key_getEndDays(endDays,index);
	if(ret==0)
	{
		printf("key_getEndDays success,endDays is:%s\n",endDays);
	}
	else
	{
		printf("key_getEndDays failed,err no is:%d,err string  is:%s\n",ret,key_get_err_msg(ret));
	}
	return ret;
}

int generateRandom(int index)
{
	int ret = -1;
	int k = 0;
	unsigned char random[64]={'\0'};
//	ret=key_generateRandom(random,64,index);
	if(ret==0)
	{
		printf("key_generateRandom succes\n");
	}
	else
	{
		printf("key_generateRandom failed,err no is:%d,err string  is:%s\n",ret,key_get_err_msg(ret));
	}
	return ret;
}

int setNowDeviceId(char serial[128])
{
	int ret = -1;
	ret=key_setNowDeviceId((unsigned char*)serial);
	if(ret==0)
	{
		printf("key_setNowDeviceId success\n");
	}
	else
	{
		printf("key_setNowDeviceId failed,err no is:%d,err string  is:%s\n",ret,key_get_err_msg(ret));
	}
	return ret;
}

int usrLogin(char pin[32],int *leftCount)
{
	int ret = -1;
	ret=key_login(pin,leftCount);
	if(ret==0){
		printf("key_login success\n");
	}else{
		printf("key_login failed,leftCount is:%d\n",*leftCount);
	}
	return ret;
}

int setUsrPin(char oldPin[32],char newPin[32])
{
	int ret = -1;
	ret=key_setPin(oldPin,newPin);
	if(ret==0){
		printf("key_setPin success\n");
	}else{
		printf("key_setPin failed,err no is:%d,err string  is:%s\n",ret,key_get_err_msg(ret));
	}
	return ret;
}


int hash(unsigned char *fileContent, unsigned long fileContentLen)
{
	int ret = -1;
	unsigned char* digest=NULL;
	unsigned long  digestLen=0;
	ret=key_digest(fileContent,fileContentLen,digest,&digestLen);
	if(ret==0)
	{
		digest=(unsigned char*)malloc(digestLen);
		memset(digest,'\0',digestLen);
		ret=key_digest(fileContent,fileContentLen,digest,&digestLen);
		if(ret==0)
		{
			printf("key_digest sussess!\n");
		}
		else
		{
			printf("key_digest failed,err no is:%d,err string  is:%s\n",ret,key_get_err_msg(ret));
		}
	}

	if(digest)
	{
		free(digest);
		digest = NULL;

	}
	return ret;
}

int writeData(char fileName[32],unsigned char *fileContent,unsigned long fileContentLen)
{
	int ret = -1;
	ret=key_writeData(fileName,fileContent,fileContentLen);
	if(ret==0)
	{
		printf("key_writeData success\n");
	}
	else
	{
		printf("key_writeData failed,err no is:%d,err string  is:%s\n",ret,key_get_err_msg(ret));
	}
	return ret;
}

int readData(char fileName[32])
{
	int ret = -1;
	//unsigned char readData[2048]={'\0'};
	//unsigned long readDataLen=0;
	//ret=key_readData(fileName,readData,&readDataLen);
	//if(ret==0)
	//{
	//	printf("readData success\n");
	//}
	//else
	//{
	//	printf("readData failed,errorcode is:%d\n",ret);
	//}


	unsigned char *readData=NULL;
	unsigned long readDataLen=0;
	ret = key_readData(fileName,readData,&readDataLen);
	if(ret == 0){
		readData = (unsigned char*)malloc(readDataLen+2);
		memset(readData,'\0',readDataLen+2);
		ret = key_readData(fileName,readData,&readDataLen);
		if(ret == 0){
			printf("key_readData success\n");
			WriteToFile(fileName,readData,readDataLen);
		}else{
			printf("key_readData second failed,err no is:%d,err string  is:%s\n",ret,key_get_err_msg(ret));
		}
	}else{
		printf("key_readData first failed,err no is:%d,err string  is:%s\n",ret,key_get_err_msg(ret));
	}
	if(readData){
		free(readData);
		readData=NULL;
	}

	return ret;
}

int verify(unsigned char* signData, long signDataLen,unsigned char *fileContent,long fileContentLen,LPTSTR szCACertFile,LPTSTR szCrlFile)
{
	int ret = -1;
	unsigned int j = -1;
	ret=key_verify(signData,signDataLen,fileContent,fileContentLen,szCACertFile);
	if(ret==0)
	{
		printf("key_verify success\n");
	}
	else
	{
		printf("key_verify failed,err no is:%d,err string  is:%s\n",ret,key_get_err_msg(ret));
	}
	return ret;
}


int verify_ex(unsigned char* signData, long signDataLen,unsigned char *fileContent,long fileContentLen,LPTSTR szCACertFile,LPTSTR szCrlFile)
{
	int ret = -1;
	unsigned char* pCertContent=NULL;
	unsigned int  dwCertLen=0;

	ReadFromFile(SIGCERT_LABEL,&pCertContent,&dwCertLen);

	//key_verify_ex
	ret=key_verify_ex(signData,signDataLen,fileContent,fileContentLen,pCertContent,dwCertLen,szCACertFile);
	if(ret==0)
	{
		printf("key_verify_ex success!\n");
	}
	else
	{
		printf("key_verify_ex failed,err no is:%d,err string  is:%s\n",ret,key_get_err_msg(ret));
	}

	if(pCertContent != NULL)
	{
		free(pCertContent);
		pCertContent = NULL;
	}

end:

	return ret;
}

int verify_nohash(	unsigned char *sign,unsigned int signLen,	unsigned char *paint,	unsigned int paintLen,	unsigned char *pubkey,	unsigned int pubkeyLen)
{
	int ret = -1;

	ret=key_verify_ex1_nohash(sign,signLen,paint,paintLen,pubkey,pubkeyLen);
	if(ret==0){
		printf("key_verify_ex1_nohash success\n");
	}else{
		printf("key_verify_ex1_nohash failed,err no is:%d,err string  is:%s\n",ret,key_get_err_msg(ret));
	}

	return ret;
}

int asyEnc(unsigned char *inData,unsigned long inDataLen,unsigned char outData[224],unsigned long *outDataLen)
{
	int ret = -1;
	unsigned int j = -1;
	unsigned char* pEncCert=NULL;
	unsigned int  dwEncCertLen=0;

	unsigned char* outTmpData=NULL;
	unsigned long outTmpDataLen=0;

	ReadFromFile(ENCCERT,&pEncCert,&dwEncCertLen);

	ret=key_asyEnc(inData,inDataLen,pEncCert,dwEncCertLen,outTmpData,&outTmpDataLen);
	if(ret==0){
		outTmpData=(unsigned char*)malloc(outTmpDataLen);
		memset(outTmpData,'\0',outTmpDataLen);
		ret=key_asyEnc(inData,inDataLen,pEncCert,dwEncCertLen,outTmpData,&outTmpDataLen);
		if(ret==0){
			printf("key_asyEnc success!\n");
			memcpy(outData,outTmpData,outTmpDataLen);
			*outDataLen = outTmpDataLen;
		}else{
			printf("key_asyEnc failed,err no is:%d,err string  is:%s\n",ret,key_get_err_msg(ret));
		}
	}else{
		printf("key_asyEnc failed,err no is:%d,err string  is:%s\n",ret,key_get_err_msg(ret));
	}

end:

	if(pEncCert){
		free(pEncCert);
		pEncCert = NULL;
	}

	if(outTmpData){
		free(outTmpData);
		outTmpData = NULL;
	}
	return ret;

}

int asyEnc_ex(unsigned char *inData,unsigned long inDataLen,unsigned char outData[224],unsigned long *outDataLen)
{
	int ret = -1;
	unsigned int j = -1;
	unsigned char* pEncCert=NULL;
	unsigned int  dwEncCertLen=0;

	unsigned char* outTmpData=NULL;
	unsigned long outTmpDataLen=0;

	PCCERT_CONTEXT   pCertContext = NULL;  //证书上下文
	char publicKey[1024];
	int publicKeyLen = 0;

	ReadFromFile(ENCCERT,&pEncCert,&dwEncCertLen);

	if  ((pCertContext=CertCreateCertificateContext(X509_ASN_ENCODING,pEncCert,dwEncCertLen)))  {
		memset(publicKey,'\0',1024);
		publicKeyLen =  pCertContext->pCertInfo->SubjectPublicKeyInfo.PublicKey.cbData ;
		memcpy(publicKey,pCertContext->pCertInfo->SubjectPublicKeyInfo.PublicKey.pbData,publicKeyLen );
	}

	ret=key_asyEnc_ex(inData,inDataLen,(unsigned char*)publicKey,publicKeyLen,outTmpData,&outTmpDataLen);
	if(ret==0)	{
		outTmpData=(unsigned char*)malloc(outTmpDataLen);
		memset(outTmpData,'\0',outTmpDataLen);
		ret=key_asyEnc_ex(inData,inDataLen,(unsigned char*)publicKey,publicKeyLen,outTmpData,&outTmpDataLen);
		if(ret==0){
			printf("key_asyEnc_ex success!\n");
			memcpy(outData,outTmpData,outTmpDataLen);
			*outDataLen = outTmpDataLen;
		}else{
			printf("key_asyEnc_ex failed,err no is:%d,err string  is:%s\n",ret,key_get_err_msg(ret));
		}
	}else{
		printf("key_asyEnc_ex failed,err no is:%d,err string  is:%s\n",ret,key_get_err_msg(ret));
	}

end:

	if(outTmpData){
		free(outTmpData);
		outTmpData = NULL;
	}
	return ret;

}

int getCert(int index)
{
	int ret = -1;
	 char *certContent = NULL;
	int certContentLen = 0;
	ret = key_getCert(certContent,&certContentLen,index);
	if(ret == 0)
	{
		certContent = ( char *)malloc(certContentLen*sizeof(unsigned char));
		memset(certContent,'\0',certContentLen);
		ret = key_getCert(certContent,&certContentLen,index);
		if(ret == 0)
		{
			printf("key_getCert success!\n");
			WriteToFile("sigCert.cer",(unsigned char*)certContent,certContentLen);
		}
		else
		{
			printf("key_getCert failed,err no is:%d,err string  is:%s\n",ret,key_get_err_msg(ret));
		}
	}
	else
	{
		printf("key_getCert failed,err no is:%d,err string  is:%s\n",ret,key_get_err_msg(ret));
	}

	if(certContent != NULL)
	{
		free(certContent);
		certContent = NULL;
	}

	return ret;
}


int getDevStatus(long statusProp)
{
	long statusValue = 0;
	int ret = key_getDeviceStatus(statusProp,&statusValue);
	if(ret == 0){
		printf("key_getDeviceStatus success,statusValue is:%d\n",statusValue);
		return statusValue;
	}else{
		printf("key_getDeviceStatus failed,errno is:%d,err string is:%s\n",ret,key_get_err_msg(ret));
	}
}

int enumFileList(int fileType)
{
	int ret = -1;
	int j=0;
	unsigned char fileList[1024];
	memset(fileList,0x00,1024);
	unsigned char tmp[32];
	memset(tmp,'\0',32);
	int fileListCount = 0;

	ret = key_enumFileList(fileType,fileList,&fileListCount);
	if(ret == 0){
		printf("key_enumFileList success,fileList is:\n");
		for (unsigned int i=0; i<1024; i++){
			if ( fileList[i] == 0x00 ){  
				memset(tmp,'\0',32);
				memcpy(tmp,fileList+j,i-j);
				printf("label is：%s\n",tmp);
				j=i+1;
				if ( fileList[j] == '\0') break;
			}
		}
	}else{
		printf("key_enumFileList failed,err no is:%d, err string is:%s\n",ret,key_get_err_msg(ret));
	}
	return ret;
}


int enumContainerList(void )
{
	int ret = -1;
	unsigned char containerList[1024];
	memset(containerList,0x00,1024);
	int containerListCount = 0;
	unsigned char tmp[32];
	memset(tmp,'\0',32);

	ret = key_enumContainerList(containerList,&containerListCount);
	if(ret == 0)
	{
		printf("key_enumContainerList success,containerList is:\n");
		int j=0;
		for (unsigned int i=0; i<1024; i++)
		{
			if ( containerList[i] == 0x00 )
			{  
				memset(tmp,'\0',32);
				memcpy(tmp,containerList+j,i-j);
				printf("容器标签为：%s\n",tmp);
				j=i+1;
				if ( containerList[j] == '\0') break;
			}
		}
	}else{
		printf("key_enumContainerList failed,err no is:%d, err string is:%s\n",ret,key_get_err_msg(ret));
	}

	return ret;
}

int 	setPinStat()
{
	int ret = -1,  flag = 0, para = 0;

	printf("please input set pin stat: set pin stat type:length=1, set pin trytime=2\n");
	scanf("%d",&flag);
	printf("\n,please input pin or trytime length:\n");
	scanf("%d",&para);
	ret = key_setUserPinStat(flag,para);
	if(ret != 0){
		printf("key_SetUserPinStat err,return err no is:%.2x\n",ret);
	}
	return ret;
}

int getPinStat()
{
	int ret = -1,  flag = 0, para = 0;
	printf("please input set pin stat: set pin stat type:length=1, set pin trytime=2\n");
	scanf("%d",&flag);

	ret = key_getUserPinStat(flag,&para);
	if(ret != 0){
		printf("key_getUserPinStat err,return err no is:%.2x\n",ret);
	}
	return ret;
}

int main(int argc, char* argv[])
{
	_CrtSetDbgFlag( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
	_CrtDumpMemoryLeaks(); //检查内存泄漏
	_CrtSetReportMode( _CRT_ERROR, _CRTDBG_MODE_DEBUG); //报告输出到屏幕

	int ret = -1, ensuleFlag = 1, hashFlag = 1;
	int k=0;
	unsigned int j=0;
	int index=1;
	char serial[128]={'\0'};
	char pin[32] = {'\0'};
	strcpy(pin,"12345asdfg");
	char oldPin[32]={'\0'};
	strcpy(oldPin,"12345asdfg");
	char newPin[32]={'\0'};
	strcpy(newPin,"asdfg12345");
	int leftCount=0;
	char pubkey[256]={'\0'};
	char signdata[256] = {'\0'};	
	unsigned char* fileContent=NULL;
	unsigned int  fileContentLen=0;
	LPTSTR lpszCurDir = NULL; 
	errno_t err = 0,  pos = 0;	
	unsigned char *tmpSignData = NULL;
	unsigned int tmpSignDataLen = 0;
	unsigned char tmpPubkey[] ={
		0xb8,0x0d,0x77,0xf5,0xe6,0x80,0x4c,0x9a,0xe4,0xba,0x3e,0xc5,0x03,0x26,0x25,0xc0,
		0xc4,0x58,0x34,0x2f,0x8f,0x71,0x92,0x2d,0x46,0xb2,0xe2,0xe9,0x55,0x0e,0x28,0x3e,
		0x6e,0xc4,0x2a,0xb2,0x9e,0xb8,0x43,0xab,0x46,0x67,0xf4,0xa0,0xf4,0x84,0xfa,0xea,
		0xdb,0xd2,0x84,0x46,0x91,0x9b,0xc1,0x0d,0xc2,0xc7,0xb8,0x35,0x7b,0xf2,0x45,0xa3,
		0x53,0x97,0x73,0xd0,0xf0,0x5d,0x77,0x94,0xe9,0x46,0x80,0x6e,0xfb,0x67,0x40,0xa5,
		0xb0,0xd5,0xfb,0x50,0x96,0x2a,0xc3,0x3d,0x0f,0x20,0x25,0x8c,0xde,0x30,0xa6,0xc2
	};
	unsigned int tmpPubkeyLen = 96;
	unsigned char *encData = NULL;
	unsigned int encDataLen = 0;

	TCHAR tchBuffer[256]={'\0'}; 
	FILE *hfp1 = NULL;

	unsigned char* signData=NULL;
	long	signDataLen=0;
	unsigned char* digest=NULL;
	unsigned long  digestLen=0;
	unsigned char *inData = NULL;
	unsigned int inDataLen = 0;
	LPTSTR szCACertFile = NULL; 
	LPTSTR szCrlFile = NULL; 
	unsigned char *signCert = NULL;
	unsigned int signCertLen = 0;

	unsigned char outData[224];
	memset(outData,'\0',224);
	unsigned long outDataLen = 224;
	unsigned char *paint=NULL;
	unsigned long  paintLen=0;

	//key_setDevice
	SetDevice();

	//key_setInterface
	SetInterface();

	//key_getDeviceNum
	getDeviceNum();

	usrLogin(pin,&leftCount);

	//key_setPinStat
	setPinStat();

	//key_getPinStat
	getPinStat();

	ret = key_destroyShareMem();
	printf("key_destroyShareMem return value is:%d\n",ret);

	//key_setPin
//	setUsrPin(oldPin,newPin);

	//key_getCert
//	getCert(index);

	
	//verify out data
	ReadFromFile("file.dat",&fileContent,&fileContentLen);
	ReadFromFile("signature.dat",&tmpSignData,&tmpSignDataLen);
//	ReadFromFile("pubkey.dat",&tmpPubkey,&tmpPubkeyLen);
	ReadFromFile("0s_c",&signCert,&signCertLen);
	{
		//szCACertFile		
		TCHAR tchBuffer[256]={'\0'}; 	
		szCACertFile = tchBuffer; 	
		GetModuleFileName(NULL,szCACertFile,256);
		int pos;	
		for ( pos=strlen(szCACertFile)-1; pos>0; pos--)
		{
			if  ( szCACertFile[pos] == '\\' )
				break;
		}	
		for ( j=pos+1; j<strlen(szCACertFile) ; j++)
		{
			memset(szCACertFile+j,'\0',1);
		}	
		strcat_s(szCACertFile,256,"cacert.der");
	}
//	verify_nocode(SIGN_DATA_NO_NEED_ENSULE,NEED_HASH,tmpSignData,tmpSignDataLen,fileContent,fileContentLen,signCert,signCertLen,szCACertFile,NULL);
	verify_nocode_pubkey(NEED_HASH,tmpSignData,tmpSignDataLen,fileContent,fileContentLen,tmpPubkey,tmpPubkeyLen);
	
	
	//asydec out data
	ReadFromFile("cipher.dat",&encData,&encDataLen);
	ret=key_asyDec(encData,encDataLen,paint,&paintLen);
	if(ret==0){
		paint=(unsigned char*)malloc(paintLen);
		memset(paint,'\0',paintLen);
		ret=key_asyDec(encData,encDataLen,paint,&paintLen);
		if(ret==0){
			printf("key_asyDec success!\n");
			WriteToFile("decData.bat",paint,paintLen);
		}else{
			printf("key_asyDec failed,err no is:%d,err string  is:%s\n",ret,key_get_err_msg(ret));
		}
	}else{
		printf("key_asyDec failed,err no is:%d,err string  is:%s\n",ret,key_get_err_msg(ret));
	}

	//getDevStatus
	getDevStatus(PINTRYNUM_STATUS);
	getDevStatus(FREESPACE_STATUS);

	enumFileList(LBL_CONFIG);
	enumFileList(LBL_CERT);
	enumFileList(LBL_KEY);
	enumFileList(LBL_OTHER);

	enumContainerList();

	//key_getDeviceSN
	getDeviceSN();

	//key_getUsrid
	GetId();

	//key_getCertNum
	getCertNum();

	//key_getCertId
	getCertId(index,serial);

	//key_getAttribute
	getAttribute(index);

	//key_getUid
	getUid(index);

	//key_getUserName
	getUserName(index);

	//key_getSubject
	getSubject(index);

	//key_getPublicKey
	getPublicKey(index,pubkey);

	//key_getSignData
	getSignData(index,signdata);

	//key_getIssuer
	getIssuer(index);

	//key_getValidDays
	getValidDays(index);

	//key_getEndDays
	getEndDays(index);

	//key_generateRandom
	generateRandom(index);

	//key_setNowDeviceId
	setNowDeviceId(serial);

	//key_login
	usrLogin(pin,&leftCount);

//	ret = key_logout();
	printf("key_logout return ret is;%x\n",ret);

	//获取原文数据
	ReadFromFile(PAINFILE,&fileContent,&fileContentLen);

	//key_delUselessData
//	ret = key_delUselessData();
	printf("key_delUselessData return value is:%d\n",ret);

	//key_writeData
	//writeData(PAINFILE,fileContent,fileContentLen);

	////key_readData
	//readData(PAINFILE);
	readData(SIGNCERTNAME);
	readData(ENCCERTNAME);
	readData(ROOTCERTNAME);

	//ret = key_delData(PAINFILE);
	//printf("key_delData return value is:%d\n",ret);

	//key_sign
	ret=key_sign((char *)fileContent,fileContentLen,signData,&signDataLen);
	if(ret==0){
		signData=(unsigned char*)malloc(signDataLen);
		memset(signData,'\0',signDataLen);
		ret=key_sign((char *)fileContent,fileContentLen,signData,&signDataLen);
		if(ret==0){
			printf("key_sign success\n");
		}else{
			printf("key_sign second failed,err no is:%d,err string  is:%s\n",ret,key_get_err_msg(ret));
		}
	}else{
		printf("key_sign first failed,err no is:%d,err string  is:%s\n",ret,key_get_err_msg(ret));
	}

	//key_verify
	{
		//szCACertFile		
		TCHAR tchBuffer[256]={'\0'}; 	
		szCACertFile = tchBuffer; 	
		GetModuleFileName(NULL,szCACertFile,256);
		int pos;	
		for ( pos=strlen(szCACertFile)-1; pos>0; pos--)
		{
			if  ( szCACertFile[pos] == '\\' )
				break;
		}	
		for ( j=pos+1; j<strlen(szCACertFile) ; j++)
		{
			memset(szCACertFile+j,'\0',1);
		}	
		strcat_s(szCACertFile,256,CACERT_LABEL);
	}

	{
		//szCrlFile		
		TCHAR tchBuffer[256]={'\0'}; 	
		szCrlFile = tchBuffer; 	
		GetModuleFileName(NULL,szCrlFile,256);
		int pos;	
		for ( pos=strlen(szCrlFile)-1; pos>0; pos--)
		{
			if  ( szCrlFile[pos] == '\\' )
				break;
		}	
		for ( j=pos+1; j<strlen(szCrlFile) ; j++)
		{
			memset(szCrlFile+j,'\0',1);
		}	
		strcat_s(szCrlFile,256,"CrlFile.crl");
	}

	verify(signData,signDataLen,fileContent,fileContentLen,szCACertFile,szCrlFile);

	//key_sign_ex
	if(signData){
		free(signData);
		signData=NULL;
	}
	for( k=0;k<1;k++){
		ret=key_sign_ex((char *)fileContent,fileContentLen,signData,&signDataLen);
		if(ret==0){
			signData=(unsigned char*)malloc(signDataLen);
			memset(signData,'\0',signDataLen);
			ret=key_sign_ex((char *)fileContent,fileContentLen,signData,&signDataLen);
			if(ret==0){
				printf("key_sign_ex success\n");	
			}else{
				printf("key_sign_ex second failed,errorcode is:%d\n",ret);
			}
		}else{
			printf("key_sign_ex first failed,errorcode is:%d\n",ret);
		}
	}

	//key_verify_ex
	ret = verify_ex(signData,signDataLen,fileContent,fileContentLen,szCACertFile,szCrlFile);
	if(ret==0){
		printf("key_verify_ex success!\n");
	}else{
		printf("key_verify_ex failed,errorcode is:%d\n",ret);
	}

	ret=key_verify_ex1(signData,signDataLen,fileContent,fileContentLen,(unsigned char*)pubkey,96);
	if(ret==0){
		printf("key_verify_ex1 success!\n");
	}else{
		printf("key_verify_ex1 failed,errorcode is:%d\n",ret);
	}

	//key_sign_ex_nohash
	ret=key_digest(fileContent,fileContentLen,digest,&digestLen);
	if(ret==0){
		digest=(unsigned char*)malloc(digestLen);
		memset(digest,'\0',digestLen);
		ret=key_digest(fileContent,fileContentLen,digest,&digestLen);
		if(ret==0){
			printf("key_digest sussess!\n");
		}else{
			printf("key_digest failed,errorcode is:%d\n",ret);
		}
	}	
	
	//digestData
	WriteToFile(HASHFILE,digest,digestLen);

	if(signData){
		free(signData);
		signData = NULL;
	}
	ret=key_sign_ex_nohash((char *)digest,digestLen,signData,&signDataLen);
	if(ret==0){
		signData=(unsigned char*)malloc(signDataLen);
		memset(signData,'\0',signDataLen);
		ret=key_sign_ex_nohash((char *)digest,digestLen,signData,&signDataLen);
		if(ret==0){
			printf("key_sign_ex_nohash success\n");

		}else{
			printf("key_sign_ex_nohash second failed,errorcode is:%d\n",ret);
		}
	}else{
		printf("key_sign_ex_nohash first failed,errorcode is:%d\n",ret);
	}

	//verify_nohash
	verify_nohash(signData,signDataLen,digest,digestLen,(unsigned char *)pubkey,96);

	//sign_node
	sign_nocode(SIGN_DATA_NEED_ENSULE,NEED_HASH,(char *)fileContent,fileContentLen,&tmpSignData,&signDataLen);
	verify_nocode(SIGN_DATA_NEED_ENSULE,NEED_HASH,tmpSignData,signDataLen,fileContent,fileContentLen,NULL,0,szCACertFile,szCrlFile);
	
	if(tmpSignData){
		free(tmpSignData);
		tmpSignData = NULL;
	}
	signDataLen = 0;

	//sign_node
	sign_nocode(SIGN_DATA_NEED_ENSULE,NO_NEED_HASH,(char *)digest,digestLen,&tmpSignData,&signDataLen);
	verify_nocode(SIGN_DATA_NEED_ENSULE,NO_NEED_HASH,tmpSignData,signDataLen,digest,digestLen,NULL,0,szCACertFile,szCrlFile);

	if(tmpSignData){
		free(tmpSignData);
		tmpSignData = NULL;
	}
	signDataLen = 0;

	ReadFromFile(SIGCERT_LABEL,&signCert,&signCertLen);
	sign_nocode(SIGN_DATA_NO_NEED_ENSULE,NEED_HASH,(char *)fileContent,fileContentLen,&tmpSignData,&signDataLen);
	//save signData
	WriteToFile(SIGNFILE,(unsigned char*)tmpSignData,signDataLen);
	WriteToFile(PUBKEYFILE,(unsigned char*)pubkey,96);
	verify_nocode(SIGN_DATA_NO_NEED_ENSULE,NEED_HASH,tmpSignData,signDataLen,fileContent,fileContentLen,signCert,signCertLen,szCACertFile,szCrlFile);
	verify_nocode_pubkey(NEED_HASH,tmpSignData,signDataLen,fileContent,fileContentLen,(unsigned char*)pubkey,96);

	if(tmpSignData){
		free(tmpSignData);
		tmpSignData = NULL;
	}
	signDataLen = 0;

	sign_nocode(SIGN_DATA_NO_NEED_ENSULE,NO_NEED_HASH,(char *)digest,digestLen,&tmpSignData,&signDataLen);
	verify_nocode(SIGN_DATA_NO_NEED_ENSULE,NO_NEED_HASH,tmpSignData,signDataLen,digest,digestLen,signCert,signCertLen,szCACertFile,szCrlFile);
	verify_nocode_pubkey(NO_NEED_HASH,tmpSignData,signDataLen,digest,digestLen,(unsigned char*)pubkey,96);

	ReadFromFile("paint.bat",&inData,&inDataLen);
	//key_asyEnc
	ret = asyEnc(inData,inDataLen,outData,&outDataLen);
	if(ret != 0)
	{
		goto end;
	}
	WriteToFile("enc.bat",outData,outDataLen);
	

	//key_asyEnc_ex
	ret = asyEnc_ex(inData,inDataLen,outData,&outDataLen);
	if(ret != 0){
		goto end;
	}
	WriteToFile("enc1.bat",outData,outDataLen);
	//key_asydec
	ret=key_asyDec(outData,outDataLen,paint,&paintLen);
	if(ret==0){
		paint=(unsigned char*)malloc(paintLen);
		memset(paint,'\0',paintLen);
		ret=key_asyDec(outData,outDataLen,paint,&paintLen);
		if(ret==0){
			printf("key_asyDec success!\n");
			WriteToFile("decData.bat",paint,paintLen);
		}else{
			printf("key_asyDec failed,err no is:%d,err string  is:%s\n",ret,key_get_err_msg(ret));
		}
	}else{
		printf("key_asyDec failed,err no is:%d,err string  is:%s\n",ret,key_get_err_msg(ret));
	}


end:

	if(fileContent)
	{
		free(fileContent);
		fileContent=NULL;
	}
	if(signData)
	{
		free(signData);
		signData=NULL;
	}
	if(paint)
	{
		free(paint);
		paint = NULL;
	}

	//if(digest)
	//{
	//	free(digest);
	//	digest = NULL;
	//}

	system("Pause");
	return 0;
}

int sign_nocode(int nEnuseFlag, int nHashFlag,char *pData, int nDataLen, unsigned char **pSignData, long *pdwSignedLen){
	int ret = -1;
	char errString[512];
	memset(errString,'\0',512);
	ret = key_sign_nocode(nEnuseFlag, nHashFlag, pData,nDataLen,*pSignData,pdwSignedLen);
	if(ret != 0){
		key_getErrorInfo(ret,errString);
		printf("key_sign_nocode err,errno is:%d,err string is:%s\n",ret,errString);
		return ret;
	}
	*pSignData = (unsigned char *)malloc((*pdwSignedLen)*sizeof(unsigned char));
	memset(*pSignData,'\0',*pdwSignedLen);
	ret = key_sign_nocode(nEnuseFlag, nHashFlag, pData,nDataLen,*pSignData,pdwSignedLen);
	if(ret != 0){
		key_getErrorInfo(ret,errString);
		printf("key_sign_nocode err,errno is:%d,err string is:%s\n",ret,errString);
		return ret;
	}else{
		printf("key_sign_nocode success!\n");
	}
	return ret;
}

int verify_nocode(int nEnuleFlag, int nHashFlag, unsigned char *pSignedData, long dwSignedLen,unsigned char *pData,long dwDataLen,
	unsigned char *pCertCont,long dwCertLen,const char *szCACertFile,const char *szCrlFile ){
	int ret = -1;
	char errString[512];
	memset(errString,'\0',512);
	ret = key_verify_nocode(nEnuleFlag,nHashFlag,pSignedData,dwSignedLen,pData,dwDataLen,pCertCont,dwCertLen,szCACertFile,NULL);
	if(ret != 0){
		key_getErrorInfo(ret,errString);
		printf("key_verify_nocode err,errno is:%d, err string is:%s\n",ret,errString);
		return ret;
	}else{
		printf("key_verify_nocode success!\n");
	}
	return ret;
}

int  verify_nocode_pubkey(int nHashFlag, unsigned char *pSignedData, long dwSignedLen,unsigned char * pData,long dwDataLen, 
	unsigned char* pPubKey,long nPubKeyLen){
		int ret = -1;
		char errString[512];
		memset(errString,'\0',512);
		ret = key_verify_nocode_pubkey(nHashFlag,pSignedData,dwSignedLen,pData,dwDataLen,pPubKey,nPubKeyLen);
		if(ret != 0){
			key_getErrorInfo(ret,errString);
			printf("key_verify_nocode_pubkey err,errno is:%d, err string is:%s\n",ret,errString);
			return ret;
		}else{
			printf("key_verify_nocode_pubkey success!\n");
		}
		return ret;
}