#include "WHHardwareApi.h"
#include <apr_dso.h>

typedef BOOL (WINAPI *isPVKEnabled)(const char  *keycontainer);//检测登录状态
typedef DWORD (WINAPI *login)(int nType, char* szPin);//登录
typedef DWORD (WINAPI *getSupportedAlgId)(ALG_ID *SupportedAlgid, DWORD *buflen);//得到支持的算法ID
typedef DWORD (WINAPI *createContainer)(char *pszContainer);//创建容器（在硬件中开辟一个存储区，以便将来存储两对非对称密钥对）
typedef DWORD (WINAPI *genAsymmetricKey)(char  *pContainerName,KEYVALUE *publickey,KEYVALUE *privatekey,DWORD  keysize,DWORD dwFlag,ALG_ID Algorithm);//创造非对称对(硬件产生非对称密钥)
typedef DWORD (WINAPI *format)(BYTE *Password, DWORD DataLen); // 格式化硬件设备
typedef DWORD (WINAPI *importPublicKey)(KEYVALUE *keyValue,ALG_ID flag, KEYVALUE *keyIdex);// 向硬件中输入公钥
typedef DWORD (WINAPI *importPrivateKey)(char *containerName, KEYVALUE *keyValue, ALG_ID flag, KEYVALUE *keyIdexofPuk,KEYVALUE *keyIdexofPvk);// 向硬件中输入私钥
typedef DWORD (WINAPI *enumCidcDev)(); //枚举设备中所有的密钥容器
typedef DWORD (WINAPI *readLabelList)(LABEL_TYPE type,BYTE *lbl_arr, int *num);// 根据标签类型获得所有该标签类型的标签名称(枚举所有的标签)
typedef DWORD (WINAPI *findContainerAndKeypair)(char *szContainer,KEYVALUE *publickSigKey,KEYVALUE *privateSigKey,KEYVALUE *publickExKey,KEYVALUE *privateExKey);//在硬件存储区中查找对应的密钥容器和密钥
typedef DWORD (WINAPI *destroyContainer)(char *szContainer);//在硬件存储区中查找并删除对应的密钥容器
typedef DWORD (WINAPI *genRandom)(KEYVALUE *random);//硬件内部产生随机数，长度不能超过16个字节
typedef DWORD (WINAPI *genSymmetricKey)(ALG_ID algid,KEYVALUE *keyValue,DWORD flags);//硬件产生对称密钥
typedef DWORD (WINAPI *importSymmetricKey)(KEYVALUE *keyValue, ALG_ID algid,KEYVALUE *keyid);//向硬件中导入对称密钥
typedef DWORD (WINAPI *exportSymmetricKey)(KEYVALUE *keyIndex, KEYVALUE *encrptedKeyIndex,KEYVALUE *keyValue,ALG_ID algid);//从加密设备中输出对称密钥
typedef DWORD (WINAPI *exportPublicKey)(KEYVALUE *keyIndex, KEYVALUE *keyValue);//从硬件中输出公钥密钥
typedef DWORD (WINAPI *exportPrivateKey)(KEYVALUE *keyIndex,KEYVALUE *encrptedKeyIndex,KEYVALUE *keyValue,ALG_ID algid);//从加密设备中输出私钥密钥
typedef DWORD (WINAPI *symmetricEncrypt)(KEYVALUE *key,KEYVALUE *keyContext,DWORD flag,BYTE *partIn,DWORD partInLen,BYTE *encryptedData,DWORD *encrytedDataLen,ALG_ID algorithm, DWORD algMode);//按指定的对称密码算法加密数据
typedef DWORD (WINAPI *symmetricDecrypt)(KEYVALUE *key,KEYVALUE *keyContext,DWORD flag,BYTE *partIn,DWORD partInLen,BYTE *decryptedData,DWORD *decrytedDataLen, ALG_ID algorithm, DWORD algMode);//按照指定的对称密码算法解密数据
typedef DWORD (WINAPI *asymmetricEncrypt)(KEYVALUE *keyIndex,KEYVALUE *keyContext,DWORD flag,BYTE *encrytedData,DWORD *encryptedDataLen,BYTE *partIn,DWORD partInLen,ALG_ID algorithm);//按照指定的非对称密码算法加密数据
typedef DWORD (WINAPI *asymmetricDecrypt)(KEYVALUE *keyIndex,KEYVALUE *keyContext,DWORD flag,BYTE *decryptData,DWORD *decryptDataLen,BYTE *partIn,DWORD partInLen,ALG_ID algorithm);//按照指定的非对称密码算法解密数据
typedef DWORD (WINAPI *asymmetricSignData)(KEYVALUE *keyIndex,BYTE *partIn,DWORD partInLen,BYTE *signature,DWORD *signatureLen,ALG_ID algorithm);//按照指定的非对称密码算法进行数字签名，获取数字签名的值
typedef DWORD (WINAPI *asymmetricVerifySign)(KEYVALUE *keyIndex,BYTE *originalPart,DWORD *originalPartLen,BYTE *signature,DWORD signatureLen,ALG_ID algo);//按照指定的非对称密码算法进行验证数字签名，获取签名原文
typedef DWORD (WINAPI *destroyKey)(KEYVALUE *keyIndex);// 销毁密钥
typedef DWORD (WINAPI *getKeyProperty)(KEYVALUE *keyIndex,DWORD param,BYTE *data,DWORD *dataLen);// 获取指定密钥的属性值
typedef DWORD (WINAPI *digest)(BYTE *digest,DWORD *digestLen,BYTE *partIn,DWORD partInLen,DWORD digestAlgorithm);//按照指定的散列算法进行散列操作
typedef DWORD (WINAPI *digestEx)(BYTE *digest,DWORD *digestLen,BYTE *partIn,DWORD partInLen,DWORD digestAlgorithm,unsigned long totalBit[2],DWORD flag);// 按照指定的散列算法进行散列操作，此函数可以暂时不实现
typedef DWORD (WINAPI *getKeyPropertyEx)(ALG_ID algoid,DWORD param,BYTE *data,DWORD *dataLen);// 获取指定密钥的属性值
/************************************************************************/
/*                          PIN码相关函数                               */
/************************************************************************/
typedef DWORD (WINAPI *enablePVK)(const char *keyContainer,PANYVALUE anyValue,DWORD flags);// 激活私钥，以便能够用来解密和签名
typedef DWORD (WINAPI *disablePVK)(const char *keyContainer);// 释放已激活的私钥，使得下一次要使用私钥时必须调用WH_EnabelPKV函数
typedef DWORD (WINAPI *setPVKEnableData)(const char *keyContainer,PANYVALUE oldValue,PANYVALUE newValue,DWORD flags);// 设置能够激活私钥的鉴别数据，如果oldvalue为空，newvalue不为空，则表示设置初始值
/************************************************************************/
/*                           Key相关函数                                */
/************************************************************************/
typedef DWORD (WINAPI *logout)();// 取消登录硬件设备
typedef DWORD (WINAPI *setPin)(int pinType, char *pin);// 设置PIN码
typedef DWORD (WINAPI *importFile)(BYTE *label,LABEL_TYPE type,BYTE *buf,int size);// 根据标签将数据写入存储区
typedef DWORD (WINAPI *exportFile)(BYTE *label,LABEL_TYPE type,BYTE *buf,int *size);// 根据标签将数据从存储区读出
typedef DWORD (WINAPI *deleteFileEx)(BYTE *label,LABEL_TYPE type);// 根据标签将数据从存储区删除
typedef DWORD (WINAPI *getDeviceStatus)(long statusProperty,long *statusValue);// 检查设备的状态
typedef DWORD (WINAPI *unLock)(BYTE *data,DWORD dataLen);// 解锁被锁住的硬件设备
/************************************************************************/
/*							补充                                        */
/************************************************************************/
typedef DWORD (WINAPI *asymmetricVerifySignEx)(KEYVALUE *keyIndex,BYTE *hash,DWORD hashLen,BYTE *signature,DWORD signatureLen);
typedef DWORD (WINAPI *asymmetricSend)(DWORD algid,BYTE *random,DWORD randomLen,BYTE *point,DWORD *pointLen);
typedef DWORD (WINAPI *getKeyPairDer)(DWORD keySize,KEYVALUE *publicKey,KEYVALUE *privateKey);
typedef DWORD (WINAPI *asymmetricReceive)(KEYVALUE *keyIndex,BYTE *random,DWORD randomLen,KEYVALUE *pubKey,BYTE *point,DWORD pointLen,BYTE *NA,DWORD *NALen,BYTE *NB,DWORD NBLen,BYTE *WorkKey,DWORD *WorkKeyLen);
typedef DWORD (WINAPI *importKEK)(BYTE *KEK,DWORD KEKLen);
typedef DWORD (WINAPI *exportKEKEncPrk)(char *containerName,BYTE *SN,DWORD SNLen,BYTE *signKey,DWORD *signKeyLen,BYTE *enKey,DWORD *encKeyLen);
typedef DWORD (WINAPI *loadKEKEncPrk)(BYTE *SN,DWORD SNLen,BYTE *signKey,DWORD signKeyLen,BYTE *encKey,DWORD encKeyLen);
typedef DWORD (WINAPI *clearPrk)();
/************************************************************************/
/*							双key                                       */
/************************************************************************/
typedef DWORD (WINAPI *getDeviceList)(HANDLE *phDevList,int& nCount);
typedef HANDLE (WINAPI *getDevice)();
typedef BOOL (WINAPI *setDevice)(HANDLE hDevice);
typedef BOOL (WINAPI *checkDevice)(HANDLE hDevice);
typedef DWORD (WINAPI *enumKeyContainerName)(BYTE *pbData,DWORD *pbDataLen);
typedef DWORD (WINAPI *keyIsExist)();//判断key是否存在

typedef BOOL (WINAPI *pSnFunctionType)(char *szDevSN, int *nSnLen);

/************************************************************************/
/*							常量	                                        */
/************************************************************************/
#define CALG_SCB2                   12
#define CALG_ECC256_KEYX			7
#define CALG_ECC256_SIGN			6
#define CALG_SM3					32785
#define CALG_ECC_KEYX				5
#define CALG_ECC_SIGN				4
#define CALG_HASH_MS_49             32779
#define TIANYUKEY					1
#define HUASHENGKEY					2
#define HUAXIANGKEY					3
#define KEYNUM						2
#define WENHAOINTERFACE				1
#define MIGUANINTERFACE				2
#define CONFIG_FLAG					100
#define SERIALEN				    128    //原来的位数有些短
#define USERNAMELEN					256
#define SUBJECTLEN					256   
#define PUBKEYLEN					256
#define ISSUERLEN					256
#define ENDDAYLEN					256
#define USERIDLEN					256
#define SIGNDATALEN					256
#define CERTLEN						2048
//DEV_STATUS_XXX
#define DEV_STATUS_USEFUL			0
#define CONNECT_STATUS				1
#define FILESYSTEM_STATUS			2
#define PINTRYNUM_STATUS			3
#define FREESPACE_STATUS			4
//是否进行hash处理标示
#define	HASHFLAG					1						//需进行哈希处理

struct WH_SIMPLEBLOB
{
	BLOBHEADER blobheader;
	ALG_ID algid;
};


//算法类型定义
enum ALG_TYPE
{
	RSA1024 = 1,	
	RSA2048,		
	ECC256,
	ECC384
};

//算法用途定义
enum USAGE_TYPE
{
	ENC = 0,	
	SIG,		
	ROOT,
	PRIFILE
};

#pragma data_seg("Shared")
//双KEY信息
struct WH_KEYINFO
{
	HANDLE		keyHandle;
	char		CertSerialNo[SERIALEN];
	char		UserName[USERNAMELEN];
	char		Subject[SUBJECTLEN];
	char		Pubkey[PUBKEYLEN];
	char		Issuer[ISSUERLEN];
	char		UserId[USERIDLEN];
	char		EndDays[ENDDAYLEN];
	char		SignData[SIGNDATALEN];
	char		Cert[CERTLEN];
	int			nValidDays;
	int			nCertLen;
	int			nPubKeyLen;
	int			nSignDataLen;
};
WH_KEYINFO keyInfo[KEYNUM];
int G_DeviceState=0;// (取值为0或者1，0代表未设置，1代表设置)
int G_InterfaceState=0;// (取值为0或者1，0代表未设置，1代表设置)
int G_NowDev=0;//（取值为1、2、3，默认为0，分别代表天喻、华生、华翔腾）
int G_NowInterface=0;//当前的接口：1:维豪的协议接口 2:密管的协议接口
int G_DevNum=0;//设备数量
int G_CertNum=0;//证书数量
int G_UserNameNum=0;//用户名数量
int G_SubjectNum=0;
int G_PubkeyNum=0;
int G_IssuerNum=0;
int G_VaildDayNum=0;
int G_EndDayNum=0;
int G_UserIdNum=0;
int G_SignDataNum = 0;
int	G_AsyType=4;  //1-RSA1024 2-RSA2048 3-ECC256 4-ECC384
int	G_DigestType=2; //1:SCH2算法 2:ms2-49算法 3:sm3
char G_MgrSerialNo[SERIALEN]={'\0'};
int SerialNoLen=0;//序列号的长度
char deviceId[SERIALEN]={'\0'};//设备实体编号
#pragma data_seg()
#pragma comment(linker,"/section:Shared,rws")

apr_dso_handle_t		*G_hDll=NULL;		//设备句柄
apr_dso_handle_t		*G_hSnDll = NULL;  //设备序列号动态库句柄  

isPVKEnabled					pIsPVKEnabled=NULL;
login							pLogin=NULL;
getSupportedAlgId				pGetSupportedAlgId=NULL;
genAsymmetricKey				pGenAsymmetricKey=NULL;
createContainer					pCreateContainer=NULL;
format							pFormat=NULL;  
importPublicKey					pImportPublicKey=NULL;
importPrivateKey				pImportPrivateKey=NULL;
enumCidcDev						pEnumCidcDev=NULL;
readLabelList					pReadLabelList=NULL;
findContainerAndKeypair			pFindContainerAndKeypair=NULL;//在硬件存储区中查找对应的密钥容器和密钥
destroyContainer				pDestroyContainer=NULL;//在硬件存储区中查找并删除对应的密钥容器
genRandom						pGenRandom=NULL;//硬件内部产生随机数，长度不能超过16个字节
genSymmetricKey					pGenSymmetricKey=NULL;//硬件产生对称密钥
importSymmetricKey				pImportSymmetricKey=NULL;//向硬件中导入对称密钥
exportSymmetricKey				pExportSymmetricKey=NULL;//从加密设备中输出对称密钥
exportPublicKey					pExportPublicKey=NULL;//从硬件中输出公钥密钥
exportPrivateKey				pExportPrivateKey=NULL;//从加密设备中输出私钥密钥
symmetricEncrypt				pSymmetricEncrypt=NULL;//按指定的对称密码算法加密数据
symmetricDecrypt				pSymmetricDecrypt=NULL;//按照指定的对称密码算法解密数据
asymmetricEncrypt				pAsymmetricEncrypt=NULL;//按照指定的非对称密码算法加密数据
asymmetricDecrypt				pAsymmetricDecrypt=NULL;//按照指定的非对称密码算法解密数据
asymmetricSignData				pAsymmetricSignData=NULL;//按照指定的非对称密码算法进行数字签名，获取数字签名的值
asymmetricVerifySign			pAsymmetricVerifySign=NULL;//按照指定的非对称密码算法进行验证数字签名，获取签名原文
destroyKey						pDestroyKey=NULL;// 销毁密钥
getKeyProperty					pGetKeyProperty=NULL;// 获取指定密钥的属性值
digest							pDigest=NULL;//按照指定的散列算法进行散列操作
digestEx						pDigestEx=NULL;// 按照指定的散列算法进行散列操作，此函数可以暂时不实现
getKeyPropertyEx				pGetKeyPropertyEx=NULL;// 获取指定密钥的属性值
enablePVK						pEnablePVK=NULL;// 激活私钥，以便能够用来解密和签名
disablePVK						pDisablePVK=NULL;// 释放已激活的私钥，使得下一次要使用私钥时必须调用WH_EnabelPKV函数
setPVKEnableData				pSetPVKEnableData=NULL;// 设置能够激活私钥的鉴别数据，如果oldvalue为空，newvalue不为空，则表示设置初始值
logout							pLogout=NULL;// 取消登录硬件设备
setPin							pSetPin=NULL;// 设置PIN码
importFile						pImportFile=NULL;// 根据标签将数据写入存储区
exportFile						pExportFile=NULL;// 根据标签将数据从存储区读出
deleteFileEx					pDeleteFileEx=NULL;// 根据标签将数据从存储区删除
getDeviceStatus					pGetDeviceStatus=NULL;// 检查设备的状态
unLock							pUnLock=NULL;// 解锁被锁住的硬件设备
asymmetricVerifySignEx			pAsymmetricVerifySignEx=NULL;
asymmetricSend					pAsymmetricSend=NULL;
getKeyPairDer					pGetKeyPairDer=NULL;
asymmetricReceive				pAsymmetricReceive=NULL;
importKEK						pImportKEK=NULL;
exportKEKEncPrk					pExportKEKEncPrk=NULL;
loadKEKEncPrk					pLoadKEKEncPrk=NULL;
clearPrk						pClearPrk=NULL;
getDeviceList					pGetDeviceList=NULL;
getDevice						pGetDevice=NULL;
setDevice						pSetDevice=NULL;
checkDevice						pCheckDevice=NULL;
enumKeyContainerName			pEnumKeyContainerName=NULL;
keyIsExist						pKeyIsExist=NULL;
pSnFunctionType					pSnFunction = NULL; 
KEYVALUE  WorkKeyIndex;


